package com.postepay.automation.core.ui.pages.selectpostepayrecharge;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.SelectPostepayRechargeMolecola;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;


public class SelectPostepayRecharge extends Page {
	public static final String NAME="T131";
	

	public SelectPostepayRecharge(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(SelectPostepayRechargeMolecola.NAME, UiObjectRepo.get().get(SelectPostepayRechargeMolecola.NAME), true);
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), false);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SelectPostepayRechargeManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void clickMyPostePay() {
		((SelectPostepayRechargeManager) this.manager).clickMyPostePay();
	}
	
	public void clickOtherPostePay() {
		((SelectPostepayRechargeManager) this.manager).clickOtherPostePay();
	}
	
}

