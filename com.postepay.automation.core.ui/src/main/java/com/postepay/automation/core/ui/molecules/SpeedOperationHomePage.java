package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SpeedOperationHomePage extends Molecule {
	public static final String NAME="M203";
	
	public static final String SPEEDOPERATIONINVOICE="speedOperationInvoice";
public static final String SPEEDOPERATIONOTHERPOSTEPYA="speedOperationOtherPostePya";
public static final String SPEEDOPERATIONP2P="speedOperationP2P";
public static final String QUICKACTIONBAR="quickActionBar";

	public SpeedOperationHomePage(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

