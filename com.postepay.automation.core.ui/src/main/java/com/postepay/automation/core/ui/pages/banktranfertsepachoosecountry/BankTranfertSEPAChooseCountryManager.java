package com.postepay.automation.core.ui.pages.banktranfertsepachoosecountry;

import com.postepay.automation.core.ui.molecules.ChooseCountryOnList;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class BankTranfertSEPAChooseCountryManager extends PageManager {

	public BankTranfertSEPAChooseCountryManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void selectCountry() {
		Particle country=(Particle) UiObjectRepo.get().get(ChooseCountryOnList.CHOOSEITALYCOUNTRYBANKTRANFERTSEPA);
		
		country.getElement().click();
		
		WaitManager.get().waitShortTime();
	}

}

