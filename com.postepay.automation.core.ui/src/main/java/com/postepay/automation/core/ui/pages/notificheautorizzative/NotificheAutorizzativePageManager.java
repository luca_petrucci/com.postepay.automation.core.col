package com.postepay.automation.core.ui.pages.notificheautorizzative;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.AbilitaInAppMolecola;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.NotificheAutorizzativeM265;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class NotificheAutorizzativePageManager extends PageManager {


	public NotificheAutorizzativePageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderPage(String inputTitle) throws Error {
//		Particle title = (Particle) UiObjectRepo.get().get(HeaderGenericAllPage.TITLEGENERICWITHTITLE);
//		title.setDriver(page.getDriver());
//		String nL = Utility.replacePlaceHolders(title.getLocator(), new Entry("title", inputTitle));
//		assertTrue(page.getDriver().findElement(By.xpath(nL)).isDisplayed());
		System.out.println(page.getDriver().getClass());
		Particle title = (Particle) UiObjectRepo.get().get(NotificheAutorizzativeM265.COPYNOTIFICAWEB);
		title.setDriver(page.getDriver());
		
		System.out.println(title);
		String t = title.getLocator();
		String titleS = Utility.replacePlaceHolders(title.getLocator(), new Entry("title", inputTitle));
		System.out.println(titleS);
		WebElement titleEl = page.getDriver().findElement(By.xpath(titleS));
		System.out.println(titleEl);
		assertTrue("Error titolo non presente. ",titleEl != null);
		
//		LayoutTools layOutTool= new LayoutTools();
//		layOutTool.verifyPresenceHeaderPage(inputTitle);
	}
	
	public void verifyLayout(String copy, String copyTarget, String btnAutorizza, String btnNega) {
		Particle copyAuto = (Particle) UiObjectRepo.get().get(NotificheAutorizzativeM265.COPYNOTIFICAWEB);
		copyAuto.setDriver(page.getDriver());
		String copyTxtS = Utility.replacePlaceHolders(copyAuto.getLocator(), new Entry("title", copy));
//		WebElement copyTxt = page.getDriver().findElement(By.xpath(copyTxtS));
		
		Particle btnAutoE = (Particle) UiObjectRepo.get().get(NotificheAutorizzativeM265.BTNNOTIFICAAUTORIZZA);
		Particle btnNegaE = (Particle) UiObjectRepo.get().get(NotificheAutorizzativeM265.BTNNOTIFICANEGA);
		btnAutoE.setDriver(page.getDriver());
		btnNegaE.setDriver(page.getDriver());
//		assertTrue("ERROR: AR: "+ copyTxt.getText() + "ER: "+ copy, copyTxt.getText().equals(copyTarget));
		assertTrue("ERROR: AR:"+ btnAutoE.getElement().getText() + "ER: "+ btnAutorizza, btnAutoE.getElement().getText().equals(btnAutorizza));
		assertTrue("ERROR: AR:"+ btnNegaE.getElement().getText() + "ER: "+ btnNega, btnNegaE.getElement().getText().equals(btnNega));
		
	}
	
	public void clickOnAutorizza() {
		Particle btnAutoE = (Particle) UiObjectRepo.get().get(NotificheAutorizzativeM265.BTNNOTIFICAAUTORIZZA);
		btnAutoE.setDriver(page.getDriver());
		btnAutoE.getElement().click();
		WaitManager.get().waitMediumTime();
	}	
	
	public void clickOnNegaAutorizzazione() {
		Particle btnNegaE = (Particle) UiObjectRepo.get().get(NotificheAutorizzativeM265.BTNNOTIFICANEGA);
		btnNegaE.setDriver(page.getDriver());
		btnNegaE.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	
}

