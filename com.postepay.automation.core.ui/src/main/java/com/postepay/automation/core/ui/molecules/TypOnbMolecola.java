package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class TypOnbMolecola extends Molecule {
	public static final String NAME="M245";
	
	public static final String COPYTYPONP="copyTypOnp";
public static final String CLOSETYPONP="closeTypOnp";
public static final String TITLETYPONP="titleTypOnp";
public static final String IMAGETYPONP="imageTypOnp";


	public TypOnbMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

