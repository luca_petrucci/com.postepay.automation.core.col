package com.postepay.automation.core.ui.pages.automaticrechargepostepaysectionpage;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.AutomaticRechargePostepaySectionPageMolecola;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import test.automation.core.UIUtils;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import static io.appium.java_client.touch.offset.PointOption.point;

public class AutomaticRechargePostepaySectionPageManager extends PageManager {

	public AutomaticRechargePostepaySectionPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderPage() {
		Particle title=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySectionPageMolecola.HEADERPAGEAUTOMATICRECHARGEPOSTEPAYSECTION);

		title.getElement().isDisplayed();

		try {Thread.sleep(1000);} catch (Exception err )  {}
	}

	public void verifyCopy() {
		Particle copy=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySectionPageMolecola.COPYAUTOMATICRECHARGEPOSTEPAYSECTION);

		copy.getElement().isDisplayed();

	}

	public void verifyIsMyCard(String cardNumber) {
		Particle getCard=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySectionPageMolecola.CARDUSEDAUTOMATICRECHARGEPOSTEPAYSECTION);

		String card=getCard.getElement().getText();

		assertEquals(cardNumber, card);

	}

	public void verifyRecurrencyRechargeName(String recurrencyRechargeName) {
		Particle getNameRecharge=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySectionPageMolecola.NAMEOFRECHARGEAUTOMATICRECHARGEPOSTEPAYSECTION);

		String nameRecharge=getNameRecharge.getElement().getText();

		String nameNameRechargeInput=recurrencyRechargeName.toUpperCase();

		assertEquals(nameNameRechargeInput, nameRecharge);
	}

	public void verifyRecurrencyRechargeDate(String recurrencyRechargeDate) {
		Particle getDate=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySectionPageMolecola.TIMEOFRECHARGEAUTOMATICRECHARGEPOSTEPAYSECTION);

		String dateRecharge=getDate.getElement().getText();

		//		StringAndNumberOperationTools tool=new StringAndNumberOperationTools();
		//		String nameDateRechargeInput=tool.convertWithoutATempoLabel(recurrencyRechargeDate);
		String prefix="A tempo, ";
		String nameDateRechargeInput=prefix.concat(recurrencyRechargeDate);

		assertEquals(nameDateRechargeInput, dateRecharge);
	}

	public void verifyImporto(String importoInput) {
		Particle getImporto=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySectionPageMolecola.IMPORTOFAUTOMATICRECHARGEPOSTEPAYSECTION);

		String importo=getImporto.getElement().getText();

		String prefix="- ";
		String newImportoInput=prefix.concat(importoInput);

		assertEquals(newImportoInput, importo);
	}

	public void clickOnRecharge() {
		Particle btn=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySectionPageMolecola.RECHARGEBUTTONAUTOMATICRECHARGEPOSTEPAYSECTION);

		btn.getElement().click();

		try {Thread.sleep(2000);} catch (Exception err )  {}
	}

	public void clickOnDeleteAutomaticRecharge() {
		Particle btn=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySectionPageMolecola.DELETEOPERATIONBUTTONAUTOMATICRECHARGEPOSTEPAYSECTION);
		btn.getElement().isDisplayed();
		btn.getElement().click();
		
		
		try {Thread.sleep(2000);} catch (Exception err )  {}
	}

	public void verifyStaticLayoutPage(String cardNumber) {
		verifyHeaderPage();
		verifyCopy();
		verifyIsMyCard(cardNumber);
	}

	public void verifyIsPresentSpecificAutomaticRecharge(String recurrencyRechargeName, String recurrencyRechargeDate, String importoInput) {
		verifyRecurrencyRechargeName(recurrencyRechargeName);
		verifyRecurrencyRechargeDate(recurrencyRechargeDate);
		verifyImporto(importoInput);
	}

	public void swipOnSpecificRecharge(String recurrencyRechargeName) {
		Particle automaticRecharge=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySectionPageMolecola.NAMEOFRECHARGEAUTOMATICRECHARGEPOSTEPAYSECTION);
		String nuovoLocator=Utility.replacePlaceHolders(automaticRecharge.getLocator(), new Entry("title",recurrencyRechargeName.toUpperCase()));
		String swipeLocator="//*[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_money']";
		
//		System.out.println("Locator Caricato:: "+nuovoLocator);
		
		WebDriver driver=UIUtils.ui().getCurrentDriver();
//		System.out.println("WebDriver Caricato");
		try {
			WebElement elem=driver.findElement(By.xpath(nuovoLocator));
//			System.out.println("Elemento Trovato");
			// Alternativva vatriabile
			WebElement elem2=driver.findElement(By.xpath(swipeLocator));
			try {
				// Coordinate dell'oggetto
				int coordinateX; int coordinateY; int coordinateXnew;

				coordinateX=elem2.getLocation().getX();
//				System.out.println("Elemento 1 Trovato "+coordinateX);
				coordinateXnew=coordinateX-800;
//				System.out.println("Elemento 2 Trovato "+coordinateXnew);
				coordinateY=elem.getLocation().getY();
//				System.out.println("Elemento 3 Trovato "+coordinateY);
				
				// TouchAction e imposto il driver
				TouchAction touchAction = new TouchAction((MobileDriver<?>) page.getDriver());
//				System.out.println("Prova lo Swipe");
				touchAction.longPress(point(coordinateX,coordinateY)).moveTo(point(coordinateXnew,coordinateY)).release().perform();
//				System.out.println("Lo Swipe e fatto!");
				try {Thread.sleep(3000);} catch (Exception err )  {}
				
				
			} catch (Exception e) {
				// TODO: handle exception
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}

