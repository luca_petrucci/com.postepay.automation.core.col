package com.postepay.automation.core.ui.pages.newaccesspage;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.NewAccessPageMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import org.openqa.selenium.WebDriver;

public class NewAccessPageManager extends PageManager {
	
	LayoutTools toolLayout = new LayoutTools();

	Particle imageLogo = (Particle) UiObjectRepo.get().get(NewAccessPageMolecola.POSTEPAYIMAGEACCESS);
	Particle helloUserTitle = (Particle) UiObjectRepo.get().get(NewAccessPageMolecola.HELLOUSERTITLEACCESS);
	Particle newAccessButton = (Particle) UiObjectRepo.get().get(NewAccessPageMolecola.NEWACCESSBUTTONACCESS);
	Particle newNotYouLink = (Particle) UiObjectRepo.get().get(NewAccessPageMolecola.NEWNOTYOULINKACCESS);
	Particle authorizeViaWebButton = (Particle) UiObjectRepo.get().get(NewAccessPageMolecola.AUTHORIZEVIAWEBBUTTONACCESS);
	Particle authorizeListButton = (Particle) UiObjectRepo.get().get(NewAccessPageMolecola.AUTHORIZELISTBUTTONACCESS);
	Particle genericIconTile = (Particle) UiObjectRepo.get().get(NewAccessPageMolecola.GENERICICONTILEACCESS);
	Particle genericArrowTile = (Particle) UiObjectRepo.get().get(NewAccessPageMolecola.GENERICARROWTILEACCESS);
	Particle discountTitle = (Particle) UiObjectRepo.get().get(NewAccessPageMolecola.DISCOUNTTITLEACCESS);
	Particle discountSubTitle = (Particle) UiObjectRepo.get().get(NewAccessPageMolecola.DISCOUNTSUBTITLEACCESS);
	Particle discoverDiscount = (Particle) UiObjectRepo.get().get(NewAccessPageMolecola.DISCOVERDISCOUNTACCESS);
	Particle qrImage = (Particle) UiObjectRepo.get().get(NewAccessPageMolecola.QRIMAGEACCESS);
	
	public NewAccessPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyImage(WebDriver driver, String imageNameInput, double discrepanza) {
		String imageName = "newLogin/" + imageNameInput;
		LayoutImage.get().verifyImage(driver, imageName, discrepanza);
	}
	
	public void verifyIconImage(WebDriver driver, double discrepanza) {
		verifyImage(driver, "login_icon_camera", discrepanza);
		verifyImage(driver, "login_icon_campana", discrepanza);
		verifyImage(driver, "login_img_qrCode", discrepanza);
	}
	
	public void verifyLogoImage(WebDriver driver, double discrepanza) {
		verifyImage(driver, "login_new", discrepanza);
	}
	
	public void verifyBtnTextImage(WebDriver driver, double discrepanza) {
		verifyImage(driver, "login_btn_scopri", discrepanza);
		verifyImage(driver, "login_btn_arrow", discrepanza);
		verifyImage(driver, "login_btn_accedi", discrepanza);
		verifyImage(driver, "login_non_sei_tu", discrepanza);
	}
	
	
	public void verifyLayout() {
		//assertTrue(imageLogo.getElement().isDisplayed());
		//assertTrue(helloUserTitle.getElement().isDisplayed());
		//assertTrue(newAccessButton.getElement().isDisplayed());
		assertTrue(newNotYouLink.getElement().isDisplayed());
		//assertTrue(authorizeViaWebButton.getElement().isDisplayed());
		//assertTrue(authorizeListButton.getElement().isDisplayed());
		//assertTrue(genericIconTile.getElement().isDisplayed());
//		assertTrue(genericArrowTile.getElement().isDisplayed());
		//assertTrue(discountTitle.getElement().isDisplayed());
		//assertTrue(discountSubTitle.getElement().isDisplayed());
		//assertTrue(discoverDiscount.getElement().isDisplayed());
		//assertTrue(qrImage.getElement().isDisplayed());
	}

	public void clickOnAccedi() {
		newAccessButton.getElement().click();
		WaitManager.get().waitLongTime();
	}
	
	public void clickOnNonSeiTu() {
		newNotYouLink.getElement().click();
		WaitManager.get().waitLongTime();
	}
	
	public void clickOnAutorizzaSitoWeb() {
		authorizeViaWebButton.getElement().click();
		WaitManager.get().waitLongTime();
	}
	
	public void clickOnListaAutorizzazioni() {
		authorizeListButton.getElement().click();
		WaitManager.get().waitLongTime();
	}
	
	public void clickOnScopriSconti() {
		discoverDiscount.getElement().click();
		WaitManager.get().waitLongTime();
	}
	
	public void verifyHelloName(String nameInput) {
		String tName = "Ciao " + nameInput;
		String eName = helloUserTitle.getElement().getText();
		assertTrue(eName.equals(tName));
	}
}

