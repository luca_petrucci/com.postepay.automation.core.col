package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class ThankYouPageInsertCodePosteIdDuringDeleteId extends Molecule {
	public static final String NAME="M109";
	
	public static final String TEXTNOWYOUAREREADYTOSTART="textNowYouAreReadyToStart";
public static final String CHECKBUTTONNOWYOUAREREADYTOSTART="checkButtonNowYouAreReadyToStart";
public static final String CLOSEBUTTONNOWAREREADYTOSTART="closeButtonNowAreReadyToStart";
public static final String COPYTEXTNOWYOUAREREADYTOSTART="copyTextNowYouAreReadyToStart";


	public ThankYouPageInsertCodePosteIdDuringDeleteId(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

