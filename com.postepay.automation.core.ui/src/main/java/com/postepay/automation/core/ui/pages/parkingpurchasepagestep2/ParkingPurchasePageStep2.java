package com.postepay.automation.core.ui.pages.parkingpurchasepagestep2;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.pages.parkingpurchasepagestep1.ParkingPurchasePageStep1Manager;
import com.postepay.automation.core.ui.molecules.BodyChooseHourAndZoneParkingPurchase;


public class ParkingPurchasePageStep2 extends Page {
	public static final String NAME="T058";
	

	public ParkingPurchasePageStep2(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
this.addToTemplate(BodyChooseHourAndZoneParkingPurchase.NAME, UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new ParkingPurchasePageStep2Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new ParkingPurchasePageStep2ManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	public void verifyHeaderpage(String titlePageInput)
	{
		((ParkingPurchasePageStep2Manager)this.manager).verifyHeaderpage(titlePageInput);
	}
	public void verifyLayout()
	{
		((ParkingPurchasePageStep2Manager)this.manager).verifyLayout();
	}
	
	public void clickOnPaga()
	{
		((ParkingPurchasePageStep2Manager)this.manager).clickOnPaga();

	}

}

