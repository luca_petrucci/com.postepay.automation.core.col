package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SummaryG2gSend extends Molecule {
	public static final String NAME="M033";
	
	public static final String BUTTONSENDGIGASUMMARY="buttonSendGigaSummary";
public static final String DESCRIPTIONSENDGIGA="descriptionSendGiga";
public static final String AMOUNTGIGATOSEND="amountGigaToSend";
public static final String BARINDICATOR="barIndicator";
public static final String ICONCONTACT="iconContact";
public static final String INDICATORGIGANEW="indicatorGigaNew";
public static final String INDICATORGIGAOLD="indicatorGigaOld";


	public SummaryG2gSend(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

