package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class CompileFormToRechargeMyPostepay extends Molecule {
	public static final String NAME="M061";
	
	public static final String AMOUNTTOSENDTOMYPOSTEPAY="amountToSendToMyPostepay";
public static final String PROCEEDRECHARGEBUTTONOMPILEFORM="proceedRechargeButtonompileForm";
public static final String NUMBERMYPOSTEPAY="numberMyPostepay";
public static final String AUTOMATICHRECHARGETOGGLEINCOMPILEFORM="automatichRechargeToggleInCompileForm";
public static final String REASONOFRECHARGEMYPOSTEPAY="reasonOfRechargeMyPostepay";
public static final String HOLDERMYPOSTEPAY="holderMyPostepay";


	public CompileFormToRechargeMyPostepay(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

