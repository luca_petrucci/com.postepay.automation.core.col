package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class NotificheAutorizzativeM265 extends Molecule {
	public static final String NAME="M265";

	public static final String COPYNOTIFICAWEB ="copyNotificaM265";
	public static final String BTNNOTIFICAAUTORIZZA ="autoBtnNotificaM265";
	public static final String BTNNOTIFICANEGA ="negaNotificaM265";

	public NotificheAutorizzativeM265(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

