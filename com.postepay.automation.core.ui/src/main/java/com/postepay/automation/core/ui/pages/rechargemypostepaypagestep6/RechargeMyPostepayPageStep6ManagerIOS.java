package com.postepay.automation.core.ui.pages.rechargemypostepaypagestep6;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.ThankYouPageForTelephoneRecharge;

import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.SoftAssertion;

public class RechargeMyPostepayPageStep6ManagerIOS extends RechargeMyPostepayPageStep6Manager{

	public RechargeMyPostepayPageStep6ManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyLayout() {
		Particle img=(Particle) UiObjectRepo.get().get(ThankYouPageForTelephoneRecharge.THANKYOUIMAGERECHARGEMYPP);
		Particle title=(Particle) UiObjectRepo.get().get(ThankYouPageForTelephoneRecharge.THANKYOUTITLERECHARGEMYPP);
		Particle subTitle=(Particle) UiObjectRepo.get().get(ThankYouPageForTelephoneRecharge.THANKYOUCOPYRECHARGEMYPP);
		Particle btn=(Particle) UiObjectRepo.get().get(ThankYouPageForTelephoneRecharge.THANKYOUCLOSEBUTTONRECHARGEMYPP);

		// Imagine
		assertTrue(img.getElement()!=null);
		// Title
		assertTrue("Titolo errato: " +title.getElement().getText(),title.getElement().getText().contains("confermata"));
		// Copy
		assertTrue(subTitle.getElement()!=null);
//		SoftAssertion.get().getAssertions().assertThat
//		(subTitle.getElement().getText()).withFailMessage("Descrizione errata").contains
//		("Potrai consultare la ricevuta in bacheca e in \"Cerca operazioni\" sul sito Poste.it");
		// Bottone
		assertTrue(btn.getElement()!=null);
		
		WaitManager.get().waitShortTime();
	}
}
