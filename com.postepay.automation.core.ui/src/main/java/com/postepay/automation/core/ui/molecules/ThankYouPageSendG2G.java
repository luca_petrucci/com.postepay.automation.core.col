package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class ThankYouPageSendG2G extends Molecule {
	public static final String NAME="M035";
	
	public static final String ICONRECEIVETHANKYOUG2G="iconReceiveThankyouG2G";
public static final String AMOUNTTHANKYOUG2G="amountThankyouG2G";
public static final String ICONTHANKYOUPAGEG2G="iconThankyouPageG2G";
public static final String TITLETHANKYOUG2G="titleThankyouG2G";
public static final String ICONSENDTHANKYOUG2G="iconSendThankyouG2G";
public static final String VALUETHANKYOUG2G="valueThankyouG2G";
public static final String SUBTITLETHANKYOUG2G="subtitleThankyouG2G";
public static final String BUTTONCLOSETHANKYOUG2G="buttonCloseThankyouG2G";


	public ThankYouPageSendG2G(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

