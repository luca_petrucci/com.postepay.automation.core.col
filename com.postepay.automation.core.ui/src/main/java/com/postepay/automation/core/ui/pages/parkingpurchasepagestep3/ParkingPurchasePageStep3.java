package com.postepay.automation.core.ui.pages.parkingpurchasepagestep3;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.BodyChooseHourAndZoneParkingPurchaseSummary;


public class ParkingPurchasePageStep3 extends Page {
	public static final String NAME="T059";
	

	public ParkingPurchasePageStep3(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
this.addToTemplate(BodyChooseHourAndZoneParkingPurchaseSummary.NAME, UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchaseSummary.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new ParkingPurchasePageStep3Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeaderpage(String titlePageInput)
	{
		((ParkingPurchasePageStep3Manager)this.manager).verifyHeaderpage(titlePageInput);
	}
	
	public void verifyLayout(String cardNumber)
	{
		((ParkingPurchasePageStep3Manager)this.manager).verifyLayout(cardNumber);
	}
	
	public void clickOnPagaSummary()
	{
		((ParkingPurchasePageStep3Manager)this.manager).clickOnPagaSummary();

	}

	public void clickOnAnnullaSummary()
	{
		((ParkingPurchasePageStep3Manager)this.manager).clickOnAnnullaSummary();

	}
}

