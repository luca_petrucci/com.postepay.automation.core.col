package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class AutomaticRechargePostepaySectionPageMolecola extends Molecule {
	public static final String NAME="M208";
	
	public static final String COPYAUTOMATICRECHARGEPOSTEPAYSECTION="copyAutomaticRechargePostepaySection";
public static final String HEADERPAGEAUTOMATICRECHARGEPOSTEPAYSECTION="headerPageAutomaticRechargePostepaySection";
public static final String CARDUSEDAUTOMATICRECHARGEPOSTEPAYSECTION="cardUsedAutomaticRechargePostepaySection";
public static final String IMPORTOFAUTOMATICRECHARGEPOSTEPAYSECTION="importOfAutomaticRechargePostepaySection";
public static final String RECHARGEBUTTONAUTOMATICRECHARGEPOSTEPAYSECTION="rechargeButtonAutomaticRechargePostepaySection";
public static final String TIMEOFRECHARGEAUTOMATICRECHARGEPOSTEPAYSECTION="timeOfRechargeAutomaticRechargePostepaySection";
public static final String ANNULLBUTTONAUTOMATICRECHARGEPOSTEPAYSECTION="annullButtonAutomaticRechargePostepaySection";
public static final String NAMEOFRECHARGEAUTOMATICRECHARGEPOSTEPAYSECTION="nameOfRechargeAutomaticRechargePostepaySection";
public static final String DELETEOPERATIONBUTTONAUTOMATICRECHARGEPOSTEPAYSECTION="deleteOperationButtonAutomaticRechargePostepaySection";


	public AutomaticRechargePostepaySectionPageMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

