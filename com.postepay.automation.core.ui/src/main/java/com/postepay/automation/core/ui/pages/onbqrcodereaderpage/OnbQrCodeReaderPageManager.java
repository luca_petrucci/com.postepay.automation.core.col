package com.postepay.automation.core.ui.pages.onbqrcodereaderpage;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.AbilitaInAppMolecola;
import com.postepay.automation.core.ui.molecules.OnbQrCodeReaderMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class OnbQrCodeReaderPageManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();

	Particle cameraView = (Particle) UiObjectRepo.get().get(OnbQrCodeReaderMolecola.CAMERAVIEW);
	Particle descriptionCamera = (Particle) UiObjectRepo.get().get(OnbQrCodeReaderMolecola.DESCRIPTIONCAMERA);
	Particle qrCodeCamera = (Particle) UiObjectRepo.get().get(OnbQrCodeReaderMolecola.QRCODECAMERA);
	Particle aiutoCamera = (Particle) UiObjectRepo.get().get(OnbQrCodeReaderMolecola.AIUTOCAMERA);
	
	public OnbQrCodeReaderPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}

	public void verifyLayout() {
		assertTrue(cameraView.getElement().isDisplayed());
		assertTrue(descriptionCamera.getElement().isDisplayed());
		assertTrue(qrCodeCamera.getElement().isDisplayed());
		assertTrue(aiutoCamera.getElement().isDisplayed());
	}
}

