package com.postepay.automation.core.ui.pages.rechargemypostepaypagestep6;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.ThankYouPageForTelephoneRecharge;


public class RechargeMyPostepayPageStep6 extends Page {
	public static final String NAME="T044";
	

	public RechargeMyPostepayPageStep6(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(ThankYouPageForTelephoneRecharge.NAME, UiObjectRepo.get().get(ThankYouPageForTelephoneRecharge.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new RechargeMyPostepayPageStep6Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new RechargeMyPostepayPageStep6ManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	public void verifyLayout() {
		((RechargeMyPostepayPageStep6Manager)this.manager).verifyLayout();
	}
	
	public void clickOnButtonClose() {
		((RechargeMyPostepayPageStep6Manager)this.manager).clickOnCloseButton();
	}

	public void verifyLayout(Properties language) {
		((RechargeMyPostepayPageStep6Manager)this.manager).verifyLayout(language);
	}
}

