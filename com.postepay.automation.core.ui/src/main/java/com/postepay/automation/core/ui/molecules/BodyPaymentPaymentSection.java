package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyPaymentPaymentSection extends Molecule {
	public static final String NAME="M044";
	
	public static final String TRANSFERTSEPAANDPOSTAGIRO="transfertSEPAandPostagiro";
public static final String PAYMENTBULLETIN="paymentBulletin";
public static final String PAYMENTWITHQRCODE="paymentWithQrCode";
public static final String SENDP2P="sendP2P";
public static final String DINAMICTITLECARD = "transfertCardDynamic";
public static final String PAYMENTMINISTEROTRASPORTI= "paymentMinisteroTrasporti";

	public BodyPaymentPaymentSection(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

