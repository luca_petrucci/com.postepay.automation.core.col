package com.postepay.automation.core.ui.pages.urbanpurchasepagestep1;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.BodyExtraurbanPurchaseFinalSummary;
import com.postepay.automation.core.ui.molecules.BodyUrbanPurchase;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class UrbanPurchasePageStep1Manager extends PageManager {

	Particle tabCity = (Particle) UiObjectRepo.get().get(BodyUrbanPurchase.SELECTCITYURBANPURCHASE);
	Particle tabVettore = (Particle) UiObjectRepo.get().get(BodyUrbanPurchase.SELECTVECTORURBANPURCHASE);
	Particle searchCity = (Particle) UiObjectRepo.get().get(BodyUrbanPurchase.SEARCHCITYURBANPURCHASE);
	Particle resultedCity = (Particle) UiObjectRepo.get().get(BodyUrbanPurchase.CITYNAMEURBANPURCHASE);

	public UrbanPurchasePageStep1Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderpage(String inputTitle) {
		LayoutTools tool = new LayoutTools();
		tool.verifyPresenceHeaderPage(inputTitle);
	}
	
	public void verifyLayoutPage() {
		Particle title = (Particle) UiObjectRepo.get().get(BodyUrbanPurchase.TITLEPAGEURBANPURCHASE);
		
		assertTrue(title.getElement().isDisplayed());
		assertTrue(tabCity.getElement().isDisplayed());
		assertTrue(tabVettore.getElement().isDisplayed());
		assertTrue(searchCity.getElement().isDisplayed());
		assertTrue(resultedCity.getElement().isDisplayed());
	}
	
	public void clickOnTabCity() {
		tabCity.getElement().click();
		WaitManager.get().waitShortTime();
	}
	
	public void clickOnTabVettore() {
		tabVettore.getElement().click();
		WaitManager.get().waitShortTime();
	}
	
	public void searchCity(String city) {
		searchCity.getElement().sendKeys(city);
		WaitManager.get().waitMediumTime();
	}

	public void clickOnSearchedCity() {
		resultedCity.getElement().click();
		WaitManager.get().waitMediumTime();
	}
}

