package com.postepay.automation.core.ui.pages.devicenativopage;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.DeviceNativoMolecola;

public class DeviceNativoPage extends Page {
	public static final String NAME="T142";
	

	public DeviceNativoPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(DeviceNativoMolecola.NAME, UiObjectRepo.get().get(DeviceNativoMolecola.NAME), true);


	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new DeviceNativoPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyLayout(String inputTitolo, String inputCopy, String inputDescrizione)
	{
		((DeviceNativoPageManager)this.manager).verifyLayout(inputTitolo, inputCopy, inputDescrizione);
	}
	public void clickOnNotification(String inputCopy)
	{
		((DeviceNativoPageManager)this.manager).clickOnNotification(inputCopy);
	}
	
}

