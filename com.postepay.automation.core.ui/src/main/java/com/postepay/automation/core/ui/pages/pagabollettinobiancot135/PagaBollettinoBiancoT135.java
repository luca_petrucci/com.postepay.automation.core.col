package com.postepay.automation.core.ui.pages.pagabollettinobiancot135;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.TipologiaBollettinoM255;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;

import com.postepay.automation.core.ui.molecules.DatiBeneficiarioM256;
import com.postepay.automation.core.ui.molecules.DatiPaganteM257;


public class PagaBollettinoBiancoT135 extends Page {
	public static final String NAME="T135";
	

	public PagaBollettinoBiancoT135(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
this.addToTemplate(TipologiaBollettinoM255.NAME, UiObjectRepo.get().get(TipologiaBollettinoM255.NAME), true);
this.addToTemplate(DatiBeneficiarioM256.NAME, UiObjectRepo.get().get(DatiBeneficiarioM256.NAME), true);
this.addToTemplate(DatiPaganteM257.NAME, UiObjectRepo.get().get(DatiPaganteM257.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new PagaBollettinoBiancoT135Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new PagaBollettinoBiancoT135ManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderPage(String inputTitle) throws Error {	
		((PagaBollettinoBiancoT135Manager)this.manager).verifyHeaderPage(inputTitle);
	}
	
	public void isBollettinoBianco() {
		((PagaBollettinoBiancoT135Manager)this.manager).isBollettinoBianco();
	}
	
	
	public void fillBeneficiarioField(String contoCorrente, String targetUser, String importo, String causale) {
		((PagaBollettinoBiancoT135Manager)this.manager).fillBeneficiarioField(contoCorrente, targetUser, importo, causale);
	}
	
	public void scrollDown() {
		((PagaBollettinoBiancoT135Manager)this.manager).scrollDown();
	}
	
	public void scrollUp() {
		((PagaBollettinoBiancoT135Manager)this.manager).scrollUp();
	}
	
	public void verifyChiPagaSection(String nome, String cognome, String indirizzo, String citta, String cap, String provincia) {
		((PagaBollettinoBiancoT135Manager)this.manager).verifyChiPagaSection(nome, cognome, indirizzo, citta, cap, provincia);
	}
	
	public void clickOnContinua() {
		((PagaBollettinoBiancoT135Manager)this.manager).clickOnContinua();
	}
	
	public void clickOnRubrica() {
		((PagaBollettinoBiancoT135Manager)this.manager).clickOnRubrica();
	}
}

