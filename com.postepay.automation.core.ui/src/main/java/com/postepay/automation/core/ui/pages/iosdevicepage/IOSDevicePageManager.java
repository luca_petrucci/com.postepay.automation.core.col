package com.postepay.automation.core.ui.pages.iosdevicepage;

import java.time.Duration;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.appium.java_client.InteractsWithApps;
import test.automation.core.UIUtils;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import com.postepay.automation.core.ui.molecules.IOSDeviceGegericMolecola;

public class IOSDevicePageManager extends PageManager {

	public IOSDevicePageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void openSetting() {
		((InteractsWithApps) page.getDriver()).runAppInBackground(Duration.ofSeconds(-1));
		WaitManager.get().waitMediumTime();
		((InteractsWithApps) page.getDriver()).activateApp("com.apple.Preferences");
	}
	
	public void resumePostePay(Properties pro) {
		((InteractsWithApps) page.getDriver()).runAppInBackground(Duration.ofSeconds(-1));
		
		((InteractsWithApps) page.getDriver()).activateApp(pro.getProperty("ios.bundle.id"));
		WaitManager.get().waitShortTime();
	}
	
	public void clickOnWiFiLabel() {
		Particle label = (Particle) UiObjectRepo.get().get(IOSDeviceGegericMolecola.WIFILABEL);
		UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.presenceOfElementLocated(By.xpath(label.getLocator()))).click();
		WaitManager.get().waitShortTime();
		System.out.println("Click == Wi-Fi Label");
	}
	
	public boolean isWifiEnable() {
		Particle toggle = (Particle) UiObjectRepo.get().get(IOSDeviceGegericMolecola.WIFITOGGLE);
		System.out.println("WiFi value: " + toggle.getElement().getAttribute("value"));
		boolean is = false;
		if(toggle.getElement().getAttribute("value").equals("1")) {
			is = true;
		}
		System.out.println("WiFi is ON: " + is);
		return is;
	}

	public boolean isAirModeEnable() {
		Particle toggle = (Particle) UiObjectRepo.get().get(IOSDeviceGegericMolecola.AIRPLANETOGGLE);
		
//		System.out.println("enabled: " + toggle.getElement().getAttribute("enabled"));
//		System.out.println("selected: " + toggle.getElement().getAttribute("selected"));
//		System.out.println("visible: " + toggle.getElement().getAttribute("visible"));
//		System.out.println("wdValue: " + toggle.getElement().getAttribute("wdValue"));
//		System.out.println("wdVisible: " + toggle.getElement().getAttribute("wdVisible"));
//		System.out.println("wdEnabled: " + toggle.getElement().getAttribute("wdEnabled"));
		System.out.println("AirMode value: " + toggle.getElement().getAttribute("value"));
		boolean is = false;
		if(toggle.getElement().getAttribute("value").equals("1")) {
			is = true;
		}
		System.out.println("AirMode is ON: " + is);
		return is;
	}
	
	public void clickOnWifiToggle() {
		Particle toggle = (Particle) UiObjectRepo.get().get(IOSDeviceGegericMolecola.WIFITOGGLE);
		UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.presenceOfElementLocated(By.xpath(toggle.getLocator()))).click();
		WaitManager.get().waitMediumTime();
		System.out.println("Click == Wi-Fi Toggle");
	}

	public void clickOnAirModeToggle() {
		Particle toggle = (Particle) UiObjectRepo.get().get(IOSDeviceGegericMolecola.AIRPLANETOGGLE);
		UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.presenceOfElementLocated(By.xpath(toggle.getLocator()))).click();
		WaitManager.get().waitMediumTime();
		System.out.println("Click == Air Mode Toggle");
	}
	
	public void clickOnBackFromWifiSetting() {
		System.out.println("Click == Wi-Fi Back");
		Particle backBtn = (Particle) UiObjectRepo.get().get(IOSDeviceGegericMolecola.WIFIBACKBTN);
		UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.presenceOfElementLocated(By.xpath(backBtn.getLocator()))).click();
		WaitManager.get().waitShortTime();

	}

}

