package com.postepay.automation.core.ui.pages.rechargemypostepaypagestep1;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.SelectACardToRechargeMyPostepay;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class RechargeMyPostepayPageStep1Manager extends PageManager {
	
	LayoutTools toolLayout = new LayoutTools();
	
	Particle title=(Particle) UiObjectRepo.get().get(SelectACardToRechargeMyPostepay.TARGETTITLESENDERRECHARGE);
	Particle icon=(Particle) UiObjectRepo.get().get(SelectACardToRechargeMyPostepay.TARGETICONSENDERRECHARGE);
	Particle card_type=(Particle) UiObjectRepo.get().get(SelectACardToRechargeMyPostepay.TARGETTYPESENDERRECHARGE);
	Particle card_number=(Particle) UiObjectRepo.get().get(SelectACardToRechargeMyPostepay.TARGETNUMBERCARDSENDERRECHARGE);
	Particle avaibleLabel=(Particle) UiObjectRepo.get().get(SelectACardToRechargeMyPostepay.TARGETAVAIBLELABELSENDERRECHARGE);
	Particle card_amount=(Particle) UiObjectRepo.get().get(SelectACardToRechargeMyPostepay.TARGETAMOUNTSENDERRECHARGE);
	
	public RechargeMyPostepayPageStep1Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}
	
	public WebElement getElementTitle(WebDriver driver, String titleInput) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, title, titleInput);
		assertTrue(elm.isDisplayed());
		return elm;
	}
	
	public WebElement getElementType(WebDriver driver, String typeInput) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, card_type, typeInput);
		assertTrue(elm.isDisplayed());
		return elm;
	}
	
	public WebElement getElementCardNumber(WebDriver driver, String numberInput) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, card_number, numberInput);
		assertTrue(elm.isDisplayed());
		return elm;
	}
	
	public void verifyImageCard(WebDriver driver, String imageName, double discrepanza) {
		String nameText = "ricaricaPostepay/" + imageName;
		LayoutImage.get().verifyImage(driver, nameText, discrepanza);
	}
	
	
	public void verifyLayoutPage(WebDriver driver, String titleInput, String typeInput, String numberInput, double discrepanza) {
		getElementTitle(driver, titleInput);
//		getElementType(driver, typeInput);
		getElementCardNumber(driver, numberInput);
		assertTrue(avaibleLabel.getElement().isDisplayed());
		assertTrue(card_amount.getElement().isDisplayed());
		verifyImageCard(driver, "icona_nera", discrepanza);
		System.out.println("\nCardBiancoNera è stata verificata");		
		verifyImageCard(driver, "icona_grigia", discrepanza);
		System.out.println("\nCardBiancoGrigia è stata verificata");

	}
	
	public void selectCardToRecharge(WebDriver driver, String numberInput) {
		
		getElementCardNumber(driver, numberInput).click();
		WaitManager.get().waitLongTime();
	}
	
}

