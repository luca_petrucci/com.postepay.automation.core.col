package com.postepay.automation.core.ui.pages.salvadanaiopage;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;

import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;
import ui.core.support.page.Page;

public class SalvadanaioPageManagerIOS extends SalvadanaioPageManager {

	public SalvadanaioPageManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyLayoutPage(WebDriver driver, String nameInput, double discrepanza) {

		assertTrue(imageView.getElement().isDisplayed());
		assertTrue(titleView.getElement().isDisplayed());
		assertTrue(textView.getElement()!=null);
		getElementName(driver, nameInput);
		assertTrue(accantonamentoIcon.getElement().isDisplayed());
		assertTrue(accantonamentoStatusBar.getElement()!=null);
		assertTrue(accantonamentoActualAmount.getElement().isDisplayed());
		assertTrue(accantonamentoTotalAmount.getElement().isDisplayed());
		assertTrue(accantonamentoScadenza.getElement().isDisplayed());
		assertTrue(accantonamentoDettagli.getElement()!= null);
		assertTrue(accantonamentoYellowIconDettagli.getElement()!=null);
		System.out.println("Verifica l'immagine blu del Salvadanaio");
		verifyImage(driver, "salvadanaio_image", discrepanza);
		
		UIUtils.ui().mobile().swipe((MobileDriver) page.getDriver(), SCROLL_DIRECTION.DOWN, 200);
		
		//		Parte del footer che non si vede se ho più elementi
		assertTrue(accantonamentoFooterText.getElement().getText().contains("Puoi creare fino a un massimo di 5 obiettivi, per un importo totale di €5000,00."));
//		assertTrue(accantonamentoFooterText.getElement().getText().contains("Vai su App BancoPosta e crea il tuo obiettivo."));
		assertTrue(accantonamentoCreaNewObiettivo.getElement()!=null);
		
		UIUtils.ui().mobile().swipe((MobileDriver) page.getDriver(), SCROLL_DIRECTION.UP, 200);
		
		//		Icona orologio su automatismo
//		assertTrue(accantonamentoIconaOrologioRicorrenza.getElement().isDisplayed());
	}
	
}
