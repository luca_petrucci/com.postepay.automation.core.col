package com.postepay.automation.core.ui.pages.salvadanaiopichercalender;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.SalvadanaioPicherCalenderMolecola;

import io.appium.java_client.android.AndroidDriver;


public class SalvadanaioPicherCalender extends Page {
	public static final String NAME="T116";
	

	public SalvadanaioPicherCalender(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(SalvadanaioPicherCalenderMolecola.NAME, UiObjectRepo.get().get(SalvadanaioPicherCalenderMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SalvadanaioPicherCalenderManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new SalvadanaioPicherCalenderManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyLayout() {
		((SalvadanaioPicherCalenderManager)this.manager).verifyLayout();
	}
	
	public void clickOnNextMounth() {
		((SalvadanaioPicherCalenderManager)this.manager).clickOnNextMounth();
	}
	
	public void setADay(WebDriver driver, String day) {
		((SalvadanaioPicherCalenderManager)this.manager).setADay(driver, day);
	}
	
	public void clickOnOK() {
		((SalvadanaioPicherCalenderManager)this.manager).clickOnOK();
	}
	
	public void clickOnCANCEL() {
		((SalvadanaioPicherCalenderManager)this.manager).clickOnCANCEL();
	}
	
	public void setADefaultDay(WebDriver driver) {
		((SalvadanaioPicherCalenderManager)this.manager).setADefaultDay(driver);
	}
}

