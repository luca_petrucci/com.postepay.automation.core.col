package com.postepay.automation.core.ui.pages.detailsconnectsimpurchasegigapage;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.BodyG2gPurchase;

import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class DetailsConnectSimPurchaseGigaPageManagerIOS extends DetailsConnectSimPurchaseGigaPageManager {

	public DetailsConnectSimPurchaseGigaPageManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyLayoutPage() {
		Particle description = (Particle) UiObjectRepo.get().get(BodyG2gPurchase.HEADERCONNECTSIMPURCHASEGIGAPAGE);
		Particle gigaToPurchase = (Particle) UiObjectRepo.get().get(BodyG2gPurchase.NUMBEROFGIGATOPURCHASE);
		Particle payWith = (Particle) UiObjectRepo.get().get(BodyG2gPurchase.PAYWITHCONNECTFORPURCHASEGIGAPAGE);
		
		assertTrue(description.getElement().isDisplayed());
		assertTrue(gigaToPurchase.getElement().isDisplayed());
		assertTrue(payWith.getElement().getText().contains("PostePay ****"));
		assertTrue(amountToPay.getElement().isDisplayed());	
	}

}
