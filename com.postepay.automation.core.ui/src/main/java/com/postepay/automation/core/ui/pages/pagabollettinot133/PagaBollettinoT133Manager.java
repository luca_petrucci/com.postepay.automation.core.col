package com.postepay.automation.core.ui.pages.pagabollettinot133;

import org.openqa.selenium.By;

import com.postepay.automation.core.ui.molecules.PagaBollettinoM253;

import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.UiObject;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class PagaBollettinoT133Manager extends PageManager {

	public PagaBollettinoT133Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void onClickBollettinoManuale()
	{
		Particle bollettino= (Particle) UiObjectRepo.get().get(PagaBollettinoM253.M253BTNCOMPILAMANUALE);
		bollettino.getElement().click();
		WaitManager.get().waitMediumTime();
	}
}

