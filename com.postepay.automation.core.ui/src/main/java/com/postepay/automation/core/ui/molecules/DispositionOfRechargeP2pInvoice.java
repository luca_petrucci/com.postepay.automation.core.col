package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class DispositionOfRechargeP2pInvoice extends Molecule {
	public static final String NAME="M016";
	
	public static final String PAYMENTBULLETINTEXTP2P="paymentBulletinTextP2p";
public static final String PTOPICONP2P="pToPIconP2p";
public static final String PAYMENTBULLETINICONP2P="paymentBulletinIconP2p";
public static final String RECHARGEPOSTEPAYICONP2PINVOICE="rechargePostePayIconP2pInvoice";
public static final String RECHARGEPOSTEPAYTEXTP2P="rechargePostePayTextP2p";


	public DispositionOfRechargeP2pInvoice(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

