package com.postepay.automation.core.ui.pages.thelephonerechargepagestep3;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.BodyTelephoneRechargePage;
import com.postepay.automation.core.ui.molecules.BodyTelephoneRechargePageSummary;
import com.postepay.automation.core.ui.molecules.ThankYouPageForTelephoneRecharge;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.pages.thelephonerechargepagestep1.ThelephoneRechargePageStep1Manager;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class ThelephoneRechargePageStep3Manager extends PageManager {
	ThelephoneRechargePageStep1Manager paginaNumero = new ThelephoneRechargePageStep1Manager(page);
	Particle title = (Particle) UiObjectRepo.get().get(BodyTelephoneRechargePageSummary.TITLEHEADERSUMMARYTHELEPHONERECHARGE);
	Particle pagaCon = (Particle) UiObjectRepo.get().get(BodyTelephoneRechargePageSummary.PAYWITHSUMMARYTHELEPHONERECHARGE);
	Particle numeroTel = (Particle) UiObjectRepo.get().get(BodyTelephoneRechargePageSummary.NUMBERTELEPHONERECHARGE);
	Particle operatoreTel = (Particle) UiObjectRepo.get().get(BodyTelephoneRechargePageSummary.OPERATORTELEPHONERECHARGE);
	Particle amountRecharge = (Particle) UiObjectRepo.get().get(BodyTelephoneRechargePageSummary.TOTALRECHARGETELEPHONERECHARGE);
	Particle commissioni = (Particle) UiObjectRepo.get().get(BodyTelephoneRechargePageSummary.COMMISSIONTELEPHONERECHARGE);
	Particle btnPaga = (Particle) UiObjectRepo.get().get(BodyTelephoneRechargePageSummary.CONFIRMBUTTONTELEPHONERECHARGE);
	
	public ThelephoneRechargePageStep3Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeader(String inputTitle) {
		LayoutTools tool= new LayoutTools();
		tool.verifyHeaderPage(inputTitle);	
	}
	
	public void verifyLayoutPage() {
		assertTrue(title.getElement().isDisplayed());
		assertTrue(pagaCon.getElement().isDisplayed());
		assertTrue(numeroTel.getElement().isDisplayed());
		assertTrue(operatoreTel.getElement().isDisplayed());
		assertTrue(amountRecharge.getElement().isDisplayed());
		assertTrue(commissioni.getElement().isDisplayed());
		assertTrue(btnPaga.getElement().isDisplayed());
	}
	
	public void clickOnPay() {
		btnPaga.getElement().click();
		WaitManager.get().waitMediumTime();
	}
		
	public void verifySummary(String iPayWith, String iNumberTel, String iOperatore, String iAmountToRecharge) {
		String numeroTelefono = paginaNumero.getNumber();
		// Estraiamo il testo dai diversi campi
		String payWith = pagaCon.getElement().getText();
		String numberTel = numeroTel.getElement().getText();
		String operatore = operatoreTel.getElement().getText();
		String amount = amountRecharge.getElement().getText();
		String commission = commissioni.getElement().getText();
		
		// Aggiustiamo stringe di input
		String strPayWith = "";
		switch (iPayWith) {
		case "**** 4679":
			strPayWith = "Postepay Evolution ";
			break;
		case "**** 0922":
			strPayWith = "Postepay ";
			break;
		// TODO TAA-727
		default:
			break;
		}
		String mPayWith = strPayWith + iPayWith;
		String mNumberTel = "+39 " + iNumberTel;
		String mOperatore;		
//		String mAmountToRecharge = "€ " + iAmountToRecharge;
		String mAmountToRecharge = iAmountToRecharge;
		
		System.out.println("payWith: AR:\t"+ payWith + "\t\tER:\t"+ mPayWith);
//		System.out.println("numberTel: AR:\t"+ numberTel + "\t\tER:\t"+ mNumberTel);
		System.out.println("amount: AR:\t"+ amount + "\t\tER:\t"+ mAmountToRecharge);
		System.out.println("commission: AR:\t"+ commission + "\t\tER:\t0 €");
//		System.out.println("operatore: AR:\t"+ operatore + "\t\tER:\t"+ iOperatore);
		assertTrue(payWith.contains(mPayWith));
		assertTrue(numberTel.equals(numeroTelefono));
		assertTrue(amount.equals(mAmountToRecharge));
		assertTrue(commission.equals("0 €"));
		if (iOperatore.equals("poste")) {
			mOperatore = "PosteMobile";			
			assertTrue(operatore.equals(mOperatore));
		}else if (iOperatore.equals("vodafone")) {
			mOperatore = "Vodafone";
			System.out.println("operatore: AR:\t"+ operatore + "\t\tER:\t"+ mOperatore);
			assertTrue(operatore.equals(mOperatore));
//			System.out.println("operatore: AR:\t"+ operatore + "AR:\t"+ mOperatore); 
			
		}
		WaitManager.get().waitShortTime();
	}
	
	public void verifySummaryTypTelephone() {
		Particle title = (Particle) UiObjectRepo.get().get(ThankYouPageForTelephoneRecharge.THANKYOUTITLERECHARGEMYPP);
		Particle img = (Particle) UiObjectRepo.get().get(ThankYouPageForTelephoneRecharge.THANKYOUIMAGERECHARGEMYPP);
		Particle btnPaga = (Particle) UiObjectRepo.get().get(ThankYouPageForTelephoneRecharge.THANKYOUCLOSEBUTTONRECHARGEMYPP);
		
		assertTrue(title.getElement().getText().contains("Operazione eseguita!"));
		assertTrue(img.getElement().isDisplayed());
		assertTrue(btnPaga.getElement().getText().contains("CHIUDI"));
		
		WaitManager.get().waitShortTime();
		
		btnPaga.getElement().click();
		WaitManager.get().waitShortTime();
	}
}

