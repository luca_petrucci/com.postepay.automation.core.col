package com.postepay.automation.core.ui.pages.rechargemypostepaypagestep2;

import com.postepay.automation.core.ui.molecules.BodyRechargeOtherPostepay;
import com.postepay.automation.core.ui.molecules.CompileFormToRechargeMyPostepay;

import io.appium.java_client.AppiumDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class RechargeMyPostepayPageStep2Manager extends PageManager {

	public RechargeMyPostepayPageStep2Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void clickOnProceedButton() {
		Particle tab=(Particle) UiObjectRepo.get().get(CompileFormToRechargeMyPostepay.PROCEEDRECHARGEBUTTONOMPILEFORM);

		tab.getElement().click();		

		WaitManager.get().waitShortTime();

	}
	
	public void clickOnToggleAutomatickRecharge() {
		Particle tab=(Particle) UiObjectRepo.get().get(CompileFormToRechargeMyPostepay.AUTOMATICHRECHARGETOGGLEINCOMPILEFORM);

		tab.getElement().click();		

		WaitManager.get().waitShortTime();

	}
	
	public void insertReasonOfRecharge(String value) {
		Particle tab=(Particle) UiObjectRepo.get().get(CompileFormToRechargeMyPostepay.REASONOFRECHARGEMYPOSTEPAY);

		tab.getElement().sendKeys(value);	

		WaitManager.get().waitShortTime();

//		((AppiumDriver<?>)page.getDriver()).hideKeyboard();	
		//chiude la tastiera
		Particle tastiera=(Particle) UiObjectRepo.get().get(CompileFormToRechargeMyPostepay.NUMBERMYPOSTEPAY);
		tastiera.getElement().click();
		WaitManager.get().waitShortTime();
	}
	
	public void insertAmountToSendTargetUser(String value) {
		Particle tab=(Particle) UiObjectRepo.get().get(CompileFormToRechargeMyPostepay.AMOUNTTOSENDTOMYPOSTEPAY);

		tab.getElement().click();
		tab.getElement().sendKeys(value);			

		WaitManager.get().waitShortTime();

//		((AppiumDriver<?>)page.getDriver()).hideKeyboard();	
		//chiude la tastiera
		Particle tastiera=(Particle) UiObjectRepo.get().get(CompileFormToRechargeMyPostepay.NUMBERMYPOSTEPAY);
		tastiera.getElement().click();
		WaitManager.get().waitShortTime();
	}

}

