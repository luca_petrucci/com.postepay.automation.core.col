package com.postepay.automation.core.ui.pages.loginpostepage;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.CredentialsCardLogin;
import com.postepay.automation.core.ui.molecules.SignInNewUserButton;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.HidesKeyboard;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class LoginPostePageManager extends PageManager {

	Particle editTextUsername=(Particle) UiObjectRepo.get().get(CredentialsCardLogin.USERNAMELOGIN);
	Particle editTextPassword=(Particle) UiObjectRepo.get().get(CredentialsCardLogin.PASSWORDLOGIN);
	Particle box=(Particle) UiObjectRepo.get().get(CredentialsCardLogin.REMEMBERMYCREDENTIALLOGIN);
	Particle btnAccedi=(Particle) UiObjectRepo.get().get(CredentialsCardLogin.SIGNINBUTTONLOGIN);
	Particle btnRegistra=(Particle) UiObjectRepo.get().get(CredentialsCardLogin.OPPUREREGISTRATI);          
	Particle btnLostCred=(Particle) UiObjectRepo.get().get(CredentialsCardLogin.LOSTCREDENTIALLINKDURINGLOGIN);
	Particle benvenutiText=(Particle) UiObjectRepo.get().get(CredentialsCardLogin.BENVENUTITEXT);
	Particle benvenutiSubText=(Particle) UiObjectRepo.get().get(CredentialsCardLogin.BENVENUTISUBTEXT);
		
	public LoginPostePageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	/*
	 *  Verifica il Titolo ddella pagina
	 */
	public int verifyHeaderPage(String title) throws Error 
	{
		LayoutTools layOutTool= new LayoutTools();
		int value=layOutTool.verifyHeaderPage(title);
		return value;
	}

	public void insertCredential(String username, String password) {
				
		// Inserisci Username
		editTextUsername.getElement().clear();		
		editTextUsername.getElement().sendKeys(username);
		WaitManager.get().waitShortTime();		
		// Inserisci Password
		editTextPassword.getElement().clear();		
		editTextPassword.getElement().sendKeys(password);
		WaitManager.get().waitShortTime();
//		((HidesKeyboard) page.getDriver()).hideKeyboard();
		benvenutiSubText.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnRememberMyCredential() {
		// Click sul box
		box.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnAccedi() {
		// Click sul bottone accedi
		btnAccedi.getElement().click();
		WaitManager.get().waitLongTime();
	}
	
	public void clickOnRegisterNewUser() {
		// Click su Registra nuovo Utente
		btnRegistra.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnLostCredential() {
		// Click su Registra nuovo Utente
		btnLostCred.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void verifyLayout() {
		clearUsername();
		assertTrue(benvenutiText.getElement().isDisplayed());
		assertTrue(benvenutiSubText.getElement().isDisplayed());
		assertTrue(editTextUsername.getElement().getText().equals("Username"));
		assertTrue(editTextPassword.getElement().isDisplayed());
		assertTrue(box.getElement().isDisplayed());
		assertTrue(btnAccedi.getElement().isDisplayed());
		assertTrue(btnRegistra.getElement().isDisplayed());
		assertTrue(btnLostCred.getElement().isDisplayed());
	}
	
	public void clearUsername() {
		//Pulici il contenuto del campo Username
		boolean getValue = editTextUsername.getElement().getText().equals("Username");
		
		if (getValue == false)
		
			editTextUsername.getElement().clear();
		
	}
}

