package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class ExtraurbaPurchaseTicketMolecola extends Molecule {
	public static final String NAME="M215";
	
	public static final String ANNULLABTNEXTRAURBAN="annullaBtnExtraurban";
public static final String PROVIDEROFSERVICEEXTRAURBAN="providerOfServiceExtraurban";
public static final String DATEOFPURCHASEEXTRAURBAN="dateOfPurchaseExtraurban";
public static final String AMOUNTSUMMARYEXTRAURBAN="amountSummaryExtraUrban";
public static final String PAYWITHHEADEREXTRAURBAN="payWithHeaderExtraurban";
public static final String PAYBTNEXTRAURBAN="payBtnExtraurban";
public static final String PAYWITHTHECARD="payWithTheCard";
public static final String TITLETEXTSUMMARYEXTRAURBAN="titleTextSummaryExtraurban";


	public ExtraurbaPurchaseTicketMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

