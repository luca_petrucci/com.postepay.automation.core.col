package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyRechargePostepayUsingTelephonContacts extends Molecule {
	public static final String NAME="M069";
	
	public static final String TITLESELECTWHOTAKERECHARGE="titleSelectWhoTakeRecharge";
public static final String TABMYCARDS="tabMyCards";
public static final String LISTOFCONTACTSCARDS="listOfContactsCards";
public static final String TABOTHERCONTACTSCARDS="tabOtherContactsCards";
public static final String LISTOFCONTACTSBYCARDNUMBER = "listOfContactsCardsByCard";

	public BodyRechargePostepayUsingTelephonContacts(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

