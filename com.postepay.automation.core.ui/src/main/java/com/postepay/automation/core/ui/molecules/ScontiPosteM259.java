package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class ScontiPosteM259 extends Molecule {
	public static final String NAME = "M259";

	public static final String SCONTISTORICOM259 = "scontiStoricoM259";
	public static final String SCONTIACCREDITATOLABELM259 = "scontiAccreditatoLabelM259";
	public static final String SCONTICARDLINKM259 = "scontiCardLinkM259";
	public static final String SCONTICERCACOUNTERM259 = "scontiCercaCounterM259";
	public static final String SCONTICERCAFILTROM259 = "scontiCercaFiltroM259";
	public static final String SCONTICARDIMAGEM259 = "scontiCardImageM259";
	public static final String SCONTICARDDESCRM259 = "scontiCardDescrM259";
	public static final String SCONTICERCAM259 = "scontiCercaM259";
	public static final String SCONTIACCREDITATOIMPORTOM259 = "scontiAccreditatoImportoM259";
	public static final String SCONTICARDTITLEM259 = "scontiCardTitleM259";
//	public static final String SCONTIACCREDITATIM259 = "scontiAccreditatiM259";

	public ScontiPosteM259(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}
