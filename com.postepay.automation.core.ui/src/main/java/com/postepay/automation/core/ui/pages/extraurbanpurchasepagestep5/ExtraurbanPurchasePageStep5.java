package com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep5;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.molecules.BodyExtraurbanPurchaseFinalSummary;


public class ExtraurbanPurchasePageStep5 extends Page {
	public static final String NAME="T064";
	

	public ExtraurbanPurchasePageStep5(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(BodyExtraurbanPurchaseFinalSummary.NAME, UiObjectRepo.get().get(BodyExtraurbanPurchaseFinalSummary.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new ExtraurbanPurchasePageStep5Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderpage(String inputTitle) {
		((ExtraurbanPurchasePageStep5Manager)this.manager).verifyHeaderpage(inputTitle);
	}
	
	public void verifyDestination(String inputFrom, String inputTo) {
		((ExtraurbanPurchasePageStep5Manager)this.manager).verifyDestination(inputFrom, inputTo);
	}
	
	public void verifyName(String inputName, String inputSurname) {
		((ExtraurbanPurchasePageStep5Manager)this.manager).verifyName(inputName, inputSurname);
	}
	
	public void isPayButtonEnabled(String statusBtn) {
		((ExtraurbanPurchasePageStep5Manager)this.manager).isPayButtonEnabled(statusBtn);
	}
	
	public void clickOnPrivacy() {
		((ExtraurbanPurchasePageStep5Manager)this.manager).clickOnPrivacy();
	}
	
	public void clickOnPay() {
		((ExtraurbanPurchasePageStep5Manager)this.manager).clickOnPay();
	}
}

