package com.postepay.automation.core.ui.pages.loginwithposteid;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.MsgsHeaderOldPosteId;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

import com.postepay.automation.core.ui.molecules.EditTextNotIAmConfirmBtnOldPosteId;
import com.postepay.automation.core.ui.molecules.FingerPrintAccessMolecola;


public class LoginWithPosteId extends Page {
	public static final String NAME="T002";
	

	public LoginWithPosteId(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
//		this.addToTemplate(MsgsHeaderOldPosteId.NAME, UiObjectRepo.get().get(MsgsHeaderOldPosteId.NAME), true);
		this.addToTemplate(EditTextNotIAmConfirmBtnOldPosteId.NAME, UiObjectRepo.get().get(EditTextNotIAmConfirmBtnOldPosteId.NAME), true);
		this.addToTemplate(FingerPrintAccessMolecola.NAME, UiObjectRepo.get().get(FingerPrintAccessMolecola.NAME), false);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new LoginWithPosteIdManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	public void insertPosteId(String posteId) {
		((LoginWithPosteIdManager)this.manager).insertPosteId(posteId);
	}
	
	public void clickOnConfirmButton() {
		((LoginWithPosteIdManager)this.manager).clickOnConfirmButton();
	}
	
	public void clickOnCambiaAccount() {
		((LoginWithPosteIdManager)this.manager).clickOnCambiaAccount();
	}
	
	public void verifyLayout() {
		((LoginWithPosteIdManager)this.manager).verifyLayout();
	}
	
	public void verifyHeaderPage(String inputTitle) throws Error {
		((LoginWithPosteIdManager)this.manager).verifyHeaderPage(inputTitle);
		
	}
	
	public void verifyLayoutPopupSendFingerprint() {
		((LoginWithPosteIdManager)this.manager).verifyLayoutPopupSendFingerprint();
	}
	
	public void sendFingerprint(String adbExe, String deviceName, int fingerId) throws Exception {
		((LoginWithPosteIdManager)this.manager).sendFingerprint(adbExe, deviceName, fingerId);
	}
	public void verifyHelloName(String nameInput) {
		((LoginWithPosteIdManager)this.manager).verifyHelloName(nameInput);
	}

}

