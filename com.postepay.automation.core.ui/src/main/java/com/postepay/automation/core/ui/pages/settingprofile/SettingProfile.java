package com.postepay.automation.core.ui.pages.settingprofile;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.SettingProfileMolecola;


public class SettingProfile extends Page {
	public static final String NAME="T098";
	

	public SettingProfile(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), false);
		this.addToTemplate(SettingProfileMolecola.NAME, UiObjectRepo.get().get(SettingProfileMolecola.NAME), false);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SettingProfileManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderpage(String inputTitle) {
		((SettingProfileManager)this.manager).verifyHeaderpage(inputTitle);
	}
	
	public void verifyLayoutPage() {
		((SettingProfileManager)this.manager).verifyLayoutPage();
	}
	
	public void clickOnIlTuoPieno() {
		((SettingProfileManager)this.manager).clickOnIlTuoPieno();
	}
	
	public void clickOnIlTuoRifornimentoPreferito() {
		((SettingProfileManager)this.manager).clickOnIlTuoRifornimentoPreferito();
	}
	
	public void clickOnCHIUDI() {
		((SettingProfileManager)this.manager).clickOnCHIUDI();
	}
	
	public void clickOnCHIUDIRight() {
		((SettingProfileManager)this.manager).clickOnCHIUDIRight();
	}
}

