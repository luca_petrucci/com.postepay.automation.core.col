package com.postepay.automation.core.ui.pages.newaccesspage;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.NewAccessPageMolecola;

import io.appium.java_client.android.AndroidDriver;


public class NewAccessPage extends Page {
	public static final String NAME="T129";
	

	public NewAccessPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(NewAccessPageMolecola.NAME, UiObjectRepo.get().get(NewAccessPageMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new NewAccessPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new NewAccessPageIOSManager(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
		
	public void verifyIconImage(WebDriver driver, double discrepanza) {
		((NewAccessPageManager)this.manager).verifyIconImage(driver, discrepanza);
	}
	
	public void verifyLogoImage(WebDriver driver, double discrepanza) {
		((NewAccessPageManager)this.manager).verifyLogoImage(driver, discrepanza);
	}
	
	public void verifyBtnTextImage(WebDriver driver, double discrepanza) {
		((NewAccessPageManager)this.manager).verifyBtnTextImage(driver, discrepanza);
	}
	
	public void verifyLayout() {
		((NewAccessPageManager)this.manager).verifyLayout();
	}

	public void clickOnAccedi() {
		((NewAccessPageManager)this.manager).clickOnAccedi();
	}
	
	public void clickOnNonSeiTu() {
		((NewAccessPageManager)this.manager).clickOnNonSeiTu();
	}
	
	public void clickOnAutorizzaSitoWeb() {
		((NewAccessPageManager)this.manager).clickOnAutorizzaSitoWeb();
	}
	
	public void clickOnListaAutorizzazioni() {
		((NewAccessPageManager)this.manager).clickOnListaAutorizzazioni();
	}
	
	public void clickOnScopriSconti() {
		((NewAccessPageManager)this.manager).clickOnScopriSconti();
	}
	
	public void verifyHelloName(String nameInput) {
		((NewAccessPageManager)this.manager).verifyHelloName(nameInput);	
	}
}

