package com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep4;

import com.postepay.automation.core.ui.molecules.BodyDetailsOfTravelersExtraurbanPurchase;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class ExtraurbanPurchasePageStep4ManagerIOS extends ExtraurbanPurchasePageStep4Manager {

	public ExtraurbanPurchasePageStep4ManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void sendTelephono(String telephone) {
		Particle input = (Particle) UiObjectRepo.get().get(BodyDetailsOfTravelersExtraurbanPurchase.INPUTPHONE);
		input.getElement().clear();
		input.getElement().sendKeys(telephone);
		WaitManager.get().waitShortTime();
		//chiude la tastiera
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.LEFT, 100);
//		Particle tastiera = (Particle) UiObjectRepo.get().get(BodyDetailsOfTravelersExtraurbanPurchase.INFOPASSENGERCONTAINER);
//		tastiera.getElement().click();
		try {((AppiumDriver<?>)page.getDriver()).hideKeyboard();}catch(Exception e ){}
	}
}
