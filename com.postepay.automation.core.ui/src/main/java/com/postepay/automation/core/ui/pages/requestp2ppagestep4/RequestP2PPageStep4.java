package com.postepay.automation.core.ui.pages.requestp2ppagestep4;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.AmoutCreditToSend;


public class RequestP2PPageStep4 extends Page {
	public static final String NAME="T015";


	public RequestP2PPageStep4(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(AmoutCreditToSend.NAME, UiObjectRepo.get().get(AmoutCreditToSend.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {

		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {

		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {

		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new RequestP2PPageStep4Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {

		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {

		return null;
	}

	public void insetAmoutToRequest(String amount) {

		((RequestP2PPageStep4Manager)this.manager).insetAmoutToRequest(amount);	

		WaitManager.get().waitShortTime();
	}

	public void clickOnConfirmButton() {

		((RequestP2PPageStep4Manager)this.manager).clickOnConfirmButton();	

		WaitManager.get().waitMediumTime();

	}
}

