package com.postepay.automation.core.ui.pages.thelephonerechargepagestep1;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.BodyTelephoneRechargePage;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.ThankYouPagePurchaseG2G;
import com.postepay.automation.core.ui.molecules.ToolbarTitlePurchaseGiga;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.HidesKeyboard;
import test.automation.core.UIUtils;
import test.automation.core.cmd.adb.AdbCommandPrompt;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class ThelephoneRechargePageStep1Manager extends PageManager {
	Particle fieldNumber = (Particle) UiObjectRepo.get().get(BodyTelephoneRechargePage.INPUTNUMBERTHELEPHONERECHARGE);
	Particle fieldOperatore = (Particle) UiObjectRepo.get().get(BodyTelephoneRechargePage.MOBILEPROVIDERTHELEPHONERECHARGE);
	Particle fieldImporto = (Particle) UiObjectRepo.get().get(BodyTelephoneRechargePage.INPUTIMPORTHELEPHONERECHARGE);
	Particle btnNumeroTelefonoRubrica = (Particle) UiObjectRepo.get().get(BodyTelephoneRechargePage.NUMEROTELEFONORUBRICA);
	private String numero;
	
	public ThelephoneRechargePageStep1Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}


	public void verifyHeader(String inputTitle) {
		LayoutTools tool= new LayoutTools();
		tool.verifyHeaderPage(inputTitle);	
	}
	
	public void verifyLayoutPage() {
		Particle text = (Particle) UiObjectRepo.get().get(BodyTelephoneRechargePage.TITLEPAGETHELEPHONERECHARGE);
		
		assertTrue(text.getElement().isDisplayed());
		assertTrue(fieldNumber.getElement().isDisplayed());
		assertTrue(fieldOperatore.getElement().isDisplayed());
		assertTrue(fieldImporto.getElement().isDisplayed());
	}
	
	public void clickOnProceed() {
		Particle btn = (Particle) UiObjectRepo.get().get(BodyTelephoneRechargePage.BUTTONCONFIRMTHELEPHONERECHARGE);
		System.out.println("Click su conferma");
		btn.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}

	
	public String getNumber() {
		return numero = btnNumeroTelefonoRubrica.getElement().getText();
	}
	
	
	public void sendTelephoneNumber(String telephone, String adbExe) {
		Particle btnRubrica = (Particle) UiObjectRepo.get().get(BodyTelephoneRechargePage.BUTTONRUBRICA);
		System.out.println("Click su conferma");
		btnRubrica.getElement().click();		
		WaitManager.get().waitShortTime();
		
		Particle btnNumeroTelefonoRubrica = (Particle) UiObjectRepo.get().get(BodyTelephoneRechargePage.NUMEROTELEFONORUBRICA);
		System.out.println("Click su conferma");
		btnNumeroTelefonoRubrica.getElement().click();		
		WaitManager.get().waitShortTime();
		getNumber();
		
//		fieldNumber.getElement().click();
//		WaitManager.get().waitHighTime();
//		
//		Properties devName = UIUtils.ui().getCurrentProperties();
//		//A.F. FIX TAA-2057
//		String udid=devName.containsKey("ios.udid") ? devName.getProperty("ios.udid") : devName.getProperty("capability.udid.string");
//		AdbCommandPrompt cmd=new AdbCommandPrompt(adbExe, udid);
//		cmd.typeText(telephone);
		
//		Non funziona il metodo sendKeys()
//		fieldNumber.getElement().sendKeys(telephone);
//		try {
//			((HidesKeyboard) page.getDriver()).hideKeyboard();
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//		WaitManager.get().waitLongTime();
	}
	
	public void sendAmount(String amount) {
		System.out.println("Click su importo ricarica");
		fieldImporto.getElement().click();
		WaitManager.get().waitMediumTime();
		
		Particle p=(Particle) UiObjectRepo.get().get(BodyTelephoneRechargePage.CHOOSETHELEPHON);
		String nuovoLocator=Utility.replacePlaceHolders(p.getLocator(), new Entry("title",amount));
		
		WebDriver driver=UIUtils.ui().getCurrentDriver();
		
		WebElement specificAmount = driver.findElement(By.xpath(nuovoLocator));
		
		specificAmount.click();
		
		WaitManager.get().waitMediumTime();
	}
	
	public void isOperatorePreValorizedString(String gestoreRicarica) {
		String getValue = fieldOperatore.getElement().getText();
		
		String posteMobile = "PosteMobile";
		String vodafone = "Vodafone";
		
		switch (gestoreRicarica) {
		case "posta":
			assertTrue(getValue.equals(posteMobile));
			break;
		case "vodafone":
			assertTrue(getValue.equals(vodafone));			
			break;
		default:
			break;
		}
	}
}

