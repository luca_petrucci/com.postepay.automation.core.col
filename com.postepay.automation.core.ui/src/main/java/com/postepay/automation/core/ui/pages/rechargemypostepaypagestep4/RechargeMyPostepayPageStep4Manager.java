package com.postepay.automation.core.ui.pages.rechargemypostepaypagestep4;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;

import com.postepay.automation.core.ui.molecules.BodyRechargeMyPostepaySummary;
import com.postepay.automation.core.ui.molecules.WhomWontSendAPostagiroSummary;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class RechargeMyPostepayPageStep4Manager extends PageManager {

	public RechargeMyPostepayPageStep4Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderPage(String inputTitle) throws Error {
		
		LayoutTools layOutTool= new LayoutTools();
		layOutTool.verifyHeaderPage(inputTitle);
	}
	
	public String getNameTargetUser() {

		Particle intestatario=(Particle) UiObjectRepo.get().get(BodyRechargeMyPostepaySummary.SUMMARYHOLDERRECHARGEPAYPP);

		return intestatario.getElement().getText();
	}

	public String getCardNumberTargetUser() {

		Particle iban=(Particle) UiObjectRepo.get().get(BodyRechargeMyPostepaySummary.SUMMARYPAYTOPP);

		return iban.getElement().getText();
	}

	public String getAmountToPay() {

		Particle amount=(Particle) UiObjectRepo.get().get(BodyRechargeMyPostepaySummary.SUMMARYAMOUNTTOPAYPP);

		return amount.getElement().getText();
	}

	public String getMyCardNumber() {

		Particle cardNumebr=(Particle) UiObjectRepo.get().get(BodyRechargeMyPostepaySummary.SUMMARYPAYWITHPP);

		return cardNumebr.getElement().getText();
	}

	public String getReason() {

		Particle reason=(Particle) UiObjectRepo.get().get(BodyRechargeMyPostepaySummary.SUMMARYREASONOFRECHARGE);

		return reason.getElement().getText();
	}

	public String getAmountSumFromButton() {

		Particle sum=(Particle) UiObjectRepo.get().get(BodyRechargeMyPostepaySummary.TOTALAMOUNTRECHARGESUMMARY);

		return sum.getElement().getText();
	}
	
	public String getAmountCommition() {

		Particle commition=(Particle) UiObjectRepo.get().get(BodyRechargeMyPostepaySummary.SUMMARYAMOUTOFCOMMISSION);

		return commition.getElement().getText();
	}

	public void verifySummaryRecharge(String targetOperation,String nameInput, String cardTargetInput, String amountInput, String reasonInput) {

		String nameTarget=getNameTargetUser().toUpperCase();
		String cardTarget=getCardNumberTargetUser();
//		String cardNumber=getMyCardNumber();     DA IMPLEMENTARE CONTROLLO SU QUESTA VARIABILE
		String reason=getReason();
		String amount=getAmountToPay();
//		String amountSum=getAmountSumFromButton();
		Particle sommTot=(Particle) UiObjectRepo.get().get(BodyRechargeMyPostepaySummary.TOTALAMOUNTRECHARGESUMMARY);
		String amountSum= sommTot.getElement().getText();
		String commition=getAmountCommition();
		
		System.out.println(nameTarget);
		System.out.println(cardTarget);
		System.out.println(amount);
//		System.out.println(cardNumber);
		System.out.println(reason);
		System.out.println(amountSum);
		System.out.println("--------------------------------");
		
		// Tool per Stringhe e numeri
		StringAndNumberOperationTools operationTool=new StringAndNumberOperationTools();
		
		// Aggiustamento nomeTargetInput
		String newNameInput=nameInput.toUpperCase();
//		System.out.println("newNameInput " + newNameInput);
		
		// Aggiustamento cardTargetInput
//		System.out.println("cardTargetInput "+ cardTargetInput);
		//String newCardTargetInput= operationTool.convertNumberToHiddeNumber(cardTargetInput, targetOperation);
//		System.out.println("newCardTargetInput "+newCardTargetInput);
		
		// Aggiustamento input SOLDI INVIATI
//		System.out.println("amountInput " + amountInput);
		String newAmount=operationTool.convertAmountWithEuro(amountInput);
//		System.out.println("newAmount"+newAmount);
		// Aggiustamento verifica su IMPORTO BOTTONE PAGA
		// Otteniamo il valore numerico del bottone PAGA
//		System.out.println("amountSum"+amountSum);
		double newAmountButtonInteger=operationTool.convertWithoutEUROlabel(amountSum);
//		System.out.println("newAmountButtonInteger " + newAmountButtonInteger);
		// Otteniamo la somma di COMMISSIONE e IMPORTO
		double newAmountInteger=operationTool.convertWithoutEUROlabel(amount);
//		System.out.println("newAmountInteger "+newAmountInteger);
		double newCommissionInteger=operationTool.convertWithoutEUROlabel(commition);
//		System.out.println(newCommissionInteger);
		// Somma
		double sumCommImp= newCommissionInteger+newAmountInteger;
//		System.out.println(sumCommImp);		
		
		// Verifiche
		System.out.println("Expected: "+ newNameInput +" ---> Actual: "+nameTarget);
		assertEquals(newNameInput, nameTarget);
		
		System.out.println("newNameInput OK");
		
		String panCard4digit=cardTargetInput.substring(cardTargetInput.length()-4);
		System.out.println("Expected: "+ panCard4digit +" ---> Actual: "+cardTarget);
//		assertEquals(newCardTargetInput, cardTarget);
		Assert.assertTrue("Expected: "+ panCard4digit +" ---> Actual: "+cardTarget, cardTarget.contains(panCard4digit));
		
		System.out.println("newCardTargetInput OK");
		
		System.out.println("Expected: "+ newAmount +" ---> Actual: "+amount);
		assertEquals(newAmount, amount);
		
		System.out.println("newAmount OK");
//		assertEquals(cardNumberInput, cardNumber);
		
		System.out.println("Expected: "+ reasonInput +" ---> Actual: "+reason);
		assertEquals(reasonInput, reason);
		
		System.out.println("reasonInput OK");
		
		System.out.println("Expected: € 1,00" +" ---> Actual: "+commition);
		assertEquals("\u20ac 1,00", commition);
		
		System.out.println("commition OK");
		
		Particle paga = (Particle) UiObjectRepo.get().get(BodyRechargeMyPostepaySummary.CONFIRMPAYMENTTOSEND);
		String pagaTxt=paga.getElement().getText();
		System.out.println("Expected: "+ sumCommImp +" ---> Actual: "+pagaTxt);
		String totalToPay=operationTool.convertAmountWithEuro(""+sumCommImp).replace("\u20ac", "").trim();
		Assert.assertTrue("Expected: "+ totalToPay +" ---> Actual: "+pagaTxt, pagaTxt.contains(totalToPay));
//		assertEquals(sumCommImp, newAmountButtonInteger, 0.0);
		System.out.println("sumCommImp OK");

		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnPayButton() {
		Particle btn=(Particle) UiObjectRepo.get().get(BodyRechargeMyPostepaySummary.CONFIRMPAYMENTTOSEND);
		
		btn.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
}

