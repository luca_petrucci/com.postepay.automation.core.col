package com.postepay.automation.core.ui.verifytools;
import org.openqa.selenium.WebDriver;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import io.appium.java_client.android.AndroidDriver;
import test.automation.core.ImageUtils;
import test.automation.core.UIUtils;

public class LayoutImage {

	private static LayoutImage singleton;
	
	public LayoutImage() {
		// TODO Auto-generated constructor stub
	}

	public static LayoutImage get() {
		if (singleton==null) {
			singleton = new LayoutImage();
		}
		return singleton;
	}
	
	public void verifyImage(WebDriver driver, String imageName, double discrepanza) {
	
		String imgToFinde = UIUtils.ui().getCurrentProperties().getProperty("path.imageFind") + imageName + ".png";
		String imgScreen = UIUtils.ui().getCurrentProperties().getProperty("path.imageScreenshot") + "screen.png";
		
		System.out.println(imgToFinde);
		System.out.println(imgScreen);
		
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		// Now you can do whatever you need to do with it, for example copy somewhere
		try {
			FileUtils.copyFile(scrFile, new File(imgScreen));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			if (discrepanza > 0)  {
			boolean isImage = ImageUtils.image().findImage(imgToFinde, imgScreen, discrepanza);
			System.out.println(discrepanza);
			
			assertTrue(isImage);
		}
		
	}
	
	public void faiLoScreeshotRODOLFO(WebDriver driver) {
		String imgScreen = "";
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		// Now you can do whatever you need to do with it, for example copy somewhere
		try {
			FileUtils.copyFile(scrFile, new File(imgScreen));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
}
