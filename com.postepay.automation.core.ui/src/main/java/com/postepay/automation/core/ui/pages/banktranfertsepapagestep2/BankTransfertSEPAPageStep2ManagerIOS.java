package com.postepay.automation.core.ui.pages.banktranfertsepapagestep2;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.postepay.automation.core.ui.molecules.WhomWontSendABonificoSepa;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class BankTransfertSEPAPageStep2ManagerIOS extends BankTranfertSEPAPageStep2Manager {

	private By pickers = MobileBy.className("XCUIElementTypePickerWheel");;

	public BankTransfertSEPAPageStep2ManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void selectCountry()
	{
		Particle country = (Particle) UiObjectRepo.get()
				.get(WhomWontSendABonificoSepa.HOLDERCOUNTRYRESIDENCEOFBANKTRANFERTSEPA);

//		country.getElement().click();
//		WaitManager.get().waitShortTime();
//		
//        List<WebElement> pickerEls = UIUtils.ui().waitForCondition(page.getDriver(),ExpectedConditions.presenceOfAllElementsLocatedBy(pickers));
//        System.out.println(pickerEls.size());
//        pickerEls.get(0).sendKeys("Italia (IT)");
		String paese=country.getElement().getText();
		assertTrue("Controllo paese",paese.equals("Italia (IT)"));
        
        clickCloseKeyBoard();
	}
	
	public void insertIntestatario(String valueIntestatario) {
		System.out.println("insertIntestatario - iOS MANAGER - key Intestatario: "+ valueIntestatario);
		
		Particle intestatario = (Particle) UiObjectRepo.get()
				.get(WhomWontSendABonificoSepa.HEADEROFTARGETCARDOFBANKTRANFERTSEPA);
		
		System.out.println("insertIntestatario - iOS MANAGER - CLICK");
		intestatario.getElement().click();

		WaitManager.get().waitShortTime();

		System.out.println("insertIntestatario - iOS MANAGER - SEND");
		intestatario.getElement().sendKeys(valueIntestatario);

		WaitManager.get().waitShortTime();

		Particle title= (Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepa.HEADEROFBANKTRANFERTSEPA);
		title.getElement().click();
//		((AppiumDriver<?>) page.getDriver()).hideKeyboard();
		
		clickCloseKeyBoard();

		WaitManager.get().waitShortTime();
	}
	
	public void clickCloseKeyBoard() {
		WebElement el = null;
		try {
			el = page.getDriver().findElement(By.xpath("//*[contains(@*,'Fine')]"));	
		} catch (Exception err) {
			
		}
		
		if( el != null ) {
			page.getDriver().findElement(By.xpath("//*[contains(.,'rubrica')]")).click();
			WaitManager.get().waitShortTime();	
		}
	}
	
	public void insertReason(String valueReason) {

		try {
			UIUtils.mobile().swipe((MobileDriver<?>) page.getDriver(), SCROLL_DIRECTION.DOWN, 500);
		} catch (Exception err) {
		}

		Particle reason = (Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepa.SUMMARYREASONOFBANKTRANFERTSEPA);

		reason.getElement().sendKeys(valueReason);

		WaitManager.get().waitShortTime();

//		((AppiumDriver<?>) page.getDriver()).hideKeyboard();
		Particle title= (Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepa.HEADEROFBANKTRANFERTSEPA);
		title.getElement().click();

		clickCloseKeyBoard();
		
		WaitManager.get().waitShortTime();
	}
}
