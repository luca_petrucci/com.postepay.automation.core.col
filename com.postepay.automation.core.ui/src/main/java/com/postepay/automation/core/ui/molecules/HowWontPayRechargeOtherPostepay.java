package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class HowWontPayRechargeOtherPostepay extends Molecule {
	public static final String NAME="M068";
	
	public static final String TITLEHOWTOPAYOTHERCARD="titleHowToPayOtherCard";
public static final String LINKAPPBANCOPOSTA="linkAppBancoPosta";
public static final String SUBTITLEHOWTOPAYOTHERCARD="subTitleHowToPayOtherCard";
public static final String NUMBERCARDSENDERRECHARGE="numberCardSenderRecharge";


	public HowWontPayRechargeOtherPostepay(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

