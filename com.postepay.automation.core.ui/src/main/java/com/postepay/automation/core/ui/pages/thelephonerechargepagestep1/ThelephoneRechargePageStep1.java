package com.postepay.automation.core.ui.pages.thelephonerechargepagestep1;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.molecules.BodyTelephoneRechargePage;


public class ThelephoneRechargePageStep1 extends Page {
	public static final String NAME="T072";
	

	public ThelephoneRechargePageStep1(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(BodyTelephoneRechargePage.NAME, UiObjectRepo.get().get(BodyTelephoneRechargePage.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new ThelephoneRechargePageStep1Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new ThelephoneRechargePageStep1ManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeader(String inputTitle) {
		((ThelephoneRechargePageStep1Manager)this.manager).verifyHeader(inputTitle);
	}
	
	public void verifyLayoutPage() {
		((ThelephoneRechargePageStep1Manager)this.manager).verifyLayoutPage();
	}
	
	public void clickOnProceed() {
		((ThelephoneRechargePageStep1Manager)this.manager).clickOnProceed();
	}
	
	public void sendTelephoneNumber(String telephone, String adbExe) {
		((ThelephoneRechargePageStep1Manager)this.manager).sendTelephoneNumber(telephone, adbExe);
	}
	
	public void sendAmount(String amount) {
		((ThelephoneRechargePageStep1Manager)this.manager).sendAmount(amount);
	}
	
	public void isOperatorePreValorizedString(String gestoreRicarica) {
		((ThelephoneRechargePageStep1Manager)this.manager).isOperatorePreValorizedString(gestoreRicarica);
	}
}

