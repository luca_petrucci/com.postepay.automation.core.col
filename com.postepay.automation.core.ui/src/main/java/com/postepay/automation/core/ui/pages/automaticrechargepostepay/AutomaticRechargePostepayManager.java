package com.postepay.automation.core.ui.pages.automaticrechargepostepay;

import com.postepay.automation.core.ui.molecules.BodyAutomaticRechargePostepay;
import com.postepay.automation.core.ui.pages.selectrecurrenceautomaticrechargepostepay.SelectRecurrenceAutomaticRechargePostepay;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class AutomaticRechargePostepayManager extends PageManager {

	public AutomaticRechargePostepayManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void selectRecurrencyRecharge(String valueRechargeAutomatic) {
		Particle clickRecurrency=(Particle) UiObjectRepo.get().get(BodyAutomaticRechargePostepay.SETTINGTIMERECURSIVERECHARGE);

		clickRecurrency.getElement().click();

		SelectRecurrenceAutomaticRechargePostepay selectCurrencyPage=(SelectRecurrenceAutomaticRechargePostepay) PageRepo.get().get(SelectRecurrenceAutomaticRechargePostepay.NAME);

		selectCurrencyPage.clickOnRecurrency(valueRechargeAutomatic);

	}

	public void selectRecurrencyRechargeDate(String valueRechargeAutomaticDate) {
		Particle clickRecurrencyDate=(Particle) UiObjectRepo.get().get(BodyAutomaticRechargePostepay.TODATEOFRECURSIVERECHARGE);

		clickRecurrencyDate.getElement().clear();
		
		WaitManager.get().waitShortTime();
		
		clickRecurrencyDate.getElement().sendKeys(valueRechargeAutomaticDate);		
		
		WaitManager.get().waitShortTime();
	}

	public void selectRecurrencyRechargeName(String valueRechargeAutomaticName) {
		Particle clickRecurrencyName=(Particle) UiObjectRepo.get().get(BodyAutomaticRechargePostepay.NAMEOFRECURSIVERECHARGE);

		clickRecurrencyName.getElement().sendKeys(valueRechargeAutomaticName);		
		
		WaitManager.get().waitShortTime();
	}
	
	public void compileFormAutomaticRecharge(String valueRechargeAutomatic, String valueRechargeAutomaticDate, String valueRechargeAutomaticName) {
		
		selectRecurrencyRecharge(valueRechargeAutomatic);
		selectRecurrencyRechargeDate(valueRechargeAutomaticDate);
		selectRecurrencyRechargeName(valueRechargeAutomaticName);
	}
	
	public void clickOnRechargSOGLIA() {
		Particle sogliaBtn=(Particle) UiObjectRepo.get().get(BodyAutomaticRechargePostepay.SWITCHSOGLIABUTTON);
		sogliaBtn.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void sendSaldoMinimo(String saldoMinimo) {
		Particle sogliaBtn=(Particle) UiObjectRepo.get().get(BodyAutomaticRechargePostepay.AMOUNTMINIMOSOGLIA);
		sogliaBtn.getElement().sendKeys(saldoMinimo);
		WaitManager.get().waitShortTime();
	}
	
	
	
}

