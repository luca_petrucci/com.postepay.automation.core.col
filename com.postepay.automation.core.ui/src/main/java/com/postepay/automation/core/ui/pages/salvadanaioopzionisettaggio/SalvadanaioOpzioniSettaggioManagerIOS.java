package com.postepay.automation.core.ui.pages.salvadanaioopzionisettaggio;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.appium.java_client.MobileBy;
import test.automation.core.UIUtils;
import ui.core.support.page.Page;
import ui.core.support.waitutil.WaitManager;

public class SalvadanaioOpzioniSettaggioManagerIOS extends SalvadanaioOpzioniSettaggioManager {

	private By pickers = MobileBy.className("XCUIElementTypePickerWheel");
	public SalvadanaioOpzioniSettaggioManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyLayoutOfFrequenza() {
		assertTrue(frequenzaOgniGiorno.getElement()!=null);
//		assertTrue(frequenzaOgni7giorni.getElement()!=null);
//		assertTrue(frequenzaOgni15giorni.getElement()!=null);
//		assertTrue(frequenzaOgni30giorni.getElement()!=null);
	}
	public void selectFrequenza(String frequenza) {
		switch (frequenza) {
		case "ogni giorno":
			 List<WebElement> picker1Giorno= UIUtils.ui().waitForCondition(page.getDriver(),ExpectedConditions.presenceOfAllElementsLocatedBy(pickers));
		        System.out.println(picker1Giorno.size());
		        picker1Giorno.get(0).sendKeys("Ogni giorno");
			//frequenzaOgniGiorno.getElement().click();
			break;
		case "ogni 7 giorni":
			 List<WebElement> picker7Giorni = UIUtils.ui().waitForCondition(page.getDriver(),ExpectedConditions.presenceOfAllElementsLocatedBy(pickers));
		        System.out.println(picker7Giorni.size());
		        picker7Giorni.get(0).sendKeys("Ogni 7 giorni");
			frequenzaOgni7giorni.getElement().click();
			break;
		case "ogni 15 giorni":
			 List<WebElement> picker15Giorni = UIUtils.ui().waitForCondition(page.getDriver(),ExpectedConditions.presenceOfAllElementsLocatedBy(pickers));
		        System.out.println(picker15Giorni.size());
		        picker15Giorni.get(0).sendKeys("Ogni 15 giorni");
			frequenzaOgni15giorni.getElement().click();
			break;
		case "ogni 30 giorni":
			 List<WebElement> picker30Giorni = UIUtils.ui().waitForCondition(page.getDriver(),ExpectedConditions.presenceOfAllElementsLocatedBy(pickers));
		        System.out.println(picker30Giorni.size());
		        picker30Giorni.get(0).sendKeys("Ogni 30 giorni");
			frequenzaOgni30giorni.getElement().click();
			break;
		default:
			frequenzaOgniGiorno.getElement().click();
			break;
		}
		WaitManager.get().waitMediumTime();
	}
}
