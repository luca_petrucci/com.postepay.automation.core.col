package com.postepay.automation.core.ui.pages.pagabollettinobiancot136;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;

import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.RiepilogoPagamanetoM258;


public class RiepilogoPagamentoSummaryT136 extends Page {
	public static final String NAME="T136";
	

	public RiepilogoPagamentoSummaryT136(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
this.addToTemplate(RiepilogoPagamanetoM258.NAME, UiObjectRepo.get().get(RiepilogoPagamanetoM258.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new RiepilogoPagamentoSummaryT136Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderPage(String inputTitle) throws Error {	
		((RiepilogoPagamentoSummaryT136Manager)this.manager).verifyHeaderPage(inputTitle);
	}
	
	public void verifyFilledData(String contoCorrente, String targetUser, String importo, String causale, String nome, String cognome, String indirizzo, String cap, String citta, String provincia, String cardNumber) {
		((RiepilogoPagamentoSummaryT136Manager)this.manager).verifyFilledData(contoCorrente, targetUser, importo, causale, nome, cognome, indirizzo, cap, citta, provincia, cardNumber);
	}
	
	public void clickOnPaga() {
		((RiepilogoPagamentoSummaryT136Manager)this.manager).clickOnPaga();
	}

	public void clickOnAnnulla() {
		((RiepilogoPagamentoSummaryT136Manager)this.manager).clickOnAnnulla();
	}
}

