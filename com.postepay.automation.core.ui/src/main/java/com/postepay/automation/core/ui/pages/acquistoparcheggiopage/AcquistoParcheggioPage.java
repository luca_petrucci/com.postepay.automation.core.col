package com.postepay.automation.core.ui.pages.acquistoparcheggiopage;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.AbilitaInAppMolecola;
import com.postepay.automation.core.ui.molecules.AcquistoParcheggioMolecola;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;


public class AcquistoParcheggioPage extends Page {
	public static final String NAME="T150";
	

	public AcquistoParcheggioPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(AcquistoParcheggioMolecola.NAME, UiObjectRepo.get().get(AcquistoParcheggioMolecola.NAME), true);
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new AcquistoParcheggioPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyData(String titolo, String tipo, String luogo, String indirizzo, String msgStatico, String msgDinamico) {
		((AcquistoParcheggioPageManager)this.manager).verifyData(titolo, tipo, luogo, indirizzo, msgStatico, msgDinamico);
	}
	
}

