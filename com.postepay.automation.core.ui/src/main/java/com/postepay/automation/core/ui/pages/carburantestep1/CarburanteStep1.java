package com.postepay.automation.core.ui.pages.carburantestep1;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.CarburanteStep1Molecola;
import com.postepay.automation.core.ui.molecules.BannerCarburanteMolecola;


public class CarburanteStep1 extends Page {
	public static final String NAME="T100";
	

	public CarburanteStep1(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), false);
this.addToTemplate(CarburanteStep1Molecola.NAME, UiObjectRepo.get().get(CarburanteStep1Molecola.NAME), false);
this.addToTemplate(BannerCarburanteMolecola.NAME, UiObjectRepo.get().get(BannerCarburanteMolecola.NAME), false);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new CarburanteStep1Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new CarburanteStep1ManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderpage() {
		((CarburanteStep1Manager)this.manager).verifyHeaderpage();
	}
	
	public void verifyLayoutPage() {
		((CarburanteStep1Manager)this.manager).verifyLayoutPage();
	}
	
	public void isSettedImportoPreferito(String defaultImporto) {
		((CarburanteStep1Manager)this.manager).isSettedImportoPreferito(defaultImporto);
	}
	
	public void isSettedPieno(String defaultPieno) {
		((CarburanteStep1Manager)this.manager).isSettedPieno(defaultPieno);
	}
	
	public void moveTo20() {
		((CarburanteStep1Manager)this.manager).moveTo20();
	}
	
	public void moveToOtherAmount() {
		((CarburanteStep1Manager)this.manager).moveToOtherAmount();
	}
	
	public void moveTo5() {
		((CarburanteStep1Manager)this.manager).moveTo5();
	}
}

