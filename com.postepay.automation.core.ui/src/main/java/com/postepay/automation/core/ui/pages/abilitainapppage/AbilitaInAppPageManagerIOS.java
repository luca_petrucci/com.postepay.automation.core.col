package com.postepay.automation.core.ui.pages.abilitainapppage;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.postepay.automation.core.ui.molecules.AbilitaInAppMolecola;

import io.appium.java_client.HidesKeyboard;
import io.appium.java_client.MobileBy;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.CoreUtility;


public class AbilitaInAppPageManagerIOS extends AbilitaInAppPageManager {

	private By pickers1 = MobileBy.xpath("//*[@*='XCUIElementTypePickerWheel'][1]"); 
	private By pickers2 = MobileBy.xpath("//*[@*='XCUIElementTypePickerWheel'][2]"); 

	public AbilitaInAppPageManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void clickOnOnbDevice(String list) {
		try {

			Particle inizaBtn = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242INIZIAONBDESC);
			CoreUtility.visibilityOfElement(page.getDriver(), inizaBtn, 20).click();
			WaitManager.get().waitShortTime();

		} catch (Exception e) {
			// TODO: handle exception
		}


		// Dobbiamo selezionare la carta
		// Prima ci importiamo i dati dal file di language
		String[] listaDatiCarta = list.split(";");

		Particle selezionaCarta = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242SELEZIONACARTA);
		String nuovoLocator=Utility.replacePlaceHolders(selezionaCarta.getLocator(), new Entry("title",listaDatiCarta[0]));
		//		page.getDriver().findElement(By.xpath(nuovoLocator)).click();
		CoreUtility.visibilityOfElement(page.getDriver(), By.xpath(nuovoLocator), 20).click();
		WaitManager.get().waitShortTime();


		// Inserisco la scadenza(mese)
		WebDriverWait wait = new WebDriverWait(page.getDriver(), 25);
		List<WebElement> pickerEls = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(pickers1));
		char mese=listaDatiCarta[1].charAt(1);
		String s=String.valueOf(mese);
		pickerEls.get(0).sendKeys(s);
		WaitManager.get().waitShortTime();

		// Inserisco la scadenza(anno)
		pickerEls = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(pickers2));
		char num2=listaDatiCarta[1].charAt(2);
		char num3=listaDatiCarta[1].charAt(3);
		String s2=String.valueOf(num2);
		String s3=String.valueOf(num3);
		String s4=s2.concat(s3);
		String anno="20";
		pickerEls.get(0).sendKeys(anno.concat(s4));

		// Clicco su conferma
		try {CoreUtility.visibilityOfElement(page.getDriver(), By.xpath("//*[@*='Fine' and @*='XCUIElementTypeStaticText']"), 20).click();} catch (Exception e) {}
		Particle confermaScadenza = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242SCADENZACONFERMA);
		CoreUtility.visibilityOfElement(page.getDriver(), confermaScadenza, 20).click();
		WaitManager.get().waitShortTime();

		// Inserisco il CVV
		Particle cvv = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242CVV);
		CoreUtility.visibilityOfElement(page.getDriver(), cvv, 20).sendKeys(listaDatiCarta[2]);
		WaitManager.get().waitShortTime();
		try {CoreUtility.visibilityOfElement(page.getDriver(), By.xpath("//*[@*='Fine' and @*='XCUIElementTypeStaticText']"), 20).click();} catch (Exception e) {}
		// Clicco su conferma
		Particle cvvScadenza = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242CVVCONFERMA);
		CoreUtility.visibilityOfElement(page.getDriver(), cvvScadenza, 20).click();
		WaitManager.get().waitMediumTime();

		// Devo confermare con un OTP per SMS
		Particle smsConferma = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242SMSBTNCONFERMA);
		CoreUtility.visibilityOfElement(page.getDriver(), smsConferma, 20).click();
		WaitManager.get().waitShortTime();
	}

	public void clickOnDeleteDevice() {
		//System.out.println(nonAutorizzaA.getElement().getText());
		WaitManager.get().waitShortTime();
		Dimension size = page.getDriver().manage().window().getSize();
		new TouchAction((PerformsTouchActions) page.getDriver())
		.press(PointOption.point(76, 447))
		//         .press(PointOption.point(108, 135))
		.release()
		.perform();
		//		nonAutorizzaA.getElement().click();
		WaitManager.get().waitShortTime();

		Particle popupConferma = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242POPUPCONTINUA);
		CoreUtility.visibilityOfElement(page.getDriver(), popupConferma, 20).click();
		WaitManager.get().waitShortTime();

		// Confermiamo la cancellazione con Sms
		Particle sms = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242SMSBTNCONFERMA);
		CoreUtility.visibilityOfElement(page.getDriver(), sms, 20).click();

	}

	public void inserisciMeseanno(String daticarta) {
		String[] listaDatiCarta = daticarta.split(";");
		// Inserisco la scadenza(mese)
		WebDriverWait wait = new WebDriverWait(page.getDriver(), 25);
		List<WebElement> pickerEls = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(pickers1));
		char mese=listaDatiCarta[1].charAt(1);
		String s=String.valueOf(mese);
		pickerEls.get(0).sendKeys(s);
		WaitManager.get().waitShortTime();

		// Inserisco la scadenza(anno)
		pickerEls = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(pickers2));
		char num2=listaDatiCarta[1].charAt(2);
		char num3=listaDatiCarta[1].charAt(3);
		String s2=String.valueOf(num2);
		String s3=String.valueOf(num3);
		String s4=s2.concat(s3);
		String anno="20";
		pickerEls.get(0).sendKeys(anno.concat(s4));

		// Clicco su conferma
		try {CoreUtility.visibilityOfElement(page.getDriver(), By.xpath("//*[@*='Fine' and @*='XCUIElementTypeStaticText']"), 20).click();} catch (Exception e) {}
		Particle confermaScadenza = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242SCADENZACONFERMA);
		CoreUtility.visibilityOfElement(page.getDriver(), confermaScadenza, 20).click();
		WaitManager.get().waitShortTime();

	}

	public void inserisciCvv(String daticarta) {
		String[] listaDatiCarta = daticarta.split(";");
		// Inserisco il CVV
		Particle cvv = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242CVV);
		CoreUtility.visibilityOfElement(page.getDriver(), cvv, 20).sendKeys(listaDatiCarta[2]);
		WaitManager.get().waitShortTime();
		try {CoreUtility.visibilityOfElement(page.getDriver(), By.xpath("//*[@*='Fine' and @*='XCUIElementTypeStaticText']"), 20).click();} catch (Exception e) {}
		// Clicco su conferma
		Particle cvvScadenza = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242CVVCONFERMA);
		CoreUtility.visibilityOfElement(page.getDriver(), cvvScadenza, 20).click();
		WaitManager.get().waitMediumTime();

		// Devo confermare con un OTP per SMS
		Particle smsConferma = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242SMSBTNCONFERMA);
		CoreUtility.visibilityOfElement(page.getDriver(), smsConferma, 20).click();
		WaitManager.get().waitShortTime();

	}
}
