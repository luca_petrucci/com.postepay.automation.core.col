package com.postepay.automation.core.ui.pages.loginpostepage;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.CredentialsCardLogin;
import com.postepay.automation.core.ui.molecules.SignInNewUserButton;
import io.appium.java_client.android.AndroidDriver;


public class LoginPostePage extends Page {
	public static final String NAME="T001";
	

	public LoginPostePage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(CredentialsCardLogin.NAME, UiObjectRepo.get().get(CredentialsCardLogin.NAME), true);
//		this.addToTemplate(SignInNewUserButton.NAME, UiObjectRepo.get().get(SignInNewUserButton.NAME), true);
		
	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new LoginPostePageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public int verifyHeaderPage(String title) {
		
		return ((LoginPostePageManager)this.manager).verifyHeaderPage(title);
	}
		
	public void insertCredential(String username, String password) {
		
		((LoginPostePageManager)this.manager).insertCredential(username, password);
	}

	public void clickOnRememberMyCredential() {
		
		((LoginPostePageManager)this.manager).clickOnRememberMyCredential();
	}

	public void clickOnAccedi() {
		
		((LoginPostePageManager)this.manager).clickOnAccedi();
	}

	public void clickOnRegisterNewUser() {
		
		((LoginPostePageManager)this.manager).clickOnRegisterNewUser();
	}

	public void clickOnLostCredential() {
		
		((LoginPostePageManager)this.manager).clickOnLostCredential();
	}
	
	public void verifyLayout() {
		((LoginPostePageManager)this.manager).verifyLayout();
	}
	
}

