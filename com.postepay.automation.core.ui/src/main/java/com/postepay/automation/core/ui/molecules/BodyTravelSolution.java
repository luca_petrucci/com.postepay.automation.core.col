package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;


import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyTravelSolution extends Molecule {
	public static final String NAME="M092";

	public static final String CARDRESUMEEXTRAURBANPURCHASE="cardResumeExtraurbanPurchase";
	public static final String BUTTONEXTRAURBANPURCHASE="buttonExtraurbanPurchase";
	public static final String PRICEEXTRAURBANPURCHASE="priceExtraurbanPurchase";
	public static final String CARDTICKETEXTRAURBANPURCHASE="cardTicketExtraurbanPurchase";
	public static final String STARTDATEEXTRAURBAN = "startDateExtraurban";
	public static final String DESTINATIONEXTRAUBAN = "destinationExtrauban";
	public static final String MODIFYDESTINATIONICON = "modifyDestinationIcon";
	public static final String DROPMENUALLFILTER = "dropMenuAllFilter";
	public static final String DROPMENUPRINCIPALFILTER = "dropMenuPrincipalFilter";
	public static final String TRAINICONEXTRAURBANFILTER = "trainIconExtraurbanFilter";
	public static final String BUSICONEXTRAURBANFILTER = "busIconExtraurbanFilter";
	public static final String TICKETCARDTENITALIALOGO = "ticketCardTenitaliaLogo";
	public static final String TICKETCARDFROM = "ticketCardFrom";
	public static final String TICKETCARDTO = "ticketCardTo";
	public static final String TICKETCARDAMOUNT = "ticketCardAmount";
	public static final String FILTROVETTORI = "filtroVettori";
	public static final String FILTROORDINAMENTO= "filtroOrdinamento";
	public static final String FILTROVALUE= "filtroValues";
	
	
	public BodyTravelSolution(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

