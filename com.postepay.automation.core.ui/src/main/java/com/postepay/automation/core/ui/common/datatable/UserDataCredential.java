package com.postepay.automation.core.ui.common.datatable;

public class UserDataCredential {

	private String password;
	private String testIdCheck;
	private String device;
	private String posteId;
	private String username;
	private String targetUser;
	private String cardNumber;
	private String listaTarget;
	private String importo;
	private String comment;
	private String iban;
	private String myIban;
	private String targetOperation;
	private String cardNumberDestinatario;
	private String recurrencyRecharge;
	private String recurrencyRechargeDate;
	private String recurrencyRechargeName;
	private String tipeMessage;
	private String dateOfOperation;
	private String userNumberCards;
	private String fingerprintID;
	private String gestoreRicarica;
	private String numeroTel;
	private String serviceType;
	private String moreInfo;
	private String fromCity;
	private String toCity;
	private String namePassenger;
	private String surnamePassenger;
	private String cardPassenger;
	private String emailPassenger;
	private String phonePassenger;
	private String andataRitorno;
	private String startDate;
	private String startTime; 
	private String passengers;
	private String kids;
	private String searchBy;
	private String qty;
	private String importoPieno; 
	private String importoPreferito;
	private String carburantePage;
	private String discrepanza;
	private String name;
	private String driver;
	private String causale;
	private String contoCorrente;
	private String regione;
	private String targa;
	private String veicolo;
	private String libretto;
	private String fortunatoNome;    
	private String fortunatoCognome;
	private String fortunatoIndirizzo;
	private String fortunatoCitta;
	private String fortunatoCap;
	private String fortunatoProvincia;
	private String typeTYP;
	public String getTypeTYP() {
		return typeTYP;
	}

	public void setTypeTYP(String typeTYP) {
		this.typeTYP = typeTYP;
	}
	private String codiceFiscale;
	


	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

	public String getFortunatoNome() {
		return fortunatoNome;
	}

	public void setFortunatoNome(String fortunatoNome) {
		this.fortunatoNome = fortunatoNome;
	}

	public String getFortunatoCognome() {
		return fortunatoCognome;
	}

	public void setFortunatoCognome(String fortunatoCognome) {
		this.fortunatoCognome = fortunatoCognome;
	}

	public String getFortunatoIndirizzo() {
		return fortunatoIndirizzo;
	}

	public void setFortunatoIndirizzo(String fortunatoIndirizzo) {
		this.fortunatoIndirizzo = fortunatoIndirizzo;
	}

	public String getFortunatoCitta() {
		return fortunatoCitta;
	}

	public void setFortunatoCitta(String fortunatoCitta) {
		this.fortunatoCitta = fortunatoCitta;
	}

	public String getFortunatoCap() {
		return fortunatoCap;
	}

	public void setFortunatoCap(String fortunatoCap) {
		this.fortunatoCap = fortunatoCap;
	}

	public String getFortunatoProvincia() {
		return fortunatoProvincia;
	}

	public void setFortunatoProvincia(String fortunatoProvincia) {
		this.fortunatoProvincia = fortunatoProvincia;
	}

	public String getLibretto() {
		return libretto;
	}

	public void setLibretto(String libretto) {
		this.libretto = libretto;
	}

	public String getRegione() {
		return regione;
	}

	public void setRegione(String regione) {
		this.regione = regione;
	}

	public String getTarga() {
		return targa;
	}

	public void setTarga(String targa) {
		this.targa = targa;
	}

	public String getVeicolo() {
		return veicolo;
	}

	public void setVeicolo(String veicolo) {
		this.veicolo = veicolo;
	}

	public String getCausale() {
		return causale;
	}
	
	public void setCausale(String causale) {
		this.causale = causale;
	}
	
	public String getContoCorrente() {
		return contoCorrente;
	}
	
	public void setContoCorrente(String contoCorrente) {
		this.contoCorrente = contoCorrente;
	}
	
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver = driver;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDiscrepanza() {
		return discrepanza;
	}
	public void setDiscrepanza(String discrepanza) {
		this.discrepanza = discrepanza;
	}
	
	public String getCarburantePage() {
		return carburantePage;
	}
	public void setCarburantePage(String carburantePage) {
		this.carburantePage = carburantePage;
	}
	
	public String getImportoPieno() {
		return importoPieno;
	}
	public void setImportoPieno(String importoPieno) {
		this.importoPieno = importoPieno;
	}
	public String getImportoPreferito() {
		return importoPreferito;
	}
	public void setImportoPreferito(String importoPreferito) {
		this.importoPreferito = importoPreferito;
	}
	public String getSearchBy() {
		return searchBy;
	}
	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getPosteId() {
		return posteId;
	}
	public void setPosteId(String posteId) {
		this.posteId = posteId;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getMoreInfo() {
		return moreInfo;
	}
	public void setMoreInfo(String moreInfo) {
		this.moreInfo = moreInfo;
	}
	public String getFromCity() {
		return fromCity;
	}
	public void setFromCity(String fromCity) {
		this.fromCity = fromCity;
	}
	public String getToCity() {
		return toCity;
	}
	public void setToCity(String toCity) {
		this.toCity = toCity;
	}
	public String getNamePassenger() {
		return namePassenger;
	}
	public void setNamePassenger(String namePassenger) {
		this.namePassenger = namePassenger;
	}
	public String getSurnamePassenger() {
		return surnamePassenger;
	}
	public void setSurnamePassenger(String surnamePassenger) {
		this.surnamePassenger = surnamePassenger;
	}
	public String getCardPassenger() {
		return cardPassenger;
	}
	public void setCardPassenger(String cardPassenger) {
		this.cardPassenger = cardPassenger;
	}
	public String getEmailPassenger() {
		return emailPassenger;
	}
	public void setEmailPassenger(String emailPassenger) {
		this.emailPassenger = emailPassenger;
	}
	public String getPhonePassenger() {
		return phonePassenger;
	}
	public void setPhonePassenger(String phonePassenger) {
		this.phonePassenger = phonePassenger;
	}
	public String getAndataRitorno() {
		return andataRitorno;
	}
	public void setAndataRitorno(String andataRitorno) {
		this.andataRitorno = andataRitorno;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getPassengers() {
		return passengers;
	}
	public void setPassengers(String passengers) {
		this.passengers = passengers;
	}
	public String getKids() {
		return kids;
	}
	public void setKids(String kids) {
		this.kids = kids;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getNumeroTel() {
		return numeroTel;
	}
	public void setNumeroTel(String numeroTel) {
		this.numeroTel = numeroTel;
	}
	public String getGestoreRicarica() {
		return gestoreRicarica;
	}
	public void setGestoreRicarica(String gestoreRicarica) {
		this.gestoreRicarica = gestoreRicarica;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String value) {
		this.password = value;
	}
	public String getTestIdCheck() {
		return testIdCheck;
	}
	public void setTestIdCheck(String value) {
		this.testIdCheck = value;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String value) {
		this.device = value;
	}
	public String getPosteid() {
		return posteId;
	}
	public void setPosteid(String value) {
		this.posteId = value;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String value) {
		this.username = value;
	}
	public String getTargetUser() {
		return targetUser;
	}
	public void setTargetUser(String value) {
		this.targetUser = value;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCcardNumber(String value) {
		this.cardNumber = value;
	}
	public String getImporto() {
		return importo;
	}
	public void setImporto(String value) {
		this.importo = value;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String value) {
		this.comment = value;
	}
	public String getIban() {
		return iban;
	}
	public void setIban(String value) {
		this.iban = value;
	}
	public String getMyIban() {
		return myIban;
	}
	public void setMyIban(String value) {
		this.myIban = value;
	}
	public String getTargetOperation() {
		return targetOperation;
	}
	public void setTargetOperation(String value) {
		this.targetOperation = value;
	}
	public String getCardNumberDestinatario() {
		return cardNumberDestinatario;
	}
	public void setCardNumberDestinatario(String value) {
		this.cardNumberDestinatario = value;
	}
	public String getRecurrencyRecharge() {
		return recurrencyRecharge;
	}
	public void setRecurrencyRecharge(String value) {
		this.recurrencyRecharge = value;
	}
	public String getRecurrencyRechargeDate() {
		return recurrencyRechargeDate;
	}
	public void setRecurrencyRechargeDate(String value) {
		this.recurrencyRechargeDate = value;
	}
	public String getRecurrencyRechargeName() {
		return recurrencyRechargeName;
	}
	public void setRecurrencyRechargeName(String value) {
		this.recurrencyRechargeName = value;
	}
	public String getTipeMessage() {
		return tipeMessage;
	}
	public void setTipeMessage(String value) {
		this.tipeMessage = value;	
	}
	public String getDateOfOperation() {
		return dateOfOperation;
	}
	public void setDateOfOperation(String value) {
		this.dateOfOperation = value;
	}
	public String getUserNumberCards() {
		return userNumberCards;
	}
	public void setUserNumberCards(String value) {
		this.userNumberCards = value;
	}
	public String getFingerprintID() {
		return fingerprintID;
	}
	public void setFingerprintID(String fingerprintID) {
		this.fingerprintID = fingerprintID;
	}
	public String getListaTarget() {
		return listaTarget;
	}

	public void setListaTarget(String listaTarget) {
		this.listaTarget = listaTarget;
	}
}
