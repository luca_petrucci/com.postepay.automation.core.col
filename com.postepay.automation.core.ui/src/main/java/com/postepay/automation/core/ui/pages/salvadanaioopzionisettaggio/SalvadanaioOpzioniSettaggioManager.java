package com.postepay.automation.core.ui.pages.salvadanaioopzionisettaggio;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.SalvadanaioOpzioniSettaggioMolecola;
import com.postepay.automation.core.ui.molecules.SalvadanaioVersaSuObiettivoMolecola;
import com.postepay.automation.core.ui.molecules.SalvadanaioVersaSuObiettivoRicorrenteMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class SalvadanaioOpzioniSettaggioManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();

	Particle bottomPicker = (Particle) UiObjectRepo.get().get(SalvadanaioOpzioniSettaggioMolecola.SALVADANAIOBOTTOMPICKER);
	Particle pickerVersaconPpEvolution = (Particle) UiObjectRepo.get().get(SalvadanaioOpzioniSettaggioMolecola.SALVADANAIOPICKERVERSACONPPEVOLUTION);
	Particle pickerGestisciBp = (Particle) UiObjectRepo.get().get(SalvadanaioOpzioniSettaggioMolecola.SALVADANAIOPICKERGESTISCIBP);
	Particle pickerCondividi = (Particle) UiObjectRepo.get().get(SalvadanaioOpzioniSettaggioMolecola.SALVASANAIOPICKERCONDIVIDI);
	Particle frequenzaOgniGiorno = (Particle) UiObjectRepo.get().get(SalvadanaioOpzioniSettaggioMolecola.FREQUENZAOGNIGIORNO);
	Particle frequenzaOgni7giorni = (Particle) UiObjectRepo.get().get(SalvadanaioOpzioniSettaggioMolecola.FREQUENZAOGNI7GIORNI);
	Particle frequenzaOgni15giorni = (Particle) UiObjectRepo.get().get(SalvadanaioOpzioniSettaggioMolecola.FREQUENZAOGNI15GIORNI);
	Particle frequenzaOgni30giorni = (Particle) UiObjectRepo.get().get(SalvadanaioOpzioniSettaggioMolecola.FREQUENZAOGNI30GIORNI);
	Particle gestisciVersamentiRicorrenti = (Particle) UiObjectRepo.get().get(SalvadanaioOpzioniSettaggioMolecola.GESTISCIVERSAMENTIRICORRENTI);

	public SalvadanaioOpzioniSettaggioManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyLayoutOfFrequenza() {
		assertTrue(frequenzaOgniGiorno.getElement().isDisplayed());
		assertTrue(frequenzaOgni7giorni.getElement().isDisplayed());
		assertTrue(frequenzaOgni15giorni.getElement().isDisplayed());
		assertTrue(frequenzaOgni30giorni.getElement().isDisplayed());
	}

	public void selectFrequenza(String frequenza) {
		switch (frequenza) {
		case "ogni giorno":
			frequenzaOgniGiorno.getElement().click();
			break;
		case "ogni 7 giorni":
			frequenzaOgni7giorni.getElement().click();
			break;
		case "ogni 15 giorni":
			frequenzaOgni15giorni.getElement().click();
			break;
		case "ogni 30 giorni":
			frequenzaOgni30giorni.getElement().click();
			break;
		default:
			frequenzaOgniGiorno.getElement().click();
			break;
		}
		WaitManager.get().waitMediumTime();
	}

	public void verifyLayoutOfSettings() {
		assertTrue(pickerVersaconPpEvolution.getElement().isDisplayed());
		assertTrue(pickerGestisciBp.getElement().isDisplayed());
		assertTrue(pickerCondividi.getElement().isDisplayed());
	}
	
	public void selectSetting(String sceltaMenu) {
		switch (sceltaMenu) {
		case "Versa con Postepay Evolution":
			pickerVersaconPpEvolution.getElement().click();
			break;
		case "Gestisci in BP":
			pickerGestisciBp.getElement().click();
			break;
		case "Condividi":
			pickerCondividi.getElement().click();
			break;
		default:
			pickerVersaconPpEvolution.getElement().click();
			break;
		}
		WaitManager.get().waitMediumTime();
	}

	public void verifyLayoutOfDettaglioPicker() {
		assertTrue(gestisciVersamentiRicorrenti.getElement().isDisplayed());
	}
	
	public void clickOnGestisciVersamentoRicorrente() {
		gestisciVersamentiRicorrenti.getElement().click();
	}
}

