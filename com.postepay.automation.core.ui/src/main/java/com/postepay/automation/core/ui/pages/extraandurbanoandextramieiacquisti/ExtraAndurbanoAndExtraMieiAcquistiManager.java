package com.postepay.automation.core.ui.pages.extraandurbanoandextramieiacquisti;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.ExtraAndurbanoAndExtraMieiAcquistiMolecola;
import com.postepay.automation.core.ui.molecules.MieiAcquistiPageMolecole;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import ui.core.support.waitutil.WaitManager;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class ExtraAndurbanoAndExtraMieiAcquistiManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();

	Particle titolo = (Particle) UiObjectRepo.get().get(ExtraAndurbanoAndExtraMieiAcquistiMolecola.URBANOANDEXTRAMIEIACQUISTITITOLO);
	Particle finanziariLabel = (Particle) UiObjectRepo.get().get(ExtraAndurbanoAndExtraMieiAcquistiMolecola.URBANOANDEXTRAMIEIACQUISTILABELSERVIZIFINANZIARI);
	Particle data = (Particle) UiObjectRepo.get().get(ExtraAndurbanoAndExtraMieiAcquistiMolecola.URBANOANDEXTRAMIEIACQUISTILABELDATA);
	Particle iconBus = (Particle) UiObjectRepo.get().get(ExtraAndurbanoAndExtraMieiAcquistiMolecola.URBANOANDEXTRAMIEIACQUISTIICONABUS);
	Particle titoloCorsa = (Particle) UiObjectRepo.get().get(ExtraAndurbanoAndExtraMieiAcquistiMolecola.URBANOANDEXTRAMIEIACQUISTITITOLOCORSA);
	Particle compagnia = (Particle) UiObjectRepo.get().get(ExtraAndurbanoAndExtraMieiAcquistiMolecola.URBANOANDEXTRAMIEIACQUISTICOMPAGNIA);
	Particle messaggio = (Particle) UiObjectRepo.get().get(ExtraAndurbanoAndExtraMieiAcquistiMolecola.URBANOANDEXTRAMIEIACQUISTIMESSAGGIOSTATICO);
	Particle messaggioDinamico = (Particle) UiObjectRepo.get().get(ExtraAndurbanoAndExtraMieiAcquistiMolecola.URBANOANDEXTRAMIEIACQUISTIMESSAGGIODINAMICO);
	Particle btnScaricaPDF = (Particle) UiObjectRepo.get().get(ExtraAndurbanoAndExtraMieiAcquistiMolecola.URBANOANDEXTRAMIEIACQUISTIBUTTONESCARICARICEVUTA);

	public ExtraAndurbanoAndExtraMieiAcquistiManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderPage(String targetOperation) {
		switch (targetOperation) {
		case "urbano":
			toolLayout.verifyHeaderPage("Acquisto trasporto Urbano");			
			break;
		case "extraurbano":
			toolLayout.verifyHeaderPage("Acquisto trasporto Extraurbano");
			break;
		default:
			break;
		}
	}

	public void verifyTitle(WebDriver driver, String targetOperation) {
		WebElement elmTitle;
		switch (targetOperation) {
		case "urbano":
			elmTitle = toolLayout.replaceGenericPathOfElement(driver, titolo, "Biglietto urbano");
			assertTrue(elmTitle.isDisplayed());
			break;
		case "extraurbano":
			elmTitle = toolLayout.replaceGenericPathOfElement(driver, titolo, "Biglietto extraurbano");
			assertTrue(elmTitle.isDisplayed());
			break;
		default:
			elmTitle = toolLayout.replaceGenericPathOfElement(driver, titolo, "Biglietto urbano");
			assertTrue(elmTitle.isDisplayed());
			break;
		}
	}
	
	public void verifyImageCard(WebDriver driver, String imageName, double discrepanza) {
		String path = "biglietto/" + imageName;
		LayoutImage.get().verifyImage(driver, path, discrepanza);
	}
	
	public void verifyLayoutCorsa(WebDriver driver, String targetOperation, String corsaTitleInput, String compagniaInput, String fromCityInput, String toCityInput, double discrepanza) {
		WebElement elmTitle;
		WebElement elmImag;
		WebElement elmcompagnia;
		
		switch (targetOperation) {
		case "urbano":
			elmTitle = toolLayout.replaceGenericPathOfElement(driver, titoloCorsa, corsaTitleInput);
			assertTrue(elmTitle.isDisplayed());
			
			elmcompagnia = toolLayout.replaceGenericPathOfElement(driver, compagnia, compagniaInput);
			assertTrue(elmcompagnia.isDisplayed());
			
			verifyImageCard(driver, "idonaDettaglioUrbano", discrepanza);
			break;
			
		case "extraurbano":
			elmTitle = toolLayout.replaceGenericPathOfElement(driver, titoloCorsa, corsaTitleInput);
			assertTrue(elmTitle.isDisplayed());
			
			String textDestination = fromCityInput + " - " + toCityInput;
			elmcompagnia = toolLayout.replaceGenericPathOfElement(driver, compagnia, textDestination);
			assertTrue(elmcompagnia.isDisplayed());
			
			verifyImageCard(driver, "idonaDettaglioExtraurbano", discrepanza);
			break;
		default:
			// Urbano
			elmTitle = toolLayout.replaceGenericPathOfElement(driver, titoloCorsa, corsaTitleInput);
			assertTrue(elmTitle.isDisplayed());
			
			elmcompagnia = toolLayout.replaceGenericPathOfElement(driver, compagnia, compagniaInput);
			assertTrue(elmcompagnia.isDisplayed());
			
			verifyImageCard(driver, "idonaDettaglioUrbano", discrepanza);
			break;
		}
	}
	
	public void verifyLayoutPage(WebDriver driver, String targetOperation, String corsaTitleInput, String compagniaInput, String fromCityInput, String toCityInput, double discrepanza) {
		verifyHeaderPage(targetOperation);
		verifyTitle(driver, targetOperation);
		WaitManager.get().waitShortTime();
		
		assertTrue(finanziariLabel.getElement().isDisplayed());
		assertTrue(data.getElement().isDisplayed());
		
		verifyLayoutCorsa(driver, targetOperation, corsaTitleInput, compagniaInput, fromCityInput, toCityInput, discrepanza);
		WaitManager.get().waitShortTime();
		
		assertTrue(messaggio.getElement().isDisplayed());
		assertTrue(messaggioDinamico.getElement().isDisplayed());
		assertTrue(btnScaricaPDF.getElement().isDisplayed());
	}
	
	public void clickOnDownloadPDF() {
		btnScaricaPDF.getElement().click();
		WaitManager.get().waitMediumTime();
	}

}

