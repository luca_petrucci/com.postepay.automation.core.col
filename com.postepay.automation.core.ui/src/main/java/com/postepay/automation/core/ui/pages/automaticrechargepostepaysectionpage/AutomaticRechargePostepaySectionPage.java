package com.postepay.automation.core.ui.pages.automaticrechargepostepaysectionpage;

import static io.appium.java_client.touch.offset.PointOption.point;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.AutomaticRechargePostepaySectionPageMolecola;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;


public class AutomaticRechargePostepaySectionPage extends Page {
	public static final String NAME="T091";
	

	public AutomaticRechargePostepaySectionPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(AutomaticRechargePostepaySectionPageMolecola.NAME, UiObjectRepo.get().get(AutomaticRechargePostepaySectionPageMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new AutomaticRechargePostepaySectionPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	public void clickOnRecharge() {
		((AutomaticRechargePostepaySectionPageManager)this.manager).clickOnRecharge();
	}
	
	public void clickOnDeleteAutomaticRecharge() {
		((AutomaticRechargePostepaySectionPageManager)this.manager).clickOnDeleteAutomaticRecharge();
	}
	
	public void verifyStaticLayoutPage(String cardNumber) {
		((AutomaticRechargePostepaySectionPageManager)this.manager).verifyStaticLayoutPage(cardNumber);
	}
	
	public void verifyIsPresentSpecificAutomaticRecharge(String recurrencyRechargeName, String recurrencyRechargeDate, String importoInput) {
		((AutomaticRechargePostepaySectionPageManager)this.manager).verifyIsPresentSpecificAutomaticRecharge(recurrencyRechargeName, recurrencyRechargeDate, importoInput);
	}
	
	public void swipOnSpecificRecharge(String recurrencyRechargeName) {
		((AutomaticRechargePostepaySectionPageManager)this.manager).swipOnSpecificRecharge(recurrencyRechargeName);
	}

}

