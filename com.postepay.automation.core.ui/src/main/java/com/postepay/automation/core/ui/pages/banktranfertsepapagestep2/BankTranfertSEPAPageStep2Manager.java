package com.postepay.automation.core.ui.pages.banktranfertsepapagestep2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.postepay.automation.core.ui.molecules.WhomWontSendABonificoSepa;
import com.postepay.automation.core.ui.pages.banktranfertsepachoosecountry.BankTranfertSEPAChooseCountry;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class BankTranfertSEPAPageStep2Manager extends PageManager {

	public BankTranfertSEPAPageStep2Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	LayoutTools toolLayout = new LayoutTools();

	public void verifyHeaderpage(String titlePageInput) {
		WaitManager.get().waitShortTime();
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}

	public void insertIban(String valueIban) {
		Particle title= (Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepa.HEADEROFBANKTRANFERTSEPA);
		title.getElement().click();
		
//		((AppiumDriver<?>) page.getDriver()).hideKeyboard();

		WaitManager.get().waitShortTime();
		
		
		
		Particle iban = (Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepa.IBANINPUTOFBANKTRANFERTSEPA);

		iban.getElement().sendKeys(valueIban);

		WaitManager.get().waitShortTime();

		
	}

	public void insertIntestatario(String valueIntestatario) {
		Particle intestatario = (Particle) UiObjectRepo.get()
				.get(WhomWontSendABonificoSepa.HEADEROFTARGETCARDOFBANKTRANFERTSEPA);

		intestatario.getElement().sendKeys(valueIntestatario);

		WaitManager.get().waitShortTime();

		Particle title= (Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepa.HEADEROFBANKTRANFERTSEPA);
		title.getElement().click();
//		((AppiumDriver<?>) page.getDriver()).hideKeyboard();

		WaitManager.get().waitShortTime();
	}
	

	public void selectCountry() {
		Particle country = (Particle) UiObjectRepo.get()
				.get(WhomWontSendABonificoSepa.HOLDERCOUNTRYRESIDENCEOFBANKTRANFERTSEPA);

		country.getElement().click();

		WaitManager.get().waitShortTime();

		BankTranfertSEPAChooseCountry chooseCountry = (BankTranfertSEPAChooseCountry) PageRepo.get()
				.get(BankTranfertSEPAChooseCountry.NAME);

		chooseCountry.selectCountry();

	}

	public void insertAmout(String valueAmount) {
		Particle amount = (Particle) UiObjectRepo.get()
				.get(WhomWontSendABonificoSepa.AMOUTTOSENTTOTARGETHOLDEROFBANKTRANFERTSEPA);

		amount.getElement().sendKeys(valueAmount);

		WaitManager.get().waitShortTime();

		Particle title= (Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepa.HEADEROFBANKTRANFERTSEPA);
		title.getElement().click();
//		((AppiumDriver<?>) page.getDriver()).hideKeyboard();

		WaitManager.get().waitShortTime();
	}

	public void insertReason(String valueReason) {
		Particle title= (Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepa.HEADEROFBANKTRANFERTSEPA);
		title.getElement().click();

//		((AppiumDriver<?>) page.getDriver()).hideKeyboard();
		
		WaitManager.get().waitShortTime();
		
		
		try {
			UIUtils.mobile().swipe((MobileDriver<?>) page.getDriver(), SCROLL_DIRECTION.DOWN, 500);
		} catch (Exception err) {
		}

		Particle reason = (Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepa.SUMMARYREASONOFBANKTRANFERTSEPA);

		reason.getElement().sendKeys(valueReason);

		WaitManager.get().waitShortTime();

	}

	public void clickOnMoreData() {
		Particle btnMoreData = (Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepa.MOREDATAOFBANKTRANFERTSEPA);

		btnMoreData.getElement().click();

		WaitManager.get().waitShortTime();
	}

	public void clickOnProceed() {
		Particle btn = (Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepa.PROCEEDBUTTONOFBANKTRANFERTSEPA);

		btn.getElement().click();

		WaitManager.get().waitLongTime();
	}

	public void compileAllStandardForm(String valueIban, String valueIntestatario, String valueAmount,
			String valueReason) {

		insertIban(valueIban);

		insertIntestatario(valueIntestatario);

		selectCountry();

		insertAmout(valueAmount);

		insertReason(valueReason);

	}

	public void compileAllExceptOne(String emptyField, String valueIban, String valueIntestatario, String valueAmount,
			String valueReason) {
		switch (emptyField) {
		case "iban":

			insertIntestatario(valueIntestatario);

			selectCountry();

			insertAmout(valueAmount);

			insertReason(valueReason);

			break;
		case "beneficiario":
			insertIban(valueIban);

			selectCountry();

			insertAmout(valueAmount);

			insertReason(valueReason);

			break;
		case "importo":
			insertIban(valueIban);

			insertIntestatario(valueIntestatario);

			selectCountry();

			insertReason(valueReason);

			break;
		case "causale":
			insertIban(valueIban);

			insertIntestatario(valueIntestatario);

			selectCountry();

			insertAmout(valueAmount);

			break;
		default:
			break;
		}
	}
}
