package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodySettingGeneral extends Molecule {
	public static final String NAME="M100";
	
	public static final String NOTIFICATIONARROW="notificationArrow";
public static final String ACCESSANDAUTHORIZATIONARROW="accessAndAuthorizationArrow";
public static final String MANAGEPOSTEIDARROW="managePosteIdArrow";


	public BodySettingGeneral(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

