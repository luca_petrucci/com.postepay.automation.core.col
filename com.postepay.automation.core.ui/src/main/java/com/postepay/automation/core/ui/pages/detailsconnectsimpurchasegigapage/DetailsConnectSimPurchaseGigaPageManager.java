package com.postepay.automation.core.ui.pages.detailsconnectsimpurchasegigapage;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.BodyG2gPurchase;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;
import ui.core.support.waitutil.WaitManager;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class DetailsConnectSimPurchaseGigaPageManager extends PageManager {
	
	Particle btnPurchase = (Particle) UiObjectRepo.get().get(BodyG2gPurchase.PAYBUTTONTOPURCHASEFIVEGIGA);
	Particle amountToPay = (Particle) UiObjectRepo.get().get(BodyG2gPurchase.AMOUNTOFCOSTFOFIVEGIGA);
	
	public DetailsConnectSimPurchaseGigaPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeader(String inputTitle) {
		
		LayoutTools tool=new LayoutTools();
		tool.verifyHeaderPage(inputTitle);
	}
	
	public void verifyLayoutPage() {
		Particle description = (Particle) UiObjectRepo.get().get(BodyG2gPurchase.HEADERCONNECTSIMPURCHASEGIGAPAGE);
		Particle gigaToPurchase = (Particle) UiObjectRepo.get().get(BodyG2gPurchase.NUMBEROFGIGATOPURCHASE);
		Particle payWith = (Particle) UiObjectRepo.get().get(BodyG2gPurchase.PAYWITHCONNECTFORPURCHASEGIGAPAGE);
		
		assertTrue(description.getElement().isDisplayed());
		assertTrue(gigaToPurchase.getElement().isDisplayed());
		assertTrue(payWith.getElement().isDisplayed());
		assertTrue(amountToPay.getElement().isDisplayed());	
	}
	
	public void clickOnPurchaseGiga() {
		btnPurchase.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public double getAmountToPay() {
		
		StringAndNumberOperationTools tool = new StringAndNumberOperationTools();
		double dAmount = tool.convertWithoutEUROlabel(amountToPay.getElement().getText());
		
		return dAmount;
	}
}

