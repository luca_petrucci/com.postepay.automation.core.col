package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyOfNewPosteID extends Molecule {
	public static final String NAME="M034";
	
	public static final String COPYMESSAGESENDG2G="copyMessageSendG2G";
public static final String ENTERPOSTID="enterPostId";
public static final String BUTTONCONFIRM="buttonConfirm";
public static final String TITLEHEADERSENDG2G="titleHeaderSendG2G";
public static final String BTNAUTORIZZAM034 = "btnAutorizzaM034";

	public BodyOfNewPosteID(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

