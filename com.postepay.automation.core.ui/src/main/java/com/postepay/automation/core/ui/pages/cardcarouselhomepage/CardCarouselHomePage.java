package com.postepay.automation.core.ui.pages.cardcarouselhomepage;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.DispositionOfRechargeP2pInvoice;
import com.postepay.automation.core.ui.molecules.CardDetailTotal;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.FooterHomePage;


public class CardCarouselHomePage extends Page {
	public static final String NAME="T005";
	

	public CardCarouselHomePage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(DispositionOfRechargeP2pInvoice.NAME, UiObjectRepo.get().get(DispositionOfRechargeP2pInvoice.NAME), true);
this.addToTemplate(CardDetailTotal.NAME, UiObjectRepo.get().get(CardDetailTotal.NAME), true);
this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
this.addToTemplate(FooterHomePage.NAME, UiObjectRepo.get().get(FooterHomePage.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new CardCarouselHomePageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

}

