package com.postepay.automation.core.ui.pages.urbanpurchasepagestep2;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.molecules.BodyTicketUrbanPurchase;


public class UrbanPurchasePageStep2 extends Page {
	public static final String NAME="T053";
	

	public UrbanPurchasePageStep2(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(BodyTicketUrbanPurchase.NAME, UiObjectRepo.get().get(BodyTicketUrbanPurchase.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new UrbanPurchasePageStep2Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderpage() {
		((UrbanPurchasePageStep2Manager)this.manager).verifyHeaderpage();
	}
	
	public void verifyLayoutPage() {
		((UrbanPurchasePageStep2Manager)this.manager).verifyLayoutPage();
	}
	
	public void clickOnTicketCard() {
		((UrbanPurchasePageStep2Manager)this.manager).clickOnTicketCard();
	}
}

