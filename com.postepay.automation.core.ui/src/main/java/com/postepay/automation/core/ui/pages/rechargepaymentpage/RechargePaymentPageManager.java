package com.postepay.automation.core.ui.pages.rechargepaymentpage;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.postepay.automation.core.ui.molecules.BodyRechargePaymentSection;
import com.postepay.automation.core.ui.molecules.GenericErrorPopUpMolecola;
import com.postepay.automation.core.ui.molecules.TabsPaymentSection;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import org.openqa.selenium.WebDriver;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class RechargePaymentPageManager extends PageManager {

	public RechargePaymentPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	LayoutTools toolLayout = new LayoutTools();
	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}
	
	public void clickOnTabRecharge() {
		Particle tab=(Particle) UiObjectRepo.get().get(TabsPaymentSection.RECHARGETABPAYMENT);
		
		tab.getElement().click();
		
		WaitManager.get().waitShortTime();
	}

	public void clickOnTabPayment() {
		Particle tab=(Particle) UiObjectRepo.get().get(TabsPaymentSection.PAYMENTTABPAYMENT);
		
		tab.getElement().click();
		
		WaitManager.get().waitShortTime();
	}

	public void clickOnTabService() {
		Particle tab=(Particle) UiObjectRepo.get().get(TabsPaymentSection.SERVICETABPAYMENT);
		
		tab.getElement().click();		
		
		WaitManager.get().waitShortTime();
	}
	
//	DEPRECATO
	public void clickOnRechargeOtherPostePay() {
		Particle click=(Particle) UiObjectRepo.get().get(BodyRechargePaymentSection.RECHARGEOTHERCARDPAYMENT);
		
		click.getElement().click();		
		
		WaitManager.get().waitShortTime();
	}

	public void clickOnRechargeTelephonePosteSim() {
		Particle click=(Particle) UiObjectRepo.get().get(BodyRechargePaymentSection.RECHARGESIMPOSTEMOBILE);
		
		click.getElement().click();		
		
		WaitManager.get().waitShortTime();
	}

	public void clickOnRechargeTelephoneVodafone() {
		Particle click=(Particle) UiObjectRepo.get().get(BodyRechargePaymentSection.RECHARGESIMVODAFONE);
		
		UIUtils.mobile().swipe((MobileDriver<?>) page.getDriver(), SCROLL_DIRECTION.DOWN, 500);
		click.getElement().click();		
		
		WaitManager.get().waitShortTime();
	}

	public void clickOnSendP2p() {
		Particle click=(Particle) UiObjectRepo.get().get(BodyRechargePaymentSection.SENDP2PPAYMENT);
		
		click.getElement().click();		
		
		WaitManager.get().waitShortTime();
	}

	public void clickOnRechargeMyPostePay() {
		try {
			Particle atac=(Particle) UiObjectRepo.get().get(GenericErrorPopUpMolecola.ATACBUTTONOKM202);
			UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.visibilityOfElementLocated(atac.getXPath()));
			atac.getElement().click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		Particle click=(Particle) UiObjectRepo.get().get(BodyRechargePaymentSection.RECHARGEMYCARDPAYMENT);
				
		click.getElement().click();		
		
		WaitManager.get().waitShortTime();
	}
	
	public void clickOnCardAutomaticRechargeSection() {
		Particle card=(Particle) UiObjectRepo.get().get(BodyRechargePaymentSection.AUTOMATICRECHARGECARDONPAYMENTSECTION);
		
		try {
			Particle atac=(Particle) UiObjectRepo.get().get(GenericErrorPopUpMolecola.ATACBUTTONOKM202);
			UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.visibilityOfElementLocated(atac.getXPath()));
			atac.getElement().click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		card.getElement().click();
		WaitManager.get().waitShortTime();
	}
	
	public void scrollToOtherSIM(WebDriver driver) {
		
		LayoutTools toolLayout=new LayoutTools();
		Particle posteSIM=(Particle) UiObjectRepo.get().get(BodyRechargePaymentSection.RECHARGESIMPOSTEMOBILE);
				
		UIUtils.mobile().swipeToElement((MobileDriver<?>) driver, UIUtils.SCROLL_DIRECTION.DOWN, 1000,posteSIM.getLocator() , 5);
		
//		toolLayout.scrollWithCoordinate(driver,
//				posteSIM.getElement().getLocation().getX()+200, posteSIM.getElement().getLocation().getX()+200,
//				posteSIM.getElement().getLocation().getY(), posteSIM.getElement().getLocation().getY()-1000);
//		WaitManager.get().waitHighTime();
	}
	
	public void clickOnRechargePostePay() {
		System.out.println(page.getDriver().getPageSource());
		Particle click=(Particle) UiObjectRepo.get().get(BodyRechargePaymentSection.CARDRECHARGEPOSTEPAY);
		
		click.getElement().click();		
		
		WaitManager.get().waitShortTime();
	}
}

