package com.postepay.automation.core.ui.pages.salvadanaiopichercalender;

import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.time.DateUtils;
import org.codehaus.groovy.control.messages.LocatedMessage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.postepay.automation.core.ui.molecules.SalvadanaioVersaSuObiettivoMolecola;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;
import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class SalvadanaioPicherCalenderManagerIOS extends SalvadanaioPicherCalenderManager {

	private By pickers = MobileBy.className("XCUIElementTypePickerWheel");
	public SalvadanaioPicherCalenderManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyLayout() {
		assertTrue(calenderAnno.getElement().isDisplayed());
//		assertTrue(calenderDataEstesa.getElement().isDisplayed());
//		assertTrue(calenderNextMounth.getElement().isDisplayed());
		assertTrue(calenderOK.getElement().isDisplayed());
//		assertTrue(calenderCANCEL.getElement().isDisplayed());
	}
	
	public void setADefaultDay(WebDriver driver) {
//		Calendar.getInstance().add(Calendar.MONTH, 1);
		Date newDate = DateUtils.addMonths(Calendar.getInstance().getTime(), 1);
//		Date dt = Calendar.getInstance().getTime();
		SimpleDateFormat s= new SimpleDateFormat("dd MMMM yyyy", Locale.ITALY);
		String data = s.format(newDate);
		System.out.println(data);
		List<WebElement> picker1Giorno= UIUtils.ui().waitForCondition(page.getDriver(),ExpectedConditions.presenceOfAllElementsLocatedBy(pickers));
        System.out.println(picker1Giorno.size());
        picker1Giorno.get(0).sendKeys(data);
	}
	
	public void setADay(WebDriver driver, String day) {
	}
	
	public void clickOnNextMounth() {
	}
	
	public WebElement getElementDay(WebDriver driver, String day) {
	return null;
	}
	
	public void clickOnCANCEL() {
		Particle versamentoRicorrenteLabel = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTORICORRENTELABEL);
		versamentoRicorrenteLabel.getElement().click();
		WaitManager.get().waitMediumTime();
		UIUtils.ui().mobile().swipe((MobileDriver) page.getDriver(), SCROLL_DIRECTION.DOWN, 200);
	}
}
