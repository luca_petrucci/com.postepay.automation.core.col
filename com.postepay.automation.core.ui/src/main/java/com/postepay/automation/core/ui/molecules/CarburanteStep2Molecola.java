package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class CarburanteStep2Molecola extends Molecule {
	public static final String NAME="M220";
	
	public static final String EROGATOREICON="erogatoreIcon";
	public static final String SELECTEROFATORELABEL="selectErofatoreLabel";
	public static final String EROGATOREAVIBLE="erogatoreAvible";
	public static final String EROGATOREPOINTER="erogatorePointer";
	public static final String EROGATORENUMBER="erogatoreNumber";
	public static final String BUTTONPAGA="buttonPaga";

	public CarburanteStep2Molecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

