package com.postepay.automation.core.ui.pages.bachecapage;

import static io.appium.java_client.MobileCommand.setSettingsCommand;
import static io.appium.java_client.touch.offset.PointOption.point;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.AutomaticRechargePostepaySectionPageMolecola;
import com.postepay.automation.core.ui.molecules.BachecaPageMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;

import io.appium.java_client.CommandExecutionHelper;
import io.appium.java_client.ExecutesMethod;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.android.nativekey.PressesKey;
import test.automation.core.UIUtils;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class BachecaPageManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();
	StringAndNumberOperationTools toolNumber = new StringAndNumberOperationTools();
	
	public BachecaPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderPage() {
//		Particle title=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.HEADERBACHECAPAGE);
//		assertTrue(title.getElement().isDisplayed());
	}
	
	public void searchOperation(String operationToSearch, boolean iOS) {
		if (iOS) {
			CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("useFirstMatch", true));
		}
		Particle search=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.SEARCHFIELDBACHECAPAGE);

		search.getElement().sendKeys(operationToSearch);

		WaitManager.get().waitHighTime();
		
		if (iOS) {
			CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("useFirstMatch", false));
			WaitManager.get().waitShortTime();
		}
	}
	
	public void clickOnBackButton() {
		Particle title=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.BACKBUTTONBACHECAPAGE);

		title.getElement().click();

		WaitManager.get().waitShortTime();
	}
	
	public void clickOnMESSAGGItab() {
		Particle title=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.MESSAGETABBACHECAPAGE);

		title.getElement().click();

		WaitManager.get().waitShortTime();
	}

	public void clickOnNOTIFICHEtab() {
		Particle title=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.NOTIFICHETABBACHECAPAGE);

		title.getElement().click();

		WaitManager.get().waitShortTime();
	}
	
	public void clickOnFilterIcon() {
		Particle title=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.FILTERLINKBACHECAPAGE);

		title.getElement().click();

		WaitManager.get().waitShortTime();
	}
	
	public void verifyThereIsASpecificAutomaticRecharge(String tipeMessage, String dateOfOperation) {
		
//		Particle title=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.TITLENAMEOPERATIONBACHECAPAGE);
		Particle description=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.SUBTITLEDESCRIPTIONPAYEDBACHECAPAGE);
		Particle date=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.DATEPAYEDBACHECAPAGE);
//		Particle amount=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.AMOUNTPAYEDBACHECAPAGE);

		String nuovoDescription=Utility.replacePlaceHolders(description.getLocator(), new Entry("title",tipeMessage));
		
		StringAndNumberOperationTools tool=new StringAndNumberOperationTools();
		String tipeDate=tool.stringDATEforBachecaMessageTab(dateOfOperation);
		String nuovoDate=Utility.replacePlaceHolders(date.getLocator(), new Entry("title",tipeDate));
		
//		System.out.println("Locator Caricato:: "+nuovoLocator);
		
		WebDriver driver=UIUtils.ui().getCurrentDriver();
//		System.out.println("WebDriver Caricato");
		
		WebElement elem1=driver.findElement(By.xpath(nuovoDescription));
		assertTrue(elem1.isDisplayed());
//		System.out.println("Elemento Trovato");
		// Alternativva vatriabile
		WebElement elem2=driver.findElement(By.xpath(nuovoDate));
		assertTrue(elem2.isDisplayed());
		
		int coordinateX; int coordinateY;

		coordinateX=elem2.getLocation().getX();
//		System.out.println("Elemento 1 Trovato "+coordinateX);
		coordinateY=elem1.getLocation().getY();
//		System.out.println("Elemento 3 Trovato "+coordinateY);
		
		// TouchAction e imposto il driver
		TouchAction touchAction = new TouchAction((MobileDriver<?>) page.getDriver());
//		System.out.println("Prova il Tap");
		touchAction.tap(point(coordinateX,coordinateY)).release().perform();
//		System.out.println("Il Tap e fatto!");
		WaitManager.get().waitLongTime();
	}
	
	public WebElement getElementTitle(WebDriver driver, String titleInput) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, name, titleInput);
		assertTrue(elm.isDisplayed());
		return elm;
	}
	
	public WebElement getElementType(WebDriver driver, String subtitleInput) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, type, subtitleInput);
		assertTrue(elm.isDisplayed());
		return elm;
	}
	
	public WebElement getElementDate(WebDriver driver, String dateInput) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, date, dateInput);
		assertTrue(elm.isDisplayed());
		return elm;
	}
	
	Particle type=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.TITLENAMEOPERATIONBACHECAPAGE);
	Particle name=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.SUBTITLEDESCRIPTIONPAYEDBACHECAPAGE);
	Particle amount=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.AMOUNTPAYEDBACHECAPAGE);
	Particle date=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.DATEPAYEDBACHECAPAGE);
	
	public void clickOnSpecificMessage(WebDriver driver, String typeInput, String titleInput, String dateInput) {
		WebElement eType = getElementType(driver, typeInput.toUpperCase());
		WebElement eTitle = getElementTitle(driver, titleInput);
		WebElement eDate = getElementDate(driver, dateInput.toUpperCase());
		
		// Coordinate X - Media tra tipo e data
		int xType = eType.getLocation().getX();
		int xDate = eDate.getLocation().getX();
		int elmX = (xType + xDate) / 2;
		
		// Coordinate Y - Dal titolo del messaggio
		int elmY = eTitle.getLocation().getY();
		
		// Click sulle coordinate
		TouchAction touchAction = new TouchAction((MobileDriver<?>) driver);
		touchAction.longPress(point(elmX,elmY)).release().perform();
		
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnSpecificNotification(WebDriver driver, String typeInput, String titleInput, String dateInput) {
		WebElement eType = getElementType(driver, typeInput);
		WebElement eTitle = getElementTitle(driver, titleInput);
		WebElement eDate = getElementDate(driver, dateInput);
		
		// Coordinate X - Media tra tipo e data
		int xType = eType.getLocation().getX();
		int xDate = eDate.getLocation().getX();
		int elmX = (xType + xDate) / 2;
		
		// Coordinate Y - Dal titolo del messaggio
		int elmY = eTitle.getLocation().getY();
		
		// Click sulle coordinate
		TouchAction touchAction = new TouchAction((MobileDriver<?>) driver);
		touchAction.longPress(point(elmX,elmY)).release().perform();
		
		WaitManager.get().waitMediumTime();
	}
	
	public void verifyResultList(String message) {
		Particle search=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.SEARCHFIELDBACHECAPAGE);
		search.getElement().click();
		search.getElement().sendKeys(message);
		WaitManager.get().waitShortTime();
		((PressesKey) page.getDriver()).pressKey(new KeyEvent(AndroidKey.SEARCH));
		Particle description=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.SUBTITLEDESCRIPTIONPAYEDBACHECAPAGE);
		String nuovoDescription=Utility.replacePlaceHolders(description.getLocator(), new Entry("title",message));
		WebElement elm = page.getDriver().findElement(By.xpath(nuovoDescription));
		assertTrue(elm.isDisplayed());
		WaitManager.get().waitMediumTime();
	}
	
	public void clickPrimoElemento(String titoloTile)
	{
		Particle primoEle=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.TITOLODINAMICONOTIFICAAUTORIZZATIVA);
		String nuovoLocator=Utility.replacePlaceHolders(primoEle.getLocator(), new Entry("title", titoloTile));
		page.getDriver().findElement(By.xpath(nuovoLocator)).click();
		WaitManager.get().waitMediumTime();
	}
	
	public void controlloFiltroData(String dataInput)
	{
		Particle lista=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.ROWDATA);
		List<WebElement> elementi=lista.getListOfElements();
		for(WebElement e:elementi)
		{
			String data=e.getText();
			assertTrue("Controllo Data lista bacheca"+data,data.equals(dataInput));
		}
	}
	
	public int sendAndVerify(String keySearch, boolean isIOS) {
		int result; // 0 se la lista è vuota - 1 se la ricerca è corretta
		boolean[] isContained = null; // true se la keySearch è contenuta nella lista dei risultati
		
		if(isIOS) {
//			CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("useFirstMatch", true));
			CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("actionAcknowledgmentTimeout", true));
		}
		Particle search=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.SEARCHFIELDBACHECAPAGE);

		search.getElement().sendKeys(keySearch);
		if(!isIOS) {
			WaitManager.get().waitShortTime();
			((PressesKey) page.getDriver()).pressKey(new KeyEvent(AndroidKey.ENTER));
		}
		WaitManager.get().waitHighTime();
		
		Particle xPath=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.ROWDESCRIPTION);
		List<WebElement> resultsList = page.getDriver().findElements(xPath.getXPath());
		isContained = new boolean[resultsList.size()];
		
		for (int i = 0; i < resultsList.size(); i++) {
			WebElement el = resultsList.get(i);
			System.out.println(i + " " + el.getText());
			if (el.getText().toLowerCase().trim().contains(keySearch.toLowerCase().trim())) {
				isContained[i] = true;	
			} else {
				isContained[i] = false;
			}
		}
		
//		Controllo su risultati
		boolean isAllTrue = areAllTrue(isContained);
		
		if((isAllTrue == true) && (resultsList.size() > 0)) {
			result = 1;
		}else {
			result = 0;
		}
		return result;
	}
	
	public static boolean areAllTrue(boolean[] array)
	{
	    for(boolean b : array) if(!b) return false;
	    return true;
	}
	
	
	public List<String> verifyAndOpenTarget(String description) {
		List<String> listResult = new ArrayList<String>();
		
		Particle container=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.CONTAINER);
		Particle titolo=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.ROWTITLE);
		Particle descrizione=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.ROWDESCRIPTION);
		Particle data=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.ROWDATA);
		
		List<WebElement> listRow = page.getDriver().findElements(container.getXPath());
		
		System.out.println("listRow --> "+listRow);
		
		for (int i = 0; i < listRow.size(); i++) {
			WebElement el = listRow.get(i);
			System.out.println("elRow --> "+el);
			WebElement elDescription = el.findElement(descrizione.getXPath());
			
			if(elDescription.getText().toUpperCase().trim().contains(description.toUpperCase())) {
				WebElement elTitlo = el.findElement(titolo.getXPath());
				WebElement elData = el.findElement(data.getXPath());
				
				System.out.println("elTitlo --> "+elTitlo.getText());
				System.out.println("elDescription --> "+elDescription.getText());
				
				String ti = elTitlo.getText();
				listResult.add(0, ti);
				String de = elDescription.getText();
				listResult.add(1, de);
				String da = elData.getText();
				listResult.add(2, da);
				
				System.out.println("listResult --> "+listResult);
				
				el.click();
				WaitManager.get().waitShortTime();
				return listResult;
			}
		}
		return listResult;
	}
	
	public void reserch(String message) {
		Particle search=(Particle) UiObjectRepo.get().get(BachecaPageMolecola.SEARCHFIELDBACHECAPAGE);
		search.getElement().click();
		search.getElement().sendKeys(message);
		WaitManager.get().waitShortTime();
		((PressesKey) page.getDriver()).pressKey(new KeyEvent(AndroidKey.SEARCH));
	}
	
}

