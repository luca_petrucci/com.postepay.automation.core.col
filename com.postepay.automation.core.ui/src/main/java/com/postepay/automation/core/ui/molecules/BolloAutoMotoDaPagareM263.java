package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BolloAutoMotoDaPagareM263 extends Molecule {
	public static final String NAME="M263";
	
	public static final String IMPORTOM263="importoM263";
public static final String TARGAM263="targaM263";
public static final String TITLEM263="titleM263";
public static final String VEICOLOM263="veicoloM263";
public static final String PAGACONM263="pagaConM263";
public static final String CONTINUAM263="continuaM263";


	public BolloAutoMotoDaPagareM263(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

