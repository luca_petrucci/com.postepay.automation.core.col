package com.postepay.automation.core.ui.pages.rechargemypostepaypagestep6;

import static org.junit.Assert.assertTrue;

import java.text.MessageFormat;
import java.util.Properties;

import org.junit.Assert;

import com.postepay.automation.core.ui.molecules.ThankYouPageForTelephoneRecharge;
import com.postepay.automation.core.ui.molecules.ThankYouPageGeneric;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.CoreUtility;
import utility.SoftAssertion;

public class RechargeMyPostepayPageStep6Manager extends PageManager {

	private Properties lang;

	public RechargeMyPostepayPageStep6Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void clickOnCloseButton() {
		
		Particle btn=(Particle) UiObjectRepo.get().get(ThankYouPageForTelephoneRecharge.THANKYOUCLOSEBUTTONRECHARGEMYPP);
		
		btn.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
	
	public void verifyLayout() {
		Particle img=(Particle) UiObjectRepo.get().get(ThankYouPageForTelephoneRecharge.THANKYOUIMAGERECHARGEMYPP);
		Particle title=(Particle) UiObjectRepo.get().get(ThankYouPageForTelephoneRecharge.THANKYOUTITLERECHARGEMYPP);
		Particle subTitle=(Particle) UiObjectRepo.get().get(ThankYouPageForTelephoneRecharge.THANKYOUCOPYRECHARGEMYPP);
		Particle btn=(Particle) UiObjectRepo.get().get(ThankYouPageForTelephoneRecharge.THANKYOUCLOSEBUTTONRECHARGEMYPP);

//		// Imagine
//		assertTrue(img.getElement().isDisplayed());
//		// Title
//		assertTrue(title.getElement().getText().contains("Operazione confermata"));
//		// Copy
//		assertTrue(subTitle.getElement().isDisplayed());
//		SoftAssertion.get().getAssertions().assertThat
//		(subTitle.getElement().getText()).withFailMessage("Descrizione errata").contains
//		("Grazie per aver effettuato la ricarica");
//		// Bottone
//		assertTrue(btn.getElement().isDisplayed());
//		
//		WaitManager.get().waitShortTime();
		
		CoreUtility.visibilityOfElement(page.getDriver(), img, 10);
		
		String text=CoreUtility.visibilityOfElement(page.getDriver(), title, 10).getText().trim();

		//APPENA LA TYP VERRà CAMBIATA IN PRODUZIONE CANCELLARE TUTTO L'IF E SOSTITUIRLO CON LA RIGA 69 
		if (lang.getProperty("typTitleRicaricaPPNew").equalsIgnoreCase(text)) {	

			Assert.assertTrue(MessageFormat.format("Copy TYP Ricarica PP Errato {0}, Expected {1}", text,lang.getProperty("typTitleRicaricaPPNew")), lang.getProperty("typTitleRicaricaPPNew").equalsIgnoreCase(text));
		
		}else if(lang.getProperty("typTitleRicaricaPPNew2").equalsIgnoreCase(text)){
		
			Assert.assertTrue(MessageFormat.format("Copy TYP Ricarica PP Errato {0}, Expected {1}", text,lang.getProperty("typTitleRicaricaPPNew2")), lang.getProperty("typTitleRicaricaPPNew2").equalsIgnoreCase(text));
		
		}
		
		try {
			CoreUtility.visibilityOfElement(page.getDriver(), subTitle, 5);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		CoreUtility.visibilityOfElement(page.getDriver(), btn, 10);
		
		WaitManager.get().waitShortTime();
	}

	public void verifyLayout(Properties language) 
	{
		this.lang=language;
		this.verifyLayout();
	}
}

