package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyUsefulNumber extends Molecule {
	public static final String NAME="M120";
	
	public static final String INFORMATIONANDASSISTENCEONSERVICEFROMITALY="informationAndAssistenceOnServiceFromItaly";
public static final String NUMBERFROMOTHERCOUNTRY="numberFromOtherCountry";
public static final String INFORMATIONANDASSISTENCEONSERVICEFROMOTHERCOUNTRY="informationAndAssistenceOnServiceFromOtherCountry";
public static final String INFORMATIONANDASSISTENCEONSIMFROMOTHERCOUNTRY="informationAndAssistenceOnSIMFromOtherCountry";
public static final String INFORMATIONANDASSISTENCEONSIM="informationAndAssistenceOnSIM";
public static final String NUMBERFROMITALY="numberFromItaly";
public static final String THEFTCARDSECTION="theftCardSection";
public static final String INFORMATIONANDASSISTENCEONSIMFROMITALY="informationAndAssistenceOnSIMFromItaly";
public static final String INFORMATIONANDASSISTENCEONSERVICE="informationAndAssistenceOnService";


	public BodyUsefulNumber(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

