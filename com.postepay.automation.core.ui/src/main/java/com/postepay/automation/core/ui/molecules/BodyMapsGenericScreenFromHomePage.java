package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyMapsGenericScreenFromHomePage extends Molecule {
	public static final String NAME="M111";
	
	public static final String ATMLABEL="atmLabel";
	public static final String MAPSLAYOUTTEXTURE="mapsLayoutTexture";
	public static final String ICONAPROFILOMAPPE="iconaProfiloMaps";
	public static final String TABMAPS="tabMaps";
	public static final String TABLIST="tabList";
	public static final String POSTALOFFICELABEL="postalOfficeLabel";
	public static final String BUTTONSCONTIDETTAGLIO="buttonScontiDettaglio";
	public static final String DETTAGLIOTITOLO= "dettaglioTitolo";
	public static final String DETTAGLIOCARDACCREDITO = "dettaglioCardAccreditati";
	public static final String DETTAGLIOCARDACCREDITOdESCRIZIONE  = "dettaglioCardAccreditatiDescrizione";
	public static final String DETTAGLIOLABEL = "dettaglioAccreditatiLabel";
	public static final String MAPSLABELCODICEQR = "mapsLabelCodiceQr";
	public static final String SOSTALABEL = "sostaLabel";
	public static final String CARBURANTELABEL = "carburanteLabel";
	public static final String CHIPLABEL = "chipLabel";
	public static final String LISTAPINMAPPE = "listaPinMappe";
	public static final String CARDQRCODE="cardQRCode";
	public static final String CARDUFFICIPOSTALI="cardUfficiPostali";
	public static final String CARDATM="cardATM";
	public static final String CARDCARBURANTE= "cardCarburante";
	public static final String CARDSCONTI= "cardSconti";
	public static final String CARDSOSTA="cardSosta";
	public static final String FILTERMAPS="filterMaps";
	public static final String IMGSCONTO="imgSconto";
	public static final String TITLESCONTIDAACCREDITARE="titleScontiDaAccreditare";
	public static final String SUBTITLESCONTI="subTitleSconti";
	public static final String BTNAPRIDETTAGLIO="btnApriDettaglio";
	public static final String IMGMARKERNEGOZIO="imgMarkerNegozio";
	public static final String SEARCHFIELDMAP="fieldSearchMap";
	public static final String CARDRITIROINNEGOZIO="cardRitiroNegozioMappe";
	public static final String CARDCASHBACK="cardCashbackMappe";
	public static final String CARDADOMICILIO="cardADomicilioMappe";
	public static final String CARDABBIGLIAMENTO="cardAbbigliamento";
	
	public BodyMapsGenericScreenFromHomePage(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

