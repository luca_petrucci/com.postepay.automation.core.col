package com.postepay.automation.core.ui.pages.communitycardonepage;

import static io.appium.java_client.touch.offset.PointOption.point;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import com.postepay.automation.core.ui.molecules.CardsOfG2gCommunity;
import com.postepay.automation.core.ui.molecules.CardsOfP2pCommunity;
import com.postepay.automation.core.ui.pages.communitycardtwopage.CommunityCardTwoPage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class CommunityCardOnePageManager extends PageManager {

	LayoutTools toolLayout= new LayoutTools();

	public CommunityCardOnePageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void clickOnSendP2p() {
		
		Particle sendP2p=(Particle) UiObjectRepo.get().get(CardsOfP2pCommunity.SENDP2PCOMMUNITY);
		
		sendP2p.getElement().click();
	}
	
	public void clickOnRequestP2p() {
		
		Particle requestP2p=(Particle) UiObjectRepo.get().get(CardsOfP2pCommunity.REQUESTP2PCOMMUNITY);
		
		requestP2p.getElement().click();
		
		try {Thread.sleep(1000);} catch (Exception err )  {}
	}
	
	public void verifyHeaderPage() throws Error 	{		
		toolLayout.verifyHeaderPage("Community");
	}
	
	public void verifyGoalAndWinCard() {
		
		Particle title=(Particle) UiObjectRepo.get().get(CardsOfP2pCommunity.HEADERGAMEANDWIN);
		Particle text=(Particle) UiObjectRepo.get().get(CardsOfP2pCommunity.TEXTGAMEANDWIN);
		
		title.getElement().isDisplayed();
		text.getElement().isDisplayed();	
		
		try {Thread.sleep(1000);} catch (Exception err )  {}
	}
	
	public void swipOnG2g() {
		Particle requestP2p=(Particle) UiObjectRepo.get().get(CardsOfP2pCommunity.REQUESTP2PCOMMUNITY);
		
		Point p = requestP2p.getElement().getLocation();
		
		// TouchAction e imposto il driver
		TouchAction touchAction = new TouchAction((AppiumDriver<?>) page.getDriver());
//		System.out.println("Prova lo Swipe");
		touchAction.longPress(point(p.x,p.y)).moveTo(point(p.x+500,p.y)).release().perform();
//		System.out.println("Lo Swipe e fatto!");
		WaitManager.get().waitLongTime();
	}
	
	public WebElement lastRootTab(WebDriver driver) {
		List<WebElement> rootTab = (List<WebElement>) driver.findElements(By.xpath("//*[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_header']"));
		int last = rootTab.size() - 1;
		WebElement lastRoot = rootTab.get(last);
		return lastRoot;
	}

	public void swipOnG2g(WebDriver driver) {
		WebElement lastRoot = lastRootTab(driver);
		toolLayout.scrollWithCoordinate(driver,
				lastRoot.getLocation().getX(), lastRoot.getLocation().getX()-500,
				lastRoot.getLocation().getY(), lastRoot.getLocation().getY());
		WaitManager.get().waitHighTime();
	}
}

