package com.postepay.automation.core.ui.pages.productsstandardpage;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.GenericOverviewsCardDetails;
import com.postepay.automation.core.ui.molecules.StandardCardFilterAndMoviment;
import com.postepay.automation.core.ui.molecules.DettaglioMovimentiCard;
import com.postepay.automation.core.ui.molecules.FooterHomePage;


public class ProductsStandardPage extends Page {
	public static final String NAME="T029";
	

	public ProductsStandardPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(GenericOverviewsCardDetails.NAME, UiObjectRepo.get().get(GenericOverviewsCardDetails.NAME), true);
		this.addToTemplate(StandardCardFilterAndMoviment.NAME, UiObjectRepo.get().get(StandardCardFilterAndMoviment.NAME), true);
		this.addToTemplate(FooterHomePage.NAME, UiObjectRepo.get().get(FooterHomePage.NAME), true);
		this.addToTemplate(DettaglioMovimentiCard.NAME, UiObjectRepo.get().get(DettaglioMovimentiCard.NAME), true);
	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new ProductsStandardPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new ProductsStandardPageManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void clickOnCard() {
		((ProductsStandardPageManager)this.manager).clickOnCard();
	}
	
	public void verifyPresenceOfG2GCard() {
		
		((ProductsStandardPageManager)this.manager).verifyPresenceOfG2GCard();
	}
	
	public void verifyLayoutPage() {
		((ProductsStandardPageManager)this.manager).verifyLayoutPage();
	}
	
	public void verifyHeader(String inputTitle) {
		((ProductsStandardPageManager)this.manager).verifyHeader(inputTitle);
	}
	
	public void clickOnG2GLabel() {
		((ProductsStandardPageManager)this.manager).clickOnG2GLabel();
	}
	
	public void searchMoviment(String movimento) {
		((ProductsStandardPageManager)this.manager).searchMoviment(movimento);
	}
	
	public void clickPrimoMovimento(String titoloMovimento, String importoMovimento)
	{
		((ProductsStandardPageManager)this.manager).clickPrimoMovimento(titoloMovimento, importoMovimento);
	}

	public void verifyDettaglioMovimenti(String titoloMovimento, String importoMovimento, String dataMovimentoValuta, String dataMovimentoContabile, String cardPostepay, String copyCardPostepay)
	{
		((ProductsStandardPageManager)this.manager).verifyDettaglioMovimenti(titoloMovimento, importoMovimento, dataMovimentoValuta, dataMovimentoContabile, cardPostepay, copyCardPostepay);
	}
}

