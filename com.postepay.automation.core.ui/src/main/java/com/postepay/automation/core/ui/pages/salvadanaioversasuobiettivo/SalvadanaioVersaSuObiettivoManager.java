package com.postepay.automation.core.ui.pages.salvadanaioversasuobiettivo;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.SalvadanaioAccantonamentoDettaglioMolecola;
import com.postepay.automation.core.ui.molecules.SalvadanaioVersaSuObiettivoMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class SalvadanaioVersaSuObiettivoManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();

	Particle iconaYellow = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTOIMAGEYELLOW);
	Particle dettaglioNome = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTOTITOLO);
	Particle versamentoTestoVersameto = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTOTESTO);
	Particle versamentoLimiteMassimo = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTOLIMITEMASSIMO);
	Particle versamentoMeno = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTOMENO);
	Particle versamentoPiu = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTOPIU);
	Particle versamentoAmount = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTOAMOUNT);
	Particle versamentoRicorrenteLabel = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTORICORRENTELABEL);
	Particle versamentoRicorrenteToggle = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTORICORRENTETOGGLE);
	Particle versamentoSuggestionImage = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTOSUGGESTIONIMAGE);
	Particle versamentoSuggestionText = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTOSUGGESTIONTEXT);
	Particle versamentoButton = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTOBUTTON);

	public SalvadanaioVersaSuObiettivoManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}

	public WebElement getElementName(WebDriver driver, String nameInput) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, dettaglioNome, nameInput);
		System.out.println(elm.getText());
		assertTrue(elm.isEnabled());
		return elm;
	}
	
	public WebElement getElementTextOnCard(WebDriver driver, String isRicorrente) {
		WebElement elm; 
		String text;
		if (isRicorrente.equals("true")) {
			text = "Il versamento ricorrente non è attivabile in quanto l’obiettivo è scaduto/ è già presente un versamento ricorrente";
		}else {
			text = "Il versamento ricorrente ti permette di versare periodicamente un importo sul tuo obiettivo";
		}
		elm = toolLayout.replaceGenericPathOfElement(driver, versamentoSuggestionText, text);
		assertTrue(elm.isDisplayed());
		return elm;
	}
	
	public void verifyImage(WebDriver driver, String imageNameInput, double discrepanza) {
		String imageName = "salvadanaio/" + imageNameInput;
		if (discrepanza > 0) 
		LayoutImage.get().verifyImage(driver, imageName, discrepanza);
	}
	
	public void verifyLayoutPage(WebDriver driver, String nameInput, String typeObiettivoInput, String isRicorrente, double discrepanza) {
		assertTrue(iconaYellow.getElement().isDisplayed());
		getElementName(driver, nameInput);
		assertTrue(versamentoTestoVersameto.getElement().isDisplayed());
		assertTrue(versamentoLimiteMassimo.getElement().isDisplayed());
		assertTrue(versamentoMeno.getElement().isDisplayed());
		assertTrue(versamentoPiu.getElement().isDisplayed());
		assertTrue(versamentoAmount.getElement().isDisplayed());
		assertTrue(versamentoRicorrenteLabel.getElement().isDisplayed());
		assertTrue(versamentoRicorrenteToggle.getElement().isDisplayed());
		assertTrue(versamentoSuggestionImage.getElement().isDisplayed());
		getElementTextOnCard(driver, isRicorrente);
		assertTrue(versamentoButton.getElement().isDisplayed());
		
		switch (typeObiettivoInput) {
		case "viaggi":
			System.out.println("Vedifica del tipo immagini - VIAGGI");
			verifyImage(driver, "viaggi_icon_versa", discrepanza);
			break;
		case "salute":
			System.out.println("Vedifica del tipo immagini - SALUTE E BENESSERE");
			verifyImage(driver, "salute_icon_versa", discrepanza);
			break;
		case "risparmio":
			System.out.println("Vedifica del tipo immagini - RISPARMIO");
			verifyImage(driver, "risparmio_icon_versa", discrepanza);
			break;
		default:
			System.out.println("Errore nel tipo immagini");
			break;
		}
		System.out.println("Vedifica del tipo immagini - ICONA VERSA IN CASSAFORTE");
		verifyImage(driver, "cassaforte_versa", discrepanza);
		System.out.println("Vedifica del tipo immagini - ICONA MENO");
		verifyImage(driver, "versa_minus", discrepanza);
		System.out.println("Vedifica del tipo immagini - ICONA PIU");
		verifyImage(driver, "versa_plus", discrepanza);
		System.out.println("Vedifica delle immagini - FINITA");
		
	}
	
	public void clickOnContinua() {
		versamentoButton.getElement().click();
		WaitManager.get().waitLongTime();
	}
	
	public void clickOnVersamentoRicorrente() {
		System.out.println("Vedifica e click su TOGGLE");
		versamentoRicorrenteToggle.getElement().click();
		WaitManager.get().waitLongTime();
		
	}
}

