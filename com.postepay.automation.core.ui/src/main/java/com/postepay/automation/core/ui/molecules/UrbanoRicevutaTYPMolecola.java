package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class UrbanoRicevutaTYPMolecola extends Molecule {
	public static final String NAME="M224";
	
	public static final String URBANORICEVUTADATA="urbanoRicevutaData";
public static final String URBANORICEVUTAPOSTEPAY="urbanoRicevutaPostepay";
public static final String URBANORICEVUTACOMPAGNIA="urbanoRicevutaCompagnia";
public static final String URBANORICEVUTAIMAGE="urbanoRicevutaImage";
public static final String URBANORICEVUTATIPOBIGLIETTOLABEL="urbanoRicevutaTipoBigliettoLabel";
public static final String URBANORICEVUTABOTTONEMIEIACQUISTI="urbanoRicevutaBottoneMieiAcquisti";
public static final String URBANORICEVUTADATALABEL="urbanoRicevutaDataLabel";
public static final String URBANORICEVUTATIPOBIGLIETTO="urbanoRicevutaTipoBiglietto";
public static final String URBANORICEVUTACOMPAGNIALABEL="urbanoRicevutaCompagniaLabel";
public static final String URBANORICEVUTATOTALELABEL="urbanoRicevutaTotaleLabel";
public static final String URBANORICEVUTAPOSTEPAYLABEL="urbanoRicevutaPostepayLabel";
public static final String URBANORICEVUTATOTALE="urbanoRicevutaTotale";


	public UrbanoRicevutaTYPMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

