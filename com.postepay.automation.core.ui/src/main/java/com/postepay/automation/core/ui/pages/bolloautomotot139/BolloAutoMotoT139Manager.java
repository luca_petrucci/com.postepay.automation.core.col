package com.postepay.automation.core.ui.pages.bolloautomotot139;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;

import com.postepay.automation.core.ui.molecules.BolloAutoMotoCalcoloM262;
import com.postepay.automation.core.ui.molecules.BolloAutoMotoDaPagareM263;
import com.postepay.automation.core.ui.molecules.BolloAutoMotoRiepilogoM264;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;

import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class BolloAutoMotoT139Manager extends PageManager {

	public BolloAutoMotoT139Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderTitle(String title)
	{
		LayoutTools tool = new LayoutTools();
		tool.verifyHeaderPage(title);
//		Particle titleP = (Particle) UiObjectRepo.get().get(HeaderGenericAllPage.TITLEGENERICWITHTITLE);
//		String nuovoLocator= Utility.replacePlaceHolders(titleP.getLocator(), new Entry("title", title));
//		assertTrue(page.getDriver().findElement(By.xpath(nuovoLocator)).isDisplayed());
	}
	
	public void inserisciRegione(String regioneInput)
	{
		Particle regione = (Particle) UiObjectRepo.get().get(BolloAutoMotoCalcoloM262.REGIONEM262);
		regione.getElement().click();
		WaitManager.get().waitShortTime();
		Particle regioneSelect = (Particle) UiObjectRepo.get().get(BolloAutoMotoCalcoloM262.REGIONESELECTM262);
		String nuovoLocator= Utility.replacePlaceHolders(regioneSelect.getLocator(), new Entry("title", regioneInput));
		page.getDriver().findElement(By.xpath(nuovoLocator)).click();
		WaitManager.get().waitShortTime();
	}
	
	public void inserisciVeicolo(String veicoloInput)
	{
		Particle veicolo = (Particle) UiObjectRepo.get().get(BolloAutoMotoCalcoloM262.VEICOLOM262);
		veicolo.getElement().click();
		WaitManager.get().waitShortTime();
		Particle regioneSelect = (Particle) UiObjectRepo.get().get(BolloAutoMotoCalcoloM262.VEICOLOSELECTM262);
		String nuovoLocator= Utility.replacePlaceHolders(regioneSelect.getLocator(), new Entry("title", veicoloInput));
		page.getDriver().findElement(By.xpath(nuovoLocator)).click();
		WaitManager.get().waitShortTime();
	}
	
	public void inserisciTarga(String targaInput)
	{
		Particle targa = (Particle) UiObjectRepo.get().get(BolloAutoMotoCalcoloM262.TARGAM262);
		targa.getElement().sendKeys(targaInput);
		WaitManager.get().waitShortTime();
	}
	
	public void verifyLayoutCalcolaM262(String copyInput, String annualitaInput, String btnCalcolaInput, String regioneInput, String targaInput, String veicoloInput)
	{
		Particle regione = (Particle) UiObjectRepo.get().get(BolloAutoMotoCalcoloM262.REGIONEM262);
		Particle veicolo = (Particle) UiObjectRepo.get().get(BolloAutoMotoCalcoloM262.VEICOLOM262);
		Particle targa = (Particle) UiObjectRepo.get().get(BolloAutoMotoCalcoloM262.TARGAM262);
		Particle copy = (Particle) UiObjectRepo.get().get(BolloAutoMotoCalcoloM262.COPYTEXTM262);
		Particle annualita = (Particle) UiObjectRepo.get().get(BolloAutoMotoCalcoloM262.ANNUALITAPREGRESSAM262);
		Particle btnCalcola = (Particle) UiObjectRepo.get().get(BolloAutoMotoCalcoloM262.CALCOLABTNM262);
		assertTrue("Copy errato AR: "+  copy.getElement().getText() + ", ER: "+ copyInput, copy.getElement().getText().equals(copyInput));
		assertTrue("Regione errata AR: "+  regione.getElement().getText() + ", ER: "+ regioneInput, regione.getElement().getText().equals(regioneInput));
		assertTrue("Targa errata AR: "+  targa.getElement().getText() + ", ER: "+targaInput, targa.getElement().getText().equals(targaInput));
		assertTrue("Veicolo errato AR: "+  veicolo.getElement().getText() + ", ER: "+ veicoloInput, veicolo.getElement().getText().equals(veicoloInput));
		assertTrue("Annualita errata AR: "+  annualita.getElement().getText() + ", ER: "+ annualitaInput, annualita.getElement().getText().equals(annualitaInput));
		assertTrue("Pulsante Calcola errato AR: "+  btnCalcola.getElement().getText() + ", ER: "+ btnCalcolaInput, btnCalcola.getElement().getText().equals(btnCalcolaInput));
	}
	
	public void clickBtnCalcolaM262()
	{
		Particle btnCalcola = (Particle) UiObjectRepo.get().get(BolloAutoMotoCalcoloM262.CALCOLABTNM262);
		btnCalcola.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void verifyLayoutCalcolaM263(String targaInput, String veicoloInput, String titleInput, String btnContinuaInput, String pagaConInput)
	{
		Particle title = (Particle) UiObjectRepo.get().get(BolloAutoMotoDaPagareM263.TITLEM263);
		Particle importo = (Particle) UiObjectRepo.get().get(BolloAutoMotoDaPagareM263.IMPORTOM263);
		Particle targa = (Particle) UiObjectRepo.get().get(BolloAutoMotoDaPagareM263.TARGAM263);
		Particle veicolo = (Particle) UiObjectRepo.get().get(BolloAutoMotoDaPagareM263.VEICOLOM263);
		Particle pagaCon = (Particle) UiObjectRepo.get().get(BolloAutoMotoDaPagareM263.PAGACONM263);
		Particle btnContinua = (Particle) UiObjectRepo.get().get(BolloAutoMotoDaPagareM263.CONTINUAM263);
		assertTrue("Titolo errato AR: "+  title.getElement().getText() + ", ER: "+ titleInput, title.getElement().getText().equals(titleInput));
		assertTrue("Importo isDisplayed? AR: "+  importo.getElement().isDisplayed(), importo.getElement().isDisplayed());
		assertTrue("Targa errata AR: "+  targa.getElement().getText() + ", ER: "+ targaInput, targa.getElement().getText().equals(targaInput));
		assertTrue("Veicolo errato AR: "+  veicolo.getElement().getText() + ", ER: "+ veicoloInput, veicolo.getElement().getText().equals(veicoloInput));
		assertTrue("Paga Con errato? AR: "+  pagaCon.getElement().getText()+", ER: "+  pagaConInput, pagaCon.getElement().getText().contains(pagaConInput));
		assertTrue("Pulsante Continua errato AR: "+  btnContinua.getElement().getText() + ", ER: "+ btnContinuaInput, btnContinua.getElement().getText().equals(btnContinuaInput));
	}
	
	public String getImportoM263()
	{
		Particle importo = (Particle) UiObjectRepo.get().get(BolloAutoMotoDaPagareM263.IMPORTOM263);
		return importo.getElement().getText();
	}
	
	public void clickBtnContinuaM263()
	{
		Particle btnContinua = (Particle) UiObjectRepo.get().get(BolloAutoMotoDaPagareM263.CONTINUAM263);
		btnContinua.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void verifyLayoutSummaryM264(String titleInput, String regioneInput, String targaInput, String importoInput, String commissioneInput, String pagaConInput, String btnAnnullaInput)
	{
		StringAndNumberOperationTools tools = new  StringAndNumberOperationTools();
		Particle title = (Particle) UiObjectRepo.get().get(BolloAutoMotoRiepilogoM264.TITLEM264);
		Particle regione = (Particle) UiObjectRepo.get().get(BolloAutoMotoRiepilogoM264.REGIONEM264);
		Particle targa = (Particle) UiObjectRepo.get().get(BolloAutoMotoRiepilogoM264.TARGAM264);
		Particle importo = (Particle) UiObjectRepo.get().get(BolloAutoMotoRiepilogoM264.IMPORTOM264);
		Particle commissione = (Particle) UiObjectRepo.get().get(BolloAutoMotoRiepilogoM264.COMMISSIONEM264);
		Particle pagaCon = (Particle) UiObjectRepo.get().get(BolloAutoMotoRiepilogoM264.PAGACONM264);
		Particle btnPaga = (Particle) UiObjectRepo.get().get(BolloAutoMotoRiepilogoM264.PAGABTNM264);
		Particle btnAnnulla = (Particle) UiObjectRepo.get().get(BolloAutoMotoRiepilogoM264.ANNULLAM264);
		assertTrue("Titolo errato AR: "+  title.getElement().getText() + ", ER: "+ titleInput, title.getElement().getText().equals(titleInput));
		assertTrue("Regione errata AR: "+  regione.getElement().getText() + ", ER: "+ regioneInput, regione.getElement().getText().equals(regioneInput.toUpperCase()));
		assertTrue("Targa errata AR: "+  targa.getElement().getText() + ", ER: "+ targaInput, targa.getElement().getText().equals(targaInput.toUpperCase()));
		assertTrue("Importo errato AR: "+  importo.getElement().getText() + ", ER: "+ importoInput, importo.getElement().getText().equals(importoInput));
		assertTrue("Commissione errata AR: "+  commissione.getElement().getText() + ", ER: "+ commissioneInput, commissione.getElement().getText().equals(commissioneInput));
		assertTrue("pagaCon errato AR: "+ pagaCon.getElement().getText() + ", ER: "+ pagaConInput, pagaCon.getElement().getText().contains(pagaConInput));
		double eImporto=tools.convertWithoutEUROlabel(importoInput) +1.0;
		String nImporto= "PAGA € "+eImporto+"0";
		nImporto= nImporto.replace(".", ",");
		assertTrue("Pulsante Paga errato AR: "+  btnPaga.getElement().getText() + ", ER: "+ nImporto, btnPaga.getElement().getText().equals(nImporto));
		assertTrue("Pulsante Annulla errato AR: "+  btnAnnulla.getElement().getText() + ", ER: "+ btnAnnullaInput, btnAnnulla.getElement().getText().equals(btnAnnullaInput));
	}
	
	public void clickBtnPagaM264()
	{
		Particle btnPaga = (Particle) UiObjectRepo.get().get(BolloAutoMotoRiepilogoM264.PAGABTNM264);
		btnPaga.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickBtnAnnullaM264()
	{
		Particle btnAnnulla = (Particle) UiObjectRepo.get().get(BolloAutoMotoRiepilogoM264.ANNULLAM264);
		btnAnnulla.getElement().click();
		WaitManager.get().waitMediumTime();
	}
}

