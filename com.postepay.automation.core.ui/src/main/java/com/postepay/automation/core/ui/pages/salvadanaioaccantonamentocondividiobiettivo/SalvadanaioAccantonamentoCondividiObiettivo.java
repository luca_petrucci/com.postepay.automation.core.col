package com.postepay.automation.core.ui.pages.salvadanaioaccantonamentocondividiobiettivo;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.SalvadanaioAccantonamentoCondividiObiettivoMolecola;


public class SalvadanaioAccantonamentoCondividiObiettivo extends Page {
	public static final String NAME="T113";
	

	public SalvadanaioAccantonamentoCondividiObiettivo(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(SalvadanaioAccantonamentoCondividiObiettivoMolecola.NAME, UiObjectRepo.get().get(SalvadanaioAccantonamentoCondividiObiettivoMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SalvadanaioAccantonamentoCondividiObiettivoManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

}

