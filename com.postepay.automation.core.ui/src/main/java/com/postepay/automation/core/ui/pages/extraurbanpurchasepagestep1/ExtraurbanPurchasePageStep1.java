package com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep1;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.android.AndroidDriver;

import com.postepay.automation.core.ui.molecules.BodyExtraurbanPurchase;


public class ExtraurbanPurchasePageStep1 extends Page {
	public static final String NAME="T060";
	

	public ExtraurbanPurchasePageStep1(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(BodyExtraurbanPurchase.NAME, UiObjectRepo.get().get(BodyExtraurbanPurchase.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new ExtraurbanPurchasePageStep1Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		return new ExtraurbanPurchasePageStep1ManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderpage(String titlePage) {
		((ExtraurbanPurchasePageStep1Manager)this.manager).verifyHeaderpage(titlePage);
	}
	
	public void verifyLayoutPage() {
		((ExtraurbanPurchasePageStep1Manager)this.manager).verifyLayoutPage();
	}
	
	public void clickOnChooseStartPlace() {
		((ExtraurbanPurchasePageStep1Manager)this.manager).clickOnChooseStartPlace();
	}

	public void clickOnChooseDestinationPlace() {
		((ExtraurbanPurchasePageStep1Manager)this.manager).clickOnChooseDestinationPlace();
	}
	
	public void sendStartPlace(WebDriver driver, String startPlace) {
		((ExtraurbanPurchasePageStep1Manager)this.manager).sendStartPlace(driver, startPlace);
	}
	
	public void sendDestinationPlace(WebDriver driver, String destinationPlace) {
		((ExtraurbanPurchasePageStep1Manager)this.manager).sendDestinationPlace(driver, destinationPlace);
	}
	
	public void clickOnPassenger(String type) {
		((ExtraurbanPurchasePageStep1Manager)this.manager).clickOnPassenger(type);
	}
	
	public void verifyPassenger(String moreInfo) {
		((ExtraurbanPurchasePageStep1Manager)this.manager).verifyPassenger(moreInfo);
	}
	
	public void verifyData(String moreInfo) {
		((ExtraurbanPurchasePageStep1Manager)this.manager).verifyData(moreInfo);
	}
	public void clickOnSearchTicket() {
		((ExtraurbanPurchasePageStep1Manager)this.manager).clickOnSearchTicket();
	}

}

