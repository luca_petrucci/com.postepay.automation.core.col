package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class WhomWontSendAPostagiroSummary extends Molecule {
	public static final String NAME="M057";

	public static final String SUMMARYAMOUTOFCOMMISSIONBANKTRANFERTPOSTAGIRO="summaryAmoutOfCommissionBankTranfertPostagiro";
	public static final String SUMMARYIBANOFCARDFORPAYBANKTRANFERTPOSTAGIRO="summaryIbanOfCardForPayBankTranfertPostagiro";
	public static final String SUMMARYNUMBEROFCARDFORPAYBANKTRANFERTPOSTAGIRO="summaryNumberOfCardForPayBankTranfertPostagiro";
	public static final String SUMMARYAMOUNTTOPAYBANKTRANFERTPOSTAGIRO="summaryAmountToPayBankTranfertPostagiro";
	public static final String SUMMARYPAYTOIBANBANKTRANFERTPOSTAGIRO="summaryPayToIbanBankTranfertPostagiro";
	public static final String CONFIRMBANKTRANFERTPOSTAGIROTOSEND="confirmBankTranfertPostagiroToSend";
	public static final String SUMMARYHOLDERTORECHARGEPAYBANKTRANFERTPOSTAGIRO="summaryHolderToRechargePayBankTranfertPostagiro";
	public static final String SUMMARYREASONOFBANKTRANFERTPOSTAGIRO="summaryReasonOfBankTranfertPostagiro";
	public static final String HOLDERNAMEBANKTRANFERTPOSTAGIROTOSEND="holderNameBankTranfertPostagiroToSend";
	public static final String SUBTITLESUMMARYBANKTRANFERTPOSTAGIRO="subTitleSummaryBankTranfertPostagiro";


	public WhomWontSendAPostagiroSummary(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

