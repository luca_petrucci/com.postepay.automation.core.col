package com.postepay.automation.core.ui.pages.urbanpurchasepagestep1;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.molecules.BodyUrbanPurchase;


public class UrbanPurchasePageStep1 extends Page {
	public static final String NAME="T052";
	

	public UrbanPurchasePageStep1(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(BodyUrbanPurchase.NAME, UiObjectRepo.get().get(BodyUrbanPurchase.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new UrbanPurchasePageStep1Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderpage(String inputTitle) {
		((UrbanPurchasePageStep1Manager)this.manager).verifyHeaderpage(inputTitle);
	}
	
	public void verifyLayoutPage() {
		((UrbanPurchasePageStep1Manager)this.manager).verifyLayoutPage();
	}
	
	public void clickOnTabCity() {
		((UrbanPurchasePageStep1Manager)this.manager).clickOnTabCity();
	}
	
	public void clickOnTabVettore() {
		((UrbanPurchasePageStep1Manager)this.manager).clickOnTabVettore();
	}
	
	public void searchCity(String city) {
		((UrbanPurchasePageStep1Manager)this.manager).searchCity(city);
	}

	public void clickOnSearchedCity() {
		((UrbanPurchasePageStep1Manager)this.manager).clickOnSearchedCity();
	}
}

