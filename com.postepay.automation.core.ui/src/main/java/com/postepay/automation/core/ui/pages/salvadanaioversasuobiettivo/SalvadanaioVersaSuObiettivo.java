package com.postepay.automation.core.ui.pages.salvadanaioversasuobiettivo;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.SalvadanaioVersaSuObiettivoMolecola;

import io.appium.java_client.android.AndroidDriver;


public class SalvadanaioVersaSuObiettivo extends Page {
	public static final String NAME="T114";
	

	public SalvadanaioVersaSuObiettivo(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(SalvadanaioVersaSuObiettivoMolecola.NAME, UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SalvadanaioVersaSuObiettivoManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new SalvadanaioVersaSuObiettivoManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderpage(String titlePageInput) {
		((SalvadanaioVersaSuObiettivoManager)this.manager).verifyHeaderpage(titlePageInput);
	}
	
	public void verifyLayoutPage(WebDriver driver, String nameInput, String typeObiettivoInput, String isRicorrente, double discrepanza) {
		((SalvadanaioVersaSuObiettivoManager)this.manager).verifyLayoutPage(driver, nameInput, typeObiettivoInput, isRicorrente, discrepanza);
	}
	
	public void clickOnContinua() {
		((SalvadanaioVersaSuObiettivoManager)this.manager).clickOnContinua();
	}

	public void clickOnVersamentoRicorrente() {
		((SalvadanaioVersaSuObiettivoManager)this.manager).clickOnVersamentoRicorrente();
	}
}

