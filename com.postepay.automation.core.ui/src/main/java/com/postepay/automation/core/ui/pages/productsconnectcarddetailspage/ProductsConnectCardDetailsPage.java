package com.postepay.automation.core.ui.pages.productsconnectcarddetailspage;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;
import com.postepay.automation.core.ui.molecules.BodyProductsConnectCardDetail;


public class ProductsConnectCardDetailsPage extends Page {
	public static final String NAME="T051";
	

	public ProductsConnectCardDetailsPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(BodyProductsConnectCardDetail.NAME, UiObjectRepo.get().get(BodyProductsConnectCardDetail.NAME), false);
		
	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new ProductsConnectCardDetailsPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new ProductsConnectCardDetailsPageManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeader(String inputTitle) {
		((ProductsConnectCardDetailsPageManager)this.manager).verifyHeader(inputTitle);
	}
	
	public void verifyLayoutPage() {
		
		((ProductsConnectCardDetailsPageManager)this.manager).verifyLayoutPage();
	}
	
	public double getAmountContabile() {
		return ((ProductsConnectCardDetailsPageManager)this.manager).getAmountContabile();
	}
	
	public double getAmountDisponibile() {
		return ((ProductsConnectCardDetailsPageManager)this.manager).getAmountDisponibile();
	}

	public void verifyDecrementoSaldo(double newCalContabile, double newCalDisponibile) {
		((ProductsConnectCardDetailsPageManager)this.manager).verifyDecrementoSaldo(newCalContabile, newCalDisponibile);
	}

	public void scrollTo(String target) {
		((ProductsConnectCardDetailsPageManager)this.manager).scrollTo(target);
	}

	public void enableGpatToggle() {
		((ProductsConnectCardDetailsPageManager)this.manager).enableGpatToggle();
		
	}

	public void isGpatToggle() {
		((ProductsConnectCardDetailsPageManager)this.manager).isGpatToggle();
		
	}

	public void disableGpayToggle() {
		((ProductsConnectCardDetailsPageManager)this.manager).disableGpayToggle();
		
	}

	public void isNotGpatToggle() {
		((ProductsConnectCardDetailsPageManager)this.manager).isNotGpatToggle();
		
	}
	public void clickOnBack()
	{
		((ProductsConnectCardDetailsPageManager)this.manager).clickOnBack();
	}
}

