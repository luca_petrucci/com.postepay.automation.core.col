package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SelezionaTipoBonificoMolecola extends Molecule {
	public static final String NAME="M270";
	
	public static final String TITLEM270="titlem270";
	public static final String BONIFICOSEPAPOSTAGIROM270="bonificosepapostagirom270";
	public static final String BONIFICOSEPAISTANTANEOM270="bonificosepaistantaneom270";
	public static final String TITLEBONIFICOSEPAPOSTAGIROM270="titlebonificosepapostagirom270";
	public static final String TITLEBONIFICOSEPAISTANTANEOM270="titlebonificosepaistantaneom270";
	public static final String COPYBONIFICOSEPAPOSTAGIROM270="copybonificosepapostagirom270";
	public static final String COPYBONIFICOSEPAISTANTANEOM270="copybonificosepaistantaneom270";
	
	public SelezionaTipoBonificoMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

