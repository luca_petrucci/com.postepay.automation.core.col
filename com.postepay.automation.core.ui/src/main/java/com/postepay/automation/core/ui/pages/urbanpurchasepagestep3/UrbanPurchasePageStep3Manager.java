package com.postepay.automation.core.ui.pages.urbanpurchasepagestep3;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.BodyUrbanPurchase;
import com.postepay.automation.core.ui.molecules.BodyUrbanPurchaseVerifySummary;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class UrbanPurchasePageStep3Manager extends PageManager {

	Particle icon = (Particle) UiObjectRepo.get().get(BodyUrbanPurchaseVerifySummary.ICONTIKETURBANPURCHASE);
	Particle title = (Particle) UiObjectRepo.get().get(BodyUrbanPurchaseVerifySummary.TITLETIKETURBANPURCHASE);
	Particle ticket = (Particle) UiObjectRepo.get().get(BodyUrbanPurchaseVerifySummary.TIKETCARDURBANPURCHASE);
	Particle ticketMin = (Particle) UiObjectRepo.get().get(BodyUrbanPurchaseVerifySummary.ICONMINUSURBANPURCHASE);
	Particle ticketMax = (Particle) UiObjectRepo.get().get(BodyUrbanPurchaseVerifySummary.ICONPLUSURBANPURCHASE);
	Particle ticketNumber = (Particle) UiObjectRepo.get().get(BodyUrbanPurchaseVerifySummary.NUMBERTIKETURBANPURCHASE);
	Particle amountSingleTicket = (Particle) UiObjectRepo.get().get(BodyUrbanPurchaseVerifySummary.SINGLEPRICEURBANPURCHASE);
	Particle amountTotTicket = (Particle) UiObjectRepo.get().get(BodyUrbanPurchaseVerifySummary.TOTALPRICEURBANPURCHASE);
	Particle btn = (Particle) UiObjectRepo.get().get(BodyUrbanPurchaseVerifySummary.BUTTONPAYURBANPURCHASEVERIFYSUMMARY);

	public UrbanPurchasePageStep3Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderpage() {
		LayoutTools tool = new LayoutTools();
		tool.verifyPresenceHeaderPage("Riepilogo dei biglietti");
	}

	public void verifyLayoutPage() {
		assertTrue(icon.getElement().isDisplayed());
		assertTrue(title.getElement().isDisplayed());
		assertTrue(ticket.getElement().isDisplayed());
		assertTrue(ticketMin.getElement().isDisplayed());
		assertTrue(ticketMax.getElement().isDisplayed());
		assertTrue(ticketNumber.getElement().isDisplayed());
		assertTrue(amountSingleTicket.getElement().isDisplayed());
		assertTrue(amountTotTicket.getElement().isDisplayed());
		assertTrue(btn.getElement().isDisplayed());
	}
	
	public void verifyDefaultSettings() {
		// 1 solo biglietto
		assertTrue(ticketNumber.getElement().getText().equals("1"));
		
		// Prezzo singolo uguale a prezzo totale
		assertTrue(amountSingleTicket.getElement().getText().equals(
				   amountTotTicket.getElement().getText()));
	}

	public void clickOnMinus() {
		ticketMin.getElement().click();
		WaitManager.get().waitShortTime();
	}

	public void clickOnPlus() {
		ticketMax.getElement().click();
		WaitManager.get().waitShortTime();
	}

	public void clickPay() {
		btn.getElement().click();
		WaitManager.get().waitLongTime();
	}
}

