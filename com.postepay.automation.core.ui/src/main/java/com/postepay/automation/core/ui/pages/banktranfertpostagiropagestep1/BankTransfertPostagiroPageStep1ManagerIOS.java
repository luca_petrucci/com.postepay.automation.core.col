package com.postepay.automation.core.ui.pages.banktranfertpostagiropagestep1;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;

import com.postepay.automation.core.ui.molecules.WhomWontSendABonificoSepaSummary;
import com.postepay.automation.core.ui.molecules.WhomWontSendAPostagiroSummary;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;

import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class BankTransfertPostagiroPageStep1ManagerIOS extends BankTranfertPostagiroPageStep1Manager{
	public BankTransfertPostagiroPageStep1ManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	public void verifySummarySepa(String nameInput, String ibanInput, String amountInput, String reasonInput, String myIbanInput, String cardNumberInput)
	{
		Particle intestatario=(Particle) UiObjectRepo.get().get(WhomWontSendAPostagiroSummary.SUMMARYHOLDERTORECHARGEPAYBANKTRANFERTPOSTAGIRO);
		String locatorIntestatario= Utility.replacePlaceHolders(intestatario.getLocator(), new Entry("title", nameInput));
		assertTrue("Numero carta non presente",page.getDriver().findElement(By.xpath(locatorIntestatario)).isDisplayed());
		
		Particle iban=(Particle) UiObjectRepo.get().get(WhomWontSendAPostagiroSummary.SUMMARYPAYTOIBANBANKTRANFERTPOSTAGIRO);
		String locatorIban= Utility.replacePlaceHolders(iban.getLocator(), new Entry("title", ibanInput));
		assertTrue("Iban errato",page.getDriver().findElement(By.xpath(locatorIban)).isDisplayed());
		
		Particle amount=(Particle) UiObjectRepo.get().get(WhomWontSendAPostagiroSummary.SUMMARYAMOUNTTOPAYBANKTRANFERTPOSTAGIRO);
		String euroPrefix="€";
		String locatorAmount= Utility.replacePlaceHolders(amount.getLocator(), new Entry("title", euroPrefix.concat(amountInput).replace(".", ",")));
		assertTrue("Importo errato",page.getDriver().findElement(By.xpath(locatorAmount)).isDisplayed());
		
		Particle cardNumebr=(Particle) UiObjectRepo.get().get(WhomWontSendAPostagiroSummary.SUMMARYNUMBEROFCARDFORPAYBANKTRANFERTPOSTAGIRO);
		String asterisco="**** **** ";
		String locatorCardNumber= Utility.replacePlaceHolders(cardNumebr.getLocator(), new Entry("title", asterisco.concat(cardNumberInput)));
		assertTrue("Card errata",page.getDriver().findElement(By.xpath(locatorCardNumber)).isDisplayed());
		
		Particle myIban=(Particle) UiObjectRepo.get().get(WhomWontSendAPostagiroSummary.SUMMARYIBANOFCARDFORPAYBANKTRANFERTPOSTAGIRO);
		String locatorMyIban= Utility.replacePlaceHolders(myIban.getLocator(), new Entry("title", myIbanInput));
		assertTrue("Myban errato",page.getDriver().findElement(By.xpath(locatorMyIban)).isDisplayed());
		
		Particle reason=(Particle) UiObjectRepo.get().get(WhomWontSendAPostagiroSummary.SUMMARYREASONOFBANKTRANFERTPOSTAGIRO);
		String locatorReason= Utility.replacePlaceHolders(reason.getLocator(), new Entry("title", reasonInput));
		assertTrue("Causale errato",page.getDriver().findElement(By.xpath(locatorReason)).isDisplayed());
		
		Particle commition=(Particle) UiObjectRepo.get().get(WhomWontSendAPostagiroSummary.SUMMARYAMOUTOFCOMMISSIONBANKTRANFERTPOSTAGIRO);
		String locatorCommition= Utility.replacePlaceHolders(commition.getLocator(), new Entry("title", "€0,50"));
		assertTrue("Commissione errato",page.getDriver().findElement(By.xpath(locatorCommition)).isDisplayed());
		
		Particle sum=(Particle) UiObjectRepo.get().get(WhomWontSendAPostagiroSummary.CONFIRMBANKTRANFERTPOSTAGIROTOSEND);
		String importoAR = sum.getElement().getText();
		StringAndNumberOperationTools operationTool=new StringAndNumberOperationTools();
		double newAmountButtonInteger=operationTool.convertStringToDouble(amountInput.replace(".", ","));
		double sommaTotale= newAmountButtonInteger+0.50;
		String controlloSomma= sommaTotale+"";
		if(controlloSomma.length()<=3)
			controlloSomma+="0";
		String euroPagaPrefix="PAGA €";
		String importoER = euroPagaPrefix.concat(controlloSomma.replace(".", ","));
		assertTrue("Pulsante Paga AR: "+importoAR + " ER: "+ importoER ,importoAR.contains(importoER));
		
		
	}
}
