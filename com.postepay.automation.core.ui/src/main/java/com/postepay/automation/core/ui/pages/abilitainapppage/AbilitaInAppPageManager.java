package com.postepay.automation.core.ui.pages.abilitainapppage;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.postepay.automation.core.ui.molecules.AbilitaInAppMolecola;
import com.postepay.automation.core.ui.pages.genericerrorpopup.GenericErrorPopUp;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.HidesKeyboard;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.CoreUtility;

public class AbilitaInAppPageManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();

	Particle scegliLabel = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.SCEGLILABELABILITAINAPP);
	Particle autorizzaTitolo = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.AUTORIZZATITOLOABILITAINAPP);
	Particle copyAutorizza = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.COPYAUTORIZZAABILITAINAPP);
	Particle iconaGiallaAutorizza = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.ICONAGIALLAAUTORIZZAABILITAINAPP);
	Particle nonAutorizzaA = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.NONAUTORIZZAABILITAINAPP);
	Particle vopyNonAutorizza = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.VOPYNONAUTORIZZAABILITAINAPP);
	Particle iconaGiallaNonAutorizza = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.ICONAGIALLANONAUTORIZZAABILITAINAPP);
	Particle linkCambiaredispositivo = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.LINKCAMBIAREDISPOSITIVOABILITAINAPP);

	public AbilitaInAppPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}

	public void verifyImage() {

	}

	public void verifyLayout() {
		assertTrue(scegliLabel.getElement().isDisplayed());
		assertTrue(autorizzaTitolo.getElement().isDisplayed());
		assertTrue(copyAutorizza.getElement().isDisplayed());
		assertTrue(iconaGiallaAutorizza.getElement().isDisplayed());
		assertTrue(nonAutorizzaA.getElement().isDisplayed());
		assertTrue(vopyNonAutorizza.getElement().isDisplayed());
		assertTrue(iconaGiallaNonAutorizza.getElement().isDisplayed());
		assertTrue(linkCambiaredispositivo.getElement().isDisplayed());
		verifyImage();
	}

	public void clickOnAutorizzaConPreferito() {
		iconaGiallaAutorizza.getElement().click();
		WaitManager.get().waitLongTime();
	}

	public void clickOnNonHoaccessoAlPreferito() {
		nonAutorizzaA.getElement().click();
		WaitManager.get().waitLongTime();
	}

	public void clickOnDeleteDevice() {
		//System.out.println(nonAutorizzaA.getElement().getText());
		WaitManager.get().waitShortTime();
		CoreUtility.visibilityOfElement(page.getDriver(), nonAutorizzaA, 20).click();
		WaitManager.get().waitShortTime();

		Particle popupConferma = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242POPUPCONTINUA);
		CoreUtility.visibilityOfElement(page.getDriver(), popupConferma, 20).click();
		WaitManager.get().waitShortTime();

		// Confermiamo la cancellazione con Sms
		Particle sms = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242SMSBTNCONFERMA);
		CoreUtility.visibilityOfElement(page.getDriver(), sms, 20).click();

	}

	public void clickOnOnbDevice(String list) {
		try {

			Particle inizaBtn = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242INIZIAONBDESC);
			CoreUtility.visibilityOfElement(page.getDriver(), inizaBtn, 20).click();
			WaitManager.get().waitShortTime();

		} catch (Exception e) {
			// TODO: handle exception
		}


		// Dobbiamo selezionare la carta
		// Prima ci importiamo i dati dal file di language
		String[] listaDatiCarta = list.split(";");

		Particle selezionaCarta = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242SELEZIONACARTA);
		String nuovoLocator=Utility.replacePlaceHolders(selezionaCarta.getLocator(), new Entry("title",listaDatiCarta[0]));

		//		page.getDriver().findElement(By.xpath(nuovoLocator)).click();
		CoreUtility.visibilityOfElement(page.getDriver(), By.xpath(nuovoLocator), 20).click();
		WaitManager.get().waitShortTime();


		// Inserisco la scadenza 
		Particle scadenza = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242SCADENZA);
		CoreUtility.visibilityOfElement(page.getDriver(), scadenza, 20).sendKeys(listaDatiCarta[1]);
		WaitManager.get().waitShortTime();
		((HidesKeyboard) page.getDriver()).hideKeyboard();
		// Clicco su conferma
		Particle confermaScadenza = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242SCADENZACONFERMA);
		CoreUtility.visibilityOfElement(page.getDriver(), confermaScadenza, 20).click();
		WaitManager.get().waitShortTime();

		// Inserisco il CVV
		Particle cvv = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242CVV);
		CoreUtility.visibilityOfElement(page.getDriver(), cvv, 20).sendKeys(listaDatiCarta[2]);
		WaitManager.get().waitShortTime();
		((HidesKeyboard) page.getDriver()).hideKeyboard();
		// Clicco su conferma
		Particle cvvScadenza = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242CVVCONFERMA);
		CoreUtility.visibilityOfElement(page.getDriver(), cvvScadenza, 20).click();
		WaitManager.get().waitMediumTime();

		// Devo confermare con un OTP per SMS
		Particle smsConferma = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242SMSBTNCONFERMA);
		CoreUtility.visibilityOfElement(page.getDriver(), smsConferma, 20).click();
		WaitManager.get().waitShortTime();

	}

	public void clickOnOnbDeviceDurinOperations(String list) {
		// Dobbiamo selezionare la carta
		// Prima ci importiamo i dati dal file di language
		String[] listaDatiCarta = list.split(";");


		// Inserisco la scadenza 
		Particle scadenza = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242SCADENZA);
		scadenza.getElement().sendKeys(listaDatiCarta[1]);
		WaitManager.get().waitShortTime();
		((HidesKeyboard) page.getDriver()).hideKeyboard();
		// Clicco su conferma
		Particle confermaScadenza = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242SCADENZACONFERMA);
		confermaScadenza.getElement().click();
		WaitManager.get().waitShortTime();

		// Inserisco il CVV
		Particle cvv = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242CVV);
		cvv.getElement().sendKeys(listaDatiCarta[2]);
		WaitManager.get().waitShortTime();
		((HidesKeyboard) page.getDriver()).hideKeyboard();
		// Clicco su conferma
		Particle cvvScadenza = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242CVVCONFERMA);
		cvvScadenza.getElement().click();
		WaitManager.get().waitMediumTime();

		// Devo confermare con un OTP per SMS
		Particle smsConferma = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242SMSBTNCONFERMA);
		smsConferma.getElement().click();
		WaitManager.get().waitShortTime();

		// Devo confermare con un OTP per SMS
		Particle typAccedi = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242TYPACCEDI);
		typAccedi.getElement().click();
		WaitManager.get().waitShortTime();

	}

	public void createPosteID(String posteId) {
		// Clicco su Crea BTN 
		Particle crea = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242CREAPOSTEIDBTN);
		CoreUtility.visibilityOfElement(page.getDriver(), crea, 20).click();
		WaitManager.get().waitShortTime();

		// Inserisco il primo PosteId
		Particle posteUno = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242POSTEIDUNO);
		CoreUtility.visibilityOfElement(page.getDriver(), posteUno, 20).sendKeys(posteId);
		WaitManager.get().waitShortTime();
		try {CoreUtility.visibilityOfElement(page.getDriver(), By.xpath("//*[@*='Fine' and @*='XCUIElementTypeStaticText']"), 10).click();} catch (Exception e) {}
		try {((AppiumDriver<?>)page.getDriver()).hideKeyboard();} catch (Exception err )  {}

		// Inserisco il secondo PosteId
		Particle posteDue = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242POSTEIDDUE);
		CoreUtility.visibilityOfElement(page.getDriver(), posteDue, 20).sendKeys(posteId);
		WaitManager.get().waitShortTime();
		try {CoreUtility.visibilityOfElement(page.getDriver(), By.xpath("//*[@*='Fine' and @*='XCUIElementTypeStaticText']"), 10).click();} catch (Exception e) {}
		try {((AppiumDriver<?>)page.getDriver()).hideKeyboard();} catch (Exception err )  {}

		//		clicco su conferma
		Particle posteIdConferma = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242CONFERMAPOSTEIDBTN);
		CoreUtility.visibilityOfElement(page.getDriver(), posteIdConferma, 20).click();
		WaitManager.get().waitLongTime();

		// Attendo che la TYP di creazione sia mostrata per cliccare su accedi
		Particle typBtn = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242TYPACCEDI);
		WebDriverWait w=new WebDriverWait(page.getDriver(), 20);
		w.until(ExpectedConditions.presenceOfElementLocated(typBtn.getXPath()))
		.click();
		//		typBtn.getElement().click();
		//		WaitManager.get().waitMediumTime();

		// Alcune versioni presentano la popup commerciale di ricarica post onb
		try {
			GenericErrorPopUp popup=(GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			popup.clickOnChiudiRicaricaDopoOnb();
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public boolean isOnb() {
		boolean isDisplayed = false;
		Particle scadenza = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242SCADENZA);
		if(scadenza != null) {

			try {
				CoreUtility.visibilityOfElement(page.getDriver(), scadenza, 20);
				isDisplayed=true;
			} catch (Exception e) {
				// TODO: handle exception
			}

		}

		return isDisplayed;
	}

	public boolean isCreaPosteIdVisible() 
	{
		boolean isDisplayed = false;
		Particle scadenza = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242CREAPOSTEIDBTN);
		if(scadenza != null) {

			try {
				CoreUtility.visibilityOfElement(page.getDriver(), scadenza, 20);
				isDisplayed=true;
			} catch (Exception e) {
				// TODO: handle exception
			}

		}

		return isDisplayed;
	}

	public void clickOnProseguiButton() {
		Particle prosegui = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242PROSEGUIBTN);
		CoreUtility.visibilityOfElement(page.getDriver(), prosegui, 20).click();
		WaitManager.get().waitLongTime();
	}

	public void inserisciMeseanno(String daticarta) {
		String[] listaDatiCarta = daticarta.split(";");

		// Inserisco la scadenza 
		Particle scadenza = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242SCADENZA);
		scadenza.getElement().sendKeys(listaDatiCarta[1]);
		WaitManager.get().waitShortTime();
		((HidesKeyboard) page.getDriver()).hideKeyboard();
		// Clicco su conferma
		Particle confermaScadenza = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242SCADENZACONFERMA);
		confermaScadenza.getElement().click();
		WaitManager.get().waitShortTime();

	}

	public void inserisciCvv(String daticarta) {
		String[] listaDatiCarta = daticarta.split(";");

		// Inserisco il CVV
		Particle cvv = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242CVV);
		CoreUtility.visibilityOfElement(page.getDriver(), cvv, 20).sendKeys(listaDatiCarta[2]);
		WaitManager.get().waitShortTime();
		((HidesKeyboard) page.getDriver()).hideKeyboard();
		// Clicco su conferma
		Particle cvvScadenza = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242CVVCONFERMA);
		CoreUtility.visibilityOfElement(page.getDriver(), cvvScadenza, 20).click();
		WaitManager.get().waitMediumTime();

		// Devo confermare con un OTP per SMS
		Particle smsConferma = (Particle) UiObjectRepo.get().get(AbilitaInAppMolecola.M242SMSBTNCONFERMA);
		CoreUtility.visibilityOfElement(page.getDriver(), smsConferma, 20).click();
		WaitManager.get().waitShortTime();


	}

}

