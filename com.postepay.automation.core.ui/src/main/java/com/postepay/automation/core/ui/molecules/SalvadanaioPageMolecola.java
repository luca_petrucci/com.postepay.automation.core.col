package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SalvadanaioPageMolecola extends Molecule {
	public static final String NAME="M229";
	
	public static final String SALVADANAIOACCANTONAMENTOSCADENZA="salvadanaioAccantonamentoScadenza";
public static final String SALVADANAIOACCANTONAMENTOFOOTERTEXT="salvadanaioAccantonamentoFooterText";
public static final String SALVADANAIOACCANTONAMENTONAME="salvadanaioAccantonamentoName";
public static final String SALVADANAIOACCANTONAMENTOCREANEWOBIETTIVO="salvadanaioAccantonamentoCreaNewObiettivo";
public static final String SALVADANAIOACCANTONAMENTODETTAGLI="salvadanaioAccantonamentoDettagli";
public static final String SALVADANAIOTEXTVIEW="salvadanaioTextView";
public static final String SALVADANAIOACCANTONAMENTOYELLOWICONDETTAGLI="salvadanaioAccantonamentoYellowIconDettagli";
public static final String SALVADANAIOACCANTONAMENTOACTUALAMOUNT="salvadanaioAccantonamentoActualAmount";
public static final String SALVADANAIOACCANTONAMENTOICONAOROLOGIORICORRENZA="salvadanaioAccantonamentoIconaOrologioRicorrenza";
public static final String SALVADANAIOIMAGEVIEW="salvadanaioImageView";
public static final String SALVADANAIOACCANTONAMENTOICON="salvadanaioAccantonamentoIcon";
public static final String SALVADANAIOTITLEVIEW="salvadanaioTitleView";
public static final String SALVADANAIOACCANTONAMENTOTOTALAMOUNT="salvadanaioAccantonamentoTotalAmount";
public static final String SALVADANAIOACCANTONAMENTOSTATUSBAR="salvadanaioAccantonamentoStatusBar";


	public SalvadanaioPageMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

