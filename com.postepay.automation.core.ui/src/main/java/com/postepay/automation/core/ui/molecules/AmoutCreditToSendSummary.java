package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class AmoutCreditToSendSummary extends Molecule {
	public static final String NAME="M025";
	
	public static final String ICONP2PSENDCREDITSENTSUMMARY="iconP2PSendCreditSentSummary";
public static final String ADDCOMMENTP2PSEND="addCommentP2PSend";
public static final String AMOUNTP2PSENDSUMMARY="amountP2PSendSummary";
public static final String USERCONTACTP2PSEND="userContactP2PSend";
public static final String SENDBUTTONP2PSEND="sendButtonP2PSend";
public static final String MODIFYP2PSEND="modifyP2PSend";


	public AmoutCreditToSendSummary(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

