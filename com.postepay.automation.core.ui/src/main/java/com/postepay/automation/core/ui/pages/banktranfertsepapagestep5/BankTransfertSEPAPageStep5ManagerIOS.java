package com.postepay.automation.core.ui.pages.banktranfertsepapagestep5;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.ThankYouPageGeneric;

import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class BankTransfertSEPAPageStep5ManagerIOS extends BankTranfertSEPAPageStep5Manager {
	public BankTransfertSEPAPageStep5ManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyLayout()
	{
		Particle img=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.THANKYOUIMAGEBANKTRANFERTSEPA);
		Particle title=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.THANKYOUTITLEBANKTRANFERTSEPA);
		Particle btn=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.THANKYOUCLOSEBUTTONBANKTRANFERTSEPA);
		Particle link=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.TYPDESCRIPTIONM054);
		
		String eBtn="CHIUDI";
		String titleLavorazione="Transazione presa in carico!";
		if(title.getElement().getText().equals(titleLavorazione)) {
			assertTrue("Immagine errata",img.getElement().isEnabled());
			assertTrue("Titolo AR: "+ title.getElement().getText()+ "ER: "+titleLavorazione, title.getElement().getText().contains(titleLavorazione));
			assertTrue("Pulsante Chiudi AR: "+ btn.getElement().getText()+ "ER: "+ eBtn,btn.getElement().getText().contains(eBtn));
			assertTrue("Descrizione vuota",link.getElement()!=null);
		}else {
			String eTitle="L'operazione è andata a buon fine.";
			assertTrue("Immagine errata",img.getElement().isEnabled());
			assertTrue("Titolo AR: "+ title.getElement().getText()+ "ER: "+eTitle, title.getElement().getText().contains(eTitle));
			assertTrue("Pulsante Chiudi AR: "+ btn.getElement().getText()+ "ER: "+ eBtn,btn.getElement().getText().contains(eBtn));
			assertTrue("Copy AR: "+ link.getElement().getText(),link.getElement().getText().contains("Potrai consultare la ricevuta in bacheca e in "));
			assertTrue("Copy AR",link.getElement().getText().contains("sul sito Poste.it"));
		}
	}
	public void typPraticaMIT() {
		Particle img=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.TYPMITIMAGEM054);
		Particle title=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.TYPMITTITOLOM054);
		Particle desc=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.TYPMITDESCRIPTIONM054);
		Particle btnClose=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.TYPMITCLOSEM054);
		Particle btnRicevuta=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.TYPMITRICEVUTAM054);
		Particle link=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.TYPMITLINKM054);

		String erDescription1="L’operazione è andata a buon fine.";
		String erDescription2="Potrai trovare la ricevuta dell’operazione con i bollettini nella tua bacheca sull’app e su Poste.it.";
		// Imagine
		assertTrue("Immagine non presente: ", img.getElement().isDisplayed());
		// Title
		assertTrue("Titolo errato", title.getElement().isDisplayed());
		// Desc
		assertTrue("Copy errato", desc.getElement().getText().contains(erDescription1));	
		assertTrue("Copy errato", desc.getElement().getText().contains(erDescription2));
		// Link
		assertTrue("Link non presente", link.getElement().isDisplayed());
		// Bottone
		assertTrue("Btn non presente", btnClose.getElement().isDisplayed());
		assertTrue("Ricevuta non presente", btnRicevuta.getElement().isDisplayed());
		
		WaitManager.get().waitShortTime();
		
	}
}
