package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SalvadanaioTYPMolecola extends Molecule {
	public static final String NAME="M237";

	public static final String SALVADANAIOTYPCLOSE="salvadanaioTYPClose";
	public static final String SALVADANAIOTYPIMAGE="salvadanaioTYPImage";
	public static final String SALVADANAIOTYPDESCRIZIONE="salvadanaioTYPDescrizione";
	public static final String SALVADANAIOTYPTITOLO="salvadanaioTYPTitolo";
	public static final String SALVADANAIOTYPFINGERPRINTLINK="salvadanaioTYPFingerPrintLink";
	public static final String DESCRIZIONEATTIVAZIONE="salvadanaioTYPDescrizioneAttivazione";
	public static final String DESCRIZIONEDISATTIVAZIONE="salvadanaioTYPDescrizioneDisattivazione";

	public SalvadanaioTYPMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

