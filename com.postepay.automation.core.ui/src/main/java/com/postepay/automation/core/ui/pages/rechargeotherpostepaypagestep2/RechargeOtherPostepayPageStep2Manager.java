package com.postepay.automation.core.ui.pages.rechargeotherpostepaypagestep2;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.HowToPayGeneral;
import com.postepay.automation.core.ui.molecules.HowWontPayRechargeOtherPostepay;
import com.postepay.automation.core.ui.molecules.SelectACardToRechargeMyPostepay;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
//import com.postepay.automation.core.ui.verifytools.WaitManager;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.UIUtils;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class RechargeOtherPostepayPageStep2Manager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();
	
	Particle title=(Particle) UiObjectRepo.get().get(HowWontPayRechargeOtherPostepay.TITLEHOWTOPAYOTHERCARD);
	Particle icon=(Particle) UiObjectRepo.get().get(SelectACardToRechargeMyPostepay.TARGETICONSENDERRECHARGE);
	Particle card_type=(Particle) UiObjectRepo.get().get(SelectACardToRechargeMyPostepay.TARGETTYPESENDERRECHARGE);
	Particle card_number=(Particle) UiObjectRepo.get().get(SelectACardToRechargeMyPostepay.TARGETNUMBERCARDSENDERRECHARGE);
	Particle avaibleLabel=(Particle) UiObjectRepo.get().get(SelectACardToRechargeMyPostepay.TARGETAVAIBLELABELSENDERRECHARGE);
	Particle card_amount=(Particle) UiObjectRepo.get().get(SelectACardToRechargeMyPostepay.TARGETAMOUNTSENDERRECHARGE);
	
	public RechargeOtherPostepayPageStep2Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}
	
//	public WebElement getElementTitle(WebDriver driver, String titleInput) {
//		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, title, titleInput);
//		assertTrue(elm.isDisplayed());
//		return elm;
//	}
	
	public WebElement getElementType(WebDriver driver, String typeInput) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, card_type, typeInput);
		assertTrue(elm.isDisplayed());
		return elm;
	}
	
	public WebElement getElementCardNumber(WebDriver driver, String numberInput) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, card_number, numberInput);
		assertTrue(elm.isDisplayed());
		return elm;
	}
	
	public void verifyImageCard(WebDriver driver, String imageName, double discrepanza) {
		String nameText = "ricaricaPostepay/" + imageName;
		LayoutImage.get().verifyImage(driver, nameText, discrepanza);
		WaitManager.get().waitShortTime();
	}

	public void verifyLayoutPage(WebDriver driver, String username, String titleInput, String typeInput, String numberInput, double discrepanza) {
//		getElementTitle(driver, titleInput);
		assertTrue(title.getElement().isDisplayed());
		getElementCardNumber(driver, numberInput);
		assertTrue(avaibleLabel.getElement().isDisplayed());
		assertTrue(card_amount.getElement().isDisplayed());
		System.out.println("\nInizio Verifica immagini:");
		
		switch (username) {
		case "vincenzo.fortunato-4869":
			getElementType(driver, "CONNECT");
			verifyImageCard(driver, "icona_nera", discrepanza);
			System.out.println("\nCardBiancoNera è stata verificata");
			verifyImageCard(driver, "icona_grigia", discrepanza);
			System.out.println("\nCardBiancoGrigia è stata verificata");	
			break;
			
		case "gennaro.rega-1784":
			getElementType(driver, "EVOLUTION");
			verifyImageCard(driver, "icona_grigia", discrepanza);
			System.out.println("\nCardBiancoGrigia è stata verificata");
			break;
			
		case "luca.petrucci-abmo":
			System.out.println("\nImplementare immagine gialla");
			break;

		default:
			System.out.println("\nErrore");
			break;
		}
//		verifyImageCard(driver, "icona_blu", discrepanza);
		System.out.println("\nCardBiancoBlu è stata verificata");

	}
	
	public void clickOnCard(WebDriver driver, String numberInput) {
		
		getElementCardNumber(driver, numberInput).click();
		WaitManager.get().waitLongTime();
	}
	
//	public void clickOnCard(String cardNumber) {
//		Particle card=(Particle) UiObjectRepo.get().get(HowWontPayRechargeOtherPostepay.NUMBERCARDSENDERRECHARGE);
//		String nuovoLocator=Utility.replacePlaceHolders(card.getLocator(), new Entry("title",cardNumber));
//		
//		((AppiumDriver<?>)page.getDriver()).findElement(By.xpath(nuovoLocator)).click();
//		
//		WaitManager.get().waitMediumTime();
//	}
	
	public void clickOnLinkBancoPosta() {
		Particle card=(Particle) UiObjectRepo.get().get(HowWontPayRechargeOtherPostepay.LINKAPPBANCOPOSTA);
		
		card.getElement().click();
		
		WaitManager.get().waitMediumTime();
		
	}
}

