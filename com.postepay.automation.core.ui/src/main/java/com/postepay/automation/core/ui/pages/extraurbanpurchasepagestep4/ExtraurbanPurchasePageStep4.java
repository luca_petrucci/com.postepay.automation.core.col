package com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep4;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.molecules.BodyDetailsOfTravelersExtraurbanPurchase;


public class ExtraurbanPurchasePageStep4 extends Page {
	public static final String NAME="T063";
	

	public ExtraurbanPurchasePageStep4(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(BodyDetailsOfTravelersExtraurbanPurchase.NAME, UiObjectRepo.get().get(BodyDetailsOfTravelersExtraurbanPurchase.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new ExtraurbanPurchasePageStep4Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new ExtraurbanPurchasePageStep4ManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeaderpage(String inputTitle) {
		((ExtraurbanPurchasePageStep4Manager)this.manager).verifyHeaderpage(inputTitle);
	}
	
	public void sendName(String name) {
		((ExtraurbanPurchasePageStep4Manager)this.manager).sendName(name);
	}
	
	public void sendSurname(String surname) {
		((ExtraurbanPurchasePageStep4Manager)this.manager).sendSurname(surname);
	}
	
	public void sendCard(String card) {
		((ExtraurbanPurchasePageStep4Manager)this.manager).sendCard(card);
	}
	
	public void sendEmail(String email) {
		((ExtraurbanPurchasePageStep4Manager)this.manager).sendEmail(email);
	}
	
	public void sendTelephono(String telephone) {
		((ExtraurbanPurchasePageStep4Manager)this.manager).sendTelephono(telephone);
	}
	
	public void clickOnProceed() {
		((ExtraurbanPurchasePageStep4Manager)this.manager).clickOnProceed();
	}

}

