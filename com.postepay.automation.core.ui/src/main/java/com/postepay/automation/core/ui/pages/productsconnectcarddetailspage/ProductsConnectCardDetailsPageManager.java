package com.postepay.automation.core.ui.pages.productsconnectcarddetailspage;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.BodyProductsConnectCardDetail;
import com.postepay.automation.core.ui.molecules.GenericOverviewsCardDetails;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;

import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class ProductsConnectCardDetailsPageManager extends PageManager {

	Particle card = (Particle) UiObjectRepo.get().get(BodyProductsConnectCardDetail.FRAMELAYOUTCONNECTCARD);
	Particle amountContabile = (Particle) UiObjectRepo.get().get(BodyProductsConnectCardDetail.COUNTINGBELANCEOFCARDDETAILS);
	Particle amountDisponibile = (Particle) UiObjectRepo.get().get(BodyProductsConnectCardDetail.AVAIBLEBELANCEOFCARDDETAILS);
	Particle recurrentRechege = (Particle) UiObjectRepo.get().get(BodyProductsConnectCardDetail.AUTOMATICRECHARGEINDETAILCARD);
	
	public ProductsConnectCardDetailsPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeader(String inputTitle) {

		LayoutTools tool=new LayoutTools();
		tool.verifyHeaderPage(inputTitle);
	}
	
	public void verifyLayoutPage() {
		
		assertTrue(card.getElement().isDisplayed());
		assertTrue(amountContabile.getElement().isDisplayed());
		assertTrue(amountDisponibile.getElement().isDisplayed());
		assertTrue(recurrentRechege.getElement().isDisplayed());
	}
	
	public double getAmountContabile() {
		String amount = amountContabile.getElement().getText();
		
		StringAndNumberOperationTools tool = new StringAndNumberOperationTools();
		double dAmount = tool.converAmountDisponibileAndContabile(amount);
		
		return dAmount;
	}
	
	public double getAmountDisponibile() {
		String amount = amountDisponibile.getElement().getText();
		
		StringAndNumberOperationTools tool = new StringAndNumberOperationTools();
		double dAmount = tool.converAmountDisponibileAndContabile(amount);
		
		return dAmount;
	}

	public void verifyDecrementoSaldo(double newCalContabile, double newCalDisponibile) {
		double actualContabile = getAmountContabile();
		double actualDisponibile = getAmountDisponibile();
		
//		assertTrue(newCalContabile == actualContabile);
		assertTrue("Saldo Disponibile "+ newCalDisponibile +"Actual Disponibile "+ actualDisponibile ,newCalDisponibile == actualDisponibile);		
	}

	public void scrollTo(String target) {
		for (int i = 0; i < 5; i++) {
			
			try {
				switch (target) {
				case "GOOGLEPAY":
					Particle targetP = (Particle) UiObjectRepo.get().get(BodyProductsConnectCardDetail.GOOGLEPAYLABEL);
					targetP.getElement();
					break;

				default:
					break;
				}
				return;
				
			} catch (Exception e) {
				UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 500);
				WaitManager.get().waitShortTime();
			}	
		}
	}

	public void enableGpatToggle() {
		Particle toggle = (Particle) UiObjectRepo.get().get(BodyProductsConnectCardDetail.GOOGLEPAYTOGGLE);
		
		assertTrue("Toggle GPAY gia attivo", toggle.getElement().getText().equals("No"));
		toggle.getElement().click();
		
		WaitManager.get().waitShortTime();
	}

	public void isGpatToggle() {
		Particle toggle = (Particle) UiObjectRepo.get().get(BodyProductsConnectCardDetail.GOOGLEPAYTOGGLE);

		assertTrue("Toggle GPAY non è attivo", toggle.getElement().getText().equals("Sì"));
		
	}

	public void disableGpayToggle() {
		Particle toggle = (Particle) UiObjectRepo.get().get(BodyProductsConnectCardDetail.GOOGLEPAYTOGGLE);
		assertTrue("Toggle GPAY non è attivo", toggle.getElement().getText().equals("Sì"));
		toggle.getElement().click();
		
		WaitManager.get().waitShortTime();
	}

	public void isNotGpatToggle() {
		Particle toggle = (Particle) UiObjectRepo.get().get(BodyProductsConnectCardDetail.GOOGLEPAYTOGGLE);

		assertTrue("Toggle GPAY è attivo", toggle.getElement().getText().equals("No"));
		
	}
	
	public void clickOnBack()
	{
		Particle back = (Particle) UiObjectRepo.get().get(HeaderGenericAllPage.LEFTBUTTONGENERICICON);
		back.getElement().click();
	}
}

