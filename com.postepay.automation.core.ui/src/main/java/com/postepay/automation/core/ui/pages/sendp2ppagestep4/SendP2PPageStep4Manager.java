package com.postepay.automation.core.ui.pages.sendp2ppagestep4;

import com.postepay.automation.core.ui.molecules.AmoutCreditToSend;
import com.postepay.automation.core.ui.molecules.AmoutCreditToSendSummary;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class SendP2PPageStep4Manager extends PageManager {

	public SendP2PPageStep4Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void clickOnSendButton() {

		Particle btn=(Particle) UiObjectRepo.get().get(AmoutCreditToSendSummary.SENDBUTTONP2PSEND);

		btn.getElement().click();	
	}
}

