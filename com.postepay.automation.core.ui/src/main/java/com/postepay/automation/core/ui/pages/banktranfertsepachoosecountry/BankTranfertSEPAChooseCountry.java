package com.postepay.automation.core.ui.pages.banktranfertsepachoosecountry;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.ChooseCountryOnList;


public class BankTranfertSEPAChooseCountry extends Page {
	public static final String NAME="T036";
	

	public BankTranfertSEPAChooseCountry(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(ChooseCountryOnList.NAME, UiObjectRepo.get().get(ChooseCountryOnList.NAME), false);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new BankTranfertSEPAChooseCountryManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void selectCountry() {
		
		((BankTranfertSEPAChooseCountryManager)this.manager).selectCountry();
		
	}

}

