package com.postepay.automation.core.ui.pages.sendp2ppagestep1;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.HowToPayGeneral;

import io.appium.java_client.android.AndroidDriver;


public class SendP2PPageStep1 extends Page {
	public static final String NAME="T008";
	

	public SendP2PPageStep1(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(HowToPayGeneral.NAME, UiObjectRepo.get().get(HowToPayGeneral.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SendP2PPageStep1Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	public void clickOnCard(WebDriver driver, String cardTarget) {
		((SendP2PPageStep1Manager)this.manager).clickOnCard(driver, cardTarget);
	}
	
	public void verifyHeaderPage(String titleInput) {
		
		((SendP2PPageStep1Manager)this.manager).verifyHeaderPage(titleInput);
	}
	public void replaceCardToUseForPay(WebDriver driver, String cardTarget) {
		
		((SendP2PPageStep1Manager)this.manager).replaceCardToUseForPay(driver, cardTarget);
	}
}

