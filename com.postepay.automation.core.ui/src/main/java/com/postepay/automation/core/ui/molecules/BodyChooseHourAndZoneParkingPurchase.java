package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyChooseHourAndZoneParkingPurchase extends Molecule {
	public static final String NAME="M087";
	
	public static final String ENDDATEPARKINGPURCHASE="endDateParkingPurchase";
public static final String TARGCARPARKINGPURCHASE="targCarParkingPurchase";
public static final String LABELCOPYPARKINGPURCHASE="labelCopyParkingPurchase";
public static final String ENDTIMEPARKINGPURCHASE="endTimeParkingPurchase";
public static final String PRICEHOURPARKINGPURCHASE="priceHourParkingPurchase";
public static final String ICONPARKINGPURCHASECHOOSEHOUR="iconParkingPurchaseChooseHour";
public static final String MINUSPARKINGPURCHASE="minusParkingPurchase";
public static final String PAYBUTTONPARKINGPURCHASE="payButtonParkingPurchase";
public static final String ZONEPARKINGPURCHASECHOOSEHOUR="zoneParkingPurchaseChooseHour";
public static final String PRICEPARKINGPURCHASE="priceParkingPurchase";
public static final String PLUSPARKINGPUCHASE="plusParkingPuchase";
public static final String DURATIONPARKINGPUCHASE="durationParkingPuchase";


	public BodyChooseHourAndZoneParkingPurchase(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

