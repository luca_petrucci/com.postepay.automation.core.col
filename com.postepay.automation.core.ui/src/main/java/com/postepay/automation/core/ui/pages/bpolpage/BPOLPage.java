package com.postepay.automation.core.ui.pages.bpolpage;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.AbilitaInAppMolecola;
import com.postepay.automation.core.ui.molecules.BPOLMolecola;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;

import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.android.nativekey.PressesKey;


public class BPOLPage extends Page {
	public static final String NAME="T140";
	

	public BPOLPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(BPOLMolecola.NAME, UiObjectRepo.get().get(BPOLMolecola.NAME), false);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new BPOLPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void inserisciCredenziali(String username, String password, String NumeroCarta)
	{
		((BPOLPageManager)this.manager).inserisciCredenziali(username, password, NumeroCarta);
	}
	
	public void utenteCliccaProfilo() 
	{
		((BPOLPageManager)this.manager).utenteCliccaProfilo();
	}
	public void utenteAccedeAPostepay(String numeroCarta) 
	{
		((BPOLPageManager)this.manager).utenteAccedeAPostepay(numeroCarta);
	}
	
	public void invioAutorizzazionePostepay()
	{
		((BPOLPageManager)this.manager).invioAutorizzazionePostepay();

	}
	
	public void layoutAreaPersonalePostePay(String cardNumber) {
		((BPOLPageManager)this.manager).layoutAreaPersonalePostePay(cardNumber);
	}

	public void openMobileChrome() {
		((BPOLPageManager)this.manager).openMobileChrome();
		
	}

	public void loginMobile(String username, String password, String cardNumber) {
		((BPOLPageManager)this.manager).loginMobile(username, password, cardNumber);
		
	}

	public void utenteInviaLautorizzativa(String cardNumber) {
		((BPOLPageManager)this.manager).utenteInviaLautorizzativa(cardNumber);
		
	}

	public void goToPosteApp() {
		((BPOLPageManager)this.manager).goToPosteApp();
		
	}


}

