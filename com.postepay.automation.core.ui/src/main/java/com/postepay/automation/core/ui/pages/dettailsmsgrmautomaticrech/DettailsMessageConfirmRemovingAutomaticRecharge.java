package com.postepay.automation.core.ui.pages.dettailsmsgrmautomaticrech;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.DettailsMessageConfirmRemovingAutomaticRechargeMolecola;

import io.appium.java_client.android.AndroidDriver;


public class DettailsMessageConfirmRemovingAutomaticRecharge extends Page {
	public static final String NAME="T096";
	

	public DettailsMessageConfirmRemovingAutomaticRecharge(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(DettailsMessageConfirmRemovingAutomaticRechargeMolecola.NAME, UiObjectRepo.get().get(DettailsMessageConfirmRemovingAutomaticRechargeMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new DettailsMessageConfirmRemovingAutomaticRechargeManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderPage() {
		((DettailsMessageConfirmRemovingAutomaticRechargeManager)this.manager).verifyHeaderPage();

	}

	public void verifyMessageDetailDescription() {
		((DettailsMessageConfirmRemovingAutomaticRechargeManager)this.manager).verifyMessageDetailDescription();
	}

	public void clickOnCloseButton() {
		((DettailsMessageConfirmRemovingAutomaticRechargeManager)this.manager).clickOnCloseButton();
	}
	
	public void verifyLayoutMessage(WebDriver driver, String typeInput, String dateInput, String nameInput) {
		((DettailsMessageConfirmRemovingAutomaticRechargeManager)this.manager).verifyLayoutMessage(driver, typeInput, dateInput, nameInput);
	}
	
	public void verifyMessage(String typeInput, String dateInput, String nameInput) {
		((DettailsMessageConfirmRemovingAutomaticRechargeManager)this.manager).verifyMessage(typeInput, dateInput, nameInput);
	}
}

