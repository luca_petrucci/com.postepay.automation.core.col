package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class AbilitaInAppMolecola extends Molecule {
	public static final String NAME="M242";

	public static final String NONAUTORIZZAABILITAINAPP="nonAutorizzaAbilitaInApp";
	public static final String ICONAGIALLAAUTORIZZAABILITAINAPP="iconaGiallaAutorizzaAbilitaInApp";
	public static final String LINKCAMBIAREDISPOSITIVOABILITAINAPP="linkCambiaredispositivoAbilitaInApp";
	public static final String VOPYNONAUTORIZZAABILITAINAPP="vopyNonAutorizzaAbilitaInApp";
	public static final String SCEGLILABELABILITAINAPP="scegliLabelAbilitaInApp";
	public static final String AUTORIZZATITOLOABILITAINAPP="autorizzaTitoloAbilitaInApp";
	public static final String COPYAUTORIZZAABILITAINAPP="copyAutorizzaAbilitaInApp";
	public static final String ICONAGIALLANONAUTORIZZAABILITAINAPP="iconaGiallaNonAutorizzaAbilitaInApp";
	public static final String M242POPUPCONTINUA = "m242PopupContinua";
	public static final String M242SMSBTNCONFERMA = "m242smsBtnConferma";
	public static final String M242INIZIAONBDESC = "m242IniziaOnbDesc";
	public static final String M242SELEZIONACARTA = "m242SelezionaCarta";
	public static final String M242SCADENZA = "m242Scadenza";
	public static final String M242SCADENZACONFERMA = "m242ScadenzaConferma";
	public static final String M242CVV = "m242Cvv";
	public static final String M242CVVCONFERMA = "m242CvvConferma";
	public static final String M242CREAPOSTEIDBTN = "m242CreaPosteIdBtn";
	public static final String M242POSTEIDUNO = "m242PosteIdUno";
	public static final String M242POSTEIDDUE = "m242PosteIdDue";
	public static final String M242CONFERMAPOSTEIDBTN = "m242ConfermaPosteIdBtn";
	public static final String M242CARICAMENTO = "m242Caricamento";
	public static final String M242TYPACCEDI = "m242TYPAccedi";

	public static final String M242PROSEGUIBTN = "m24ProseguiBtn";


	public AbilitaInAppMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

