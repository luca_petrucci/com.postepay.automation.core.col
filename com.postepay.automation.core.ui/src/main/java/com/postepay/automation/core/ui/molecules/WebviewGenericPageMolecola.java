package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class WebviewGenericPageMolecola extends Molecule {
	public static final String NAME="M201";
	
	public static final String WEBVIEWGENERICPAGEPARTICLE="webviewGenericPageParticle";
	public static final String CLOSEBUTTONINFOBAR ="closeButtonInfobar";
	public static final String GOOGLEPLAYSTOREWEBVIEW ="googlePlayStoreWebVIew";
	public static final String GOOGLEPLAYBANCOPOSTAHEADERWEBVIEW ="googlePlayBancopostaHeaderWebView";
	public static final String GOOGLEPLAYBANCOPOSTAPOSTESPAWEBVIEW	="googlePlayBancopostaPosteSpaWebView";


	public WebviewGenericPageMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

