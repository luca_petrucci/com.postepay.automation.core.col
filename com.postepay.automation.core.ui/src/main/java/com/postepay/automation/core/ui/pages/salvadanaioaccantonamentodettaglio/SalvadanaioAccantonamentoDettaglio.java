package com.postepay.automation.core.ui.pages.salvadanaioaccantonamentodettaglio;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.SalvadanaioAccantonamentoDettaglioMolecola;

import io.appium.java_client.android.AndroidDriver;


public class SalvadanaioAccantonamentoDettaglio extends Page {
	public static final String NAME="T112";
	

	public SalvadanaioAccantonamentoDettaglio(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(SalvadanaioAccantonamentoDettaglioMolecola.NAME, UiObjectRepo.get().get(SalvadanaioAccantonamentoDettaglioMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SalvadanaioAccantonamentoDettaglioManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderpage(String titlePageInput) {
		((SalvadanaioAccantonamentoDettaglioManager)this.manager).verifyHeaderpage(titlePageInput);
	}
	
	public void verifyLayoutPage(WebDriver driver, String nameInput, String typeObiettivoInput, double discrepanza) {
		((SalvadanaioAccantonamentoDettaglioManager)this.manager).verifyLayoutPage(driver, nameInput, typeObiettivoInput, discrepanza);
	}
	
	public void isObiettivoAutomatico(WebDriver driver, String isRicorrente, double discrepanza) {
		((SalvadanaioAccantonamentoDettaglioManager)this.manager).isObiettivoAutomatico(driver, isRicorrente, discrepanza);
	}
	
	public void clickOnVersaConPostePay() {
		((SalvadanaioAccantonamentoDettaglioManager)this.manager).clickOnVersaConPostePay();
	}
	
	public void clickOnGestisciInAppBancoposta() {
		((SalvadanaioAccantonamentoDettaglioManager)this.manager).clickOnGestisciInAppBancoposta();
	}
	
	public void verifyAutomatismoIcon(WebDriver driver, double discrepanza) {
		((SalvadanaioAccantonamentoDettaglioManager)this.manager).verifyAutomatismoIcon(driver, discrepanza);
	}
	
	public double getImportoVersatoSuObiettivo() {
		return ((SalvadanaioAccantonamentoDettaglioManager)this.manager).getImportoVersatoSuObiettivo();
	}
	
	public void isImportoUpdated(String importoDaVersare, double importoPrimaVersamento, double importoDopoVersamento) {
		((SalvadanaioAccantonamentoDettaglioManager)this.manager).isImportoUpdated(importoDaVersare, importoPrimaVersamento, importoDopoVersamento);
	}
	
	public void clickOnVersamentoSingolo()
	{
		((SalvadanaioAccantonamentoDettaglioManager)this.manager).clickOnVersamentoSingolo();

	}
}

