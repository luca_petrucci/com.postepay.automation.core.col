package com.postepay.automation.core.ui.pages.banktranfertsepapagestep1;

import com.postepay.automation.core.ui.molecules.BodyPaymentPaymentSection;
import com.postepay.automation.core.ui.molecules.HowToPayGeneral;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import org.openqa.selenium.WebDriver;

public class BankTranfertSEPAPageStep1Manager extends PageManager {

	public BankTranfertSEPAPageStep1Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	LayoutTools toolLayout = new LayoutTools();
	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}
	
	public void replaceCardToUseForPay(WebDriver driver, String cardTarget) {

		LayoutTools tool = new LayoutTools();
		tool.replaceCardToUseForPay(driver, cardTarget);
	}
	
	public void clickOnCard(WebDriver driver, String cardTarget) {
		
		replaceCardToUseForPay(driver, cardTarget);
		
	}
}

