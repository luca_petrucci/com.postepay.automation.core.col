package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class MinisteroTrasportiMolecola extends Molecule {
	public static final String NAME="M271";
	public static final String BTNNAZIONALEMINISTERO="btnNazionaleMinistero";
	public static final String BTNTRENTOMINISTERO="btnTrentoMinistero";
	public static final String LABELTIPOLOGIAPRATICA="labelTipologiaPratica";
	public static final String LABELPRATICA="labelPratica";
	public static final String LABELDATIPAGANTE="labelDatiPagante";
	public static final String BTNCANCELLAPAGANTE="btnCancellaPagante";
	public static final String CODICEFISCALEPAGANTE="codiceFiscalePagante";
	public static final String BTNCALCOLAMINISTERO="btnCalcolaMinistero";
	public static final String FIRSTELEMENTOFLISTMINISTERO="firstElementOfListMinistero";

	public MinisteroTrasportiMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

