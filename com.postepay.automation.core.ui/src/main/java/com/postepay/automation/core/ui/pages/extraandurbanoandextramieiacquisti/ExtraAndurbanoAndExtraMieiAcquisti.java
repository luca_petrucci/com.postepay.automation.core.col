package com.postepay.automation.core.ui.pages.extraandurbanoandextramieiacquisti;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.ExtraAndurbanoAndExtraMieiAcquistiMolecola;

import io.appium.java_client.android.AndroidDriver;


public class ExtraAndurbanoAndExtraMieiAcquisti extends Page {
	public static final String NAME="T106";
	

	public ExtraAndurbanoAndExtraMieiAcquisti(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(ExtraAndurbanoAndExtraMieiAcquistiMolecola.NAME, UiObjectRepo.get().get(ExtraAndurbanoAndExtraMieiAcquistiMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new ExtraAndurbanoAndExtraMieiAcquistiManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyLayoutPage(WebDriver driver, String targetOperation, String corsaTitleInput, String compagniaInput, String fromCityInput, String toCityInput, double discrepanza) {
		((ExtraAndurbanoAndExtraMieiAcquistiManager)this.manager).verifyLayoutPage(driver, targetOperation, corsaTitleInput, compagniaInput, fromCityInput, toCityInput, discrepanza);
	}
	public void clickOnDownloadPDF() {
		((ExtraAndurbanoAndExtraMieiAcquistiManager)this.manager).clickOnDownloadPDF();
	}
}

