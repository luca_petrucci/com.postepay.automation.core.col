package com.postepay.automation.core.ui.pages.salvadanaioaccantonamentodettaglio;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.SalvadanaioAccantonamentoDettaglioMolecola;
import com.postepay.automation.core.ui.molecules.SalvadanaioPageMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class SalvadanaioAccantonamentoDettaglioManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();
	StringAndNumberOperationTools toolNumber = new StringAndNumberOperationTools();
	
	Particle iconaYellow = (Particle) UiObjectRepo.get().get(SalvadanaioAccantonamentoDettaglioMolecola.SALVADANAIODETTAGLIOICONAYELLOW);
	Particle dettaglioNome = (Particle) UiObjectRepo.get().get(SalvadanaioAccantonamentoDettaglioMolecola.SALVADANAIODETTAGLIONOME);
	Particle dettaglioCondividiBtn = (Particle) UiObjectRepo.get().get(SalvadanaioAccantonamentoDettaglioMolecola.SALVADANAIODETTAGLIOCONDIVIDIBTN);
	Particle dettaglioActualAmount = (Particle) UiObjectRepo.get().get(SalvadanaioAccantonamentoDettaglioMolecola.SALVADANAIODETTAGLIOACTUALAMOUNT);
	Particle dettaglioTotalAmount = (Particle) UiObjectRepo.get().get(SalvadanaioAccantonamentoDettaglioMolecola.SALVADANAIODETTAGLIOTOTALAMOUNT);
	Particle dettaglioBarStatus = (Particle) UiObjectRepo.get().get(SalvadanaioAccantonamentoDettaglioMolecola.SALVADANAIODETTAGLIOBARSTATUS);
	Particle dettaglioRisparmiaAlGiorno = (Particle) UiObjectRepo.get().get(SalvadanaioAccantonamentoDettaglioMolecola.SALVADANAIODETTAGLIORISPARMIAALGIORNO);
	Particle dettaglioDataAdesioneValore = (Particle) UiObjectRepo.get().get(SalvadanaioAccantonamentoDettaglioMolecola.SALVADANAIODETTAGLIODATAADESIONEVALORE);
	Particle dettaglioDataAdesioneText = (Particle) UiObjectRepo.get().get(SalvadanaioAccantonamentoDettaglioMolecola.SALVADANAIODETTAGLIODATAADESIONETEXT);
	Particle dettaglioDataScadenzaText = (Particle) UiObjectRepo.get().get(SalvadanaioAccantonamentoDettaglioMolecola.SALVADANAIODETTAGLIODATASCADENZATEXT);
	Particle dettaglioDataScadenzaValore = (Particle) UiObjectRepo.get().get(SalvadanaioAccantonamentoDettaglioMolecola.SALVADANAIODETTAGLIODATASCADENZAVALORE);
	Particle dettaglioTestBluLink = (Particle) UiObjectRepo.get().get(SalvadanaioAccantonamentoDettaglioMolecola.SALVADANAIODETTAGLIOTESTBLULINK);
	Particle dettaglioVersaConPostePayButton = (Particle) UiObjectRepo.get().get(SalvadanaioAccantonamentoDettaglioMolecola.SALVADANAIODETTAGLIOVERSACONPOSTEPAYBUTTON);
	Particle dettaglioVersaConPostePayText = (Particle) UiObjectRepo.get().get(SalvadanaioAccantonamentoDettaglioMolecola.SALVADANAIODETTAGLIOVERSACONPOSTEPAYTEXT);
	Particle dettaglioGestisciInBancoPosta = (Particle) UiObjectRepo.get().get(SalvadanaioAccantonamentoDettaglioMolecola.SALVADANAIODETTAGLIOGESTISCIINBANCOPOSTA);
	Particle dettaglioTrePuntiniListaVersamenti = (Particle) UiObjectRepo.get().get(SalvadanaioAccantonamentoDettaglioMolecola.SALVADANAIODETTAGLIOTREPUNTINILISTAVERSAMENTI);
	Particle dettaglioIconaOrologioAccantonamentoAutomatico = (Particle) UiObjectRepo.get().get(SalvadanaioAccantonamentoDettaglioMolecola.SALVADANAIODETTAGLIOICONAOROLOGIOACCANTONAMENTOAUTOMATICO);


	
	public SalvadanaioAccantonamentoDettaglioManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}

	public WebElement getElementName(WebDriver driver, String nameInput) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, dettaglioNome, nameInput.toUpperCase());
		assertTrue(elm.isDisplayed());
		return elm;
	}

	public void verifyImage(WebDriver driver, String imageNameInput, double discrepanza) {
//		String imageName = "salvadanaio/" + imageNameInput;
//		LayoutImage.get().verifyImage(driver, imageName, discrepanza);
	}
	
	public void verifyLayoutPage(WebDriver driver, String nameInput, String typeObiettivoInput, double discrepanza) {

		assertTrue(iconaYellow.getElement().isDisplayed());
		getElementName(driver, nameInput);
		assertTrue(dettaglioCondividiBtn.getElement().isDisplayed());
		assertTrue(dettaglioActualAmount.getElement().isDisplayed());
		assertTrue(dettaglioTotalAmount.getElement().isDisplayed());
		assertTrue(dettaglioBarStatus.getElement().isDisplayed());
		assertTrue(dettaglioRisparmiaAlGiorno.getElement().isDisplayed());
		assertTrue(dettaglioDataAdesioneValore.getElement().isDisplayed());
		assertTrue(dettaglioDataAdesioneText.getElement().isDisplayed());
		assertTrue(dettaglioDataScadenzaText.getElement().isDisplayed());
		assertTrue(dettaglioDataScadenzaValore.getElement().isDisplayed());
		assertTrue(dettaglioTestBluLink.getElement().isDisplayed());
		assertTrue(dettaglioVersaConPostePayButton.getElement().isDisplayed());
		assertTrue(dettaglioVersaConPostePayText.getElement().isDisplayed());
		assertTrue(dettaglioGestisciInBancoPosta.getElement().isDisplayed());
		
		// Verifica sulle immagini
		switch (typeObiettivoInput) {
		case "viaggi":
			System.out.println("Vedifica del tipo immagini - VIAGGI");
			verifyImage(driver, "viaggi_icon", discrepanza);
			break;
		case "salute":
			System.out.println("Vedifica del tipo immagini - SALUTE E BENESSERE");
			verifyImage(driver, "salute_icon", discrepanza);
			break;
		case "risparmio":
			System.out.println("Vedifica del tipo immagini - RISPARMIO");
			verifyImage(driver, "risparmio_icon", discrepanza);
			break;
		default:
			System.out.println("Errore nel tipo immagini");
			break;
		}
		System.out.println("Vedifica del tipo immagini - Bottone Paga");
		verifyImage(driver, "dettaglio_paga", discrepanza);
	}
	
	public void isObiettivoAutomatico(WebDriver driver, String isRicorrente, double discrepanza) {
		if (isRicorrente.equals("false")) {
			System.out.println("Vedifica del tipo immagini - Solo icona condividi");
			verifyImage(driver, "dettaglio", discrepanza);
			
		} else if (isRicorrente.equals("true")) {
			assertTrue(dettaglioTrePuntiniListaVersamenti.getElement().isDisplayed());
			assertTrue(dettaglioIconaOrologioAccantonamentoAutomatico.getElement().isDisplayed());
			System.out.println("Vedifica del tipo immagini - Tutte e 3 le icone");
			verifyImage(driver, "dettaglio_ricorrente", discrepanza);
			
		} else {
			System.out.println("isObiettivoAutomatico: Errore nel tipo immagini");
		}
	}
	
	public void clickOnVersaConPostePay() {
		dettaglioVersaConPostePayText.getElement().click();
		WaitManager.get().waitLongTime();
	}
	
	public void clickOnGestisciInAppBancoposta() {
		dettaglioGestisciInBancoPosta.getElement().click();
		WaitManager.get().waitLongTime();
	}
	
	public void verifyAutomatismoIcon(WebDriver driver, double discrepanza) {
		verifyImage(driver, "dettaglio_ricorrente", discrepanza);
		WaitManager.get().waitShortTime();
	}
	
	public double getImportoVersatoSuObiettivo() {
		String importoString = dettaglioActualAmount.getElement().getText().replace("€ ", "");
		double importo = toolNumber.convertStringToDouble(importoString);
		return importo;
	}
	
	public void isImportoUpdated(String importoDaVersare, double importoPrimaVersamento, double importoDopoVersamento) {
		double daVersare = toolNumber.convertStringToDouble(importoDaVersare);
		double som = daVersare + importoPrimaVersamento;
		double somma = (som * 100) / 100.0;
		double d = roundDouble(somma, 2);
		System.out.println("La somma sull'obiettivo dovrebbe essere: " + d);
		System.out.println("L'importo sull' obiettivo è: " + importoDopoVersamento);
		assertTrue(d == importoDopoVersamento);
	}
	
	double roundDouble(double d, int places) {
		 
        BigDecimal bigDecimal = new BigDecimal(Double.toString(d));
        bigDecimal = bigDecimal.setScale(places, RoundingMode.HALF_UP);
        return bigDecimal.doubleValue();
    }
	
	public void clickOnVersamentoSingolo()
	{
		Particle versamentoSingolo = (Particle) UiObjectRepo.get().get(SalvadanaioAccantonamentoDettaglioMolecola.SALVADANAIOVERSAMENTOSINGOLO);
		versamentoSingolo.getElement().click();
		WaitManager.get().waitShortTime();

	}
}

