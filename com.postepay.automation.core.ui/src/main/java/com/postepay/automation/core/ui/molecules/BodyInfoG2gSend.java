package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyInfoG2gSend extends Molecule {
	public static final String NAME="M031";
	
	public static final String MESSAGESENDGIGA="messageSendGiga";
public static final String COPYHEADERSENDGIGA="copyHeaderSendGiga";
public static final String COPYDESCRIPTIONSENDGIGA="copyDescriptionSendGiga";
public static final String BUTTONSENDGIGA="buttonSendGiga";
public static final String IMAGESENDGIGA="imageSendGiga";


	public BodyInfoG2gSend(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

