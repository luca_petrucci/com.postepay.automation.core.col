package com.postepay.automation.core.ui.pages.bolloautomotot139;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;
import com.postepay.automation.core.ui.molecules.BolloAutoMotoCalcoloM262;
import com.postepay.automation.core.ui.molecules.BolloAutoMotoDaPagareM263;
import com.postepay.automation.core.ui.molecules.BolloAutoMotoRiepilogoM264;


public class BolloAutoMotoT139 extends Page {
	public static final String NAME="T139";
	

	public BolloAutoMotoT139(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(BolloAutoMotoCalcoloM262.NAME, UiObjectRepo.get().get(BolloAutoMotoCalcoloM262.NAME), false);
		this.addToTemplate(BolloAutoMotoDaPagareM263.NAME, UiObjectRepo.get().get(BolloAutoMotoDaPagareM263.NAME), false);
		this.addToTemplate(BolloAutoMotoRiepilogoM264.NAME, UiObjectRepo.get().get(BolloAutoMotoRiepilogoM264.NAME), false);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new BolloAutoMotoT139Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeaderTitle(String title)
	{
		((BolloAutoMotoT139Manager)this.manager).verifyHeaderTitle(title);
	}
	
	public void inserisciRegione(String regioneInput)
	{
		((BolloAutoMotoT139Manager)this.manager).inserisciRegione(regioneInput);
	}
	
	public void inserisciVeicolo(String veicoloInput)
	{
		((BolloAutoMotoT139Manager)this.manager).inserisciVeicolo(veicoloInput);
	}
	
	public void inserisciTarga(String targaInput)
	{
		((BolloAutoMotoT139Manager)this.manager).inserisciTarga(targaInput);
	}
	
	public void verifyLayoutCalcolaM262(String copyInput, String annualitaInput, String btnCalcolaInput, String regioneInput, String targaInput, String veicoloInput)
	{
		((BolloAutoMotoT139Manager)this.manager).verifyLayoutCalcolaM262(copyInput, annualitaInput, btnCalcolaInput, regioneInput, targaInput, veicoloInput);
	}
	
	public void clickBtnCalcolaM262()
	{
		((BolloAutoMotoT139Manager)this.manager).clickBtnCalcolaM262();
	}
	
	public void verifyLayoutCalcolaM263(String targaInput, String veicoloInput, String titleInput, String btnContinuaInput, String pagaConInput)
	{
		((BolloAutoMotoT139Manager)this.manager).verifyLayoutCalcolaM263(targaInput, veicoloInput, titleInput, btnContinuaInput, pagaConInput);
	}
	
	public String getImportoM263()
	{
		return ((BolloAutoMotoT139Manager)this.manager).getImportoM263();
	}
	
	public void clickBtnContinuaM263()
	{
		((BolloAutoMotoT139Manager)this.manager).clickBtnContinuaM263();

	}
	public void verifyLayoutSummaryM264(String titleInput, String regioneInput, String targaInput, String importoInput, String commissioneInput, String pagaConInput, String btnAnnullaInput)
	{
		((BolloAutoMotoT139Manager)this.manager).verifyLayoutSummaryM264(titleInput, regioneInput, targaInput, importoInput, commissioneInput, pagaConInput, btnAnnullaInput);
	}
	
	public void clickBtnPagaM264()
	{
		((BolloAutoMotoT139Manager)this.manager).clickBtnPagaM264();;
	}
	
	public void clickBtnAnnullaM264()
	{
		((BolloAutoMotoT139Manager)this.manager).clickBtnAnnullaM264();;
	}

}

