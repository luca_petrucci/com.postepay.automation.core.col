package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyAutomaticRechargePostepay extends Molecule {
	public static final String NAME="M071";
	
	public static final String TODATEOFRECURSIVERECHARGE="toDateOfRecursiveRecharge";
	public static final String NAMEOFRECURSIVERECHARGE="nameOfRecursiveRecharge";
	public static final String COPYTHRESHOLDRECTANGLERECHARGEPOSTEPAY="copyThresholdRectangleRechargePostePay";
	public static final String SETTINGTIMERECURSIVERECHARGE="settingTimeRecursiveRecharge";
	public static final String THRESHOLDRECTANGLERECHARGEPOSTEPAY="thresholdRectangleRechargePostePay";
	public static final String AMOUNTMINIMOSOGLIA = "amountSogliaMinima";
	public static final String SWITCHSOGLIABUTTON = "switchSogliaButton";
	
	public BodyAutomaticRechargePostepay(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

