package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class CredentialsCardLogin extends Molecule {
	public static final String NAME="M003";
	
	public static final String USERNAMELOGIN = "userNameLogin";
	public static final String PASSWORDLOGIN = "passwordLogin";
	public static final String SIGNINBUTTONLOGIN = "signInButtonLogin";
	public static final String LOSTCREDENTIALLINKDURINGLOGIN = "lostCredentialLinkDuringLogin";
	public static final String REMEMBERMYCREDENTIALLOGIN = "rememberMyCredentialLogin";
	public static final String BENVENUTITEXT = "benvenutiText";
	public static final String BENVENUTISUBTEXT = "benvenutiSubText";
	public static final String OPPUREREGISTRATI = "oppureRegistrati";


	public CredentialsCardLogin(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

