package com.postepay.automation.core.ui.pages.deleteautomaticrechargepage;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.AutomaticRechargePostepaySectionPageMolecola;
import com.postepay.automation.core.ui.molecules.DeleteAutomaticRechargePageMolecola;

import test.automation.core.UIUtils;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class DeleteAutomaticRechargePageManager extends PageManager {

	public DeleteAutomaticRechargePageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderPopUp() {
		Particle automaticRecharge=(Particle) UiObjectRepo.get().get(DeleteAutomaticRechargePageMolecola.HEADERPOPUPDELETEAUTOMATICRECHARGE);
		String nuovoLocator=Utility.replacePlaceHolders(automaticRecharge.getLocator(), new Entry("title","Avviso"));
		
		WebDriver driver=UIUtils.ui().getCurrentDriver();
		WebElement elem=driver.findElement(By.xpath(nuovoLocator));
		
		try {
			assertTrue(elem.isDisplayed());
		
		} catch (Exception e) {
			System.out.println("Non è stato trovato il titolo");
		}	
	}
	
	public void verifyCopyPopUp() {
		Particle automaticRecharge=(Particle) UiObjectRepo.get().get(DeleteAutomaticRechargePageMolecola.COPYDELETEAUTOMATICRECHARGE);
		String nuovoLocator=Utility.replacePlaceHolders(automaticRecharge.getLocator(), new Entry("title","Sei sicuro di voler eliminare la ricarica automatica?"));
		
		WebDriver driver=UIUtils.ui().getCurrentDriver();                                                         
		WebElement elem=driver.findElement(By.xpath(nuovoLocator));
		
		try {
			assertTrue(elem.isDisplayed());
		
		} catch (Exception e) {
			System.out.println("Non è stato trovato il titolo");
		}
	}
	
	public void clickOnANNULLAbutton() {
		Particle automaticRecharge=(Particle) UiObjectRepo.get().get(DeleteAutomaticRechargePageMolecola.ANNULLBUTTONDELETEAUTOMATICRECHARGE);
		
		automaticRecharge.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnOKbutton() {
		Particle automaticRecharge=(Particle) UiObjectRepo.get().get(DeleteAutomaticRechargePageMolecola.OKBUTTONDELETEAUTOMATICRECHARGE);
		
		automaticRecharge.getElement().click();
		
		WaitManager.get().waitMediumTime();		
	}
}

