package com.postepay.automation.core.ui.pages.commentrechargep2ppage;

import com.postepay.automation.core.ui.molecules.CommentRechargeP2pMolecola;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class CommentRechargeP2pPageManager extends PageManager {

	public CommentRechargeP2pPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void inserComment(String comment) {
		
		Particle editComment=(Particle) UiObjectRepo.get().get(CommentRechargeP2pMolecola.FIELDTEXTINSERTCOMMENTP2P); 
	
		editComment.getElement().sendKeys(comment);
		
		try {Thread.sleep(2000);} catch (Exception err )  {}
	}
	
	public void clickOnConfirm() {
		
		Particle btn=(Particle) UiObjectRepo.get().get(CommentRechargeP2pMolecola.CONFIRMBUTTONCOMMENTP2P);
		
		btn.getElement().click();
		
		try {Thread.sleep(2000);} catch (Exception err )  {}
	}
}

