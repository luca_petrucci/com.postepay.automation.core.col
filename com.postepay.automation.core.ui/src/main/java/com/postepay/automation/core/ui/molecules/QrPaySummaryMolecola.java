package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class QrPaySummaryMolecola extends Molecule {
	public static final String NAME="M251";
	
	public static final String QRPAYSUMMARYPAYWITH="qrPaySummaryPayWith";
public static final String QRPAYSUMMARYDESCRIPTION="qrPaySummaryDescription";
public static final String QRPAYSUMMARYACTUALBILANCE="qrPaySummaryActualBilance";
public static final String QRPAYSUMMARYBTNANNUL="qrPaySummaryBtnAnnul";
public static final String QRPAYSUMMARYREQUESTDATE="qrPaySummaryRequestDate";
public static final String QRPAYSUMMARYAMOUNT="qrPaySummaryAmount";
public static final String QRPAYSUMMARYBTNPAY="qrPaySummaryBtnPay";
public static final String QRPAYSUMMARYMERCHANT="qrPaySummaryMerchant";
public static final String QRPAYSUMMARYCHANGECARD = "qrPaySummaryBtnChangeCard";

	public QrPaySummaryMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

