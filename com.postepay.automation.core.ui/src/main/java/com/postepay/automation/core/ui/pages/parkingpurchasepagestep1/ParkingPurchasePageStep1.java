package com.postepay.automation.core.ui.pages.parkingpurchasepagestep1;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.pages.mapsgenericscreenfromhomepage.MapsGenericScreenFromHomePageManager;
import com.postepay.automation.core.ui.pages.rechargemypostepaypagestep4.RechargeMyPostepayPageStep4Manager;
import com.postepay.automation.core.ui.molecules.BodyChooseParkingPurchase;


public class ParkingPurchasePageStep1 extends Page {
	public static final String NAME="T057";
	

	public ParkingPurchasePageStep1(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
this.addToTemplate(BodyChooseParkingPurchase.NAME, UiObjectRepo.get().get(BodyChooseParkingPurchase.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new ParkingPurchasePageStep1Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeaderpage(String titlePageInput)
	{
		((ParkingPurchasePageStep1Manager)this.manager).verifyHeaderpage(titlePageInput);
	}
	
	public void verifyLayout() {
		((ParkingPurchasePageStep1Manager)this.manager).verifyLayout();

	}
	
	public void clickOnParking() {
		((ParkingPurchasePageStep1Manager)this.manager).clickOnParking();

	}

}

