package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class AutomaticRechargeOnZeroRechargePageMolecola extends Molecule {
	public static final String NAME="M210";

	public static final String ICONEMPTYSTATEAUTOMATICRECHARGEONZERORECHARGEPAGE="iconEmptyStateAutomaticRechargeOnZeroRechargePage";
	public static final String STARTBUTTONAUTOMATICRECHARGEONZERORECHARGEPAGE="startButtonAutomaticRechargeOnZeroRechargePage";
	public static final String HEADERAUTOMATICRECHARGEONZERORECHARGEPAGE="headerAutomaticRechargeOnZeroRechargePage";
	public static final String COPYAUTOMATICRECHARGEONZERORECHARGEPAGE="copyAutomaticRechargeOnZeroRechargePage";
	public static final String ANNULBUTTONAUTOMATICRECHARGEONZERORECHARGEPAGE="annulButtonAutomaticRechargeOnZeroRechargePage";
	public static final String TITLEAUTOMATICRECHARGEONZERORECHARGEPAGE="titleAutomaticRechargeOnZeroRechargePage";


	public AutomaticRechargeOnZeroRechargePageMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

