package com.postepay.automation.core.ui.pages.sendp2ppagestep1;

import com.postepay.automation.core.ui.molecules.HowToPayGeneral;
import com.postepay.automation.core.ui.molecules.HowWontPayRechargeOtherPostepay;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class SendP2PPageStep1Manager extends PageManager {

	public SendP2PPageStep1Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void replaceCardToUseForPay(WebDriver driver, String cardTarget) {

		LayoutTools tool = new LayoutTools();
		tool.replaceCardToUseForPay(driver, cardTarget);
	}
	public void clickOnCard(WebDriver driver, String cardTarget) {
		
		replaceCardToUseForPay(driver, cardTarget);
		WaitManager.get().waitShortTime();
	}
	
	public void verifyHeaderPage(String titleInput) {
		
		LayoutTools tool = new LayoutTools();
		tool.verifyHeaderPage(titleInput);
	}
	
}

