package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class HeaderGenericAllPage extends Molecule {
	public static final String NAME="M200";

	public static final String TITLEGENERICWITHTITLE="titleGenericWithTitle";
	public static final String LEFTBUTTONGENERIC="leftButtonGeneric";
	public static final String CHIUDIBUTTONGENERICICON="chiudiButtonGenericIcon";
	public static final String SALVABUTTONGENERICICON="salvaButtonGenericIcon";
	public static final String LEFTBUTTONGENERICICON="leftButtonGenericIcon";
	public static final String RIGHTBUTTONGENERIC="rightButtonGeneric";
	public static final String RIGHTBUTTONGENERICICON="rightButtonGenericIcon";
	public static final String ANNULLABUTTONGENERICICON="annullaButtonGenericIcon";
	public static final String TITLEPOSTEPAYLOGOHOMEPAGE="titlePostePayLogoHomePage";
	public static final String BACHECAICONHOMEPAGE="bachecaLogoHomePage";
	public static final String TASTOBACKALTERNATIVO = "tastoBackAlternativo";
	public static final String BACHECAICONAHOMEPAGE = "bachecaIconaHomepage";

	public HeaderGenericAllPage(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

