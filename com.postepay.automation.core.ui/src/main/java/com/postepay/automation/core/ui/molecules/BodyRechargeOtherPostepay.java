package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyRechargeOtherPostepay extends Molecule {
	public static final String NAME="M067";
	
	public static final String TELEPHONCONTACTSOTHERPOSTEPAY="telephonContactsOtherPostepay";
public static final String HOLDEROTHERPOSTEPAY="holderOtherPostepay";
public static final String NUMBEROTHERPOSTEPAY="numberOtherPostepay";
public static final String REASONOFRECHARGEOTHERPOSTEPAY="reasonOfRechargeOtherPostepay";
public static final String AUTOMATICHRECHARGETOGGLE="automatichRechargeToggle";
public static final String AMOUNTTOSENDTOOTHERPOSTEPAY="amountToSendToOtherPostepay";
public static final String SUBTITLEOTHERPOSTEPAY="subTitleOtherPostepay";
public static final String SAVECONTACTOTHERCARD="saveContactOtherCard";
public static final String PROCEEDRECHARGEBUTTON="proceedRechargeButton";


	public BodyRechargeOtherPostepay(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

