package com.postepay.automation.core.ui.pages.carburantestep1;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.BannerCarburanteMolecola;
import com.postepay.automation.core.ui.molecules.CarburanteStep1Molecola;
import com.postepay.automation.core.ui.molecules.SettingProfileMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class CarburanteStep1Manager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();
	StringAndNumberOperationTools toolNumber = new StringAndNumberOperationTools();
	
	Particle banner = (Particle) UiObjectRepo.get().get(BannerCarburanteMolecola.BANNER);
	Particle bannerIcon = (Particle) UiObjectRepo.get().get(BannerCarburanteMolecola.BANNERICONPROVIDER);
	Particle bannerHeader = (Particle) UiObjectRepo.get().get(BannerCarburanteMolecola.BANNERHEADER);
	Particle bannerAddress = (Particle) UiObjectRepo.get().get(BannerCarburanteMolecola.BANNERADDRESS);
	
	Particle selectImportoLabel = (Particle) UiObjectRepo.get().get(CarburanteStep1Molecola.SELECTIMPORTOLABEL);
	Particle amount = (Particle) UiObjectRepo.get().get(CarburanteStep1Molecola.FUELAMOUNT);
	Particle amountMax = (Particle) UiObjectRepo.get().get(CarburanteStep1Molecola.FUELMORE);
	Particle amountMin = (Particle) UiObjectRepo.get().get(CarburanteStep1Molecola.FUELMINUS);
	Particle amount10 = (Particle) UiObjectRepo.get().get(CarburanteStep1Molecola.FIXAMOUNT10);
	Particle amount20 = (Particle) UiObjectRepo.get().get(CarburanteStep1Molecola.FIXAMOUNT20);
	Particle amount50 = (Particle) UiObjectRepo.get().get(CarburanteStep1Molecola.FIXAMOUNT50);
	Particle amount80 = (Particle) UiObjectRepo.get().get(CarburanteStep1Molecola.FIXAMOUNT80);
	Particle pieno = (Particle) UiObjectRepo.get().get(CarburanteStep1Molecola.FIXAMOUNTALL);
	Particle setComePreferito = (Particle) UiObjectRepo.get().get(CarburanteStep1Molecola.SETAMOUNTFUELPREFER);
	Particle setComePreferitoLabel  = (Particle) UiObjectRepo.get().get(CarburanteStep1Molecola.SETAMOUNTFUELPREFERLABEL);
	Particle prosegui = (Particle) UiObjectRepo.get().get(CarburanteStep1Molecola.FUELBUTTONPROSEGUI);
	
	public CarburanteStep1Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderpage() {
		toolLayout.verifyPresenceHeaderPage("Acquista Carburante");
	}
	
	public void verifyLayoutPage() {
		assertTrue(banner.getElement().isDisplayed());
		assertTrue(bannerIcon.getElement().isDisplayed());
		assertTrue(bannerHeader.getElement().isDisplayed());
		assertTrue(bannerAddress.getElement().isDisplayed());
		assertTrue(selectImportoLabel.getElement().isDisplayed());
		assertTrue(amount.getElement().isDisplayed());
		assertTrue(amountMax.getElement().isDisplayed());
		assertTrue(amountMin.getElement().isDisplayed());
		assertTrue(amount10.getElement().isDisplayed());
		assertTrue(amount20.getElement().isDisplayed());
		assertTrue(amount50.getElement().isDisplayed());
		assertTrue(amount80.getElement().isDisplayed());
		assertTrue(pieno.getElement().isDisplayed());
		assertTrue(setComePreferito.getElement().isDisplayed());
		assertTrue(setComePreferitoLabel.getElement().isDisplayed());
		assertTrue(prosegui.getElement().isDisplayed());
	}
	
	public int getAmount() {
		String actualAmount = amount.getElement().getText().replace(" € ", "");
		int startAmount = toolNumber.convertStringToInt(actualAmount);
		return startAmount;
	}
	
	public void isSettedImportoPreferito(String defaultImporto) {
		int amountAsIs = getAmount();
		int intDefaultImporto = toolNumber.convertStringToInt(defaultImporto);
		System.out.print("amountAsIs è: " + amountAsIs + "     intDefaultImporto è: " + intDefaultImporto);
		assertTrue(amountAsIs == intDefaultImporto);
	}
	
	public void isSettedPieno(String defaultPieno) {
		pieno.getElement().click();
		WaitManager.get().waitMediumTime();
		
		// Verifica che dopo il click selecte=true
		assertTrue(pieno.getElement().isSelected());
		
		// Verifica che dopo il click l'importo a vidio sia uguale al pieno importato di default		
		int intDefaultPieno = toolNumber.convertStringToInt(defaultPieno);
		int pienoAmount = getAmount();
		
		assertTrue(pienoAmount == intDefaultPieno);
	}
	
	public void isCheckedAmountCard(int importoCard) {
		
		if (importoCard == 10) {
			assertTrue(amount10.getElement().getAttribute("checked").equals("true"));
		} else if (importoCard == 20) {
			assertTrue(amount20.getElement().getAttribute("checked").equals("true"));
		} else if (importoCard == 50) {
			assertTrue(amount50.getElement().getAttribute("checked").equals("true"));
		} else if (importoCard == 80) {
			assertTrue(amount80.getElement().getAttribute("checked").equals("true"));			
		}
	}
	
	public void moveTo20() {
		int amountAsIs = getAmount();
		int target20 = 20;
		int click = Math.abs(target20 - amountAsIs) / 5;
		
		if (target20 == amountAsIs) {
			isCheckedAmountCard(20);
			
		} else if (target20 < amountAsIs) {
			assertTrue(amount20.getElement().getAttribute("checked").equals("false"));
			
			for (int i = 0; i < click; i++) {
				amountMin.getElement().click();
				WaitManager.get().waitShortTime();
			}
			isCheckedAmountCard(20);
			
		} else if (target20 > amountAsIs) {
			assertTrue(amount20.getElement().getAttribute("checked").equals("false"));

			for (int i = 0; i < click; i++) {
				amountMax.getElement().click();
				WaitManager.get().waitShortTime();
			}
			isCheckedAmountCard(20);
		}
	}
	
	public void verifyOtherCardBehavior(int importoCard) {
		if (importoCard == 10) {
			assertTrue(amount10.getElement().getAttribute("checked").equals("false"));
			amount10.getElement().click();
			WaitManager.get().waitShortTime();
			isCheckedAmountCard(10);
			
		} else if (importoCard == 50) {
			assertTrue(amount50.getElement().getAttribute("checked").equals("false"));
			amount50.getElement().click();
			WaitManager.get().waitShortTime();
			isCheckedAmountCard(50);
			
		} else if (importoCard == 80) {
			assertTrue(amount80.getElement().getAttribute("checked").equals("false"));
			amount80.getElement().click();
			WaitManager.get().waitShortTime();
			isCheckedAmountCard(80);
		}
	}
	
	public void moveToOtherAmount() {
		verifyOtherCardBehavior(50);
		verifyOtherCardBehavior(80);
		verifyOtherCardBehavior(10);
	}
	
	public void moveTo5() {
		int amountAsIs = getAmount();
		int target5 = 5;
		int click = Math.abs(target5 - amountAsIs) / 5;
		
		if (target5 == amountAsIs) {
			prosegui.getElement().click();
			WaitManager.get().waitLongTime();
			
		} else if (target5 < amountAsIs) {
			
			for (int i = 0; i < click; i++) {
				amountMin.getElement().click();
				WaitManager.get().waitShortTime();
			}
			prosegui.getElement().click();
			WaitManager.get().waitLongTime();
			
		} else if (target5 > amountAsIs) {
			
			for (int i = 0; i < click; i++) {
				amountMax.getElement().click();
				WaitManager.get().waitShortTime();
			}
			prosegui.getElement().click();
			WaitManager.get().waitLongTime();
		}
	}
	
}

