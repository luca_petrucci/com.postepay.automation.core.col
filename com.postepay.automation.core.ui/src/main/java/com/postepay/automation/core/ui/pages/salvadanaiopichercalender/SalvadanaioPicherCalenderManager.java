package com.postepay.automation.core.ui.pages.salvadanaiopichercalender;

import static org.junit.Assert.assertTrue;

import java.util.Random;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.SalvadanaioOpzioniSettaggioMolecola;
import com.postepay.automation.core.ui.molecules.SalvadanaioPicherCalenderMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import org.openqa.selenium.WebDriver;

public class SalvadanaioPicherCalenderManager extends PageManager {
	
	LayoutTools toolLayout = new LayoutTools();
	
	Particle calenderAnno = (Particle) UiObjectRepo.get().get(SalvadanaioPicherCalenderMolecola.SALVADANAIOCALENDERANNO);
	Particle calenderDataEstesa = (Particle) UiObjectRepo.get().get(SalvadanaioPicherCalenderMolecola.SALVADANAIOCALENDERDATAESTESA);
	Particle calenderNextMounth = (Particle) UiObjectRepo.get().get(SalvadanaioPicherCalenderMolecola.SALVADANAIOCALENDERNEXTMOUNTH);
	Particle calenderDay = (Particle) UiObjectRepo.get().get(SalvadanaioPicherCalenderMolecola.SALVADANAIOCALENDERDAY);
	Particle calenderOK = (Particle) UiObjectRepo.get().get(SalvadanaioPicherCalenderMolecola.SALVADANAIOCALENDEROK);
	Particle calenderCANCEL = (Particle) UiObjectRepo.get().get(SalvadanaioPicherCalenderMolecola.SALVADANAIOCALENDERCANCEL);
	
	public SalvadanaioPicherCalenderManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public WebElement getElementDay(WebDriver driver, String day) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, calenderDay, day);
		assertTrue(elm.isDisplayed());
		return elm;
	}
	
	public void verifyLayout() {
		assertTrue(calenderAnno.getElement().isDisplayed());
		assertTrue(calenderDataEstesa.getElement().isDisplayed());
		assertTrue(calenderNextMounth.getElement().isDisplayed());
		assertTrue(calenderOK.getElement().isDisplayed());
		assertTrue(calenderCANCEL.getElement().isDisplayed());
	}
	
	public void clickOnNextMounth() {
		calenderNextMounth.getElement().click();
		WaitManager.get().waitShortTime();
	}
	
	public void setADay(WebDriver driver, String day) {
		WebElement dayElm = getElementDay(driver, day);
		dayElm.click();
		WaitManager.get().waitShortTime();
	}
	
	public void clickOnOK() {
		calenderOK.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnCANCEL() {
		calenderCANCEL.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void setADefaultDay(WebDriver driver) {
		for (int i = 0; i < 2; i++) {
			clickOnNextMounth();
		}
		double numeroRandom = Math.random() * (28-1) + 1;
		int numerone = (int) numeroRandom;
		String numerino = String.valueOf(numerone);
		System.out.println(numerino);
		setADay(driver, numerino);
	}
}

