package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class WhomWontSendABonificoSepa extends Molecule {
	public static final String NAME="M051";
	
	public static final String HEADEROFBANKTRANFERTSEPA="headerOfBankTranfertSEPA";
public static final String MOREDATAOFBANKTRANFERTSEPA="moreDataOfBankTranfertSEPA";
public static final String AMOUTTOSENTTOTARGETHOLDEROFBANKTRANFERTSEPA="amoutToSentToTargetHolderOfBankTranfertSEPA";
public static final String HEADEROFTARGETCARDOFBANKTRANFERTSEPA="headerOftargetCardOfBankTranfertSEPA";
public static final String PROCEEDBUTTONOFBANKTRANFERTSEPA="proceedButtonOfBankTranfertSEPA";
public static final String SAVEHOLDERCONTACTSOFBANKTRANFERTSEPA="saveHolderContactsOfBankTranfertSEPA";
public static final String SUMMARYREASONOFBANKTRANFERTSEPA="summaryReasonOfBankTranfertSEPA";
public static final String HOLDERCOUNTRYRESIDENCEOFBANKTRANFERTSEPA="holderCountryResidenceOfBankTranfertSEPA";
public static final String IBANINPUTOFBANKTRANFERTSEPA="ibanInputOfBankTranfertSEPA";


	public WhomWontSendABonificoSepa(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

