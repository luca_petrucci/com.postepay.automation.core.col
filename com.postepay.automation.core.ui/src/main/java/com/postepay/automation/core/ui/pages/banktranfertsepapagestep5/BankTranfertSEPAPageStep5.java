package com.postepay.automation.core.ui.pages.banktranfertsepapagestep5;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.ThankYouPageGeneric;
import com.postepay.automation.core.ui.pages.rechargemypostepaypagestep6.RechargeMyPostepayPageStep6Manager;


public class BankTranfertSEPAPageStep5 extends Page {
	public static final String NAME="T035";
	

	public BankTranfertSEPAPageStep5(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}
	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(ThankYouPageGeneric.NAME, UiObjectRepo.get().get(ThankYouPageGeneric.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new BankTranfertSEPAPageStep5Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new BankTransfertSEPAPageStep5ManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyLayout() {
		((BankTranfertSEPAPageStep5Manager)this.manager).verifyLayout();
	}
	
	public void clickOnButtonClose() {
		((BankTranfertSEPAPageStep5Manager)this.manager).clickOnCloseButton();
	}
	
	public void verifyLayoutDinamic(String title, String description, String buttonTxt) {
		((BankTranfertSEPAPageStep5Manager)this.manager).verifyLayoutDinamic(title, description, buttonTxt);
	}
	
	public void clickOnClose() {
		((BankTranfertSEPAPageStep5Manager)this.manager).clickOnClose();
	}
	
	public void verifyLinkPresenza(String text) {
		((BankTranfertSEPAPageStep5Manager)this.manager).verifyLinkPresenza(text);
	}
	public void typPraticaMIT() {
		((BankTranfertSEPAPageStep5Manager)this.manager).typPraticaMIT();
	}
}

