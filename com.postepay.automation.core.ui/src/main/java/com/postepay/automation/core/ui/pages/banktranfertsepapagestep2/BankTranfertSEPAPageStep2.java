package com.postepay.automation.core.ui.pages.banktranfertsepapagestep2;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.WhomWontSendABonificoSepa;


public class BankTranfertSEPAPageStep2 extends Page {
	public static final String NAME="T032";


	public BankTranfertSEPAPageStep2(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(WhomWontSendABonificoSepa.NAME, UiObjectRepo.get().get(WhomWontSendABonificoSepa.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {

		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {

		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {

		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new BankTranfertSEPAPageStep2Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {

		return new BankTransfertSEPAPageStep2ManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		return null;
	}

	public void insertIban(String valueIban) {
		((BankTranfertSEPAPageStep2Manager)this.manager).insertIban(valueIban);
	
	}
	
	public void insertIntestatario(String valueIntestatario) {
		((BankTranfertSEPAPageStep2Manager)this.manager).insertIntestatario(valueIntestatario);
	
	}
	
	public void selectCountry() {
		((BankTranfertSEPAPageStep2Manager)this.manager).selectCountry();
	
	}
	
	public void insertAmout(String valueAmount) {
		((BankTranfertSEPAPageStep2Manager)this.manager).insertAmout(valueAmount);
	
	}
	
	public void insertReason(String valueReason) {
		((BankTranfertSEPAPageStep2Manager)this.manager).insertReason(valueReason);
	
	}

	public void clickOnMoreData() {
		((BankTranfertSEPAPageStep2Manager)this.manager).clickOnMoreData();
	
	}
	
	public void clickOnProceed() {
		((BankTranfertSEPAPageStep2Manager)this.manager).clickOnProceed();
	
	}
	
	public void compileAllStandardForm(String valueIban, String valueIntestatario, String valueAmount, String valueReason) {
		((BankTranfertSEPAPageStep2Manager)this.manager).compileAllStandardForm(valueIban, valueIntestatario, valueAmount, valueReason);
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		((BankTranfertSEPAPageStep2Manager)this.manager).verifyHeaderpage(titlePageInput);
	}
	public void compileAllExceptOne(String emptyField,String valueIban, String valueIntestatario, String valueAmount, String valueReason) {
		((BankTranfertSEPAPageStep2Manager)this.manager).compileAllExceptOne(emptyField, valueIban, valueIntestatario, valueAmount, valueReason);
	}
}

