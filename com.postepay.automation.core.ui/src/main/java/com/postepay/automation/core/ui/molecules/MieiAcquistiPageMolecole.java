package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class MieiAcquistiPageMolecole extends Molecule {
	public static final String NAME="M228";
	
	public static final String TITLEBIGLIETTOMIEIACQUISTI="titleBigliettoMieiAcquisti";
public static final String IMAGECARDMIEIACQUISTI="imageCardMieiAcquisti";
public static final String SEARCHTEXTMIEIACQUISTI="searchTextMieiAcquisti";
public static final String DATABIGLIETTOMIEIACQUISTI="dataBigliettoMieiAcquisti";
public static final String DESCRIPTIONBIGLIETTOMIEIACQUISTI="descriptionBigliettoMieiAcquisti";
public static final String IMPORTOBIGLIETTOMIEIACQUISTI="importoBigliettoMieiAcquisti";
public static final String FILTERCOUNTMIEIACQUISTI="filterCountMieiAcquisti";
public static final String FILTERMIEIACQUISTI="filterMieiAcquisti";
public static final String TOOLBARMIEIACQUISTI="toolBarMieiAcquisti";


	public MieiAcquistiPageMolecole(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

