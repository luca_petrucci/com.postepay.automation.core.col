package com.postepay.automation.core.ui.pages.requestp2ppagestep2;

import com.postepay.automation.core.ui.molecules.AmoutCreditToRequestSummary;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class RequestP2PPageStep2Manager extends PageManager {

	public RequestP2PPageStep2Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void clickOnRequestButton() {

		Particle btn=(Particle) UiObjectRepo.get().get(AmoutCreditToRequestSummary.SENDBUTTONP2PREQUEST);

		btn.getElement().click();	
		
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnAddComment() {
		
		Particle btnComment=(Particle) UiObjectRepo.get().get(AmoutCreditToRequestSummary.ADDCOMMENTP2PREQUEST);
		
		btnComment.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
}
