package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class OnbQrCodeReaderMolecola extends Molecule {
	public static final String NAME="M243";
	
	public static final String QRCODECAMERA="qrCodeCamera";
public static final String CAMERAVIEW="cameraView";
public static final String DESCRIPTIONCAMERA="descriptionCamera";
public static final String AIUTOCAMERA="aiutoCamera";


	public OnbQrCodeReaderMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

