package com.postepay.automation.core.ui.pages.iosdevicepage;

import java.time.Duration;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.IOSDeviceGegericMolecola;
import com.postepay.automation.core.ui.pages.loginpostepage.LoginPostePageManager;

import io.appium.java_client.InteractsWithApps;

public class IOSDevicePage extends Page {
	public static final String NAME="T138";
	

	public IOSDevicePage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(IOSDeviceGegericMolecola.NAME, UiObjectRepo.get().get(IOSDeviceGegericMolecola.NAME), false);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new IOSDevicePageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void openSetting() {
		((IOSDevicePageManager)this.manager).openSetting();
	}
	
	public void resumePostePay(Properties pro) {
		((IOSDevicePageManager)this.manager).resumePostePay(pro);
	}
	
	public void clickOnWiFiLabel() {
		((IOSDevicePageManager)this.manager).clickOnWiFiLabel();
	}
	
	public boolean isWifiEnable() {
		return ((IOSDevicePageManager)this.manager).isWifiEnable();
	}

	public boolean isAirModeEnable() {
		return ((IOSDevicePageManager)this.manager).isAirModeEnable();
	}
	
	public void clickOnWifiToggle() {
		((IOSDevicePageManager)this.manager).clickOnWifiToggle();
	}

	public void clickOnAirModeToggle() {
		((IOSDevicePageManager)this.manager).clickOnAirModeToggle();
	}
	
	public void clickOnBackFromWifiSetting() {
		((IOSDevicePageManager)this.manager).clickOnBackFromWifiSetting();
	}
}

