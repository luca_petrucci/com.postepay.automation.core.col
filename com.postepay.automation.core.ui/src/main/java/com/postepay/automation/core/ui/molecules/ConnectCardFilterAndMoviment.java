package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class ConnectCardFilterAndMoviment extends Molecule {
	public static final String NAME="M049";
	
	public static final String SPENDINGSERCHFIELDCONNECT="spendingSerchFieldConnect";
public static final String SPENDINGOUTRECORDSCONNECT="spendingOutRecordsConnect";
public static final String DISPOSITIONOPERATIONCONTAINERCONNECT="dispositionOperationContainerConnect";
public static final String SIMCARDCONNECTFORG2G="simCardConnectForG2g";


	public ConnectCardFilterAndMoviment(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

