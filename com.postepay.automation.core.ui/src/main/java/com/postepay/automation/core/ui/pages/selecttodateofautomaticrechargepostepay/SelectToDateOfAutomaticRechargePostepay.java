package com.postepay.automation.core.ui.pages.selecttodateofautomaticrechargepostepay;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.SelectToDateOfAutomaticRechargePostepayMolecula;


public class SelectToDateOfAutomaticRechargePostepay extends Page {
	public static final String NAME="T050";
	

	public SelectToDateOfAutomaticRechargePostepay(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(SelectToDateOfAutomaticRechargePostepayMolecula.NAME, UiObjectRepo.get().get(SelectToDateOfAutomaticRechargePostepayMolecula.NAME), false);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SelectToDateOfAutomaticRechargePostepayManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

}

