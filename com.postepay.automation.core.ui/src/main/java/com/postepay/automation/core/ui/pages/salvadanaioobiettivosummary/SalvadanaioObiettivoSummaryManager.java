package com.postepay.automation.core.ui.pages.salvadanaioobiettivosummary;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.SalvadanaioObiettivoSummaryMolecola;
import com.postepay.automation.core.ui.molecules.SalvadanaioPicherCalenderMolecola;
	import com.postepay.automation.core.ui.molecules.SalvadanaioVersaSuObiettivoRicorrenteMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class SalvadanaioObiettivoSummaryManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();
	
	Particle summaryTitolo = (Particle) UiObjectRepo.get().get(SalvadanaioObiettivoSummaryMolecola.SALVADANAIOSUMMARYTITOLO);
	Particle summaryVersaConLabel = (Particle) UiObjectRepo.get().get(SalvadanaioObiettivoSummaryMolecola.SALVADANAIOSUMMARYVERSACONLABEL);
	Particle summaryVersaConValore = (Particle) UiObjectRepo.get().get(SalvadanaioObiettivoSummaryMolecola.SALVADANAIOSUMMARYVERSACONVALORE);
	Particle summaryIntestatoA = (Particle) UiObjectRepo.get().get(SalvadanaioObiettivoSummaryMolecola.SALVADANAIOSUMMARYINTESTATOA);
	Particle summaryIntestatoAValore = (Particle) UiObjectRepo.get().get(SalvadanaioObiettivoSummaryMolecola.SALVADANAIOSUMMARYINTESTATOAVALORE);
	Particle summaryImporto = (Particle) UiObjectRepo.get().get(SalvadanaioObiettivoSummaryMolecola.SALVADANAIOSUMMARYIMPORTO);
	Particle summaryImportoValore = (Particle) UiObjectRepo.get().get(SalvadanaioObiettivoSummaryMolecola.SALVADANAIOSUMMARYIMPORTOVALORE);
	Particle summaryFrequenza = (Particle) UiObjectRepo.get().get(SalvadanaioObiettivoSummaryMolecola.SALVADANAIOSUMMARYFREQUENZA);
	Particle summaryFrequenzaValore = (Particle) UiObjectRepo.get().get(SalvadanaioObiettivoSummaryMolecola.SALVADANAIOSUMMARYFREQUENZAVALORE);
	Particle summaryAPartireDa = (Particle) UiObjectRepo.get().get(SalvadanaioObiettivoSummaryMolecola.SALVADANAIOSUMMARYAPARTIREDA);
	Particle summaryAPartireDaValore = (Particle) UiObjectRepo.get().get(SalvadanaioObiettivoSummaryMolecola.SALVADANAIOSUMMARYAPARTIREDAVALORE);
	Particle summaryTermineVersamentoRicorrente = (Particle) UiObjectRepo.get().get(SalvadanaioObiettivoSummaryMolecola.SALVADANAIOSUMMARYTERMINEVERSAMENTORICORRENTE);
	Particle summaryTermineVersamentoRicorrenteValore = (Particle) UiObjectRepo.get().get(SalvadanaioObiettivoSummaryMolecola.SALVADANAIOSUMMARYTERMINEVERSAMENTORICORRENTEVALORE);
	Particle summaryAutorizza = (Particle) UiObjectRepo.get().get(SalvadanaioObiettivoSummaryMolecola.SALVADANAIOSUMMARYAUTORIZZA);
	Particle summaryModifica = (Particle) UiObjectRepo.get().get(SalvadanaioObiettivoSummaryMolecola.SALVADANAIOSUMMARYMODIFICA);
	Particle summaryContinua = (Particle) UiObjectRepo.get().get(SalvadanaioObiettivoSummaryMolecola.SALVADANAIOSUMMARYCONTINUA);
	Particle salvadanaioGiornoFestivoNonSelezionabilePopup= (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoRicorrenteMolecola.SALVADANAIOGIORNOFESTIVONONSELEZIONABILEPOPUP);
	
	public SalvadanaioObiettivoSummaryManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}
	
	public void verifyLayoutRicorrente() {
		assertTrue(summaryFrequenza.getElement().isDisplayed());
		assertTrue(summaryFrequenzaValore.getElement().isDisplayed());
		assertTrue(summaryAPartireDa.getElement().getText().equals("Ogni giorno"));
		assertTrue(summaryAPartireDaValore.getElement().isDisplayed());
		assertTrue(summaryTermineVersamentoRicorrente.getElement().getText().equals("Oggi"));
		assertTrue(summaryTermineVersamentoRicorrenteValore.getElement().isDisplayed());
	}
	
	public void verifyLayoutNonRicorrente() {
		assertTrue(summaryTitolo.getElement().isDisplayed());
		assertTrue(summaryVersaConLabel.getElement().isDisplayed());
		assertTrue(summaryVersaConValore.getElement().isDisplayed());
		assertTrue(summaryIntestatoA.getElement().isDisplayed());
		assertTrue(summaryIntestatoAValore.getElement().isDisplayed());
		assertTrue(summaryImporto.getElement().isDisplayed());
		assertTrue(summaryImportoValore.getElement().isDisplayed());
		try {
			UIUtils.ui().mobile().swipe((MobileDriver) page.getDriver(), SCROLL_DIRECTION.DOWN, 200);
		}catch(Exception e)
		{
			//TODO
		}
		assertTrue(summaryAutorizza.getElement().isDisplayed());
		assertTrue(summaryModifica.getElement().isDisplayed());
	}
	
	public void verifyDataUser(String cardNumber, String nomeCognome, String importo) {
		// Verifica numero carta
		String enumeroCartaText = "Postepay Evolution " + cardNumber;
		String numeroCartaText=summaryVersaConValore.getElement().getText();
//		assertTrue("ER: "+ enumeroCartaText + " AR: "+ numeroCartaText, numeroCartaText.equals(enumeroCartaText));
		
		// Verifica intestatario
		String intestatario = summaryIntestatoAValore.getElement().getText();
		String eIntestatario = nomeCognome;
		assertTrue("ER: " + eIntestatario + "AR: " + intestatario, intestatario.equals(eIntestatario));
		
		// Verifica importo
		String eimportoText = "€ " + importo;
		String importoText =summaryImportoValore.getElement().getText();
		assertTrue("ER: "+ eimportoText +" AR: "+importoText, importoText.equals(eimportoText));
	}
	
	public void verifySpecificData(String frequenza, String aPartireDa, String termine) {
		assertTrue(summaryFrequenzaValore.getElement().getText().equals(frequenza));
		assertTrue(summaryAPartireDaValore.getElement().getText().equals(aPartireDa));
		assertTrue(summaryTermineVersamentoRicorrenteValore.getElement().getText().equals(termine));
	}
	
	public void clickOnAUTORIZZA() {
		summaryAutorizza.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnMODIFICA() {
		summaryModifica.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
			public void clickOnCONTINUA() {
		summaryContinua.getElement().click();
		try {
			System.out.println(salvadanaioGiornoFestivoNonSelezionabilePopup.getElement());
			System.out.println("Il giorno selezionato è un festivo");
			salvadanaioGiornoFestivoNonSelezionabilePopup.getElement().click();
		} catch (Exception e) {
		}
		try {
			
			summaryContinua.getElement().click();
		} catch (Exception e) {
		}
		WaitManager.get().waitMediumTime();
}

}

