package com.postepay.automation.core.ui.pages.productsstandardpage;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.GenericOverviewsCardDetails;

import io.appium.java_client.CommandExecutionHelper;
import io.appium.java_client.ExecutesMethod;
import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.ObjectFinderLight;

public class ProductsStandardPageManagerIOS extends ProductsStandardPageManager {

	public ProductsStandardPageManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyLayoutPage() {
		String pageSource= page.getDriver().getPageSource();
		assertTrue(ObjectFinderLight.elementExists(pageSource, cardAmount.getLocator()));
		assertTrue(ObjectFinderLight.elementExists(pageSource, cardStar.getLocator()));
		assertTrue(ObjectFinderLight.elementExists(pageSource, cardNumber.getLocator()));
		assertTrue(ObjectFinderLight.elementExists(pageSource, cardImpostazioni.getLocator()));
		assertTrue(ObjectFinderLight.elementExists(pageSource, searchBar.getLocator()));
		assertTrue(ObjectFinderLight.elementExists(pageSource, contabilizzate.getLocator()));
		assertTrue(ObjectFinderLight.elementExists(pageSource, movimentiTitle.getLocator()));
		assertTrue(ObjectFinderLight.elementExists(pageSource, movimentiDescription.getLocator()));
		assertTrue(ObjectFinderLight.elementExists(pageSource, movimentiAmount.getLocator()));
		assertTrue(ObjectFinderLight.elementExists(pageSource, movimentiDate.getLocator()));
		
	}
	
//	public void clickOnCard() {
//		Particle cardNumberIOS = (Particle) UiObjectRepo.get().get(GenericOverviewsCardDetails.NUMBERCARDDETAILS);
//		cardNumberIOS.getElement().click();
//		
//		WaitManager.get().waitMediumTime();
//	}
}
