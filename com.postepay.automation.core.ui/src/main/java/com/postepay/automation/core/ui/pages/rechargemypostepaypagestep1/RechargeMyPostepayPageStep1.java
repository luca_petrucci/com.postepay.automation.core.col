package com.postepay.automation.core.ui.pages.rechargemypostepaypagestep1;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.SelectACardToRechargeMyPostepay;

import io.appium.java_client.android.AndroidDriver;


public class RechargeMyPostepayPageStep1 extends Page {
	public static final String NAME="T039";


	public RechargeMyPostepayPageStep1(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(SelectACardToRechargeMyPostepay.NAME, UiObjectRepo.get().get(SelectACardToRechargeMyPostepay.NAME), false);

	}

	@Override
	protected PageManager loadSafariPageManager() {

		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {

		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {

		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new RechargeMyPostepayPageStep1Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {

		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {

		return null;
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		((RechargeMyPostepayPageStep1Manager)this.manager).verifyHeaderpage(titlePageInput);
	}
	
	public void verifyLayoutPage(WebDriver driver, String titleInput, String typeInput, String numberInput, double discrepanza) {
		((RechargeMyPostepayPageStep1Manager)this.manager).verifyLayoutPage(driver, titleInput, typeInput, numberInput, discrepanza);
	}
	
	public void selectCardToRecharge(WebDriver driver, String numberInput) {
		((RechargeMyPostepayPageStep1Manager)this.manager).selectCardToRecharge(driver, numberInput);
	}

}

