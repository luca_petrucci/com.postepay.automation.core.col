package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SalvadanaioPicherCalenderMolecola extends Molecule {
	public static final String NAME="M235";
	
	public static final String SALVADANAIOCALENDERANNO="salvadanaioCalenderAnno";
public static final String SALVADANAIOCALENDERCANCEL="salvadanaioCalenderCANCEL";
public static final String SALVADANAIOCALENDERDATAESTESA="salvadanaioCalenderDataEstesa";
public static final String SALVADANAIOCALENDEROK="salvadanaioCalenderOK";
public static final String SALVADANAIOCALENDERNEXTMOUNTH="salvadanaioCalenderNextMounth";
public static final String SALVADANAIOCALENDERDAY="salvadanaioCalenderDay";


	public SalvadanaioPicherCalenderMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

