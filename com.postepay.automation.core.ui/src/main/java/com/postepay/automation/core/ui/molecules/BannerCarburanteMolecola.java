package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BannerCarburanteMolecola extends Molecule {
	public static final String NAME="M219";
	
	public static final String BANNERHEADER="bannerHeader";
public static final String BANNERADDRESS="bannerAddress";
public static final String BANNER="banner";
public static final String BANNERICONPROVIDER="bannerIconProvider";


	public BannerCarburanteMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

