package com.postepay.automation.core.ui.pages.homepage;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.DiscoverMoreHomePage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class HomePageManagerIOS extends HomePageManager {

	public HomePageManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void swithCardInEvdenza(String targetTitleString) {
		Dimension size = page.getDriver().manage().window().getSize();
		Particle title=(Particle) UiObjectRepo.get().get(DiscoverMoreHomePage.INTEROCASSETTOHOMEPAGE);
		System.out.println(title.getElement().getText());
		WebElement coordinate = title.getElement();
		float x = coordinate.getLocation().getX() + coordinate.getRect().width * 0.5f; 
		float y = coordinate.getLocation().getY() + coordinate.getRect().height * 0.2f;
		System.out.println("x " + x);
		System.out.println("y " + y);

		float X = x / size.width; 
		float Y = y / size.height;

		System.out.println("X " + X);
		System.out.println("Y " + Y);
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.RIGHT, 
				X, 
				Y, 
				X - (X * 0.2f),
				Y,
				50);
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.RIGHT, 
				X, 
				Y, 
				X - (X * 0.2f),
				Y,
				50);
		
		LayoutTools toolLayout=new LayoutTools();
		WebElement wTitle = null;
		while (wTitle==null) {
			try {
				wTitle = toolLayout.replaceGenericPathOfElement(page.getDriver(), title, targetTitleString);
				break;
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		wTitle.click();
	}
}
