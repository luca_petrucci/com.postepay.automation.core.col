package com.postepay.automation.core.ui.pages.nop2pcontactthankyoupage;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.NoP2pContactThankYouPageMolecola;


public class NoP2pContactThankYouPage extends Page {
	public static final String NAME="T088";
	

	public NoP2pContactThankYouPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(NoP2pContactThankYouPageMolecola.NAME, UiObjectRepo.get().get(NoP2pContactThankYouPageMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new NoP2pContactThankYouPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new NoP2pContactThankYouPageManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyLayoutOfTkp() {
		
		((NoP2pContactThankYouPageManager)this.manager).verifyLayoutOfTkp();
	}
	
	public void clickOnInviteYouFriend() {

		((NoP2pContactThankYouPageManager)this.manager).clickOnInviteYouFriend();
	}
	
	public void clickOnBankButton() {
		((NoP2pContactThankYouPageManager)this.manager).clickOnBankButton();
	}
	
	public void clickOnChiudiInvitaAmici()
	{
		((NoP2pContactThankYouPageManagerIOS)this.manager).clickOnChiudiInvitaAmici();
	}
}

