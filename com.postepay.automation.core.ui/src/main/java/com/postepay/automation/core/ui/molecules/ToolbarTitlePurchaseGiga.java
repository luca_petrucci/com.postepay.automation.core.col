package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class ToolbarTitlePurchaseGiga extends Molecule {
	public static final String NAME="M036";

	public static final String TITLECONNECTSIMPURCHASEANDSENDGIGA="titleConnectSimPurchaseAndSendGiga";
	public static final String ASSISTENTCONNECTSIMPURCHASEANDSENDGIGA="assistentConnectSimPurchaseAndSendGiga";
	public static final String CLOSECONNECTSIMPURCHASEANDSENDGIGA="closeConnectSimPurchaseAndSendGiga";
	public static final String SIMDETAILSSUBCARD="simDetailsSubCard";
	public static final String PHONENUMBERSUBCARD="phoneNumberSubCard";
	public static final String CREDITSIMSUBCARD="creditSimSubCard";

	public ToolbarTitlePurchaseGiga(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

