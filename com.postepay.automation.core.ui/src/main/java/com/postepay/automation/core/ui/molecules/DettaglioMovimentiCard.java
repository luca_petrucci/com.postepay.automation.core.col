package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class DettaglioMovimentiCard extends Molecule {
	public static final String NAME="M267";
	
	public static final String MOVIMENTOTITLE="movimentoTitleM267";
	public static final String IMPORTO="importoM267";
	public static final String DATAVALUTALABEL="dataValutaLabelM267";
	public static final String DATACONTABILELABEL="dataContabileLabelM267";
	public static final String DATAVALUTATEXT="dataValutaTextM267";
	public static final String DATACONTABILETEXT="dataContabileTextM267";
	public static final String CARDPOSTEPAY="cardPostepayM267";
	public static final String TREPUNTINIMENU="trePuntiniMenuM267";
	
	public DettaglioMovimentiCard(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

