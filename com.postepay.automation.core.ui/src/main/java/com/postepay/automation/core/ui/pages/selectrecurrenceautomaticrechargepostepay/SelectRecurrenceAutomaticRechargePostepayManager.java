package com.postepay.automation.core.ui.pages.selectrecurrenceautomaticrechargepostepay;

import org.openqa.selenium.By;

import com.postepay.automation.core.ui.molecules.BodyRechargeOtherPostepay;
import com.postepay.automation.core.ui.molecules.SelectRecurrenceAutomaticRechargePostepayMolecula;

import io.appium.java_client.AppiumDriver;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class SelectRecurrenceAutomaticRechargePostepayManager extends PageManager {

	public SelectRecurrenceAutomaticRechargePostepayManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void clickOnRecurrency(String valueRechargeAutomatic) {
		Particle p=(Particle) UiObjectRepo.get().get(SelectRecurrenceAutomaticRechargePostepayMolecula.EVERYWEEKRECHARGEPOSTEPAY);
		String nuovoLocator=Utility.replacePlaceHolders(p.getLocator(), new Entry("title",valueRechargeAutomatic));
		
		((AppiumDriver<?>)page.getDriver()).findElement(By.xpath(nuovoLocator)).click();
		
		try {Thread.sleep(1000);} catch (Exception err )  {}
		
	}
}

