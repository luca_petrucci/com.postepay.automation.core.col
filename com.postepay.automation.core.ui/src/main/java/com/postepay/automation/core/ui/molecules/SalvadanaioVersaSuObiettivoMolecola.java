package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SalvadanaioVersaSuObiettivoMolecola extends Molecule {
	public static final String NAME="M233";
	
	public static final String SALVADANAIOVERSAMENTOSUGGESTIONTEXT="salvadanaioVersamentoSuggestionText";
public static final String SALVADANAIOVERSAMENTOPIU="salvadanaioVersamentoPiu";
public static final String SALVADANAIOVERSAMENTOMENO="salvadanaioVersamentoMeno";
public static final String SALVADANAIOVERSAMENTOSUGGESTIONIMAGE="salvadanaioVersamentoSuggestionImage";
public static final String SALVADANAIOVERSAMENTOBUTTON="salvadanaioVersamentoButton";
public static final String SALVADANAIOVERSAMENTORICORRENTETOGGLE="salvadanaioVersamentoRicorrenteToggle";
public static final String SALVADANAIOVERSAMENTOIMAGEYELLOW="salvadanaioVersamentoImageYellow";
public static final String SALVADANAIOVERSAMENTOTITOLO="salvadanaioVersamentoTitolo";
public static final String SALVADANAIOVERSAMENTOLIMITEMASSIMO="salvadanaioVersamentoLimiteMassimo";
public static final String SALVADANAIOVERSAMENTOAMOUNT="salvadanaioVersamentoAmount";
public static final String SALVADANAIOVERSAMENTOTESTO="salvadanaioVersamentoTesto";
public static final String SALVADANAIOVERSAMENTORICORRENTELABEL="salvadanaioVersamentoRicorrenteLabel";


	public SalvadanaioVersaSuObiettivoMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

