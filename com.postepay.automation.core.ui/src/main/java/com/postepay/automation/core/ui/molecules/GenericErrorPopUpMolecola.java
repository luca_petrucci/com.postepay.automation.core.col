package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class GenericErrorPopUpMolecola extends Molecule {
	public static final String NAME="M202";
	
	public static final String TEXTTITLEGENERICERRORPOPUP="textTitleGenericErrorPopUp";
	public static final String OKPOSTEACCOUNTGENERICERRORPOPUP="okPosteAccountGenericErrorPopUp";
	public static final String TEXTBODYGENERICERRORPOPUP="textBodyGenericErrorPopUp";
    public static final String TITLERATINGPOPUP = "titleRatingPopUp";
	public static final String CONTENTRATINGPOPUP = "contentRatingPopUp";
	public static final String VOTRATINGPOPUP = "votaRatingPopUp";
	public static final String NONORARATINGPOPUP = "nonOraRatingPopUp";
	public static final String NOGRAZIERATINGPOPUP = "noGrazieRatingPopUp";
	public static final String ALLOWPERMISSIONBUTTON = "allowPermissionButton";
	public static final String ATACBUTTONOKM202 = "atacButtonOkM202";
	public static final String ALLOWPERMISSIONBUTTONSMG = "allowPermissionButtonSamsung";
	public static final String ALLOWPERMISSIONPOSITIONSMG = "allowPermissionPositionSamsung";
	public static final String ATACPOSTOKM202 = "atacPostOkM202";
	public static final String GENERICAUTHORIZATION = "genericAuthorization";
	public static final String NONMOSTRAREPIUBUTTON= "nonMostrareButton";
	public static final String APPLEPAYCLOSEBTN="applePayCLoseBtn";
	public static final String CHIUDIRICARICAPOSTONB="chiudiRicaricaPostOnb";
	
	public GenericErrorPopUpMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

