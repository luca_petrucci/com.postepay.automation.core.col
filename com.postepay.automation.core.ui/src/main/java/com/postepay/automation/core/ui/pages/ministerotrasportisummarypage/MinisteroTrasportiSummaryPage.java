package com.postepay.automation.core.ui.pages.ministerotrasportisummarypage;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.AbilitaInAppMolecola;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.MinisteroTrasportiMolecola;
import com.postepay.automation.core.ui.molecules.MinisteroTrasportiSummaryMolecola;
import com.postepay.automation.core.ui.pages.ministerotrasportipage.MinisteroTrasportiPageManager;


public class MinisteroTrasportiSummaryPage extends Page {
	public static final String NAME="T147";
	

	public MinisteroTrasportiSummaryPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(MinisteroTrasportiSummaryMolecola.NAME, UiObjectRepo.get().get(MinisteroTrasportiSummaryMolecola.NAME), true);
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(AbilitaInAppMolecola.NAME), true);
		this.addToTemplate(MinisteroTrasportiMolecola.NAME, UiObjectRepo.get().get(MinisteroTrasportiMolecola.NAME), true);
	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new MinisteroTrasportiSummaryPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeaderpage(String titlePageInput)
	{
		((MinisteroTrasportiSummaryPageManager)this.manager).verifyHeaderpage(titlePageInput);
	}

	public void verifyLayoutMITSummary(String cardNumber, String codiceFiscale)
	{
		((MinisteroTrasportiSummaryPageManager)this.manager).verifyLayoutMITSummary(cardNumber, codiceFiscale);
	}
	
	public void clickOnPagaMinistero()
	{
		((MinisteroTrasportiSummaryPageManager)this.manager).clickOnPagaMinistero();
	}
}

