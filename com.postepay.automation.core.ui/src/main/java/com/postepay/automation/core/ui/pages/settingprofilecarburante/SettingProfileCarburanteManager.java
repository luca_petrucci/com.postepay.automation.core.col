package com.postepay.automation.core.ui.pages.settingprofilecarburante;

import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.SettingProfileCarburanteMolecola;
import com.postepay.automation.core.ui.molecules.SettingProfileMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class SettingProfileCarburanteManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();
	StringAndNumberOperationTools toolNumber = new StringAndNumberOperationTools();

	Particle image = (Particle) UiObjectRepo.get().get(SettingProfileCarburanteMolecola.POMPAIMAGESETTINGCARBURANTE);
	Particle textHeader = (Particle) UiObjectRepo.get().get(SettingProfileCarburanteMolecola.AMOUNTLABELSETTINGCARBURANTE);
	Particle textAmount = (Particle) UiObjectRepo.get().get(SettingProfileCarburanteMolecola.PIENOLABELSETTINGCARBURANTE);
	Particle amountLaben = (Particle) UiObjectRepo.get().get(SettingProfileCarburanteMolecola.AMOUNTPIENOSETTINGCARBURANTE);
	Particle amountMinBtn = (Particle) UiObjectRepo.get().get(SettingProfileCarburanteMolecola.AMOUNTMINUSSETTINGCARBURANTE);
	Particle amountMaxBtn = (Particle) UiObjectRepo.get().get(SettingProfileCarburanteMolecola.AMOUNTADDSETTINGCARBURANTE);
	Particle amount = (Particle) UiObjectRepo.get().get(SettingProfileCarburanteMolecola.AMOUNTSETTEDSETTINGCARBURANTE);
	Particle btnSave = (Particle) UiObjectRepo.get().get(SettingProfileCarburanteMolecola.SAVEBUTTONSETTINGCARBURANTE);

	public SettingProfileCarburanteManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderpage(String arg) {

		if (arg.equals("pieno")) {
			toolLayout.verifyPresenceHeaderPage("Il tuo pieno");

		}else if (arg.equals("rifornimento")) {
			toolLayout.verifyPresenceHeaderPage("Il tuo importo preferito");
		}
	}

	public void verifyLayoutPage(String arg, WebDriver driver) {

		assertTrue(image.getElement().isDisplayed());
		assertTrue(amountMinBtn.getElement().isDisplayed());
		assertTrue(amountMaxBtn.getElement().isDisplayed());
		assertTrue(amount.getElement()!=null);
		assertTrue(btnSave.getElement()!=null);

//		if (arg.equals("pieno")) {
//			WebElement textHeaderW = toolLayout.replaceGenericPathOfElement(driver, textHeader, "Qual è l’importo del tuo pieno?");
//			WebElement textAmountW = toolLayout.replaceGenericPathOfElement(driver, textAmount, "Il tuo pieno è:");
//
//			assertTrue(textHeaderW.isDisplayed());
//			assertTrue(textAmountW.isDisplayed());
//
//		}else if (arg.equals("rifornimento")) {
//			WebElement textHeaderW = toolLayout.replaceGenericPathOfElement(driver, textHeader, "Qual è l’importo preferito quando fai il rifornimento?");
//			WebElement textAmountW = toolLayout.replaceGenericPathOfElement(driver, textAmount, "Il tuo importo preferito è:");
//
//			assertTrue(textHeaderW.isDisplayed());
//			assertTrue(textAmountW.isDisplayed());
//		}
	}

	public void clickOnMin() {
		amountMinBtn.getElement().click();
		WaitManager.get().waitShortTime();
	}

	public void clickOnMax() {
		amountMaxBtn.getElement().click();
		WaitManager.get().waitShortTime();		
	}

	public int getAmount() {
		String actualAmount = amount.getElement().getText();
		int startAmount = toolNumber.convertStringToInt(actualAmount);
		return startAmount;
	}

	public void checkIncrementoDecremento() {
		// Controllo sull'incremento
		// Effettuiamo click randomici du incrementa e decrementa
		// controlliamo che l'incremento del differenziale sia effettivo

		int amountAsIs = getAmount();
		int differenziale = 5;

		// 
		Random r = new Random();
		int incremento;
		int decremento;

		if ((amountAsIs > 25) && (amountAsIs < 75)) {
			incremento = r.nextInt(6) + 1;  // [1 ... 5]
			decremento = r.nextInt(6) + 1;  // [1 ... 5]

			for (int i = 0; i <= incremento; i++) {
				int amountIncrementato = getAmount() + differenziale;
				clickOnMax();
				// Ammontare mostrato a video dopo il click è uguale all'incremento calcolato
				assertTrue(getAmount() == amountIncrementato);
			}
			for (int i = 0; i <= decremento; i++) {
				int amountDecrementato = getAmount() - differenziale;
				clickOnMin();
				// Ammontare mostrato a video dopo il click è uguale all'incremento calcolato
				assertTrue(getAmount() == amountDecrementato);
			}

		} else if ((amountAsIs >= 5) && (amountAsIs <= 25)) {
			// Prima eseguiamo l'incremento e dopo il decremento
			incremento = 10;
			decremento = 5;

			for (int i = 0; i <= incremento; i++) {
				int amountIncrementato = getAmount() + differenziale;
				clickOnMax();
				// Ammontare mostrato a video dopo il click è uguale all'incremento calcolato
				assertTrue(getAmount() == amountIncrementato);
			}
			for (int i = 0; i <= decremento; i++) {
				int amountDecrementato = getAmount() - differenziale;
				clickOnMin();
				// Ammontare mostrato a video dopo il click è uguale all'incremento calcolato
				assertTrue(getAmount() == amountDecrementato);
			}

		} else if ((amountAsIs >= 75) && (amountAsIs <= 100)) {
			// Prima eseguiamo il decremento e dopo l'incremento  
			incremento = 5;
			decremento = 10;

			for (int i = 0; i <= decremento; i++) {
				int amountDecrementato = getAmount() - differenziale;
				clickOnMin();
				// Ammontare mostrato a video dopo il click è uguale all'incremento calcolato
				assertTrue(getAmount() == amountDecrementato);
			}
			for (int i = 0; i <= incremento; i++) {
				int amountIncrementato = getAmount() + differenziale;
				clickOnMax();
				// Ammontare mostrato a video dopo il click è uguale all'incremento calcolato
				assertTrue(getAmount() == amountIncrementato);
			}
		}
	}

	public void setAmount(String inputSettingAmount) {
		int inputAmount = toolNumber.convertStringToInt(inputSettingAmount);
		int amountAsIs = getAmount();

		int clickToTarget = Math.abs(inputAmount - amountAsIs) / 5;
		
		System.out.println("L'importo da settare è: " + inputAmount + "           L'importo a video è: " + amountAsIs);

		if (amountAsIs == inputAmount) {
			System.out.println("L'importo da settare e quello a video sono uguali");
			btnSave.getElement().click();
			WaitManager.get().waitMediumTime();
		}

		else if (amountAsIs < inputAmount) {
			System.out.println("L'importo a video è minore dell'importo da settare");
			
			for (int i = 0; i < clickToTarget; i++) {
				clickOnMax();
				
			}
			btnSave.getElement().click();
			WaitManager.get().waitMediumTime();
		}

		else if (amountAsIs > inputAmount) {
			System.out.println("L'importo a video è più alto dell importo da settare");
			
			for (int i = 0; i < clickToTarget; i++) {
				clickOnMin();	
			}
			btnSave.getElement().click();
			WaitManager.get().waitMediumTime();
		}
	}
	
	public void checkAddSubBtn() {
		int amountAsIs = getAmount();
		int amountIncremento = getAmount() + 5;
		int amountDecremento = getAmount() - 5;
		
		// Controlli se al click su + aumenta di 5
		clickOnMax();
		assertTrue(amountIncremento - amountAsIs == 5);	
		assertTrue(amountAsIs%5 == 0);
		
		// Controlli se al click su - diminuisce di 5
		clickOnMin();
		assertTrue(amountDecremento - amountAsIs == -5);	
		assertTrue(amountAsIs%5 == 0);
	}
}

