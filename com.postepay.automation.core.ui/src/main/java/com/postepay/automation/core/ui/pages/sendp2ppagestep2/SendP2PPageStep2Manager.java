package com.postepay.automation.core.ui.pages.sendp2ppagestep2;

import com.postepay.automation.core.ui.molecules.SearchContactsNumberP2P;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class SendP2PPageStep2Manager extends PageManager {

	public SendP2PPageStep2Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void searchContact(String targetUser) {
		Particle search=(Particle) UiObjectRepo.get().get(SearchContactsNumberP2P.SEARCHP2PCONTACT);
		
		search.getElement().sendKeys(targetUser);
		
		WaitManager.get().waitLongTime();
		
		Particle userTargetCard=(Particle) UiObjectRepo.get().get(SearchContactsNumberP2P.TARGETIDP2PCONTACT);
		
		userTargetCard.getElement().click();
		
		WaitManager.get().waitShortTime();
	}

}

