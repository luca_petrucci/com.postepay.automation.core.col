package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class PreLoginPostePageMolecola extends Molecule {
	public static final String NAME="M249";
	
	public static final String POSTEPAYWELCOMEPRELOGIN="postePayWelcomePreLogin";
public static final String POSTEPAYINTROPRELOGIN="postePayIntroPreLogin";
public static final String POSTEPAYICONPRELOGIN="postePayIconPreLogin";
public static final String POSTEPAYWELCOMEDESCRIPTIONPRELOGIN="postePayWelcomeDescriptionPreLogin";
public static final String POSTEPAYWELCOMESCOPRIBUTTONPRELOGIN="postePayWelcomeScopriButtonPreLogin";
public static final String POSTEPAYWELCOMEBUTTONPRELOGIN="postePayWelcomeButtonPreLogin";


	public PreLoginPostePageMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

