package com.postepay.automation.core.ui.pages.sendg2gpagestep1;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.pages.sendg2gpagestep3.SendG2GPageStep3Manager;

import io.appium.java_client.android.AndroidDriver;

import com.postepay.automation.core.ui.molecules.BodyInfoG2gSend;


public class SendG2GPageStep1 extends Page {
	public static final String NAME="T018";
	

	public SendG2GPageStep1(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(BodyInfoG2gSend.NAME, UiObjectRepo.get().get(BodyInfoG2gSend.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SendG2GPageStep1Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new SendG2GPageStep1ManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderpage(String inputTitle) {
		((SendG2GPageStep1Manager)this.manager).verifyHeaderpage(inputTitle);
	}
	
	public void verifyLayoutPage() {
		((SendG2GPageStep1Manager)this.manager).verifyLayoutPage();
	}
	
	public void clickOnProcedi() {
		((SendG2GPageStep1Manager)this.manager).clickOnProcedi();
	}
	
	public void verifyImageCard(WebDriver driver, double discrepanza) {
		((SendG2GPageStep1Manager)this.manager).verifyImageCard(driver, discrepanza);
	}
}

