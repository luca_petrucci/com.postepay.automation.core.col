package com.postepay.automation.core.ui.pages.selezionafiltribachecapage;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.AbilitaInAppMolecola;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.SelezionaFiltriMolecola;
import com.postepay.automation.core.ui.pages.parkingpurchasepagestep3.ParkingPurchasePageStep3Manager;


public class SelezionaFiltriBachecaPage extends Page {
	public static final String NAME="T143";
	

	public SelezionaFiltriBachecaPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(SelezionaFiltriMolecola.NAME, UiObjectRepo.get().get(SelezionaFiltriMolecola.NAME), true);
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SelezionaFiltriBachecaPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeaderpage(String titlePageInput)
	{
		((SelezionaFiltriBachecaPageManager)this.manager).verifyHeaderpage(titlePageInput);
	}
	
	public void verifyLayout() 
	{
		((SelezionaFiltriBachecaPageManager)this.manager).verifyLayout();
	}
	
	public void inserisciData()
	{
		((SelezionaFiltriBachecaPageManager)this.manager).inserisciData();
	}
	
	public void controlloFiltro()
	{
		((SelezionaFiltriBachecaPageManager)this.manager).controlloFiltro();
	}
	
	public void clickOnSalva()
	{
		((SelezionaFiltriBachecaPageManager)this.manager).clickOnSalva();
	}
	
	public String getDataSelezionata()
	{
		return ((SelezionaFiltriBachecaPageManager)this.manager).getDataSelezionata();
	}
	
	public void resettaFiltro()
	{
		((SelezionaFiltriBachecaPageManager)this.manager).resettaFiltro();
	}

	public void inserisciDateDifferenti()
	{
		((SelezionaFiltriBachecaPageManager)this.manager).inserisciDateDifferenti();

	}
}

