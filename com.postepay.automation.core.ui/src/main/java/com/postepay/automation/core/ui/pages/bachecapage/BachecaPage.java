package com.postepay.automation.core.ui.pages.bachecapage;

import java.util.List;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.BachecaPageMolecola;

import io.appium.java_client.android.AndroidDriver;


public class BachecaPage extends Page {
	public static final String NAME="T095";
	

	public BachecaPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(BachecaPageMolecola.NAME, UiObjectRepo.get().get(BachecaPageMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new BachecaPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	public void verifyThereIsASpecificAutomaticRecharge(String tipeMessage, String dateOfOperation) {
		((BachecaPageManager)this.manager).verifyThereIsASpecificAutomaticRecharge(tipeMessage, dateOfOperation);
	}
	
	public void verifyHeaderPage() {
		((BachecaPageManager)this.manager).verifyHeaderPage();
	}
	
	public void searchOperation(String operationToSearch, boolean iOS) {
		((BachecaPageManager)this.manager).searchOperation(operationToSearch, iOS);
	}
	
	public void clickOnBackButton() {
		((BachecaPageManager)this.manager).clickOnBackButton();
	}
	
	public void clickOnMESSAGGItab() {
		((BachecaPageManager)this.manager).clickOnMESSAGGItab();
	}

	public void clickOnNOTIFICHEtab() {
		((BachecaPageManager)this.manager).clickOnNOTIFICHEtab();
	}
	
	public void clickOnFilterIcon() {
		((BachecaPageManager)this.manager).clickOnFilterIcon();
	}
	
	public void clickOnSpecificMessage(WebDriver driver, String typeInput, String titleInput, String dateInput) {
		((BachecaPageManager)this.manager).clickOnSpecificMessage(driver, typeInput, titleInput, dateInput);
	}
	
	public void clickOnSpecificNotification(WebDriver driver, String typeInput, String titleInput, String dateInput) {
		((BachecaPageManager)this.manager).clickOnSpecificNotification(driver, typeInput, titleInput, dateInput);
	}
	
	public void verifyResultList(String message) {
		((BachecaPageManager)this.manager).verifyResultList(message);
	}
	
	public void clickPrimoElemento(String titoloTile)
	{
		((BachecaPageManager)this.manager).clickPrimoElemento(titoloTile);
	}
	
	public void controlloFiltroData(String dataInput)
	{
		((BachecaPageManager)this.manager).controlloFiltroData(dataInput);

    }
	
	public int sendAndVerify(String keySearch, boolean isIOS) {
		return ((BachecaPageManager)this.manager).sendAndVerify(keySearch, isIOS);
	}
	
	public List<String> verifyAndOpenTarget(String description) {
		return ((BachecaPageManager)this.manager).verifyAndOpenTarget(description);
	}
	
	public void reserch(String message) {
		((BachecaPageManager)this.manager).reserch(message);
	}
}

