package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SalvadanaioObiettivoGestisciVersamentiRicorrentiMolecola extends Molecule {
	public static final String NAME="M238";
	
	public static final String SALVADANAIOGESTISCIVERSAMENTORICORRENZAATTIVAPROSSIMO="salvadanaioGestisciVersamentoRicorrenzaATTIVAProssimo";
public static final String SALVADANAIOGESTISCIVERSAMENTOAPARTIDEDA="salvadanaioGestisciVersamentoAPartideDa";
public static final String SALVADANAIOGESTISCIVERSAMENTOFREQUENZAVALORE="salvadanaioGestisciVersamentoFrequenzaValore";
public static final String SALVADANAIOGESTISCIVERSAMENTOTERMINEVERSAMENTOVALORE="salvadanaioGestisciVersamentoTermineVersamentoValore";
public static final String SALVADANAIOGESTISCIVERSAMENTOPAGACON="salvadanaioGestisciVersamentoPagaCon";
public static final String SALVADANAIOGESTISCIVERSAMENTOIMPORTO="salvadanaioGestisciVersamentoImporto";
public static final String SALVADANAIOGESTISCIVERSAMENTOFREQUENZA="salvadanaioGestisciVersamentoFrequenza";
public static final String SALVADANAIOGESTISCIVERSAMENTORICORRENZAATTIVA="salvadanaioGestisciVersamentoRicorrenzaATTIVA";
public static final String SALVADANAIOGESTISCIVERSAMENTOPAGACONVALORE="salvadanaioGestisciVersamentoPagaConValore";
public static final String SALVADANAIOGESTISCIVERSAMENTOAPARTIDEDAVALORE="salvadanaioGestisciVersamentoAPartideDaValore";
public static final String SALVADANAIOGESTISCIVERSAMENTOTERMINEVERSAMENTO="salvadanaioGestisciVersamentoTermineVersamento";
public static final String SALVADANAIOGESTISCIVERSAMENTOMODIFICAELIMINAAUTOMATISMO="salvadanaioGestisciVersamentoModificaEliminaAutomatismo";
public static final String SALVADANAIOGESTISCIVERSAMENTOIMPORTOVALORE="salvadanaioGestisciVersamentoImportoValore";


	public SalvadanaioObiettivoGestisciVersamentiRicorrentiMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

