package com.postepay.automation.core.ui.pages.credentialspid;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.PosteSpidPopUpError;
import com.postepay.automation.core.ui.pages.loginwithposteid.LoginWithPosteIdManager;


public class CredentialSPID extends Page {
	public static final String NAME="T081";
	

	public CredentialSPID(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(PosteSpidPopUpError.NAME, UiObjectRepo.get().get(PosteSpidPopUpError.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new CredentialSPIDManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void clickOnOk() {
		((CredentialSPIDManager)this.manager).clickOnOk();
	}
	
	public void clickOnIDonHaveAccount() {
		((CredentialSPIDManager)this.manager).clickOnIDonHaveAccount();
	}
	
	public void verifyLayoutPopUp() {
		((CredentialSPIDManager)this.manager).verifyLayoutPopUp();
	}
}

