package com.postepay.automation.core.ui.pages.salvadanaioobiettivosummary;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.SalvadanaioObiettivoSummaryMolecola;


public class SalvadanaioObiettivoSummary extends Page {
	public static final String NAME="T117";
	

	public SalvadanaioObiettivoSummary(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(SalvadanaioObiettivoSummaryMolecola.NAME, UiObjectRepo.get().get(SalvadanaioObiettivoSummaryMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SalvadanaioObiettivoSummaryManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		((SalvadanaioObiettivoSummaryManager)this.manager).verifyHeaderpage(titlePageInput);
	}
	
	public void verifyLayoutRicorrente() {
		((SalvadanaioObiettivoSummaryManager)this.manager).verifyLayoutRicorrente();
	}
	
	public void verifyLayoutNonRicorrente() {
		((SalvadanaioObiettivoSummaryManager)this.manager).verifyLayoutNonRicorrente();
	}
	
	public void verifyDataUser(String cardNumber, String nomeCognome, String importo) {
		((SalvadanaioObiettivoSummaryManager)this.manager).verifyDataUser(cardNumber, nomeCognome, importo);
	}
	
	public void verifySpecificData(String frequenza, String aPartireDa, String termine) {
		((SalvadanaioObiettivoSummaryManager)this.manager).verifySpecificData(frequenza, aPartireDa, termine);
	}
	
	public void clickOnAUTORIZZA() {
		((SalvadanaioObiettivoSummaryManager)this.manager).clickOnAUTORIZZA();
	}
	
	public void clickOnMODIFICA() {
		((SalvadanaioObiettivoSummaryManager)this.manager).clickOnMODIFICA();
	}
		public void clickOnCONTINUA() {
		((SalvadanaioObiettivoSummaryManager)this.manager).clickOnCONTINUA();
	}

}

