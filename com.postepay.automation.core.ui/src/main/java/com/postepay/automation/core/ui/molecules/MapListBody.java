package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class MapListBody extends Molecule {
	public static final String NAME="M252";
	
	public static final String MAPLISTNAME="mapListName";
public static final String MAPPROMOTIONAL="mapPromotional";
public static final String MAPSNACKBARAUTORIZATIONTEXT="mapSnackbarAutorizationText";
public static final String MAPSNACKBARAUTORIZATIONBUTTON="mapSnackbarAutorizationButton";
public static final String MAPCATEGORYLISTCATEGORY="mapCategoryListCategory";
public static final String MAPLISTINDIRIZZO="mapListIndirizzo";
public static final String MAPDETAILSCARD="mapDetailsCard";
public static final String MAPINSTANTPAYMENT="mapInstantPayment";


	public MapListBody(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

