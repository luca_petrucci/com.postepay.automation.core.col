package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class AmoutCreditToRequestSummary extends Molecule {
	public static final String NAME="M028";

	public static final String MODIFYP2PREQUEST="modifyP2PRequest";
	public static final String USERCONTACTP2PREQUEST="userContactP2PRequest";
	public static final String SENDBUTTONP2PREQUEST="sendButtonP2PRequest";
	public static final String AMOUNTP2PREQUESTSUMMARY="amountP2PRequestSummary";
	public static final String ADDCOMMENTP2PREQUEST="addCommentP2PRequest";
	public static final String ICONP2PREQUESTSUMMARY="iconP2PRequestSummary";


	public AmoutCreditToRequestSummary(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

