package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyExtraurbanPurchase extends Molecule {
	public static final String NAME="M090";

	public static final String DATEEXTRAURBANPURCHASE="dateExtraurbanPurchase";
	public static final String SECONDRADIOEXTRAURBANPURCHASE="secondRadioExtraurbanPurchase";
	public static final String TOCITYEXTRAURBANPURCHASE="toCityExtraurbanPurchase";
	public static final String FIRSTRADIOEXTRAURBANPURCHASE="firstRadioExtraurbanPurchase";
	public static final String FROMCITYEXTRAURBANPURCHASE="fromCityExtraurbanPurchase";
	public static final String BUTTONSEARCHEXTRAURBANPURCHASE="buttonSearchExtraurbanPurchase";
	public static final String NUMBERPASSENGERSEXTRAURBANPURCHASE="numberPassengersExtraurbanPurchase";
	public static final String TIMEEXTRAURBANPURCHASE="timeExtraurbanPurchase";
	public static final String SEARCHBAR="searchExtraurbanSearch";
	public static final String PASSENGERADULTLABELCLICK = "passengersAdultLabel";
	public static final String PASSENGERKINDLABELCLICK = "passengersKidsLabel";
	public static final String PASSENGERADULTNUMBER = "passengersAdultNumber";
	public static final String PASSENGERKINDNUMBER = "passengersKidsNumber";
	public static final String FRECCIADESTRACALENDARIO="frecciaDestraCalendario";
	public static final String CALENDARIO="calendarioExtraUrbano";
	

	public BodyExtraurbanPurchase(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

