package com.postepay.automation.core.ui.pages.mapsgenericscreenfromhomepage;

import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.html5.Location;
import org.openqa.selenium.html5.LocationContext;
import org.openqa.selenium.interactions.touch.TouchActions;

import com.postepay.automation.core.ui.molecules.BodyMapsGenericScreenFromHomePage;
import com.postepay.automation.core.ui.molecules.GenericErrorPopUpMolecola;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.MapListBody;
import com.postepay.automation.core.ui.molecules.NewAccessPageMolecola;
import com.postepay.automation.core.ui.pages.genericerrorpopup.GenericErrorPopUp;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.SoftAssertion;

public class MapsGenericScreenFromHomePageManager extends PageManager {
	private static final WebDriver MobileDriver = null;
//	Particle scontiLabel = (Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.SCONTILABEL);
	Particle apriDettaglio = (Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.BUTTONSCONTIDETTAGLIO);
	
	Particle dettaglioTitolo = (Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.DETTAGLIOTITOLO);
	Particle cardAccredito = (Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.DETTAGLIOCARDACCREDITO);
	Particle cardAccreditoDesc = (Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.DETTAGLIOCARDACCREDITOdESCRIZIONE);
	Particle accreditoLabel = (Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.DETTAGLIOLABEL);
	Particle codiceQR = (Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.MAPSLABELCODICEQR); 
	
	
	Particle qRcategory = (Particle) UiObjectRepo.get().get(MapListBody.MAPCATEGORYLISTCATEGORY);
	Particle qRname = (Particle) UiObjectRepo.get().get(MapListBody.MAPLISTNAME);
	Particle qRaddress = (Particle) UiObjectRepo.get().get(MapListBody.MAPLISTINDIRIZZO);
	Particle qRcashBack = (Particle) UiObjectRepo.get().get(MapListBody.MAPPROMOTIONAL);
	Particle qRInstatPay = (Particle) UiObjectRepo.get().get(MapListBody.MAPINSTANTPAYMENT);
	Particle qRDetails = (Particle) UiObjectRepo.get().get(MapListBody.MAPDETAILSCARD);
	
	LayoutTools toolLayout = new LayoutTools();
	
	public MapsGenericScreenFromHomePageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage(String titlePageInput)
	{
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}
	
	public void clickOnApriDettaglio() {
		SoftAssertion.get().getAssertions().assertThat(dettaglioTitolo.getElement().isDisplayed()).withFailMessage("Titolo non presente").isEqualTo(true);
		apriDettaglio.getElement().click();
		WaitManager.get().waitShortTime();
	}
	
	public void clickOnCodiceQR() {
		codiceQR.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void verifyScontiPostePage() {
//		SoftAssertion.get().getAssertions().assertThat(dettaglioTitolo.getElement().isDisplayed()).withFailMessage("Titolo non presente").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(cardAccredito.getElement().isDisplayed()).withFailMessage("Card Accredito non presente").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(cardAccreditoDesc.getElement().isDisplayed()).withFailMessage("Card Accredito Desc non presente").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(accreditoLabel.getElement().isDisplayed()).withFailMessage("Accredito label non presente").isEqualTo(true);
//		assertTrue(cardAccredito.getElement().isDisplayed());
//		assertTrue(cardAccreditoDesc.getElement().isDisplayed());
//		assertTrue(accreditoLabel.getElement().isDisplayed());
	}
	
	public void isGeoAuthorized(String text) {
		Particle permissionText = null;
		try {
			permissionText = (Particle) UiObjectRepo.get().get(MapListBody.MAPSNACKBARAUTORIZATIONTEXT);
		
			if(permissionText.getElement().isDisplayed()) {
				System.out.println("isGeoAuthorized - isDisplayed");
				Particle permissionBtn = (Particle) UiObjectRepo.get().get(MapListBody.MAPSNACKBARAUTORIZATIONBUTTON);
				permissionBtn.getElement().click();
				WaitManager.get().waitMediumTime();
				System.out.println("isGeoAuthorized - clicked");			
				try {
					GenericErrorPopUp auth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
					auth.isPermissionRequiredSamsung(text);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void clickOnIntantPayFirstMerchant() {
		qRInstatPay.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public String getQRCategory() {
		return qRcategory.getElement().getText();
	}
	
	public String getQRName() {
		return qRname.getElement().getText();
	}
	
	public String getQRAddress() {
		return qRaddress.getElement().getText();
	}
	
	public void clickOnScontiDaAccreditareCard() {
		cardAccredito.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnTabMappa()
	{
		Particle tabMappa =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.TABMAPS);
		tabMappa.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	public void clickOnTabLista()
	{
		Particle tabLista =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.TABLIST);
		tabLista.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnUfficiPostali()
	{
		Particle ufficiPost =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.CARDUFFICIPOSTALI);
		ufficiPost.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnATM()
	{
		Particle atm =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.ATMLABEL);
		atm.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnSosta()
	{
		System.out.println(page.getDriver().getPageSource());
		Particle sosta =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.CARDADOMICILIO);
		sosta.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnCarburante()
	{
		Particle carburante =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.CARDRITIROINNEGOZIO);
		carburante.getElement().click();
		WaitManager.get().waitMediumTime();
	}

	public void scrollChipLabel()
	{
		Dimension size = page.getDriver().manage().window().getSize();
		Particle chip =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.CHIPLABEL);
//		System.out.println(chip.getElement().getText());
		WebElement coordinate = chip.getElement();
		float x = coordinate.getLocation().getX() + coordinate.getRect().width * 0.5f; 
		float y = coordinate.getLocation().getY() + coordinate.getRect().height * 0.5f; 
//		int x = coordinate.getLocation().getX() + Math.round(coordinate.getRect().width * 0.5f); 
//		int y = coordinate.getLocation().getY() + Math.round(coordinate.getRect().height * 0.2f);
		System.out.println("x " + x);
		System.out.println("y " + y);

		float X = x / size.width; 
		float Y = y / size.height;
//		System.out.println("X " + X);
//		System.out.println("Y " + Y);
		System.out.println("Scroll - Start");
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.RIGHT, 
				X, 
				Y, 
				X - (X * 0.4f),
				Y,
				200);
		System.out.println("Scroll - End");
	}
	
	public void verifyPin()
	{
		WaitManager.get().waitMediumTime();
		//System.out.println(page.getDriver().getPageSource());
		Particle pin =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.LISTAPINMAPPE);
		List<WebElement> listaPin = pin.getListOfElements();
		System.out.println("lista "+listaPin);
		assertTrue(listaPin.size()>0);
	}
	
	public void clickOnPin(int inputPin)
	{
		Particle pin =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.LISTAPINMAPPE);
		pin.getListOfElements().get(inputPin).click();
		WaitManager.get().waitShortTime();
	}
	
	public void verifyCategorie(String inputCategoria)
	{
		switch (inputCategoria) {
		case "CASHBACK":
			Particle sconti =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.CARDCASHBACK);
			if(sconti.getElement()==null)
			{
				clickOnPin(0);
			}
			assertTrue(sconti.getElement().isDisplayed());
			break;
		case "CODICE QR":
			Particle qrcode =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.CARDQRCODE);
			if(qrcode.getElement()==null)
			{
				clickOnPin(0);
			}
			assertTrue(qrcode.getElement().isDisplayed());
			break;
		case "UFFICI POSTALI":
			Particle uffPost =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.CARDUFFICIPOSTALI);
			if(uffPost.getElement()==null)
			{
				clickOnPin(0);
			}
			assertTrue(uffPost.getElement().isDisplayed());
			break;
		case "ATM":
			Particle atm =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.CARDATM);
			if(atm.getElement()==null)
			{
				clickOnPin(0);
			}
			assertTrue(atm.getElement().isDisplayed());
			break;
		case "A DOMICILIO":
			Particle sosta =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.CARDADOMICILIO);
			if(sosta.getElement()==null)
			{
				clickOnPin(0);
			}
			sosta.getElement().isDisplayed();		
			break;
		case "RITIRO IN NEGOZIO":
			Particle carburante =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.CARDRITIROINNEGOZIO);
			if(carburante.getElement()==null)
			{
				clickOnPin(0);
			}
			carburante.getElement().isDisplayed();
			break;
		default:
			break;
		}
	}
	
	public void verifyLayoutNewPageMaps(boolean isIOS){
		Particle profilo =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.ICONAPROFILOMAPPE);
		Particle titolo =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.DETTAGLIOTITOLO);
		WebElement cashBackDettaglio = null;
		Particle cercaField =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.SEARCHFIELDMAP);
		Particle filtri =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.FILTERMAPS);
		Particle chipQr =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.CARDQRCODE);
		Particle chipCashback =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.CARDCASHBACK);
		Particle chipUffici =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.CARDUFFICIPOSTALI);
		Particle chipAtm =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.CARDATM);
		Particle chipADomicilio =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.CARDADOMICILIO);
		Particle chipInNegozio =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.CARDRITIROINNEGOZIO);
		Particle tabLista =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.TABLIST);
		Particle pins =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.LISTAPINMAPPE);
		Particle chipAbbigliamento =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.CARDABBIGLIAMENTO);
		Particle chipCarburante =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.CARBURANTELABEL);
		
		assertTrue("Profilo non visible", profilo.getElement() != null);
		assertTrue("Titolo non visible",	titolo.getElement() != null);
//		System.out.println(page.getDriver().getPageSource());
	
		Particle cash =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.BUTTONSCONTIDETTAGLIO);
		assertTrue("Cashback dettaglio non visible",  cash.getElement()!= null);
		
		assertTrue("Cerca non visible", cercaField.getElement() != null);
		assertTrue("Filtri non visibile", filtri.getElement() != null);
//		assertTrue("QR non visible",chipQr.getElement() != null);
		
		assertTrue("Cashback non visible", chipCashback.getElement() != null);
		assertTrue("Uffici  non visible", chipUffici.getElement() != null);
		assertTrue("Tab Lista non visible",tabLista.getElement() != null);
		
		scrollChipLabel();
		
		assertTrue("ATM non visible", chipAtm.getElement() != null);
		assertTrue("Abbigliamento non visible", chipAbbigliamento.getElement() != null);
		assertTrue("Carburante non visible", chipCarburante.getElement() != null);
//		assertTrue("A Domicilio  non visible", chipADomicilio.getElement() != null);
//		assertTrue("In Negozio non visible",chipInNegozio.getElement() != null);
		
		List<WebElement> pinsList = page.getDriver().findElements(pins.getXPath());
		System.out.println("Lista PINS: "+ pinsList);
		assertTrue("Lista minore di uno",pinsList.size() >= 1);
		
		
	}
	
	public void verifyGenericLayout()
	{
		Particle profilo =(Particle) UiObjectRepo.get().get(HeaderGenericAllPage.LEFTBUTTONGENERIC);
		Particle lente =(Particle) UiObjectRepo.get().get(HeaderGenericAllPage.RIGHTBUTTONGENERICICON);
		Particle filtro =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.FILTERMAPS);
		Particle ufficiPost =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.POSTALOFFICELABEL);
		Particle atm =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.ATMLABEL);
		Particle img =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.IMGSCONTO);
		Particle scontiAccreditare =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.TITLESCONTIDAACCREDITARE);
		Particle copySconti =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.SUBTITLESCONTI);
		Particle btnApriDettaglio =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.BTNAPRIDETTAGLIO);
		Particle imgMarker =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.IMGMARKERNEGOZIO);
		Particle tabMappa =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.TABMAPS);
		Particle tabList =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.TABLIST);
		
		
		assertTrue("Profilo non visible", profilo.getElement().isDisplayed());
		assertTrue("Lente non visible",	lente.getElement().isDisplayed());
		assertTrue("Filtro non visible", filtro.getElement().isDisplayed());
//		assertTrue("Sconti non visible",scontiLabel.getElement().isDisplayed());
//		assertTrue("Sconti non selezionato",scontiLabel.getElement().isSelected());
		assertTrue("Ufficio Postale non visible",ufficiPost.getElement().isDisplayed());
		scrollChipLabel();
		assertTrue("ATM non visible",atm.getElement().isDisplayed());
		assertTrue("IMG sconto non visible",img.getElement().isDisplayed());
		assertTrue("Titolo ScontiAccreditare non visible",scontiAccreditare.getElement().isDisplayed());
		assertTrue("Copy Sconti non visible",copySconti.getElement().isDisplayed());
		assertTrue("BTN Apri dettaglio non visible",btnApriDettaglio.getElement().isDisplayed());
		assertTrue("Categoria negozio non visible",qRcategory.getElement().isDisplayed());
		assertTrue("Categoria nome non visible",qRname.getElement().isDisplayed());
		assertTrue("Categoria indirizzo non visible",qRaddress.getElement().isDisplayed());
		assertTrue("Percentuale Sconti non visible",qRcashBack.getElement().isDisplayed());
		assertTrue("IMG negozio non visible",imgMarker.getElement().isDisplayed());
		assertTrue("Tab mappa non visible",tabMappa.getElement().isDisplayed());
		assertTrue("Tab lista non visible",tabList.getElement().isDisplayed());
		assertTrue("Tab lista non selezionato",tabList.getElement().isEnabled());
	}

	public void clickOnCashback() {
		System.out.println(page.getDriver().getPageSource());
		Particle el =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.CARDCASHBACK);
		el.getElement().click();
		
		WaitManager.get().waitShortTime();
		
	}

	public void clickOnCerca() {
		Particle el =(Particle) UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.SEARCHFIELDMAP);
		el.getElement().click();
		
		WaitManager.get().waitShortTime();
		
	}

}

