package com.postepay.automation.core.ui.pages.preloginpostepage;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.PreLoginPostePageMolecola;
import com.postepay.automation.core.ui.verifytools.BasicLayoutControl;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import org.openqa.selenium.WebDriver;

public class PreLoginPostePageManager extends PageManager {

	Particle postePayIntroPreLogin = (Particle) UiObjectRepo.get().get(PreLoginPostePageMolecola.POSTEPAYINTROPRELOGIN);
	Particle postePayIconPreLogin = (Particle) UiObjectRepo.get().get(PreLoginPostePageMolecola.POSTEPAYICONPRELOGIN);
	Particle postePayWelcomePreLogin = (Particle) UiObjectRepo.get().get(PreLoginPostePageMolecola.POSTEPAYWELCOMEPRELOGIN);
	Particle postePayWelcomeDescriptionPreLogin = (Particle) UiObjectRepo.get().get(PreLoginPostePageMolecola.POSTEPAYWELCOMEDESCRIPTIONPRELOGIN);
	Particle postePayWelcomeButtonPreLogin = (Particle) UiObjectRepo.get().get(PreLoginPostePageMolecola.POSTEPAYWELCOMEBUTTONPRELOGIN);
	Particle postePayWelcomeScopriButtonPreLogin = (Particle) UiObjectRepo.get().get(PreLoginPostePageMolecola.POSTEPAYWELCOMESCOPRIBUTTONPRELOGIN);


	public PreLoginPostePageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

//	@Override
//	public void verifyHeaderpage(String titlePageInput) {
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public void verifyImage(AppiumDriver<?> driver, String imageNameInput, double discrepanza) {
//		// TODO Auto-generated method stub
//
//	}

	
	public void verifyLayout(WebDriver driver, double discrepanza) {

		assertTrue(postePayIntroPreLogin.getElement().isDisplayed());
		assertTrue(postePayIconPreLogin.getElement().isDisplayed());
		assertTrue(postePayWelcomePreLogin.getElement().isDisplayed());
		assertTrue(postePayWelcomeDescriptionPreLogin.getElement().isDisplayed());
		assertTrue(postePayWelcomeButtonPreLogin.getElement().isDisplayed());
		assertTrue(postePayWelcomeScopriButtonPreLogin.getElement().isDisplayed());

	}

	public void clickOnAccedi() {
		System.out.println("CLICK SU ACCEDI PRE-LOGIN");
		postePayWelcomeButtonPreLogin.getElement().click();
		WaitManager.get().waitLongTime();
	}

	public void clickOnScopri() {

		postePayWelcomeScopriButtonPreLogin.getElement().click();
		WaitManager.get().waitLongTime();
	}

}

