package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class CarburanteStep1Molecola extends Molecule {
	public static final String NAME="M218";
	
	public static final String FUELBUTTONPROSEGUI="fuelButtonProsegui";
public static final String FIXAMOUNT10="fixAmount10";
public static final String FIXAMOUNT20="fixAmount20";
public static final String FUELMINUS="fuelMinus";
public static final String SETAMOUNTFUELPREFERLABEL="setAmountFuelPreferLabel";
public static final String SETAMOUNTFUELPREFER="setAmountFuelPrefer";
public static final String FIXAMOUNT80="fixAmount80";
public static final String FIXAMOUNTALL="fixAmountAll";
public static final String FUELAMOUNT="fuelAmount";
public static final String FIXAMOUNT50="fixAmount50";
public static final String SELECTIMPORTOLABEL="selectImportoLabel";
public static final String FUELMORE="fuelMore";


	public CarburanteStep1Molecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

