package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyG2gPurchase extends Molecule {
	public static final String NAME="M039";
	
	public static final String AMOUNTOFCOSTFOFIVEGIGA="amountOfCostFoFiveGiga";
public static final String PAYWITHCONNECTFORPURCHASEGIGAPAGE="payWithConnectForPurchaseGigaPage";
public static final String HEADERCONNECTSIMPURCHASEGIGAPAGE="headerConnectSimPurchaseGigaPage";
public static final String PAYBUTTONTOPURCHASEFIVEGIGA="payButtonToPurchaseFiveGiga";
public static final String NUMBEROFGIGATOPURCHASE="numberOfGigaToPurchase";


	public BodyG2gPurchase(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

