package com.postepay.automation.core.ui.pages.rechargeotherpostepaypagestep1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.BodyRechargeOtherPostepay;
import com.postepay.automation.core.ui.molecules.SelectACardToRechargeMyPostepay;
import com.postepay.automation.core.ui.molecules.TabsPaymentSection;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class RechargeOtherPostepayPageStep1Manager extends PageManager {
	
	LayoutTools toolLayout = new LayoutTools();
	
	public RechargeOtherPostepayPageStep1Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}
	
	public void verifyImageCard(WebDriver driver, double discrepanza) {
		String nameText = "ricaricaPostepay/icona_rubrica";
		LayoutImage.get().verifyImage(driver, nameText, discrepanza);
	}
	
	public void clickOnContactNumberMenu() {
		Particle tab=(Particle) UiObjectRepo.get().get(BodyRechargeOtherPostepay.TELEPHONCONTACTSOTHERPOSTEPAY);

		tab.getElement().click();		

		WaitManager.get().waitShortTime();

	}

	public void insertCardNumberTargetUser(String value) {
		Particle tab=(Particle) UiObjectRepo.get().get(BodyRechargeOtherPostepay.NUMBEROTHERPOSTEPAY);

		tab.getElement().sendKeys(value);			

		WaitManager.get().waitShortTime();

		//((AppiumDriver<?>)page.getDriver()).hideKeyboard();	

		WaitManager.get().waitShortTime();
	}
	
	public String getCardNumberTargetUser() {
		Particle tab=(Particle) UiObjectRepo.get().get(BodyRechargeOtherPostepay.NUMBEROTHERPOSTEPAY);

		String card=tab.getElement().getText();			

		WaitManager.get().waitShortTime();
		
		return card;
	}

	public void insertNameTargetUser(String value) {
		Particle tab=(Particle) UiObjectRepo.get().get(BodyRechargeOtherPostepay.HOLDEROTHERPOSTEPAY);

		tab.getElement().sendKeys(value);			

		WaitManager.get().waitShortTime();

		//((AppiumDriver<?>)page.getDriver()).hideKeyboard();	

		WaitManager.get().waitShortTime();
	}
	
	public String getNameTargetUser() {
		Particle tab=(Particle) UiObjectRepo.get().get(BodyRechargeOtherPostepay.HOLDEROTHERPOSTEPAY);

		String name=tab.getElement().getText();			
		
		WaitManager.get().waitShortTime();
		
		return name;
	}

	public void insertAmountToSendTargetUser(String value) {
		Particle tab=(Particle) UiObjectRepo.get().get(BodyRechargeOtherPostepay.AMOUNTTOSENDTOOTHERPOSTEPAY);

		tab.getElement().sendKeys(value);			

		WaitManager.get().waitShortTime();

		//((AppiumDriver<?>)page.getDriver()).hideKeyboard();	

		WaitManager.get().waitShortTime();
	}

	public void insertReasonOfRecharge(String value) {
		Particle tab=(Particle) UiObjectRepo.get().get(BodyRechargeOtherPostepay.REASONOFRECHARGEOTHERPOSTEPAY);

		tab.getElement().sendKeys(value);	

		WaitManager.get().waitShortTime();

		//((AppiumDriver<?>)page.getDriver()).hideKeyboard();
		
		//serve per chiudere la tastiera
		Particle rubrica=(Particle) UiObjectRepo.get().get(BodyRechargeOtherPostepay.SAVECONTACTOTHERCARD);
		rubrica.getElement().click();
		
		WaitManager.get().waitShortTime();
	}

	public void clickOnToggleAutomatickRecharge() {
		Particle tab=(Particle) UiObjectRepo.get().get(BodyRechargeOtherPostepay.AUTOMATICHRECHARGETOGGLE);

		tab.getElement().click();		
		
		WaitManager.get().waitMediumTime();
		
		try {
			UIUtils.mobile().swipe((MobileDriver<?>) page.getDriver(), SCROLL_DIRECTION.DOWN, 500);
		} catch (Exception err )  {}
		
		WaitManager.get().waitShortTime();
	}

	public void clickOnSaveContact() {
		Particle tab=(Particle) UiObjectRepo.get().get(BodyRechargeOtherPostepay.SAVECONTACTOTHERCARD);

		tab.getElement().click();		

		WaitManager.get().waitShortTime();

	}

	public void clickOnProceedButton() {
		Particle tab=(Particle) UiObjectRepo.get().get(BodyRechargeOtherPostepay.PROCEEDRECHARGEBUTTON);

		tab.getElement().click();		

		WaitManager.get().waitShortTime();

	}

	public void compileAllStandardForm(String valueCardTarget, String valueTargetUser, String valueAmount, String valueReason) {

		insertCardNumberTargetUser(valueCardTarget);
		
		insertNameTargetUser(valueTargetUser);

		insertAmountToSendTargetUser(valueAmount);

		insertReasonOfRecharge(valueReason);
	}
	
	public void verifyDestinatarioAndCardNumber(String valueStatus, String targetOperation, String nameTarget, String cardNumberInput) {
		if (valueStatus.equals("popolati")) {
			
			String name=getNameTargetUser();
			String card=getCardNumberTargetUser();
			
			// Tool per Stringhe e numeri
			StringAndNumberOperationTools operationTool=new StringAndNumberOperationTools();
			
			// Aggiustamento nomeTargetInput
			String newNameInput=nameTarget.toUpperCase();
//			System.out.println("newNameInput " + newNameInput);
			
			// Aggiustamento cardTargetInput
//			System.out.println("cardTargetInput "+ cardTargetInput);
			String newCardTargetInput= operationTool.convertNumberToHiddeNumber(cardNumberInput, targetOperation);
//			System.out.println("newCardTargetInput "+newCardTargetInput);
			System.out.println("Expected: "+ newNameInput +" ---> Actual: "+name);
			assertEquals(newNameInput, name);
//			System.out.println("newNameInput OK");
			System.out.println("Expected: "+ newCardTargetInput +" ---> Actual: "+card);
			assertEquals(newCardTargetInput, card);
//			System.out.println("newCardTargetInput OK");
			
		}
		else if (valueStatus.equals("vuoti")) {
			String name=getNameTargetUser();
			String card=getCardNumberTargetUser();
			
			// Aggiustamento cardTargetInput
			System.out.println("Expected: null ---> Actual: "+name);
			assertEquals("Intestata a", name);
//			System.out.println("newNameInput OK");
			System.out.println("Expected: null ---> Actual: "+card);
			assertEquals("Numero carta", card);
//			System.out.println("newCardTargetInput OK");
		}
	}
	
//	public void verifyLabelChooseFroTelephoneNumber() {
//		Particle p=(Particle) UiObjectRepo.get().get(BodyRechargeOtherPostepay.SUBTITLEOTHERPOSTEPAY);
//		String nuovoLocator=Utility.replacePlaceHolders(p.getLocator(), new Entry("title","Scegli da rubrica"));
//		
//		((AppiumDriver<?>)page.getDriver()).findElement(By.xpath(nuovoLocator)).click();
//		
//		WaitManager.get().waitShortTime();
//
//	}
	
	public void verifyTitle(WebDriver driver, String titleInput) {
		Particle title=(Particle) UiObjectRepo.get().get(BodyRechargeOtherPostepay.SUBTITLEOTHERPOSTEPAY);
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, title, titleInput);
		assertTrue(elm.isDisplayed());
	}
}

