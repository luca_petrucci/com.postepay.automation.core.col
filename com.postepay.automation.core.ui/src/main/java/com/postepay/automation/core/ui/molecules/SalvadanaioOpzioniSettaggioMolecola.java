package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SalvadanaioOpzioniSettaggioMolecola extends Molecule {
	public static final String NAME="M230";
	
	public static final String FREQUENZAOGNI7GIORNI="frequenzaOgni7giorni";
public static final String SALVADANAIOPICKERVERSACONPPEVOLUTION="salvadanaioPickerVersaconPpEvolution";
public static final String SALVADANAIOPICKERGESTISCIBP="salvadanaioPickerGestisciBp";
public static final String SALVADANAIOBOTTOMPICKER="salvadanaioBottomPicker";
public static final String SALVASANAIOPICKERCONDIVIDI="salvasanaioPickerCondividi";
public static final String FREQUENZAOGNI30GIORNI="frequenzaOgni30giorni";
public static final String GESTISCIVERSAMENTIRICORRENTI="gestisciVersamentiRicorrenti";
public static final String FREQUENZAOGNI15GIORNI="frequenzaOgni15giorni";
public static final String FREQUENZAOGNIGIORNO="frequenzaOgniGiorno";


	public SalvadanaioOpzioniSettaggioMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

