package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class WhomWontSendABonificoSepaSummary extends Molecule {
	public static final String NAME="M053";

	public static final String SUBTITLESUMMARYBANKTRANFERTSEPA="subTitleSummaryBankTranfertSEPA";
	public static final String SUMMARYHOLDERTORECHARGEPAYBANKTRANFERTSEPA="summaryHolderToRechargePayBankTranfertSEPA";
	public static final String CONFIRMBANKTRANFERTSEPATOSEND="confirmBankTranfertSEPAToSend";
	public static final String SUMMARYREASONOFBANKTRANFERTSEPASUMMARY="summaryReasonOfBankTranfertSEPASummary";
	public static final String SUMMARYPAYTOIBANBANKTRANFERTSEPA="summaryPayToIbanBankTranfertSEPA";
	public static final String SUMMARYPAYWITHBANKTRANFERTSEPA="summaryPayWithBankTranfertSEPA";
	public static final String SUMMARYAMOUNTTOPAYBANKTRANFERTSEPA="summaryAmountToPayBankTranfertSEPA";
	public static final String COUNTRYRESIDENCEHOLDERBANKTRANFERTSEPA="countryResidenceHolderBankTranfertSEPA";
	public static final String SUMMARYAMOUTOFCOMMISSIONBANKTRANFERTSEPA="summaryAmoutOfCommissionBankTranfertSEPA";


	public WhomWontSendABonificoSepaSummary(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

