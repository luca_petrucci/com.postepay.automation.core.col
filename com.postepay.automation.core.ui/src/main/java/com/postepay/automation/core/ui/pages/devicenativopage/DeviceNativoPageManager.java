package com.postepay.automation.core.ui.pages.devicenativopage;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.postepay.automation.core.ui.molecules.BPOLMolecola;
import com.postepay.automation.core.ui.molecules.BachecaPageMolecola;
import com.postepay.automation.core.ui.molecules.DeviceNativoMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import test.automation.core.UIUtils;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;


public class DeviceNativoPageManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();
	private WebDriver driver;
	public DeviceNativoPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}
	
	public void verifyImage() {
		
	}
	
	public void verifyLayout(String inputTitolo, String inputCopy, String inputDescrizione)
	{
		Particle titleNotifica = (Particle) UiObjectRepo.get().get(DeviceNativoMolecola.TITLENOTIFICAPUSH);
		titleNotifica.setDriver(page.getDriver());
		String locatorNotifica =Utility.replacePlaceHolders(titleNotifica.getLocator(), new Entry("title", inputTitolo));
		page.getDriver().findElement(By.xpath(locatorNotifica));
		assertTrue("Titolo notifica push non è presente", page.getDriver().findElement(By.xpath(locatorNotifica)).isDisplayed());
		
		Particle copy = (Particle) UiObjectRepo.get().get(DeviceNativoMolecola.COPYNOTIFICAPUSH);
		copy.setDriver(page.getDriver());
		String locatorCopy=Utility.replacePlaceHolders(copy.getLocator(), new Entry("title", inputCopy));
		page.getDriver().findElement(By.xpath(locatorCopy));
		assertTrue("Copy non presente", page.getDriver().findElement(By.xpath(locatorCopy)).isDisplayed());
		
		Particle descrizione = (Particle) UiObjectRepo.get().get(DeviceNativoMolecola.DESCRIZIONENOTIFICAPUSH);
		descrizione.setDriver(page.getDriver());
		String locatorDesc=Utility.replacePlaceHolders(descrizione.getLocator(), new Entry("title", inputDescrizione));
		page.getDriver().findElement(By.xpath(locatorDesc));
		assertTrue("Descrizione non presente", page.getDriver().findElement(By.xpath(locatorDesc)).isDisplayed());
	}
	
	public void clickOnNotification(String inputCopy)
	{
		Particle copy = (Particle) UiObjectRepo.get().get(DeviceNativoMolecola.COPYNOTIFICAPUSH);
		String locatorCopy=Utility.replacePlaceHolders(copy.getLocator(), new Entry("title", inputCopy));
		page.getDriver().findElement(By.xpath(locatorCopy));
		copy.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
}

