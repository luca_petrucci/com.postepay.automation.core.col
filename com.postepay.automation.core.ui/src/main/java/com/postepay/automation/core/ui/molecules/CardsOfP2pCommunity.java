package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class CardsOfP2pCommunity extends Molecule {
	public static final String NAME="M019";

	public static final String REQUESTP2PCOMMUNITY="requestP2PCommunity";
	public static final String SENDP2PCOMMUNITY="sendP2PCommunity";
	public static final String HEADERGAMEANDWIN="headerGameAndWin";
	public static final String TEXTGAMEANDWIN="textGameAndWin";


	public CardsOfP2pCommunity(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

