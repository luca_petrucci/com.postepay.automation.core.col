package com.postepay.automation.core.ui.common.datatable;

// Classe v2.1
public class LayoutDataBean {

	private String discrepanza;
	private String carburantePage;
	private String landingPage;
	private String targetOperation;
	private String cardNumber;
	private String compagnia;
	private String titoloMessaggio;
	private String titoloDettaglioBiglietto;
	private String nomeObiettivo;
	private String isRicorrente;
	private String typeObiettivo;
	private String importo;
	private String frequenza;
	private String startTime;
	private String userNumberCards;
	private String nomeCognome;
	
	public String getNomeCognome() {
		return nomeCognome;
	}
	public void setNomeCognome(String nomeCognome) {
		this.nomeCognome = nomeCognome;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getUserNumberCards() {
		return userNumberCards;
	}
	public void setUserNumberCards(String userNumberCards) {
		this.userNumberCards = userNumberCards;
	}
	public String getFrequenza() {
		return frequenza;
	}
	public void setFrequenza(String frequenza) {
		this.frequenza = frequenza;
	}
	public String getImporto() {
		return importo;
	}
	public void setImporto(String importo) {
		this.importo = importo;
	}
	public String getIsRicorrente() {
		return isRicorrente;
	}
	public String getTypeObiettivo() {
		return typeObiettivo;
	}
	public void setTypeObiettivo(String typeObiettivo) {
		this.typeObiettivo = typeObiettivo;
	}
	public void setIsRicorrente(String isRicorrente) {
		this.isRicorrente = isRicorrente;
	}
	public String getNomeObiettivo() {
		return nomeObiettivo;
	}
	public void setNomeObiettivo(String nomeObiettivo) {
		this.nomeObiettivo = nomeObiettivo;
	}
	public String getCompagnia() {
		return compagnia;
	}
	public String getTitoloMessaggio() {
		return titoloMessaggio;
	}
	public void setTitoloMessaggio(String titoloMessaggio) {
		this.titoloMessaggio = titoloMessaggio;
	}
	public String getTitoloDettaglioBiglietto() {
		return titoloDettaglioBiglietto;
	}
	public void setTitoloDettaglioBiglietto(String titoloDettaglioBiglietto) {
		this.titoloDettaglioBiglietto = titoloDettaglioBiglietto;
	}
	public void setCompagnia(String compagnia) {
		this.compagnia = compagnia;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getTargetOperation() {
		return targetOperation;
	}
	public void setTargetOperation(String targetOperation) {
		this.targetOperation = targetOperation;
	}
	public String getLandingPage() {
		return landingPage;
	}
	public void setLandingPage(String landingPage) {
		this.landingPage = landingPage;
	}
	
	public String getCarburantePage() {
		return carburantePage;
	}
	public void setCarburantePage(String carburantePage) {
		this.carburantePage = carburantePage;
	}

	public String getDiscrepanza() {
		return discrepanza;
	}

	public void setDiscrepanza(String discrepanza) {
		this.discrepanza = discrepanza;
	}

}
