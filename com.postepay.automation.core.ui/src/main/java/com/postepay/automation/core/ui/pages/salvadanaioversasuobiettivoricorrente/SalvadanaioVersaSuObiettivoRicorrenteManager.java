package com.postepay.automation.core.ui.pages.salvadanaioversasuobiettivoricorrente;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.SalvadanaioAccantonamentoDettaglioMolecola;
import com.postepay.automation.core.ui.molecules.SalvadanaioVersaSuObiettivoMolecola;
import com.postepay.automation.core.ui.molecules.SalvadanaioVersaSuObiettivoRicorrenteMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;
import org.openqa.selenium.WebDriver;

import io.appium.java_client.HidesKeyboard;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class SalvadanaioVersaSuObiettivoRicorrenteManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();

	Particle dettaglioNome = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTOTITOLO);
	Particle versamentoTestoVersameto = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTOTESTO);
	Particle versamentoLimiteMassimo = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTOLIMITEMASSIMO);
	Particle versamentoMeno = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTOMENO);
	Particle versamentoPiu = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTOPIU);
	Particle versamentoAmount = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTOAMOUNT);
	Particle versamentoRicorrenteLabel = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTORICORRENTELABEL);
	Particle versamentoRicorrenteToggle = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTORICORRENTETOGGLE);
	Particle versamentoButton = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoMolecola.SALVADANAIOVERSAMENTOBUTTON);
	Particle ricorrenteFrequenzaLabel = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoRicorrenteMolecola.SALVADANAIOOBIETTIVORICORRENTEFREQUENZALABEL);
	Particle ricorrenteFrequenzaValue = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoRicorrenteMolecola.SALVADANAIOOBIETTIVORICORRENTEFREQUENZAVALUE);
	Particle ricorrenteAPartireDaLabel = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoRicorrenteMolecola.SALVADANAIOOBIETTIVORICORRENTEAPARTIREDALABEL);
	Particle ricorrenteAPartireDaValue = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoRicorrenteMolecola.SALVADANAIOOBIETTIVORICORRENTEAPARTIREDAVALUE);
	Particle ricorrenteTermineVersamentoLabel = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoRicorrenteMolecola.SALVADANAIOOBIETTIVORICORRENTETERMINEVERSAMENTOLABEL);
	Particle ricorrenteTermineVersamentoLabelAlRaggiungimento = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoRicorrenteMolecola.SALVADANAIOOBIETTIVORICORRENTETERMINEVERSAMENTOLABELALRAGGIUNGIMENTO);
	Particle ricorrenteTermineVersamentoLabelDataSpecifica = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoRicorrenteMolecola.SALVADANAIOOBIETTIVORICORRENTETERMINEVERSAMENTOLABELDATASPECIFICA);
	Particle ricorrenteDescrizioneUno = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoRicorrenteMolecola.SALVADANAIOOBIETTIVORICORRENTEDESCRIZIONEUNO);
	Particle ricorrenteDescrizioneDue = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoRicorrenteMolecola.SALVADANAIOOBIETTIVORICORRENTEDESCRIZIONEDUE);
	Particle obiettivoDataSpecificaDescrizione = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoRicorrenteMolecola.SALVADANAIOOBIETTIVODATASPECIFICADESCRIZIONE);
	Particle obiettivoDataSpecificaDataSeleziona = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoRicorrenteMolecola.SALVADANAIOOBIETTIVODATASPECIFICADATASELEZIONA);
	Particle obiettivoDataSpecificaData = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoRicorrenteMolecola.SALVADANAIOOBIETTIVODATASPECIFICADATA);
	Particle raggiungeraiCardImage = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoRicorrenteMolecola.SALVADANAIORAGGIUNGERAICARDIMAGE);
	Particle raggiungeraiCardMessaggio1Settimana = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoRicorrenteMolecola.SALVADANAIORAGGIUNGERAICARDMESSAGGIO1SETTIMANA);
	Particle raggiungeraiCardMessaggio2Settimana = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoRicorrenteMolecola.SALVADANAIORAGGIUNGERAICARDMESSAGGIO2SETTIMANA);
	Particle raggiungeraiMessaggioConsiglio = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoRicorrenteMolecola.SALVADANAIORAGGIUNGERAIMESSAGGIOCONSIGLIO);
	Particle raggiungeraiCardMessaggio2SettimanaDopoLaScadenza = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoRicorrenteMolecola.SALVADANAIORAGGIUNGERAICARDMESSAGGIO2SETTIMANADOPOLASCADENZA);
	Particle raggiungeraiCardMessaggio2SettimanaEntroLaScadenza = (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoRicorrenteMolecola.SALVADANAIORAGGIUNGERAICARDMESSAGGIO2SETTIMANAENTROLASCADENZA);
		Particle salvadanaioGiornoFestivoNonSelezionabilePopup= (Particle) UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoRicorrenteMolecola.SALVADANAIOGIORNOFESTIVONONSELEZIONABILEPOPUP);
		
	public SalvadanaioVersaSuObiettivoRicorrenteManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}

	public WebElement getElementName(WebDriver driver, String nameInput) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, dettaglioNome, nameInput);
		assertTrue(elm.isEnabled());
		return elm;
	}
	
	public void verifyImage(WebDriver driver, String imageNameInput, double discrepanza) {
		String imageName = "salvadanaio/" + imageNameInput;
		LayoutImage.get().verifyImage(driver, imageName, discrepanza);
	}
	
	public void verifyLayoutPageDefault(WebDriver driver, String nameInput, double discrepanza) {
		getElementName(driver, nameInput);
		assertTrue(versamentoTestoVersameto.getElement().isDisplayed());
		assertTrue(versamentoLimiteMassimo.getElement().isDisplayed());
		assertTrue(versamentoMeno.getElement().isDisplayed());
		assertTrue(versamentoPiu.getElement().isDisplayed());
		assertTrue(versamentoAmount.getElement().isDisplayed());
		assertTrue(versamentoRicorrenteLabel.getElement().isDisplayed());
		assertTrue(versamentoRicorrenteToggle.getElement().isDisplayed());
		UIUtils.ui().mobile().swipe((MobileDriver) page.getDriver(), SCROLL_DIRECTION.DOWN, 200);
		assertTrue(versamentoButton.getElement().isDisplayed());
		
		assertTrue(ricorrenteFrequenzaLabel.getElement().isDisplayed());
		assertTrue(ricorrenteFrequenzaValue.getElement().getText().equals("Ogni giorno"));
		assertTrue(ricorrenteAPartireDaLabel.getElement().isDisplayed());
		assertTrue(ricorrenteAPartireDaValue.getElement().getText().equals("Oggi"));
		assertTrue(ricorrenteTermineVersamentoLabel.getElement().isDisplayed());
		assertTrue(ricorrenteTermineVersamentoLabelAlRaggiungimento.getElement().isDisplayed());
		assertTrue(ricorrenteTermineVersamentoLabelDataSpecifica.getElement().isDisplayed());
		assertTrue(ricorrenteDescrizioneUno.getElement().isDisplayed());
		assertTrue(ricorrenteDescrizioneDue.getElement().isDisplayed());
		
		verifyImage(driver, "toggle_enabled", discrepanza);
		verifyImage(driver, "bar_al_raggiungimento", discrepanza);
		verifyImage(driver, "matita_icon", discrepanza);
	}
	
	public void clickOnMinus(int times) {
		versamentoAmount.getElement().clear();
		for (int i = 0; i < times; i++) {
			versamentoMeno.getElement().click();
			WaitManager.get().waitShortTime();
		}
	}
	
	public void clickOnPlus(int times) {
		versamentoAmount.getElement().clear();
		for (int i = 0; i < times; i++) {
			versamentoPiu.getElement().click();
			WaitManager.get().waitShortTime();
		}
	}
	
	public void sendAmountValue(String importo) {
		versamentoAmount.getElement().clear();
		versamentoAmount.getElement().sendKeys(importo);
		WaitManager.get().waitShortTime();
	}
	
	public void scrollToElementUP(WebDriver driver) {

		WebElement firstRoot = ricorrenteFrequenzaLabel.getElement();
		toolLayout.scrollWithCoordinate(driver,
				firstRoot.getLocation().getX()+200, firstRoot.getLocation().getX()+200,
				firstRoot.getLocation().getY(), firstRoot.getLocation().getY()+1500);
		WaitManager.get().waitHighTime();
	}

	public void scrollToElementDOWN(WebDriver driver) {

		WebElement lastRoot = ricorrenteFrequenzaLabel.getElement();
		toolLayout.scrollWithCoordinate(driver,
				lastRoot.getLocation().getX()+200, lastRoot.getLocation().getX()+200,
				lastRoot.getLocation().getY(), lastRoot.getLocation().getY()-800);
		WaitManager.get().waitHighTime();

	}
	
	public void scrollToElementDOWNDinamic(WebDriver driver, int size) {

		WebElement lastRoot = ricorrenteFrequenzaLabel.getElement();
		toolLayout.scrollWithCoordinate(driver,
				lastRoot.getLocation().getX()+200, lastRoot.getLocation().getX()+200,
				lastRoot.getLocation().getY(), lastRoot.getLocation().getY()-size);
		WaitManager.get().waitHighTime();

	}
	
	public void verifyLayoutDinamicoAlRaggiungimento(WebDriver driver, String typeMessage, String importo, double discrepanza) {
		
		if (typeMessage.equals("2settimane")) {
			clickOnPlus(1);
			String amount = versamentoAmount.getElement().getText();
			assertTrue(amount.equals("5,00") );
			assertTrue(raggiungeraiCardMessaggio2Settimana.getElement().isDisplayed());
			
		} else if (typeMessage.equals("1giorno")) {
			clickOnPlus(10);
			String amount = versamentoAmount.getElement().getText();
			assertTrue(amount.equals("50,00") );
			assertTrue(raggiungeraiCardMessaggio2SettimanaEntroLaScadenza.getElement().isDisplayed());
		
		} else if (typeMessage.equals("oltre")) {
			sendAmountValue(importo);
			String amount = versamentoAmount.getElement().getText();
			assertTrue(amount.equals("0,10") );
			assertTrue(raggiungeraiCardMessaggio2SettimanaDopoLaScadenza.getElement().isDisplayed());
		
			try {
				scrollToElementDOWN(driver);
			} catch (Exception e) {
				// TODO: handle exception
			}
//			assertTrue(raggiungeraiMessaggioConsiglio.getElement().isDisplayed());
//			verifyImage(driver, "cassaforte_versa_ricorrente", discrepanza);
			
			WaitManager.get().waitShortTime();
			
			return;
		}
		
		try {
			scrollToElementDOWN(driver);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		assertTrue(raggiungeraiMessaggioConsiglio.getElement().isDisplayed());
		verifyImage(driver, "cassaforte_versa_ricorrente", discrepanza);
		
		WaitManager.get().waitShortTime();
		
		try {
			scrollToElementUP(driver);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}	
	public void controllaPopupGiornoFestivo() {
		
	}
	
	public void clickOnCONTINUA() {
			versamentoButton.getElement().click();
		WaitManager.get().waitShortTime();
		try {
			salvadanaioGiornoFestivoNonSelezionabilePopup.getElement().click();
			System.out.println("Il giorno selezionato è un festivo");
		} catch (Exception e) {
		}
		WaitManager.get().waitLongTime();
	}
	
	public void clickOnFrequenza() {
		ricorrenteFrequenzaValue.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnApartireDa() {
		ricorrenteAPartireDaValue.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public String getFrequenzaText() {
		return ricorrenteFrequenzaValue.getElement().getText();
	}
	
	public String getAPartireDaText() {
		return ricorrenteAPartireDaValue.getElement().getText();
	}
	
	public void clickOnDataSpecifica() {
		ricorrenteTermineVersamentoLabelDataSpecifica.getElement().click();
		WaitManager.get().waitMediumTime();
	}

	public void varifyLayoutDinamicoDataSpecifica() {
		assertTrue(obiettivoDataSpecificaDescrizione.getElement().isDisplayed());
		assertTrue(obiettivoDataSpecificaDataSeleziona.getElement().isDisplayed());
		assertTrue(obiettivoDataSpecificaData.getElement().isDisplayed());
	}
	
	public String getDataTermine() {
		return obiettivoDataSpecificaData.getElement().getText();
	}
	
	public void clickOnSelectDataSpecifica() {
		obiettivoDataSpecificaData.getElement().click();
		WaitManager.get().waitMediumTime();
	}
}

