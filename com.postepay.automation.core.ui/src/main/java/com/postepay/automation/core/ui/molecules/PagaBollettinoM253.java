package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class PagaBollettinoM253 extends Molecule {
	public static final String NAME="M253";
	
	public static final String M253DESCRIPTION="M253description";
public static final String M253IMAGE="M253image";
public static final String M253TITLE="M253title";
public static final String M253BTNCOMPILAMANUALE="M253btnCompilaManuale";
public static final String M253BTNINQUADRA="M253btnInquadra";


	public PagaBollettinoM253(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

