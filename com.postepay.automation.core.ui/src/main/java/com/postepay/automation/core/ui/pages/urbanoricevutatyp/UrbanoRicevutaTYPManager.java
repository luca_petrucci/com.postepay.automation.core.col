package com.postepay.automation.core.ui.pages.urbanoricevutatyp;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.ServiziTYPMolecola;
import com.postepay.automation.core.ui.molecules.UrbanoRicevutaTYPMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class UrbanoRicevutaTYPManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();

	Particle image = (Particle) UiObjectRepo.get().get(UrbanoRicevutaTYPMolecola.URBANORICEVUTAIMAGE);
	Particle totaleLebel = (Particle) UiObjectRepo.get().get(UrbanoRicevutaTYPMolecola.URBANORICEVUTATOTALELABEL);
	Particle totale = (Particle) UiObjectRepo.get().get(UrbanoRicevutaTYPMolecola.URBANORICEVUTATOTALE);
	Particle bigliettoLabel = (Particle) UiObjectRepo.get().get(UrbanoRicevutaTYPMolecola.URBANORICEVUTATIPOBIGLIETTOLABEL);
	Particle biglietto = (Particle) UiObjectRepo.get().get(UrbanoRicevutaTYPMolecola.URBANORICEVUTATIPOBIGLIETTO);
	Particle compagniaLabel = (Particle) UiObjectRepo.get().get(UrbanoRicevutaTYPMolecola.URBANORICEVUTACOMPAGNIALABEL);
	Particle compagnia = (Particle) UiObjectRepo.get().get(UrbanoRicevutaTYPMolecola.URBANORICEVUTACOMPAGNIA);
	Particle dataLabel = (Particle) UiObjectRepo.get().get(UrbanoRicevutaTYPMolecola.URBANORICEVUTADATALABEL);
	Particle data = (Particle) UiObjectRepo.get().get(UrbanoRicevutaTYPMolecola.URBANORICEVUTADATA);
	Particle postepayLabel = (Particle) UiObjectRepo.get().get(UrbanoRicevutaTYPMolecola.URBANORICEVUTAPOSTEPAYLABEL);
	Particle postepay = (Particle) UiObjectRepo.get().get(UrbanoRicevutaTYPMolecola.URBANORICEVUTAPOSTEPAY);
	Particle btnMieiAcquisti = (Particle) UiObjectRepo.get().get(UrbanoRicevutaTYPMolecola.URBANORICEVUTABOTTONEMIEIACQUISTI);
	
	public UrbanoRicevutaTYPManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderPage(String inputTitle) {
		toolLayout.verifyHeaderPage(inputTitle);
	}
	
	public void verifyLayoutPage() {
		assertTrue(image.getElement().isDisplayed());
		assertTrue(totaleLebel.getElement().isDisplayed());
		assertTrue(totale.getElement().isDisplayed());
		assertTrue(bigliettoLabel.getElement().isDisplayed());
		assertTrue(biglietto.getElement().isDisplayed());
		assertTrue(compagniaLabel.getElement().isDisplayed());
		assertTrue(compagnia.getElement().isDisplayed());
		assertTrue(dataLabel.getElement().isDisplayed());
		assertTrue(data.getElement().isDisplayed());
		assertTrue(postepayLabel.getElement().isDisplayed());
		assertTrue(postepay.getElement().isDisplayed());
		assertTrue(btnMieiAcquisti.getElement().isDisplayed());
	}
	
	public String getImporto() {
		return totale.getElement().getText();
	}
	
	public String getDate() {
		return data.getElement().getText();
	}
	
	public String getCardNumber() {
		return postepay.getElement().getText();
	}
	
	public String getBiglietto() {
		return biglietto.getElement().getText();
	}

	public String getCompagnia() {
		return compagnia.getElement().getText();
	}
	
	public void verifyDataInRicevuta(String importoInput, String dataInput, String cardInput) {
		System.out.println("Importo: " + importoInput + " --> " + getImporto());
		System.out.println("Data: " + dataInput + " --> " + getDate());
		System.out.println("Numero Carta: " + cardInput + " --> " + getCardNumber());
		System.out.println("Tipo Biglietto: Corsa Semplice --> " + getBiglietto());
		System.out.println("Compagnia: AGO UNO Srl --> " + getCompagnia());

		assertTrue(getImporto().equals(importoInput));
		assertTrue(getDate().equals(dataInput));
		assertTrue(getCardNumber().equals(cardInput));
		assertTrue(getBiglietto().equals("Corsa Semplice"));
		assertTrue(getCompagnia().equals("AGO UNO Srl"));
	}
	
	public void verifyImageCard(WebDriver driver, double discrepanza) {

		LayoutImage.get().verifyImage(driver, "biglietto/ricevuta_urbano", discrepanza);
	}
	
	public void clickOnMieiAcquisti() {
		btnMieiAcquisti.getElement().click();
		WaitManager.get().waitLongTime();
	}
	
}

