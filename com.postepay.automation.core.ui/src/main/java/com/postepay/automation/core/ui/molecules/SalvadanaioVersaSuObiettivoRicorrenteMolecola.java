package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SalvadanaioVersaSuObiettivoRicorrenteMolecola extends Molecule {
	public static final String NAME="M234";
	
	public static final String SALVADANAIOOBIETTIVORICORRENTEAPARTIREDAVALUE="salvadanaioObiettivoRicorrenteAPartireDaValue";
public static final String SALVADANAIOOBIETTIVORICORRENTETERMINEVERSAMENTOLABEL="salvadanaioObiettivoRicorrenteTermineVersamentoLabel";
public static final String SALVADANAIORAGGIUNGERAICARDMESSAGGIO2SETTIMANAENTROLASCADENZA="salvadanaioRaggiungeraiCardMessaggio2SettimanaEntroLaScadenza";
public static final String SALVADANAIOOBIETTIVODATASPECIFICADATASELEZIONA="salvadanaioObiettivoDataSpecificaDataSeleziona";
public static final String SALVADANAIOOBIETTIVORICORRENTEFREQUENZAVALUE="salvadanaioObiettivoRicorrenteFrequenzaValue";
public static final String SALVADANAIOOBIETTIVORICORRENTEFREQUENZALABEL="salvadanaioObiettivoRicorrenteFrequenzaLabel";
public static final String SALVADANAIOOBIETTIVORICORRENTETERMINEVERSAMENTOLABELDATASPECIFICA="salvadanaioObiettivoRicorrenteTermineVersamentoLabelDataSpecifica";
public static final String SALVADANAIORAGGIUNGERAICARDIMAGE="salvadanaioRaggiungeraiCardImage";
public static final String SALVADANAIOOBIETTIVORICORRENTEDESCRIZIONEDUE="salvadanaioObiettivoRicorrenteDescrizioneDue";
public static final String SALVADANAIOOBIETTIVORICORRENTEAPARTIREDALABEL="salvadanaioObiettivoRicorrenteAPartireDaLabel";
public static final String SALVADANAIOOBIETTIVODATASPECIFICADESCRIZIONE="salvadanaioObiettivoDataSpecificaDescrizione";
public static final String SALVADANAIOOBIETTIVORICORRENTEDESCRIZIONEUNO="salvadanaioObiettivoRicorrenteDescrizioneUno";
public static final String SALVADANAIOOBIETTIVORICORRENTETERMINEVERSAMENTOLABELALRAGGIUNGIMENTO="salvadanaioObiettivoRicorrenteTermineVersamentoLabelAlRaggiungimento";
public static final String SALVADANAIORAGGIUNGERAICARDMESSAGGIO2SETTIMANA="salvadanaioRaggiungeraiCardMessaggio2Settimana";
public static final String SALVADANAIORAGGIUNGERAIMESSAGGIOCONSIGLIO="salvadanaioRaggiungeraiMessaggioConsiglio";
public static final String SALVADANAIOOBIETTIVODATASPECIFICADATA="salvadanaioObiettivoDataSpecificaData";
public static final String SALVADANAIORAGGIUNGERAICARDMESSAGGIO2SETTIMANADOPOLASCADENZA="salvadanaioRaggiungeraiCardMessaggio2SettimanaDopoLaScadenza";
public static final String SALVADANAIORAGGIUNGERAICARDMESSAGGIO1SETTIMANA="salvadanaioRaggiungeraiCardMessaggio1Settimana";
	public static final String SALVADANAIOGIORNOFESTIVONONSELEZIONABILEPOPUP="salvadanaioGiornoFestivoNonSelezionabilePopup";

	public SalvadanaioVersaSuObiettivoRicorrenteMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

