package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyScreenOfAllBill extends Molecule {
	public static final String NAME="M123";
	
	public static final String WHITEBILLPAYBILLSTEP3="whiteBillPayBillStep3";
public static final String HEADERCOPYPAYBILLSTEP3="headerCopyPayBillStep3";
public static final String SEARCHBILLHEADERPAYBILLSTEP3="searchBillHeaderPayBillStep3";


	public BodyScreenOfAllBill(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

