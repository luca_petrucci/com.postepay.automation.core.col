package com.postepay.automation.core.ui.pages.ministerotrasportipage;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.AbilitaInAppMolecola;
import com.postepay.automation.core.ui.molecules.BodyPaymentPaymentSection;
import com.postepay.automation.core.ui.molecules.MinisteroTrasportiMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.DinamicData;
import utility.SoftAssertion;

public class MinisteroTrasportiPageManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();


	public MinisteroTrasportiPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}
	
	public void verifyLayout()
	{
		Particle nazionale=(Particle) UiObjectRepo.get().get(MinisteroTrasportiMolecola.BTNNAZIONALEMINISTERO);
		Particle trento=(Particle) UiObjectRepo.get().get(MinisteroTrasportiMolecola.BTNTRENTOMINISTERO);
		Particle tipologiaPratica=(Particle) UiObjectRepo.get().get(MinisteroTrasportiMolecola.LABELTIPOLOGIAPRATICA);
		Particle pratica=(Particle) UiObjectRepo.get().get(MinisteroTrasportiMolecola.LABELPRATICA);
		Particle labelPagante=(Particle) UiObjectRepo.get().get(MinisteroTrasportiMolecola.LABELDATIPAGANTE);
		Particle btnCancella=(Particle) UiObjectRepo.get().get(MinisteroTrasportiMolecola.BTNCANCELLAPAGANTE);
		Particle cFiscale=(Particle) UiObjectRepo.get().get(MinisteroTrasportiMolecola.CODICEFISCALEPAGANTE);
		Particle btnCalcola=(Particle) UiObjectRepo.get().get(MinisteroTrasportiMolecola.BTNCALCOLAMINISTERO);

		assertTrue("BTN nazionale non visibile",nazionale.getElement().isDisplayed());
		assertTrue("BTN trento non visibile",trento.getElement().isDisplayed());
		assertTrue("Label tipologiaP non visibile",tipologiaPratica.getElement().isDisplayed());
		assertTrue("Label pratica non visibile",pratica.getElement().isDisplayed());
		SoftAssertion.get().getAssertions().assertThat(labelPagante.getElement().isDisplayed()).withFailMessage("Label Pagante non visibile").isEqualTo(true);
		//assertTrue("Label pagante non visibile",labelPagante.getElement().isDisplayed());
		SoftAssertion.get().getAssertions().assertThat(btnCancella.getElement().isDisplayed()).withFailMessage("Btn Cancella non visibile").isEqualTo(true);
		//assertTrue("BTN cancella non visibile",btnCancella.getElement().isDisplayed());
		assertTrue("Codice Fiscale non visibile",cFiscale.getElement().isDisplayed());
		assertTrue("BTN calcola non visibile",btnCalcola.getElement().isEnabled());
	}
	
//	String[] pratiche={"Domanda in bollo","Veicoli","Rilascio autorizzazioni alla circolazione di prova e relative targhe",
//			"Rilascio foglio di via e relativa targa di cartone provvisoria", "Carta di circolazione (Visure)", "Reimmatricolazioni (Visure)",
//			"Immatricolazione veicoli esteri (nazionalizzazione) da paesi non ue (Visure)", "Acquisto targhe (Visure)", "Immatricolazione (Visure)",
//			"Rilascio targhe ripetitrici (gialle)", "Patenti", "Rilascio patente"};
	public void inserisciTipologiaPratica()
	{
		Particle tipologiaPratica=(Particle) UiObjectRepo.get().get(MinisteroTrasportiMolecola.LABELTIPOLOGIAPRATICA);
		tipologiaPratica.getElement().click();
		WaitManager.get().waitShortTime();
		
		Particle firstElement=(Particle) UiObjectRepo.get().get(MinisteroTrasportiMolecola.FIRSTELEMENTOFLISTMINISTERO);
		List<WebElement> list= firstElement.getListOfElements();
		int max=list.size()-1;
		int min=0;
		int idx=(int) ((Math.random() * (max - min)) + min);
		WebElement e=list.get(idx);
		System.out.println(e.getText());
		DinamicData.getIstance().set("tipoPraticaMIT", e.getText());
		e.click();
		WaitManager.get().waitShortTime();
	}
	
	public void inserisciPratica()
	{
		Particle pratica=(Particle) UiObjectRepo.get().get(MinisteroTrasportiMolecola.LABELPRATICA);
		pratica.getElement().click();
		WaitManager.get().waitShortTime();
		
		Particle firstElement=(Particle) UiObjectRepo.get().get(MinisteroTrasportiMolecola.FIRSTELEMENTOFLISTMINISTERO);
		List<WebElement> list= firstElement.getListOfElements();
		int max=list.size()-1;
		int min=0;
		int idx=(int) ((Math.random() * (max - min)) + min);
		WebElement e=list.get(idx);
		System.out.println(e.getText());
		DinamicData.getIstance().set("praticaMIT", e.getText());
		e.click();
		WaitManager.get().waitShortTime();
	}
	
	public void clickOnCalcola()
	{
		Particle btnCalcola=(Particle) UiObjectRepo.get().get(MinisteroTrasportiMolecola.BTNCALCOLAMINISTERO);
		btnCalcola.getElement().click();
		WaitManager.get().waitShortTime();
	}

	public void clickOnTrento() {
		Particle btnTrento=(Particle) UiObjectRepo.get().get(MinisteroTrasportiMolecola.BTNTRENTOMINISTERO);
		btnTrento.getElement().click();
		WaitManager.get().waitShortTime();
		
	}
}

