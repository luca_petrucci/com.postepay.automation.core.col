package com.postepay.automation.core.ui.pages.parkingpurchasepagestep2;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.BodyChooseHourAndZoneParkingPurchase;
import com.postepay.automation.core.ui.molecules.BodyChooseParkingPurchase;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.SoftAssertion;

public class ParkingPurchasePageStep2Manager extends PageManager {

	public ParkingPurchasePageStep2Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	LayoutTools toolLayout = new LayoutTools();
	
	public void verifyHeaderpage(String titlePageInput)
	{
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}
	public void verifyLayout()
	{
		Particle iconP=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.ICONPARKINGPURCHASECHOOSEHOUR);
		Particle zona=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.ZONEPARKINGPURCHASECHOOSEHOUR);
		Particle targa=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.TARGCARPARKINGPURCHASE);
		Particle copy=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.LABELCOPYPARKINGPURCHASE);
		Particle minus=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.MINUSPARKINGPURCHASE);
		Particle duration=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.DURATIONPARKINGPUCHASE);
		Particle plus=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.PLUSPARKINGPUCHASE);
		Particle price=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.PLUSPARKINGPUCHASE);
		Particle priceHour=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.PRICEHOURPARKINGPURCHASE);
		Particle endTime=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.ENDTIMEPARKINGPURCHASE);
		Particle endData=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.ENDDATEPARKINGPURCHASE);
		Particle btnPaga=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.PAYBUTTONPARKINGPURCHASE);
		attendiTarga();		
		try {
			SoftAssertion.get().getAssertions().assertThat(iconP.getElement().isDisplayed()).withFailMessage("Icona P non visibile").isEqualTo(true);
			SoftAssertion.get().getAssertions().assertThat(zona.getElement().isDisplayed()).withFailMessage("Zona non visibile").isEqualTo(true);
			SoftAssertion.get().getAssertions().assertThat(copy.getElement().isDisplayed()).withFailMessage("Copy non visibile").isEqualTo(true);
			SoftAssertion.get().getAssertions().assertThat(minus.getElement().isDisplayed()).withFailMessage("Tasto minus non visibile").isEqualTo(true);
			SoftAssertion.get().getAssertions().assertThat(duration.getElement().isDisplayed()).withFailMessage("Durata non visibile").isEqualTo(true);
			SoftAssertion.get().getAssertions().assertThat(plus.getElement().isDisplayed()).withFailMessage("Tasto plus non visibile").isEqualTo(true);
			SoftAssertion.get().getAssertions().assertThat(price.getElement().isDisplayed()).withFailMessage("Prezzo non visibile").isEqualTo(true);
			SoftAssertion.get().getAssertions().assertThat(priceHour.getElement().isDisplayed()).withFailMessage("Prezzo/Ore non visibile").isEqualTo(true);
			SoftAssertion.get().getAssertions().assertThat(endTime.getElement().isDisplayed()).withFailMessage("End Time non visibile").isEqualTo(true);
			SoftAssertion.get().getAssertions().assertThat(endData.getElement().isDisplayed()).withFailMessage("End Data non visibile").isEqualTo(true);
			SoftAssertion.get().getAssertions().assertThat(targa.getElement().isDisplayed()).withFailMessage("Targa non visibile").isEqualTo(true);
		} catch (Exception e) {
	
		}
		
		

//		zona.getElement().isDisplayed();
//		targa.getElement().isDisplayed();
//		copy.getElement().isDisplayed();
//		minus.getElement().isDisplayed();
//		duration.getElement().isDisplayed();
//		plus.getElement().isDisplayed();
//		price.getElement().isDisplayed();
//		priceHour.getElement().isDisplayed();
//		endTime.getElement().isDisplayed();
//		endData.getElement().isDisplayed();
		assertTrue(btnPaga.getElement().isDisplayed());
	}
	
	public void attendiTarga()
	{
		for (int i = 0; i < 5; i++) {
			try {
				Particle targa=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.TARGCARPARKINGPURCHASE);
                page.getDriver().getPageSource();
				targa.getElement();
                System.out.println("Targa trovata");
                return;
			} catch (Exception e) {
				WaitManager.get().waitShortTime();
				System.out.println("Targa non trovata");
			}
		}
	}
	

	public void clickOnPaga()
	{
		Particle btnPaga=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.PAYBUTTONPARKINGPURCHASE);
		btnPaga.getElement().click();
		WaitManager.get().waitShortTime();
	}
}

