package com.postepay.automation.core.ui.pages.salvadanaioopzionisettaggio;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.SalvadanaioOpzioniSettaggioMolecola;


public class SalvadanaioOpzioniSettaggio extends Page {
	public static final String NAME="T111";
	

	public SalvadanaioOpzioniSettaggio(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(SalvadanaioOpzioniSettaggioMolecola.NAME, UiObjectRepo.get().get(SalvadanaioOpzioniSettaggioMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SalvadanaioOpzioniSettaggioManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new SalvadanaioOpzioniSettaggioManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyLayoutOfFrequenza() {
		((SalvadanaioOpzioniSettaggioManager)this.manager).verifyLayoutOfFrequenza();
	}

	public void selectFrequenza(String frequenza) {
		((SalvadanaioOpzioniSettaggioManager)this.manager).selectFrequenza(frequenza);
	}

	public void verifyLayoutOfSettings() {
		((SalvadanaioOpzioniSettaggioManager)this.manager).verifyLayoutOfSettings();
	}
	
	public void selectSetting(String sceltaMenu) {
		((SalvadanaioOpzioniSettaggioManager)this.manager).selectSetting(sceltaMenu);
	}

	public void verifyLayoutOfDettaglioPicker() {
		((SalvadanaioOpzioniSettaggioManager)this.manager).verifyLayoutOfDettaglioPicker();
	}
	
	public void clickOnGestisciVersamentoRicorrente() {
		((SalvadanaioOpzioniSettaggioManager)this.manager).clickOnGestisciVersamentoRicorrente();
	}
}

