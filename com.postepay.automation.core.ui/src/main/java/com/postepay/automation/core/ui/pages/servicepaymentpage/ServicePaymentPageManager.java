package com.postepay.automation.core.ui.pages.servicepaymentpage;

import static io.appium.java_client.touch.offset.PointOption.point;

import com.postepay.automation.core.ui.molecules.BodyServicePaymentSection;
import com.postepay.automation.core.ui.molecules.TabsPaymentSection;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class ServicePaymentPageManager extends PageManager {

	public ServicePaymentPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage() {
		LayoutTools tool = new LayoutTools();
		tool.verifyPresenceHeaderPage("Paga");
	}
	
	public void clickOnTabRecharge() {
		Particle tab=(Particle) UiObjectRepo.get().get(TabsPaymentSection.RECHARGETABPAYMENT);
		
		tab.getElement().click();
		
		WaitManager.get().waitShortTime();
	}

	public void clickOnTabPayment() {
		Particle tab=(Particle) UiObjectRepo.get().get(TabsPaymentSection.PAYMENTTABPAYMENT);
		
		tab.getElement().click();
		
		WaitManager.get().waitShortTime();
	}

	public void clickOnTabService() {
		Particle tab=(Particle) UiObjectRepo.get().get(TabsPaymentSection.SERVICETABPAYMENT);
		
		tab.getElement().click();		
		
		WaitManager.get().waitShortTime();
	}
	
	public void clickOnCardCarburante(WebDriver driver) {
		Particle card=(Particle) UiObjectRepo.get().get(BodyServicePaymentSection.PURCHASEFUEL);
		card.getElement().click();
		
//		DateFormat df = new SimpleDateFormat("dd_MM_yyyy_HH-mm-ss.S");
//		Date today = Calendar.getInstance().getTime();
//		String reportDate = df.format(today);
//		System.out.println(reportDate);
//		
////		card.getElement().click();
//		
//		int xS = card.getElement().getSize().width / 2;
//		int yS = card.getElement().getSize().height / 2;
//		int xL = card.getElement().getLocation().getX();
//		int yL = card.getElement().getLocation().getY();
//		
//		int x = xL + xS;
//		int y = yL + yS;
//		
//		System.out.println("Width " + x);
//		System.out.println("Height " + y);
//		
////		TouchAction touchAction = new TouchAction(driver); // (MobileDriver<?>) 
////		touchAction.longPress(point(x,y)).release().perform();
//		
//		String exe = "adb shell input tap " + x + " " + y;
//		try {
//			Runtime.getRuntime().exec(exe);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
		WaitManager.get().waitMediumTime();
	}

	public void clickOnCardParcheggio() {
		Particle card=(Particle) UiObjectRepo.get().get(BodyServicePaymentSection.PURCHASEPARKING);
		
		card.getElement().click();		
		
		WaitManager.get().waitMediumTime();
	}

	public void clickOnCardExtraurbano(WebDriver driver) {
		Particle card=(Particle) UiObjectRepo.get().get(BodyServicePaymentSection.PURCHASEEXTRAURBANTICKET);
		
		card.getElement().click();		
		
//		int x = card.getElement().getLocation().getX();
//		int y = card.getElement().getLocation().getY();
//		
//		TouchAction touchAction = new TouchAction(driver); // (MobileDriver<?>) 
//		touchAction.longPress(point(x,y)).release().perform();
		
		WaitManager.get().waitMediumTime();
	}

	public void clickOnCardUrbano() {
		Particle card=(Particle) UiObjectRepo.get().get(BodyServicePaymentSection.PURCHASEURBANTICKET);
		
		card.getElement().click();		
		
		WaitManager.get().waitMediumTime();
	}
}

