package com.postepay.automation.core.ui.pages.parkingpurchasepagestep3;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.BodyChooseHourAndZoneParkingPurchase;
import com.postepay.automation.core.ui.molecules.BodyChooseHourAndZoneParkingPurchaseSummary;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.ios.IOSDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class ParkingPurchasePageStep3Manager extends PageManager {

	public ParkingPurchasePageStep3Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	LayoutTools toolLayout = new LayoutTools();
	
	public void verifyHeaderpage(String titlePageInput)
	{
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}
	
	public void verifyLayout(String cardNumber)
	{
		Particle title=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchaseSummary.TITLEPAGEPARKINGPURCHASE);
		Particle importo=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchaseSummary.IMPORTPARKINGPURCHASE);
		Particle data=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchaseSummary.DATAPARKINGPURCHASE);
		Particle esercente=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchaseSummary.OWNERPARKINGPURCHASE);
		Particle card=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchaseSummary.CARTPARKINGPURCHASE);
		String aPagaConCard= card.getElement().getText();
		Particle btnPaga=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchaseSummary.PAYBUTTONPARKINGPURCHASESUMMARY);
		Particle btnAnnulla=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchaseSummary.CANCELBUTTONPARKINGPURCHASE);
		
		title.getElement().isDisplayed();
		importo.getElement().isDisplayed();
		data.getElement().isDisplayed();
		esercente.getElement().isDisplayed();
		card.getElement().isDisplayed();
		btnPaga.getElement().isDisplayed();
		btnAnnulla.getElement().isDisplayed();
		
		//String ePagaConCard = "Evolution  "+cardNumber;
		//System.out.println("CC | ER: " + ePagaConCard + " AR: "+ aPagaConCard);
//		if(page.getDriver() instanceof IOSDriver) {
//			System.out.println("Controllo carta non effettuato");
//		} else {
//		//assertTrue("Controllo summary PagaNumeroCarta | ER -->" + ePagaConCard + " AR -->"+ aPagaConCard, aPagaConCard.equals(ePagaConCard));
//			assertTrue("Controllo summary PagaNumeroCarta | ER -->" + cardNumber + " AR -->"+ aPagaConCard, aPagaConCard.contains(cardNumber));
//		}
	}
	
	public void clickOnPagaSummary()
	{
		Particle btnPaga=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchaseSummary.PAYBUTTONPARKINGPURCHASESUMMARY);
		btnPaga.getElement().click();
		WaitManager.get().waitShortTime();
	}
	
	public void clickOnAnnullaSummary()
	{
		Particle btnAnnulla=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchaseSummary.CANCELBUTTONPARKINGPURCHASE);
		btnAnnulla.getElement().click();
		WaitManager.get().waitShortTime();
	}
}

