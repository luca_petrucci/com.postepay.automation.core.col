package com.postepay.automation.core.ui.pages.autorizzaonbpage;

import static org.junit.Assert.assertTrue;
import org.openqa.selenium.WebDriver;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.AutorizzaOnbMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutImage;

import io.appium.java_client.android.AndroidDriver;


public class AutorizzaOnbPage extends Page {
	public static final String NAME="T125";
	

	public AutorizzaOnbPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(AutorizzaOnbMolecola.NAME, UiObjectRepo.get().get(AutorizzaOnbMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new AutorizzaOnbPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		((AutorizzaOnbPageManager)this.manager).verifyHeaderpage(titlePageInput);
	}
	
	public void verifyLayout(WebDriver driver, double discrepanza) {
		((AutorizzaOnbPageManager)this.manager).verifyLayout(driver, discrepanza);
		
	}

	public void verifyLayoutCountdown() {
		((AutorizzaOnbPageManager)this.manager).verifyLayoutCountdown();
	}
	
	public void clickOnAutorizza() {
		((AutorizzaOnbPageManager)this.manager).clickOnAutorizza();
	}
	
	public void clickOnNega() {
		((AutorizzaOnbPageManager)this.manager).clickOnNega();
	}

}

