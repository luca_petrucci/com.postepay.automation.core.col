package com.postepay.automation.core.ui.pages.salvadanaiotyp;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.SalvadanaioObiettivoSummaryMolecola;
import com.postepay.automation.core.ui.molecules.SalvadanaioTYPMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import org.openqa.selenium.WebDriver;

public class SalvadanaioTYPManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();
	
	Particle salvadanaioTYPImage = (Particle) UiObjectRepo.get().get(SalvadanaioTYPMolecola.SALVADANAIOTYPIMAGE);
	Particle salvadanaioTYPTitolo = (Particle) UiObjectRepo.get().get(SalvadanaioTYPMolecola.SALVADANAIOTYPTITOLO);
	Particle salvadanaioTYPDescrizione = (Particle) UiObjectRepo.get().get(SalvadanaioTYPMolecola.SALVADANAIOTYPDESCRIZIONE);
	Particle salvadanaioTYPFingerPrintLink = (Particle) UiObjectRepo.get().get(SalvadanaioTYPMolecola.SALVADANAIOTYPFINGERPRINTLINK);
	Particle salvadanaioTYPClose = (Particle) UiObjectRepo.get().get(SalvadanaioTYPMolecola.SALVADANAIOTYPCLOSE);
	Particle descrizioneAttivazioneAuto = (Particle) UiObjectRepo.get().get(SalvadanaioTYPMolecola.DESCRIZIONEATTIVAZIONE);
	Particle descrizioneCancellazioneAuto = (Particle) UiObjectRepo.get().get(SalvadanaioTYPMolecola.DESCRIZIONEDISATTIVAZIONE);
	
	public SalvadanaioTYPManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public WebElement getElementDesceiption(WebDriver driver, String descriptionInput) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, salvadanaioTYPDescrizione, descriptionInput);
		assertTrue(elm.isDisplayed());
		return elm;
	}
	
	public void verifyImage(WebDriver driver, String imageNameInput, double discrepanza) {
		String imageName = "salvadanaio/" + imageNameInput;
		LayoutImage.get().verifyImage(driver, imageName, discrepanza);
	}

	public void verifyDescription(WebDriver driver, String type) {
		
		switch (type) {
		case "creazione":
			String text = "L'attivazione del servizio è stata eseguita con successo.\n Troverai un messaggio di conferma nella tua bacheca";
			String elmText = descrizioneAttivazioneAuto.getElement().getText();
			System.out.println(text);
			System.out.println(elmText);
			assertTrue(elmText.equals(text));
			assertTrue(descrizioneAttivazioneAuto.getElement().isDisplayed());
			break;
		
		case "cancellazione":
//			assertTrue(descrizioneCancellazioneAuto.getElement().isDisplayed());
			WebElement elm = toolLayout.replaceGenericPathOfElement(driver, salvadanaioTYPDescrizione, "Il servizio è stato disattivato correttamente. Troverai un messaggio di conferma nella tua Bacheca");
			assertTrue(elm.isDisplayed());
			break;
			
		case "lavorazione":
			assertTrue(descrizioneCancellazioneAuto.getElement().getText().contains("Ti invitiamo a verificare l'esito dell'operazione consulando la lista movimenti della tua Carta Postepay Evolution."));
			assertTrue(descrizioneCancellazioneAuto.getElement().getText().contains("Riceverai le informazioni relative all'operazione che hai richiesto e la conferma del Versamento nella tua Bacheca"));
			break;
		}
	}
	
	public void verifyLayout(WebDriver driver, String type, double discrepanza) {
		assertTrue(salvadanaioTYPImage.getElement().isDisplayed());
		assertTrue(salvadanaioTYPTitolo.getElement().isDisplayed());
		verifyDescription(driver, type);
		assertTrue(salvadanaioTYPFingerPrintLink.getElement().isDisplayed());
		assertTrue(salvadanaioTYPClose.getElement().isDisplayed());
//		verifyImage(driver, "cassaforte_typ", discrepanza);
	}
	
	public void clickOnClose() {
		salvadanaioTYPClose.getElement().click();
		WaitManager.get().waitLongTime();
	}
}

