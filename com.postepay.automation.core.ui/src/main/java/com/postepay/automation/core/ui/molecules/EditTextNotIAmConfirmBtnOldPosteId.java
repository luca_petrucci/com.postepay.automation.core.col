package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import io.appium.java_client.AppiumDriver;
import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class EditTextNotIAmConfirmBtnOldPosteId extends Molecule {
	public static final String NAME="M006";
	
	public static final String POSTEIDEDITTEXT ="posteIdEditText";
	public static final String CHANGEACCOUNT ="changeAccount";
	public static final String CONFIRMBUTTONOLDPOSTEID ="confirmButtonOldPosteId";
	public static final String HELLOTITLE ="hello_title";
	public static final String HELLODESCRIPTION ="hello_description";
	public static final String LINKFORGETPOSTEID ="linkForgetPosteID";



	public EditTextNotIAmConfirmBtnOldPosteId(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}
	
	protected void isLoaded() throws Error 
	{	
		try {Thread.sleep(3000);} catch (Exception e) {}
		((AppiumDriver<?>)driver).hideKeyboard();
		try {Thread.sleep(2000);} catch (Exception e) {}
		
		super.isLoaded();
		//codice di controllo
	}
}

