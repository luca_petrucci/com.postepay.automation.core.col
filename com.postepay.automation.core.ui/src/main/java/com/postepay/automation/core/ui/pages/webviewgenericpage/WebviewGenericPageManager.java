package com.postepay.automation.core.ui.pages.webviewgenericpage;

import static org.junit.Assert.assertTrue;

import javax.servlet.http.Part;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.WebviewGenericPageMolecola;

import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class WebviewGenericPageManager extends PageManager {

	public WebviewGenericPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void isGooglePlayStore() {
		
		Particle google = (Particle) UiObjectRepo.get().get(WebviewGenericPageMolecola.GOOGLEPLAYSTOREWEBVIEW);
		
		assertTrue(google.getElement().isDisplayed());
	}
	
	public void isGooglePlayStoreBancoPosta() {
		
		Particle labelBancoPosta = (Particle) UiObjectRepo.get().get(WebviewGenericPageMolecola.GOOGLEPLAYBANCOPOSTAHEADERWEBVIEW);
		Particle labelPostaSpa = (Particle) UiObjectRepo.get().get(WebviewGenericPageMolecola.GOOGLEPLAYBANCOPOSTAPOSTESPAWEBVIEW);
		
		assertTrue(labelBancoPosta.getElement().isDisplayed());
		assertTrue(labelPostaSpa.getElement().isDisplayed());
	}
	
	public void clickXonTranslate() {
		Particle btn = (Particle) UiObjectRepo.get().get(WebviewGenericPageMolecola.CLOSEBUTTONINFOBAR);
		
		if (btn.getElement().isDisplayed()) {
			btn.getElement().click();			
		}
		WaitManager.get().waitMediumTime();
	}
	
	// Metodo di verifica che siamo sulla pagina WEb View del Play Store con Bancoposta mostrato
	public void isLinkToBancopostaStore() {
		// Proviamo a chiudere il tastierino del tranlate
		clickXonTranslate();
		// Verifica che siamo su boogle store
		isGooglePlayStore();
		// Verifica che siamo nella pagina di Bancoposta
		isGooglePlayStoreBancoPosta();
		
		WaitManager.get().waitShortTime();
	}
	
	public void verifyHeaderBancopostaApp(WebDriver driver) {
		
		String titlePosteID = "//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_title' and @text='Inserisci Codice PosteID']";
		String titleLogin = "//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_title' and @text='Accedi a BancoPosta']";
		String welcomeBP = "//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_logo_intro' and @text='Benvenuti nell'App BancoPosta']";
		
		try {
			isBancoPostaPage(driver, titlePosteID);
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			isBancoPostaPage(driver, titleLogin);
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			isBancoPostaPage(driver, welcomeBP);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void isBancoPostaPage(WebDriver driver, String locator) {
		// TODO Auto-generated method stub
		WebElement title = (WebElement) driver.findElement(By.xpath(locator));
		assertTrue(title.isDisplayed());
		WaitManager.get().waitMediumTime();
	}
}

