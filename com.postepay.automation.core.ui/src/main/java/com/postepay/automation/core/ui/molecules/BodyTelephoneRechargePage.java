package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyTelephoneRechargePage extends Molecule {
	public static final String NAME="M113";
	
	public static final String TITLEPAGETHELEPHONERECHARGE="titlePageThelephoneRecharge";
	public static final String BUTTONCONFIRMTHELEPHONERECHARGE="buttonConfirmThelephoneRecharge";
	public static final String INPUTIMPORTHELEPHONERECHARGE="inputImporThelephoneRecharge";
	public static final String MOBILEPROVIDERTHELEPHONERECHARGE="mobileProviderThelephoneRecharge";
	public static final String INPUTNUMBERTHELEPHONERECHARGE="inputNumberThelephoneRecharge";
	public static final String CHOOSETHELEPHON="chooseThelephoneRecharge";
	public static final String BUTTONRUBRICA = "buttonRubrica";
	public static final String NUMEROTELEFONORUBRICA = "numeroTelefonoRubrica";

	public BodyTelephoneRechargePage(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

