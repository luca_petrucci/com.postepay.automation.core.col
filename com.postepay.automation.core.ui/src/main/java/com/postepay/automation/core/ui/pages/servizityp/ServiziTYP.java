package com.postepay.automation.core.ui.pages.servizityp;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.ServiziTYPMolecola;

import io.appium.java_client.android.AndroidDriver;


public class ServiziTYP extends Page {
	public static final String NAME="T104";
	

	public ServiziTYP(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(ServiziTYPMolecola.NAME, UiObjectRepo.get().get(ServiziTYPMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new ServiziTYPManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyLayoutPageGeneric(WebDriver driver, String serviceType, double discrepanza) {
		((ServiziTYPManager)this.manager).verifyLayoutPageGeneric(driver, serviceType, discrepanza);
	}

	public void clickOnIlTuoBiglietto() {
		((ServiziTYPManager)this.manager).clickOnIlTuoBiglietto();
	}

	public void clickOnVaiAllaRicevuta() {
		((ServiziTYPManager)this.manager).clickOnVaiAllaRicevuta();
	}
	
	public void verificaTalloncino() {
		((ServiziTYPManager)this.manager).verificaTalloncino();
	}
	
	public void interrompiTalloncino() {
		((ServiziTYPManager)this.manager).interrompiTalloncino();
	}
}

