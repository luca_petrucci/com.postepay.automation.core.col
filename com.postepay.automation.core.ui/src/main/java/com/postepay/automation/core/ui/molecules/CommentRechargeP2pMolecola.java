package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class CommentRechargeP2pMolecola extends Molecule {
	public static final String NAME="M204";
	
	public static final String CONFIRMBUTTONCOMMENTP2P="confirmButtonCommentP2p";
public static final String FIELDTEXTINSERTCOMMENTP2P="fieldTextInsertCommentP2p";


	public CommentRechargeP2pMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

