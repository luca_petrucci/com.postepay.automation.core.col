package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class AutomaticRechargePostepayThankYouPageMolecola extends Molecule {
	public static final String NAME="M207";
	
	public static final String CLOSEBUTTONAUTOMATICRECHARGEPOSTEPAYTHANKYOUPAGE="closeButtonAutomaticRechargePostepayThankYouPage";
public static final String COPYAUTOMATICRECHARGEPOSTEPAYTHANKYOUPAGE="copyAutomaticRechargePostepayThankYouPage";
public static final String TITLEAUTOMATICRECHARGEPOSTEPAYTHANKYOUPAGE="titleAutomaticRechargePostepayThankYouPage";
public static final String IMAGEAUTOMATICRECHARGEPOSTEPAYTHANKYOUPAGE="imageAutomaticRechargePostepayThankYouPage";
public static final String TYPIMAGEM207="typImageM207";
public static final String TYPTITLEM207="typTitleM207";
public static final String TYPDESCRIPTIONM207="typDescriptionM207";
public static final String TYPCLOSEBTNM207="typCloseBtnM207";


	public AutomaticRechargePostepayThankYouPageMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

