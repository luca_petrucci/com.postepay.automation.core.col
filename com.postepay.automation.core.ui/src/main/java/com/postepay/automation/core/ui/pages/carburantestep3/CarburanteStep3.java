package com.postepay.automation.core.ui.pages.carburantestep3;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.molecules.CarburanteStep3Molecola;


public class CarburanteStep3 extends Page {
	public static final String NAME="T102";
	

	public CarburanteStep3(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), false);
this.addToTemplate(CarburanteStep3Molecola.NAME, UiObjectRepo.get().get(CarburanteStep3Molecola.NAME), false);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new CarburanteStep3Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new CarburanteStep3ManagerIOS(this) ;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderpage() {
		((CarburanteStep3Manager)this.manager).verifyHeaderpage();
	}
	
	public void verifyLayoutPage() {
		((CarburanteStep3Manager)this.manager).verifyLayoutPage();
	}
	
	public void verifyDateOf() {
		((CarburanteStep3Manager)this.manager).verifyDateOf();
	}
	
	public void verifyPayWithCard(String cardInput) {
		((CarburanteStep3Manager)this.manager).verifyPayWithCard(cardInput);
	}
	
	public void clickOnPay() {
		((CarburanteStep3Manager)this.manager).clickOnPay();
	}
	
	public void clickOnAnnulla() {
		((CarburanteStep3Manager)this.manager).clickOnAnnulla();
	}
}

