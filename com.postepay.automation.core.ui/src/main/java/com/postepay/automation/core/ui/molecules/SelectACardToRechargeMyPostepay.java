package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SelectACardToRechargeMyPostepay extends Molecule {
	public static final String NAME="M060";

	public static final String SUBTITLEOFCARDTORECHARGE="subTitleOfCardToRecharge";
	public static final String TARGETNUMBERCARDSENDERRECHARGE="targetNumberCardSenderRecharge";
	public static final String TARGETTITLESENDERRECHARGE ="targetTitleSenderRecharge";
	public static final String TARGETICONSENDERRECHARGE ="targetIconSenderRecharge";
	public static final String TARGETTYPESENDERRECHARGE ="targetTypeSenderRecharge";
	public static final String TARGETAVAIBLELABELSENDERRECHARGE ="targetAvaibleLabelSenderRecharge";
	public static final String TARGETAMOUNTSENDERRECHARGE ="targetAmountSenderRecharge";


	public SelectACardToRechargeMyPostepay(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

