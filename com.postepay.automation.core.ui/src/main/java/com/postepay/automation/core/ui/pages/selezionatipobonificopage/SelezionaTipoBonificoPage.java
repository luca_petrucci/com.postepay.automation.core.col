package com.postepay.automation.core.ui.pages.selezionatipobonificopage;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.common.datatable.UserDataCredential;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.SelezionaTipoBonificoMolecola;


public class SelezionaTipoBonificoPage extends Page {
	public static final String NAME="T145";
	

	public SelezionaTipoBonificoPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(SelezionaTipoBonificoMolecola.NAME, UiObjectRepo.get().get(SelezionaTipoBonificoMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SelezionaTipoBonificoPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeaderpage(String titlePageInput)
	{
		((SelezionaTipoBonificoPageManager)this.manager).verifyHeaderpage(titlePageInput);
	}
	
	public void verifyLayout() 
	{
		((SelezionaTipoBonificoPageManager)this.manager).verifyLayout();
	}

	

	public void clickOnBonificoPostagiro(String operazione) {
		((SelezionaTipoBonificoPageManager)this.manager).clickOnBonificoPostagiro(operazione);
	}

	

	


}

