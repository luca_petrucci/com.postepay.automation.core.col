package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SearchContactsNumberG2G extends Molecule {
	public static final String NAME="M032";
	
	public static final String TABG2GCONTACTSSENDGIGA="tabG2GContactsSendGiga";
	public static final String TARGETIDCONTACT="targetIdContact";
	public static final String SEARCHCONTACTS="searchContacts";
	public static final String TABCONTACTSSENDGIGA="tabContactsSendGiga";

	public SearchContactsNumberG2G(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

