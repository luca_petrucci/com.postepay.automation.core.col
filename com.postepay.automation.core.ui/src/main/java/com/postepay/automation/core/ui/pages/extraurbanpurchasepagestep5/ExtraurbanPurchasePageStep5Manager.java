package com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep5;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import com.postepay.automation.core.ui.molecules.BodyExtraurbanPurchaseFinalSummary;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import ui.core.support.waitutil.WaitManager;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class ExtraurbanPurchasePageStep5Manager extends PageManager {

	public ExtraurbanPurchasePageStep5Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderpage(String inputTitle) {
		LayoutTools tool = new LayoutTools();
		tool.verifyPresenceHeaderPage(inputTitle);
	}
	
	public void verifyDestination(String inputFrom, String inputTo) {
		Particle travel = (Particle) UiObjectRepo.get().get(BodyExtraurbanPurchaseFinalSummary.DIRECTIONJOURNEYINFOSUMMARY);
		
		String travelInput = inputFrom + " > " + inputTo;
		
		assertTrue(travel.getElement().getText().equals(travelInput));
	}
	
	public void verifyName(String inputName, String inputSurname) {
		Particle name = (Particle) UiObjectRepo.get().get(BodyExtraurbanPurchaseFinalSummary.INFOPASSENGER);
		String nameInput = inputName + " " + inputSurname;
		
		System.out.println(name.getElement().getText());
		System.out.println(nameInput);
		
		assertTrue(name.getElement().getText().trim().equals(nameInput.trim()));		
	}
	
	public void isPayButtonEnabled(String statusBtn) {
		Particle btn = (Particle) UiObjectRepo.get().get(BodyExtraurbanPurchaseFinalSummary.PAYBUTTON);		
		if (statusBtn.equals("enabled")) {
			assertTrue(btn.getElement().isEnabled());
						
		}else if (statusBtn.equals("disabled")) {
			assertFalse(btn.getElement().isEnabled());
		}
	}
	
	public void clickOnPrivacy() {
		Particle btn = (Particle) UiObjectRepo.get().get(BodyExtraurbanPurchaseFinalSummary.CHECKBOXPRIVACY);		
		
		btn.getElement().click();
		
		WaitManager.get().waitShortTime();
	}
	
	public void clickOnPay() {
		Particle btn = (Particle) UiObjectRepo.get().get(BodyExtraurbanPurchaseFinalSummary.PAYBUTTON);		
		btn.getElement().click();
		// Il tempo di caricamento è molto lungo, cira 12/18 secondi
		WaitManager.get().waitFor(TimeUnit.SECONDS, 15);
//		WaitManager.get().waitHighTime();
		
	}
}

