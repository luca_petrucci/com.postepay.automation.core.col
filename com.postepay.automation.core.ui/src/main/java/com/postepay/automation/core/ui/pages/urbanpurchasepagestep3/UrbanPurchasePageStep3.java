package com.postepay.automation.core.ui.pages.urbanpurchasepagestep3;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;

import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.BodyUrbanPurchaseVerifySummary;


public class UrbanPurchasePageStep3 extends Page {
	public static final String NAME="T054";
	

	public UrbanPurchasePageStep3(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(BodyUrbanPurchaseVerifySummary.NAME, UiObjectRepo.get().get(BodyUrbanPurchaseVerifySummary.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new UrbanPurchasePageStep3Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderpage() {
		((UrbanPurchasePageStep3Manager)this.manager).verifyHeaderpage();
	}

	public void verifyLayoutPage() {
		((UrbanPurchasePageStep3Manager)this.manager).verifyLayoutPage();
	}
	
	public void verifyDefaultSettings() {
		((UrbanPurchasePageStep3Manager)this.manager).verifyDefaultSettings();
	}

	public void clickOnMinus() {
		((UrbanPurchasePageStep3Manager)this.manager).clickOnMinus();
	}

	public void clickOnPlus() {
		((UrbanPurchasePageStep3Manager)this.manager).clickOnPlus();
	}

	public void clickPay() {
		((UrbanPurchasePageStep3Manager)this.manager).clickPay();
	}
}

