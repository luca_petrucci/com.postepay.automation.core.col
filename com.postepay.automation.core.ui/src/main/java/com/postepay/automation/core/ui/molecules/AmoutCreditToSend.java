package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class AmoutCreditToSend extends Molecule {
	public static final String NAME="M024";
	
	public static final String AMOUNTSENDP2P="amountSendP2P";
	public static final String BUTTONSENDP2P="buttonSendP2P";
	public static final String ICONP2PSENDCREDITSENT="iconP2PSendCreditSent";
	public static final String USERCONTACTSEND="userContactSend";


	public AmoutCreditToSend(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

