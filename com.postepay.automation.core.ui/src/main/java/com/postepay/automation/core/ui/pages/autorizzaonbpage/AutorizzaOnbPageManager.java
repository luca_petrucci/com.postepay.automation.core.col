package com.postepay.automation.core.ui.pages.autorizzaonbpage;

import static org.junit.Assert.assertTrue;
import org.openqa.selenium.WebDriver;
import java.util.concurrent.TimeUnit;

import com.postepay.automation.core.ui.molecules.AutorizzaOnbMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class AutorizzaOnbPageManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();

	Particle imageAutorizzaOnp = (Particle) UiObjectRepo.get().get(AutorizzaOnbMolecola.IMAGEAUTORIZZAONP);
	Particle titleAutorizzaOnp = (Particle) UiObjectRepo.get().get(AutorizzaOnbMolecola.TITLEAUTORIZZAONP);
	Particle copyAutorizzaOnp = (Particle) UiObjectRepo.get().get(AutorizzaOnbMolecola.COPYAUTORIZZAONP);
	Particle btnAutorizzaOnp = (Particle) UiObjectRepo.get().get(AutorizzaOnbMolecola.BTNAUTORIZZAONP);
	Particle negaAutorizzaOnp = (Particle) UiObjectRepo.get().get(AutorizzaOnbMolecola.NEGAAUTORIZZAONP);
	Particle loadingBarCaricamentoOnp = (Particle) UiObjectRepo.get().get(AutorizzaOnbMolecola.LOADINGBARCARICAMENTOONP);
	Particle textCaricamentoOnb = (Particle) UiObjectRepo.get().get(AutorizzaOnbMolecola.TEXTCARICAMENTOONB);
	
	public AutorizzaOnbPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}
	
	public void verifyImage(WebDriver driver, String imageNameInput, double discrepanza) {
		String imageName = "onb/" + imageNameInput;
		LayoutImage.get().verifyImage(driver, imageName, discrepanza);
	}

	public void verifyLayout(WebDriver driver, double discrepanza) {
		assertTrue(imageAutorizzaOnp.getElement().isDisplayed());
		assertTrue(titleAutorizzaOnp.getElement().isDisplayed());
		assertTrue(copyAutorizzaOnp.getElement().isDisplayed());
		assertTrue(btnAutorizzaOnp.getElement().isDisplayed());
		assertTrue(negaAutorizzaOnp.getElement().isDisplayed());
		verifyImage(driver, "onb_autorizza_o_nega", discrepanza);
		
	}

	public void verifyLayoutCountdown() {
		assertTrue(loadingBarCaricamentoOnp.getElement().isDisplayed());
//		assertTrue(textCaricamentoOnb.getElement().isDisplayed());
		WaitManager.get().waitFor(TimeUnit.SECONDS, 11);
	}
	
	public void clickOnAutorizza() {
		btnAutorizzaOnp.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnNega() {
		negaAutorizzaOnp.getElement().click();
		WaitManager.get().waitMediumTime();
	}
}

