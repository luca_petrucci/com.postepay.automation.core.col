package com.postepay.automation.core.ui.pages.detailsconnectsimpurchasegigapage;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.molecules.BodyG2gPurchase;


public class DetailsConnectSimPurchaseGigaPage extends Page {
	public static final String NAME="T024";
	

	public DetailsConnectSimPurchaseGigaPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(BodyG2gPurchase.NAME, UiObjectRepo.get().get(BodyG2gPurchase.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new DetailsConnectSimPurchaseGigaPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeader(String inputTitle) {
		((DetailsConnectSimPurchaseGigaPageManager)this.manager).verifyHeader(inputTitle);
	}
	
	public void verifyLayoutPage() {
		((DetailsConnectSimPurchaseGigaPageManager)this.manager).verifyLayoutPage();
	}
	
	public void clickOnPurchaseGiga() {
		((DetailsConnectSimPurchaseGigaPageManager)this.manager).clickOnPurchaseGiga();
	}
	
	public double getAmountToPay() {
		return ((DetailsConnectSimPurchaseGigaPageManager)this.manager).getAmountToPay();
	}
}

