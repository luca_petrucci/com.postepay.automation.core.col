package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SalvadanaioAccantonamentoDettaglioMolecola extends Molecule {
	public static final String NAME="M231";
	
	public static final String SALVADANAIODETTAGLIOTESTBLULINK="salvadanaioDettaglioTestBluLink";
public static final String SALVADANAIODETTAGLIOCONDIVIDIBTN="salvadanaioDettaglioCondividiBtn";
public static final String SALVADANAIODETTAGLIODATAADESIONEVALORE="salvadanaioDettaglioDataAdesioneValore";
public static final String SALVADANAIODETTAGLIORISPARMIAALGIORNO="salvadanaioDettaglioRisparmiaAlGiorno";
public static final String SALVADANAIODETTAGLIOACTUALAMOUNT="salvadanaioDettaglioActualAmount";
public static final String SALVADANAIODETTAGLIOVERSACONPOSTEPAYTEXT="salvadanaioDettaglioVersaConPostePayText";
public static final String SALVADANAIODETTAGLIOTOTALAMOUNT="salvadanaioDettaglioTotalAmount";
public static final String SALVADANAIODETTAGLIOICONAYELLOW="salvadanaioDettaglioIconaYellow";
public static final String SALVADANAIODETTAGLIOICONAOROLOGIOACCANTONAMENTOAUTOMATICO="salvadanaioDettaglioIconaOrologioAccantonamentoAutomatico";
public static final String SALVADANAIODETTAGLIODATAADESIONETEXT="salvadanaioDettaglioDataAdesioneText";
public static final String SALVADANAIODETTAGLIOGESTISCIINBANCOPOSTA="salvadanaioDettaglioGestisciInBancoPosta";
public static final String SALVADANAIODETTAGLIODATASCADENZATEXT="salvadanaioDettaglioDataScadenzaText";
public static final String SALVADANAIODETTAGLIOTREPUNTINILISTAVERSAMENTI="salvadanaioDettaglioTrePuntiniListaVersamenti";
public static final String SALVADANAIODETTAGLIOBARSTATUS="salvadanaioDettaglioBarStatus";
public static final String SALVADANAIODETTAGLIOVERSACONPOSTEPAYBUTTON="salvadanaioDettaglioVersaConPostePayButton";
public static final String SALVADANAIODETTAGLIONOME="salvadanaioDettaglioNome";
public static final String SALVADANAIODETTAGLIODATASCADENZAVALORE="salvadanaioDettaglioDataScadenzaValore";
public static final String SALVADANAIOVERSAMENTOSINGOLO="salvadanaioVersamentoSingolo";


	public SalvadanaioAccantonamentoDettaglioMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

