package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class AcquistoParcheggioMolecola extends Molecule {
	public static final String NAME="M242";

	public static final String TITOLOM280 ="titoloM280";
	public static final String TYPEOPERAZIONEM280 	="typeOperazioneM280";
	public static final String DATAM280 	="dataM280";
	public static final String LOGOM280 	="logoM280";
	public static final String LUOGOM280 	="luogoM280";
	public static final String INDIRIZZOM280 	="indirizzoM280";
	public static final String MSGSTATICOM280 	="msgStaticoM280";
	public static final String MSGDINAMICOM280 	="msgDinamicoM280";
	public static final String BTNSCARICARICEVUTAM280="btnScaricaRicevutaM280";



	public AcquistoParcheggioMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

