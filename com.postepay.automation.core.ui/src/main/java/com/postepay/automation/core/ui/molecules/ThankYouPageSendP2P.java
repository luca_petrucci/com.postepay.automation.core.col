package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class ThankYouPageSendP2P extends Molecule {
	public static final String NAME="M026";
	
	public static final String AMOUNTP2PSENDTHANKYOUPAGE="amountP2PSendThankYouPage";
public static final String TITLEP2PSEND="titleP2PSend";
public static final String BUTTONCLOSEP2PSEND="buttonCloseP2PSend";
public static final String ICONRECEIVERP2PSEND="iconReceiverP2PSend";
public static final String ICONSENDERP2PSEND="iconSenderP2PSend";
public static final String ICONP2PSENDTHANKYOUPAGE="iconP2PSendThankYouPage";
public static final String SUBTITLEP2PSEND="subtitleP2PSend";


	public ThankYouPageSendP2P(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

