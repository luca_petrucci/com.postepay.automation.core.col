package com.postepay.automation.core.ui.pages.thankyoupageconnectsimpurchasegigapage;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.ThankYouPagePurchaseG2G;

import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class ThankYouPageConnectSimPurchaseGigaPageManagerIOS extends ThankYouPageConnectSimPurchaseGigaPageManager {

	public ThankYouPageConnectSimPurchaseGigaPageManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyLayoutPage() {
		Particle image = (Particle) UiObjectRepo.get().get(ThankYouPagePurchaseG2G.IMAGEOFPURCHASEGIGATHANKYOUPAGE);
		Particle header = (Particle) UiObjectRepo.get().get(ThankYouPagePurchaseG2G.HEADEROFPURCHASESUCCESSGIGATHANKYOUPAGE);
		Particle text = (Particle) UiObjectRepo.get().get(ThankYouPagePurchaseG2G.COPYOFPURCHASESUCCESSGIGATHANKYOUPAGE);
		
		assertTrue(image.getElement()!=null);
		assertTrue(header.getElement()!=null);
		assertTrue(text.getElement()!=null);
	}

}
