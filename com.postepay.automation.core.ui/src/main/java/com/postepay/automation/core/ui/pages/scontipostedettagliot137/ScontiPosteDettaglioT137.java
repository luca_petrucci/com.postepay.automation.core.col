package com.postepay.automation.core.ui.pages.scontipostedettagliot137;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.ScontiPosteM259;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;


public class ScontiPosteDettaglioT137 extends Page {
	public static final String NAME="T137";
	

	public ScontiPosteDettaglioT137(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(ScontiPosteM259.NAME, UiObjectRepo.get().get(ScontiPosteM259.NAME), true);
this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new ScontiPosteDettaglioT137Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	public void verifyLayout() {
		((ScontiPosteDettaglioT137Manager)this.manager).verifyLayout();
	
	}

}

