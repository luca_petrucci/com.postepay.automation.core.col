package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SelezionaBollettinoM254 extends Molecule {
	public static final String NAME="M254";
	
	public static final String M254IMAGE="M254image";
public static final String M254ELEMENTSUBTITLE="M254ElementSubTitle";
public static final String M254DESCRIPTION="M254description";
public static final String M254TITLE="M254title";
public static final String M254TITLECONTAINER="M254titleContainer";
public static final String M254ELEMENTTITLE="M254elementTitle";
	public static final String M254COMPILAMANUALMENTE="M254compilaManualmente";


	public SelezionaBollettinoM254(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

