package com.postepay.automation.core.ui.pages.urbanpurchasepagestep4;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep6.ExtraurbanPurchasePageStep6Manager;
import com.postepay.automation.core.ui.molecules.BodyPageUrbanTicketPurchaseSummary;


public class UrbanPurchasePageStep4 extends Page {
	public static final String NAME="T055";


	public UrbanPurchasePageStep4(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(BodyPageUrbanTicketPurchaseSummary.NAME, UiObjectRepo.get().get(BodyPageUrbanTicketPurchaseSummary.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {

		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {

		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {

		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new UrbanPurchasePageStep4Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {

		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {

		return null;
	}

	public void verifyHeaderpage(String inputTitle) {
		((UrbanPurchasePageStep4Manager)this.manager).verifyHeaderpage(inputTitle);
	}

	public void verifyLayoutPage() {
		((UrbanPurchasePageStep4Manager)this.manager).verifyLayoutPage();
	}

	public void verifyDateOf() {
		((UrbanPurchasePageStep4Manager)this.manager).verifyDateOf();
	}

	public void verifyPayWithCard(String cardInput) {
		((UrbanPurchasePageStep4Manager)this.manager).verifyPayWithCard(cardInput);
	}

	public void clickOnPay() {
		((UrbanPurchasePageStep4Manager)this.manager).clickOnPay();
	}

	public void clickOnAnnulla() {
		((UrbanPurchasePageStep4Manager)this.manager).clickOnAnnulla();
	}

	public String getImporto() {
		return ((UrbanPurchasePageStep4Manager)this.manager).getImporto();
	}
	
	public String getDate() {
		return ((UrbanPurchasePageStep4Manager)this.manager).getDate();
	}

	public boolean isCurrentPayment(String cardNumber) {
		return ((UrbanPurchasePageStep4Manager)this.manager).isCurrentPayment(cardNumber);
	}

	public void clickOnCambiaMetodoPagamento() {
		((UrbanPurchasePageStep4Manager)this.manager).clickOnCambiaMetodoPagamento();
		
	}
}

