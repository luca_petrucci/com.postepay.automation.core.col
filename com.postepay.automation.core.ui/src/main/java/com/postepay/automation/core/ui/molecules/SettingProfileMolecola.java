package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SettingProfileMolecola extends Molecule {
	public static final String NAME="M216";
	
	public static final String PROFILECARBURANTESECTIONLABEL="profileCarburanteSectionLabel";
public static final String PROFILEGENERICCLICKABLEROWCONTEINS="profileGenericClickableRowConteins";
public static final String PROFILEPARCHEGGIOTALLONCINOHEADER="profileParcheggioTalloncinoHeader";
public static final String PROFILEINDIRIZZILATUACITTAPREFERITA="profileIndirizziLaTuaCittaPreferita";
public static final String PROFILEINDIRIZZIDIFATTURAZIONE="profileIndirizziDiFatturazione";
public static final String PROFILEADDRESSSECTIONLABEL="profileAddressSectionLabel";
public static final String PROFILECARBURANTEILTUOPIENO="profileCarburanteIlTuoPieno";
public static final String PROFILEPARCHEGGIOLETUETARGHE="profileParcheggioLeTueTarghe";
public static final String PROFILEINDIRIZZILATUAEMAIL="profileIndirizziLaTuaEmail";
public static final String PROFILEINDIRIZZIDISPEDIZIONE="profileIndirizziDiSpedizione";
public static final String PROFILETRASPORTISECTIONLABEL="profileTrasportiSectionLabel";
public static final String PROFILEPARCHEGGIOSECTIONLABEL="profileParcheggioSectionLabel";
public static final String PROFILECARBURANTEILTUOIMPORTOPREFERITO="profileCarburanteIlTuoImportoPreferito";
public static final String PROFILETRASPORTILATUACARTAFRECCIA="profileTrasportiLaTuacartaFreccia";


	public SettingProfileMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

