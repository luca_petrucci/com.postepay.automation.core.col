package com.postepay.automation.core.ui.pages.requestp2ppagestep2;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.AmoutCreditToRequestSummary;


public class RequestP2PPageStep2 extends Page {
	public static final String NAME="T016";


	public RequestP2PPageStep2(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(AmoutCreditToRequestSummary.NAME, UiObjectRepo.get().get(AmoutCreditToRequestSummary.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {

		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {

		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {

		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new RequestP2PPageStep2Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {

		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {

		return null;
	}

	public void clickOnRequestButton() {

		((RequestP2PPageStep2Manager)this.manager).clickOnRequestButton();
	}
	
	public void clickOnAddComment() {
		
		((RequestP2PPageStep2Manager)this.manager).clickOnAddComment();
	}
}

