package com.postepay.automation.core.ui.pages.communitycardonepage;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.CardsOfP2pCommunity;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;

import io.appium.java_client.android.AndroidDriver;

import com.postepay.automation.core.ui.molecules.FooterHomePage;


public class CommunityCardOnePage extends Page {
	public static final String NAME="T006";


	public CommunityCardOnePage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	protected void onInit() {
		this.addToTemplate(CardsOfP2pCommunity.NAME, UiObjectRepo.get().get(CardsOfP2pCommunity.NAME), true);
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(FooterHomePage.NAME, UiObjectRepo.get().get(FooterHomePage.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {

		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {

		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {

		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new CommunityCardOnePageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {

		return new CommunityCardOnePageManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {

		return null;
	}

	public void clickOnSendP2p() {
		((CommunityCardOnePageManager)this.manager).clickOnSendP2p();

	}

	public void clickOnRequestP2p() {
		((CommunityCardOnePageManager)this.manager).clickOnRequestP2p();
	}
	
	public void verifyHeaderPage() {
		((CommunityCardOnePageManager)this.manager).verifyHeaderPage();
	}

	public void verifyGoalAndWinCard() {
		((CommunityCardOnePageManager)this.manager).verifyGoalAndWinCard();
	}
	
	public void swipOnG2g() {
		((CommunityCardOnePageManager)this.manager).swipOnG2g();
	}
	
	public void swipOnG2g(WebDriver driver) {
		((CommunityCardOnePageManager)this.manager).swipOnG2g(driver);
	}
}

