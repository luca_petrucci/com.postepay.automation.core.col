package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class ChooseFuelStationFuelPurchase extends Molecule {
	public static final String NAME="M084";
	
	public static final String CARDVIEWFUELPURCHASE="cardViewFuelPurchase";
	public static final String NEARICONPROVIDER  = "nearIconProvider";
	public static final String NEARTEXTPOMPA  = "nearTextPompa";
	public static final String NEARSTREET  = "nearStreet";
	public static final String NEARCITY  = "nearCity";
	public static final String NEARICONYELLOW  = "nearIconYellow";
	public static final String NEARKMDISTANCE  = "nearKmDistance";
	public static final String SEDEPOSTECOLLAUDO = "sedePosteCollaudo";

	public ChooseFuelStationFuelPurchase(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

