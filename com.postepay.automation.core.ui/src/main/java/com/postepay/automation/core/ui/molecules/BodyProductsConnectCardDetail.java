package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyProductsConnectCardDetail extends Molecule {
	public static final String NAME="M075";

	public static final String FRAMELAYOUTCONNECTCARD="frameLayoutConnectCard";
	public static final String AUTOMATICRECHARGEINDETAILCARD="automaticRechargeInDetailCard";
	public static final String COUNTINGBELANCEOFCARDDETAILS="countingBelanceOfCardDetails";
	public static final String AVAIBLEBELANCEOFCARDDETAILS="avaibleBelanceOfCardDetails";
	public static final String GOOGLEPAYLABEL = "googlePayLabelM075";
	public static final String GOOGLEPAYIMAGE = "googlePayImageM075";
	public static final String GOOGLEPAYTOGGLE = "googlePayToggleM075";
	public static final String GOOGLEPAYCOPY = "googlePayCopyM075";
	public static final String GOOGLEPAYSCOPRIDIPIU = "googlePayScopriPiuM075";


	public BodyProductsConnectCardDetail(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

