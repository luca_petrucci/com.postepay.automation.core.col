package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class MinisteroTrasportiSummaryMolecola extends Molecule {
	public static final String NAME="M272";
	public static final String TITLEMINISTEROSUMMARY="titleMinisteroSummary";
	public static final String LABELPAGACONMINISTEROSUMMARY="labelPagaConMinisteroSummary";
	public static final String NUMEROPRATICAMINISTEROSUMMARY="numeroPraticaMinisteroSummary";
	public static final String AMBITOMINISTEROSUMMARY="ambitoMinisteroSummary";
	public static final String TIPOLOGIAPRATICASUMMARY="tipologiaPraticaSummary";
	public static final String LABELPRATICASUMMARY="labelPraticaSummary";
	public static final String IMPORTOMINISTEROSUMMARY="importoMinisteroSummary";
	public static final String COMMISSIONEMINISTEROSUMMARY="commissioneMinisteroSummary";
	public static final String DETTAGLIOBOLLETTINOMINISTEROSUMMARY="dettaglioBollettinoMinisteroSummary";
	public static final String BTNPLUSMINISTEROSUMMARY="btnPlusMinisteroSummary";
	public static final String ESEGUITODAMINISTEROSUMMARY="eseguitoDaMinisteroSummary";
	public static final String INVIOFATTURAMINISTEROSUMMARY="invioFatturaMinisteroSummary";
	public static final String BTNPAGAMINISTEROSUMMARY="btnPagaMinisteroSummary";



	public MinisteroTrasportiSummaryMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

