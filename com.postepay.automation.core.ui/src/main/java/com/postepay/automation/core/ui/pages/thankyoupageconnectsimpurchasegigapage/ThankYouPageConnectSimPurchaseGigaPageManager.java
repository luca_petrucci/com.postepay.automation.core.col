package com.postepay.automation.core.ui.pages.thankyoupageconnectsimpurchasegigapage;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.ThankYouPagePurchaseG2G;
import com.postepay.automation.core.ui.molecules.ThankYouPageSendG2G;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class ThankYouPageConnectSimPurchaseGigaPageManager extends PageManager {

	public ThankYouPageConnectSimPurchaseGigaPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyLayoutPage() {
		Particle image = (Particle) UiObjectRepo.get().get(ThankYouPagePurchaseG2G.IMAGEOFPURCHASEGIGATHANKYOUPAGE);
		Particle header = (Particle) UiObjectRepo.get().get(ThankYouPagePurchaseG2G.HEADEROFPURCHASESUCCESSGIGATHANKYOUPAGE);
		Particle text = (Particle) UiObjectRepo.get().get(ThankYouPagePurchaseG2G.COPYOFPURCHASESUCCESSGIGATHANKYOUPAGE);
		
		assertTrue(image.getElement().isDisplayed());
		assertTrue(header.getElement().isDisplayed());
		assertTrue(text.getElement().isDisplayed());
		
	}
	
	public void clickOnClose() {
		Particle btn = (Particle) UiObjectRepo.get().get(ThankYouPagePurchaseG2G.CLOSEPURCHASEGIGATHANKYOUPAGE);
		
		btn.getElement().click();
		
		WaitManager.get().waitHighTime();
	}
}

