package com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep1;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import com.postepay.automation.core.ui.molecules.BodyExtraurbanPurchase;
import com.postepay.automation.core.ui.molecules.SummaryG2gSend;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;
import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.SoftAssertion;

public class ExtraurbanPurchasePageStep1Manager extends PageManager {

	Particle partenza = (Particle) UiObjectRepo.get().get(BodyExtraurbanPurchase.FROMCITYEXTRAURBANPURCHASE);
	Particle destinazione = (Particle) UiObjectRepo.get().get(BodyExtraurbanPurchase.TOCITYEXTRAURBANPURCHASE);
	Particle toggleAndata = (Particle) UiObjectRepo.get().get(BodyExtraurbanPurchase.FIRSTRADIOEXTRAURBANPURCHASE);
	Particle toggleAndataRitorno = (Particle) UiObjectRepo.get().get(BodyExtraurbanPurchase.SECONDRADIOEXTRAURBANPURCHASE);
	Particle date = (Particle) UiObjectRepo.get().get(BodyExtraurbanPurchase.DATEEXTRAURBANPURCHASE);
	Particle hours = (Particle) UiObjectRepo.get().get(BodyExtraurbanPurchase.TIMEEXTRAURBANPURCHASE);
	Particle passengersContains = (Particle) UiObjectRepo.get().get(BodyExtraurbanPurchase.NUMBERPASSENGERSEXTRAURBANPURCHASE);
	Particle btnSearchTicket = (Particle) UiObjectRepo.get().get(BodyExtraurbanPurchase.BUTTONSEARCHEXTRAURBANPURCHASE);
	Particle searchBarPlace = (Particle) UiObjectRepo.get().get(BodyExtraurbanPurchase.SEARCHBAR);
	Particle passengerAdultNumber = (Particle) UiObjectRepo.get().get(BodyExtraurbanPurchase.PASSENGERADULTNUMBER);
	Particle passengerKindNumber = (Particle) UiObjectRepo.get().get(BodyExtraurbanPurchase.PASSENGERKINDNUMBER);
	Particle passengerAdultLabel = (Particle) UiObjectRepo.get().get(BodyExtraurbanPurchase.PASSENGERADULTLABELCLICK);
	Particle passengerKindLabel = (Particle) UiObjectRepo.get().get(BodyExtraurbanPurchase.PASSENGERKINDLABELCLICK);
	
	
	public ExtraurbanPurchasePageStep1Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage(String titlePage) {
		LayoutTools tool = new LayoutTools();
		tool.verifyPresenceHeaderPage(titlePage);
	}
	
	public void verifyLayoutPage() {
		assertTrue(partenza.getElement().isDisplayed());
		assertTrue(destinazione.getElement().isDisplayed());
		assertTrue(toggleAndata.getElement().isDisplayed());
		assertTrue(toggleAndataRitorno.getElement().isDisplayed());
		assertTrue(date.getElement().isDisplayed());
		assertTrue(hours.getElement().isDisplayed());
		assertTrue(passengersContains.getElement().isDisplayed());
		assertTrue(passengerAdultLabel.getElement().isDisplayed());
		assertTrue(passengerKindLabel.getElement().isDisplayed());
		assertTrue(btnSearchTicket.getElement().isDisplayed());
	}
	
	public void clickOnChooseStartPlace() {
		partenza.getElement().click();
		WaitManager.get().waitMediumTime();
		
		verifyHeaderpage("Scegli partenza");
	}

	public void clickOnChooseDestinationPlace() {
		destinazione.getElement().click();
		WaitManager.get().waitMediumTime();
		
		verifyHeaderpage("Scegli destinazione");
	}
	
	public void sendStartPlace(WebDriver driver, String startPlace) {
		searchBarPlace.getElement().sendKeys(startPlace);
		WaitManager.get().waitMediumTime();
		
		// Cerchiamo nelle opzioni di ricerca l'oggetto con il nome startPlace
		String loc = "//*[@resource-id='posteitaliane.posteapp.apppostepay:id/address' and @text='" + startPlace + "']";
		
		WebElement row = (WebElement) driver.findElement(By.xpath(loc));
		row.click();
		WaitManager.get().waitMediumTime();
		
	}
	
	public void sendDestinationPlace(WebDriver driver, String destinationPlace) {
		searchBarPlace.getElement().sendKeys(destinationPlace);
		WaitManager.get().waitMediumTime();
		
		// Cerchiamo nelle opzioni di ricerca l'oggetto con il nome destinationPlace
		String loc = "//*[@resource-id='posteitaliane.posteapp.apppostepay:id/address' and @text='" + destinationPlace + "']";
		
		WebElement row = (WebElement) driver.findElement(By.xpath(loc));
		row.click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnPassenger(String type) {
		
		if (type.equals("adulto")) {
			// clicare su Adulti
			passengerAdultLabel.getElement().click();
			WaitManager.get().waitMediumTime();
		}else {
			passengerKindLabel.getElement().click();
			WaitManager.get().waitMediumTime();
		}
	}
	
	public void verifyPassenger(String moreInfo) {
		
		if (moreInfo.equals("si")) {
			// TODO Implementare controlli per quando si introduce un test con modifiche su numero passeggeri 
		}else {
			assertTrue(passengerAdultNumber.getElement().getText().equals("1"));
			assertTrue(passengerKindNumber.getElement().getText().equals("0"));			
		}
	}
	
	public void verifyData(String moreInfo) {
		
		if (moreInfo.equals("si")) {
			// TODO Implementare controlli per quando si introduce un test con modifiche su numero passeggeri 
		}else {
			// Verifichiamo che di default la data sia quella di oggi
			StringAndNumberOperationTools tool = new StringAndNumberOperationTools();
			String toDay = tool.getDateExtraurban();
			System.out.println("Actual "+ date.getElement().getText() + " toDay " +toDay);
			assertTrue(date.getElement().getText().equals(toDay));
		}
	}

	public void clickOnSearchTicket() {
		btnSearchTicket.getElement().click();
		WaitManager.get().waitMediumTime();
	}
}

