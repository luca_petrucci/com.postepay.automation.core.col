package com.postepay.automation.core.ui.pages.productsconnectpage;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.GenericOverviewsCardDetails;
import com.postepay.automation.core.ui.molecules.ConnectCardFilterAndMoviment;
import com.postepay.automation.core.ui.molecules.FooterHomePage;


public class ProductsConnectPage extends Page {
	public static final String NAME="T030";
	

	public ProductsConnectPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
this.addToTemplate(GenericOverviewsCardDetails.NAME, UiObjectRepo.get().get(GenericOverviewsCardDetails.NAME), true);
this.addToTemplate(ConnectCardFilterAndMoviment.NAME, UiObjectRepo.get().get(ConnectCardFilterAndMoviment.NAME), true);
this.addToTemplate(FooterHomePage.NAME, UiObjectRepo.get().get(FooterHomePage.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new ProductsConnectPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

}

