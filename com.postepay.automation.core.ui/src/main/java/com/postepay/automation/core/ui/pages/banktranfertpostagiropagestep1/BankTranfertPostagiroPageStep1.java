package com.postepay.automation.core.ui.pages.banktranfertpostagiropagestep1;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.WhomWontSendAPostagiroSummary;


public class BankTranfertPostagiroPageStep1 extends Page {
	public static final String NAME="T037";
	

	public BankTranfertPostagiroPageStep1(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
this.addToTemplate(WhomWontSendAPostagiroSummary.NAME, UiObjectRepo.get().get(WhomWontSendAPostagiroSummary.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new BankTranfertPostagiroPageStep1Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new BankTransfertPostagiroPageStep1ManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifySummarySepa(String nameInput, String ibanInput, String amountInput, String reasonInput, String myIbanInput, String cardNumberInput) {
		((BankTranfertPostagiroPageStep1Manager)this.manager).verifySummarySepa(nameInput, ibanInput, amountInput, reasonInput, myIbanInput, cardNumberInput);
	}
	
	public void clickOnPay() {
		((BankTranfertPostagiroPageStep1Manager)this.manager).clickOnPay();
	}
	
	public void verifyHeaderPage(String inputTitle) throws Error {
		((BankTranfertPostagiroPageStep1Manager)this.manager).verifyHeaderPage(inputTitle);
	}
}

