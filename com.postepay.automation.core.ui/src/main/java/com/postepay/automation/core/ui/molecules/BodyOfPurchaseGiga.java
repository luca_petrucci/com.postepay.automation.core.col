package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyOfPurchaseGiga extends Molecule {
	public static final String NAME="M037";

	public static final String G2GAMOUNTAVAIBLEGIGASUBCARD="g2gAmountAvaibleGigaSubCard";
	public static final String G2GPURCHASEBUTTONGIGASUBCARD="g2gPurchaseButtonGigaSubCard";
	public static final String COPYCREDITSUBCARD="copyCreditSubCard";
	public static final String TITLEGIGASUBCARD="titleGigaSubCard";
	public static final String TITLECREDITSUBCARD="titleCreditSubCard";
	public static final String G2GSENDBUTTONGIGASUBCARD="g2gSendButtonGigaSubCard";
//	public static final String SIMDETAILSSUBCARD="simDetailsSubCard";
//	public static final String PHONENUMBERSUBCARD="phoneNumberSubCard";
//	public static final String CREDITSIMSUBCARD="creditSimSubCard";
//	public static final String GIGADETAILSSUBCARD="gigaDetailsSubCard";

	public BodyOfPurchaseGiga(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

