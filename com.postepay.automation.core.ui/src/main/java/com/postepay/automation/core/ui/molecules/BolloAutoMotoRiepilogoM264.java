package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BolloAutoMotoRiepilogoM264 extends Molecule {
	public static final String NAME="M264";
	
	public static final String PAGABTNM264="pagaBtnM264";
public static final String TITLEM264="titleM264";
public static final String IMPORTOM264="importoM264";
public static final String REGIONEM264="regioneM264";
public static final String ANNULLAM264="annullaM264";
public static final String COMMISSIONEM264="commissioneM264";
public static final String PAGACONM264="pagaconM264";
public static final String TARGAM264="targaM264";


	public BolloAutoMotoRiepilogoM264(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

