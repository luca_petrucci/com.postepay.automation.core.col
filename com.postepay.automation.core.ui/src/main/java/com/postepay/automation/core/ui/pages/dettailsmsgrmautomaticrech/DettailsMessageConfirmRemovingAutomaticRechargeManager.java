package com.postepay.automation.core.ui.pages.dettailsmsgrmautomaticrech;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import com.postepay.automation.core.ui.molecules.BachecaPageMolecola;
import com.postepay.automation.core.ui.molecules.DettailsMessageConfirmRemovingAutomaticRechargeMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;

import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class DettailsMessageConfirmRemovingAutomaticRechargeManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();
	
	Particle typeMessage=(Particle) UiObjectRepo.get().get(DettailsMessageConfirmRemovingAutomaticRechargeMolecola.TYPEMESSAGE);
	Particle dateMessage=(Particle) UiObjectRepo.get().get(DettailsMessageConfirmRemovingAutomaticRechargeMolecola.DATEMESSAGE);
	Particle nomeMessage=(Particle) UiObjectRepo.get().get(DettailsMessageConfirmRemovingAutomaticRechargeMolecola.NOMEMESSAGE);
	Particle txtMessage=(Particle) UiObjectRepo.get().get(DettailsMessageConfirmRemovingAutomaticRechargeMolecola.TXTMESSAGE);
	Particle pieMessage=(Particle) UiObjectRepo.get().get(DettailsMessageConfirmRemovingAutomaticRechargeMolecola.PIEMESSAGE);
	Particle dinamicMessage=(Particle) UiObjectRepo.get().get(DettailsMessageConfirmRemovingAutomaticRechargeMolecola.DINAMICMESSAGE);
	
	public DettailsMessageConfirmRemovingAutomaticRechargeManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public WebElement getElementType(WebDriver driver, String typeInput) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, typeMessage, typeInput);
		assertTrue(elm.isDisplayed());
		return elm;
	}

	public WebElement getElementName(WebDriver driver, String nameInput) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, nomeMessage, nameInput);
		assertTrue(elm.isDisplayed());
		return elm;
	}
	
	public WebElement getElementDinamicText(WebDriver driver, String dinamicTextInput) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, dinamicMessage, dinamicTextInput);
		assertTrue(elm.isDisplayed());
		return elm;
	}
	
	public WebElement getElementDate(WebDriver driver, String dateInput) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, dateMessage, dateInput);
		assertTrue(elm.isDisplayed());
		return elm;
	}
	
	
	public void verifyLayoutMessage(WebDriver driver, String typeInput, String dateInput, String nameInput) {
		getElementType(driver, typeInput);
		getElementDate(driver, dateInput);
		getElementName(driver, nameInput);
//		getElementDinamicText(driver, "Potrai modificare o eliminare la tua richiesta accedendo in App al \"Dettaglio obiettivo\" del tuo Salvadanaio.\r\n");
			try {
			assertTrue(txtMessage.getElement().isDisplayed());
			assertTrue(pieMessage.getElement().isDisplayed());
		} catch (Exception e) {
			
		}
	}
	
	public void verifyHeaderPage() {
		Particle title=(Particle) UiObjectRepo.get().get(DettailsMessageConfirmRemovingAutomaticRechargeMolecola.HEADERDETTAILSMESSAGECONFIRMREMOVINGAUTOMATICRECHARGE);

		title.getElement().isDisplayed();

	}

	public void verifyMessageDetailDescription() {
		Particle title=(Particle) UiObjectRepo.get().get(DettailsMessageConfirmRemovingAutomaticRechargeMolecola.MESSAGEDETTAILSMESSAGECONFIRMREMOVINGAUTOMATICRECHARGE);

		title.getElement().isDisplayed();

	}

	public void clickOnCloseButton() {
		Particle title=(Particle) UiObjectRepo.get().get(DettailsMessageConfirmRemovingAutomaticRechargeMolecola.CLOSEDETTAILSMESSAGECONFIRMREMOVINGAUTOMATICRECHARGE);

		title.getElement().click();
		
		WaitManager.get().waitShortTime();
	}

	public void verifyMessage(String typeInput, String dateInput, String nameInput) {
		Particle type=(Particle) UiObjectRepo.get().get(DettailsMessageConfirmRemovingAutomaticRechargeMolecola.TYPEM212);
		Particle data=(Particle) UiObjectRepo.get().get(DettailsMessageConfirmRemovingAutomaticRechargeMolecola.DATEM212);
		Particle titolo=(Particle) UiObjectRepo.get().get(DettailsMessageConfirmRemovingAutomaticRechargeMolecola.NOMEM212);
		Particle testo=(Particle) UiObjectRepo.get().get(DettailsMessageConfirmRemovingAutomaticRechargeMolecola.TXTM212);
		Particle btnElimina=(Particle) UiObjectRepo.get().get(DettailsMessageConfirmRemovingAutomaticRechargeMolecola.ELIMINAM212);

		assertTrue("Il Typo del messaggio non è corretto", type.getElement().getText().toUpperCase().trim().equals(typeInput.toUpperCase().trim()));
		assertTrue("La data del messaggio non è corretto", data.getElement().getText().toUpperCase().trim().equals(dateInput.toUpperCase().trim()));
		assertTrue("Il titolo del messaggio non è corretto AR: "+titolo.getElement().getText().toUpperCase().trim()+ " ER: "+nameInput.toUpperCase(), titolo.getElement().getText().toUpperCase().trim().equals(nameInput.toUpperCase().trim()));
		assertTrue("Il body del messaggio non è presente", type.getElement() != null);
		assertTrue("Il bottono ELIMINA non è corretto AR: "+btnElimina.getElement().getText().toUpperCase().trim()+ " ER: ELIMINA", btnElimina.getElement().getText().toUpperCase().trim().equals("ELIMINA"));
	}
	
}

