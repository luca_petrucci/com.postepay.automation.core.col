package com.postepay.automation.core.ui.pages.requestp2ppagestep3;

import com.postepay.automation.core.ui.molecules.ThankYouPageRequestP2P;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class RequestP2PPageStep3Manager extends PageManager {

	public RequestP2PPageStep3Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void clickOnCloseButton() {
		Particle btn=(Particle) UiObjectRepo.get().get(ThankYouPageRequestP2P.BUTTONCLOSEP2PREQUEST);
		
		btn.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
}

