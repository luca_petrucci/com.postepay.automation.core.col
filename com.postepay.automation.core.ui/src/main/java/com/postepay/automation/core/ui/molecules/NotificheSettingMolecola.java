package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class NotificheSettingMolecola extends Molecule {
	public static final String NAME="M246";

	public static final String SISETTINGNOTIFICHE="siSettingNotifiche";
	public static final String ARROWSETTINGNOTIFICHE="arrowSettingNotifiche";
	public static final String MOVIMENTIUSCITASETTINGNOTIFICHE="movimentiUscitaSettingNotifiche";
	public static final String IMPOSTACOMEPREFERITOBUTTON="impostaComePreferitoButton";
	public static final String MOVIMENTIENTRATASETTINGNOTIFICHE="movimentiEntrataSettingNotifiche";
	public static final String QUESTOEPREFERITOSETTINGNOTIFICHE="questoEPreferitoSettingNotifiche";
	public static final String IMPOSTACOMEPREFERITOTEXT="impostaComePreferitoText";
	public static final String NOTIFICHELABELSETTINGNOTIFICHE="notificheLabelSettingNotifiche";
	public static final String DISPOSITIVOLABELSETTINGNOTIFICHE="dispositivoLabelSettingNotifiche";
	public static final String RICEVENDONOTIFICHESETTINGNOTIFICHE="ricevendoNotificheSettingNotifiche";
	public static final String NOTIFICHEPUSHBIGTEXT="notificaPushBigText";

	public NotificheSettingMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

