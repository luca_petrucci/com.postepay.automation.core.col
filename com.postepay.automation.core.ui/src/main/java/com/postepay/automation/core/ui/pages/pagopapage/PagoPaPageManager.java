package com.postepay.automation.core.ui.pages.pagopapage;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.PagoPaMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class PagoPaPageManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();


	public PagoPaPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void click() {
		Particle manuale = (Particle) UiObjectRepo.get().get(PagoPaMolecola.BTNCALCOLAM281);
		System.out.println("xPath "+ manuale.getXPath());
	}
	
	public void clickOnManuale() {
		Particle manuale = (Particle) UiObjectRepo.get().get(PagoPaMolecola.BTNCOMILAMANUALM281);
		System.out.println("xPath "+ manuale.getXPath());
		manuale.getElement().click();
		WaitManager.get().waitShortTime();
	}

	public void clickOnTabPostale() {
		Particle postale = (Particle) UiObjectRepo.get().get(PagoPaMolecola.TABPOSTALEM281);
		postale.getElement().click();
		WaitManager.get().waitShortTime();
	}
	
	public void clickOnTabBanche() {
		Particle banche = (Particle) UiObjectRepo.get().get(PagoPaMolecola.TABBANCHEM281);
		banche.getElement().click();
		WaitManager.get().waitShortTime();
	}
	
	public void verifyLayoutBanche() {
		Particle beneficiarioLabel = (Particle) UiObjectRepo.get().get(PagoPaMolecola.BENEFICIARIOLABELM281);
		Particle photoIcon = (Particle) UiObjectRepo.get().get(PagoPaMolecola.ICONFOTOM281);
		Particle canaleLabel = (Particle) UiObjectRepo.get().get(PagoPaMolecola.CANALEPAGAMENTOM281);
		Particle tabPoste = (Particle) UiObjectRepo.get().get(PagoPaMolecola.TABPOSTALEM281);
		Particle tabBanche = (Particle) UiObjectRepo.get().get(PagoPaMolecola.TABBANCHEM281);
		Particle codiceAvviso = (Particle) UiObjectRepo.get().get(PagoPaMolecola.CODICEAVVISOM281);
		Particle codiceFiscale = (Particle) UiObjectRepo.get().get(PagoPaMolecola.CODICEFISCALECREDITOREM281);
		
		String erBeneficiario="Dati del beneficiario";
		String arBeneficiario=beneficiarioLabel.getElement().getText();
		assertTrue("ER: "+ erBeneficiario +"AR: "+arBeneficiario,arBeneficiario.contains(erBeneficiario));
		assertTrue(photoIcon.getElement().getText() != null);
		
		String erCanale="Canale di pagamento";
		String arCanale=canaleLabel.getElement().getText();
		assertTrue("ER: "+ erCanale +"AR: "+arCanale,arCanale.equals(erCanale));
		
		String erPoste="Bollettino Postale PA";
		String arPoste=tabPoste.getElement().getText();
		assertTrue("ER: "+ erPoste +"AR: "+arPoste,arPoste.equals(erPoste));
		
		String erBanche="Banche e altri canali";
		String arBanche=tabBanche.getElement().getText();
		assertTrue("ER: "+ erBanche +"AR: "+arBanche,arBanche.equals(erBanche));
		
		String erCodice="Codice avviso";
		String arCodice=codiceAvviso.getElement().getText();
		assertTrue("ER: "+ erCodice +"AR: "+arCodice,arCodice.equals(erCodice));
		
		String erFiscale="Codice fiscale ente creditore";
		String arFiscale=codiceFiscale.getElement().getText();
		assertTrue("ER: "+ erFiscale +"AR: "+arFiscale,arFiscale.equals(erFiscale));
		
	}
}

