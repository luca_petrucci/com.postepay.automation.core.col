package com.postepay.automation.core.ui.pages.sendg2gpagestep5;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;

import com.postepay.automation.core.ui.molecules.ThankYouPageGeneric;
import com.postepay.automation.core.ui.molecules.ThankYouPageSendG2G;
import com.postepay.automation.core.ui.pages.sendg2gpagestep1.SendG2GPageStep1Manager;

import io.appium.java_client.android.AndroidDriver;


public class SendG2GPageStep5 extends Page {
	public static final String NAME="T022";
	

	public SendG2GPageStep5(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(ThankYouPageSendG2G.NAME, UiObjectRepo.get().get(ThankYouPageSendG2G.NAME), true);
		this.addToTemplate(ThankYouPageGeneric.NAME, UiObjectRepo.get().get(ThankYouPageGeneric.NAME), false);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SendG2GPageStep5Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new SendG2GPageStep5ManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyLayoutPage() {
		((SendG2GPageStep5Manager)this.manager).verifyLayoutPage();
	}
	
	public void clickOnClose() {
		((SendG2GPageStep5Manager)this.manager).clickOnClose();
	}
	
	public void verifyImageCard(WebDriver driver, double discrepanza) {
		((SendG2GPageStep5Manager)this.manager).verifyImageCard(driver, discrepanza);
	}
	
	public void verifyTYPNoG2g() {
		((SendG2GPageStep5Manager)this.manager).verifyTYPNoG2g();
	}
}

