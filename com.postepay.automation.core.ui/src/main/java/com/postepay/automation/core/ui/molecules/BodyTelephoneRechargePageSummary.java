package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyTelephoneRechargePageSummary extends Molecule {
	public static final String NAME="M116";
	
	public static final String TITLEHEADERSUMMARYTHELEPHONERECHARGE="titleHeaderSummaryThelephoneRecharge";
public static final String NUMBERTELEPHONERECHARGE="numberTelephoneRecharge";
public static final String TOTALRECHARGETELEPHONERECHARGE="totalRechargeTelephoneRecharge";
public static final String COMMISSIONTELEPHONERECHARGE="commissionTelephoneRecharge";
public static final String OPERATORTELEPHONERECHARGE="operatorTelephoneRecharge";
public static final String PAYWITHSUMMARYTHELEPHONERECHARGE="payWithSummaryThelephoneRecharge";
public static final String CONFIRMBUTTONTELEPHONERECHARGE="confirmButtonTelephoneRecharge";


	public BodyTelephoneRechargePageSummary(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

