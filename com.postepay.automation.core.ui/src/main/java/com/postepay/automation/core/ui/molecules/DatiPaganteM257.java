package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class DatiPaganteM257 extends Molecule {
	public static final String NAME="M257";
	
	public static final String M257VALOREINDIRIZZO="M257valoreIndirizzo";
public static final String M257VALORENOME="M257valoreNome";
public static final String M257VALORECITTA="M257valoreCitta";
public static final String M257BTNCONFERMA="M257btnConferma";
public static final String M257VALOREPROVINCIA="M257valoreProvincia";
public static final String M257TITLE="M257title";
public static final String M257BTNCANCELLA="M257btnCancella";
public static final String M257VALORECAP="M257valoreCap";
public static final String M257VALORECOGNOME="M257valoreCognome";


	public DatiPaganteM257(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

