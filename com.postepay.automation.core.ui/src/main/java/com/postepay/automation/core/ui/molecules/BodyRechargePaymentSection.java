package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyRechargePaymentSection extends Molecule {
	public static final String NAME="M043";

	public static final String RECHARGESIMPOSTEMOBILE="rechargeSimPosteMobile";
	public static final String RECHARGESIMVODAFONE="rechargeSimVodafone";
	public static final String SENDP2PPAYMENT="sendP2PPayment";
	public static final String RECHARGEOTHERCARDPAYMENT="rechargeOtherCardPayment";
	public static final String RECHARGEMYCARDPAYMENT="rechargeMyCardPayment";
	public static final String AUTOMATICRECHARGECARDONPAYMENTSECTION="automaticRechargeCardOnPaymentSection";
	public static final String CARDRECHARGEPOSTEPAY="cardRechargePostepay";

	public BodyRechargePaymentSection(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

