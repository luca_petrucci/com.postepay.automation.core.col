package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class CarburanteStep3Molecola extends Molecule {
	public static final String NAME="M221";
	
	public static final String CARBURANTESUMMARYHEADER="carburanteSummaryHeader";
public static final String CARBURANTEDATA="carburanteData";
public static final String CARBURANTEPROVIDER="carburanteProvider";
public static final String CARBURANTEPAYBUTTON="carburantePayButton";
public static final String CARBURANTEPAYWITH="carburantePayWith";
public static final String CARBURANTEANNULLABUTTON="carburanteAnnullaButton";
public static final String CARBURANTELABEPAYWITH="carburanteLabePayWith";
public static final String CARBURANTEIMPORTO="carburanteImporto";


	public CarburanteStep3Molecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

