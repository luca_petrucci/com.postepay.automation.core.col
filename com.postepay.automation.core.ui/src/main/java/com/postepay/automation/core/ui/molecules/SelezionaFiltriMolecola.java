package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SelezionaFiltriMolecola extends Molecule {
	public static final String NAME="M269";
	public static final String TITLEPERIODO="titlePeriodo";
	public static final String LABELFROMDATE="labelFromDate";
	public static final String LABELTODATE="labelToDate";
	public static final String TITLESERVICE="titleService";
	public static final String LABELSERVIZIO="labelServizio";
	public static final String TITLECATEGORY="titleCategory";
	public static final String LABELCATEGORIA="labelCategoria";
	public static final String GIORNOCORRENTE="giornoCorrente";
	public static final String OKCALENDARFILTRO="okCalendarFiltro";
	public static final String BTNRESETTA="btnResetta";
	public static final String GIORNOPRECEDENTE="giornoPrecedente";
	
	public SelezionaFiltriMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

