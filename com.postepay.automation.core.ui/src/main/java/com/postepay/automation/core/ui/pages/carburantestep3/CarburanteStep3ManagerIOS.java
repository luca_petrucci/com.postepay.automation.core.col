package com.postepay.automation.core.ui.pages.carburantestep3;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.CarburanteStep3Molecola;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import test.automation.core.UIUtils;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import utility.SoftAssertion;

public class CarburanteStep3ManagerIOS extends CarburanteStep3Manager {

	public CarburanteStep3ManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage() {
		Particle p=(Particle) UiObjectRepo.get().get(HeaderGenericAllPage.TITLEGENERICWITHTITLE);
		String nuovoLocator=Utility.replacePlaceHolders(p.getLocator(), new Entry("title","XCUIElementTypeStaticText"));
		WebDriver driver=UIUtils.ui().getCurrentDriver();
		WebElement newHeader = driver.findElement(By.xpath(nuovoLocator));
		SoftAssertion.get().getAssertions().assertThat(newHeader.getText()).withFailMessage("Controllo header carrburante Step2 Fallito").isEqualTo("Acquista Carburante");
	}

	public void verifyLayoutPage() {
		Particle title = (Particle) UiObjectRepo.get().get(CarburanteStep3Molecola.CARBURANTESUMMARYHEADER);
		Particle layoutPayWith = (Particle) UiObjectRepo.get().get(CarburanteStep3Molecola.CARBURANTELABEPAYWITH);
		Particle api = (Particle) UiObjectRepo.get().get(CarburanteStep3Molecola.CARBURANTEPROVIDER);

		assertTrue(title.getElement().isDisplayed());
		assertTrue(date.getElement().isDisplayed());
		assertTrue(payWithCard.getElement().isEnabled());
		assertTrue(btnAnnulla.getElement().isDisplayed());
		assertTrue(btnPaga.getElement().isDisplayed());
		assertTrue(layoutPayWith.getElement().isEnabled());
		assertTrue(api.getElement().isEnabled());
		assertTrue(amount.getElement().isEnabled());
	}
	
	public void verifyPayWithCard(String cardInput) {
		String cardIn = "Postepay Evolution" + cardInput;
		assertTrue("Errore La carta per il pagamento non viene mostrata",payWithCard.getElement().isEnabled());
	}
}
