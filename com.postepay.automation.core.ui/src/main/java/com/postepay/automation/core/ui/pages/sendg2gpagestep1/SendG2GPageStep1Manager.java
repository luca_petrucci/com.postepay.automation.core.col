package com.postepay.automation.core.ui.pages.sendg2gpagestep1;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.BodyInfoG2gSend;
import com.postepay.automation.core.ui.molecules.SettingProfileMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.SoftAssertion;

public class SendG2GPageStep1Manager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();
	
	Particle image = (Particle) UiObjectRepo.get().get(BodyInfoG2gSend.IMAGESENDGIGA);
	Particle copy = (Particle) UiObjectRepo.get().get(BodyInfoG2gSend.COPYHEADERSENDGIGA);
	Particle descrizione = (Particle) UiObjectRepo.get().get(BodyInfoG2gSend.COPYDESCRIPTIONSENDGIGA);
	Particle messaggio = (Particle) UiObjectRepo.get().get(BodyInfoG2gSend.MESSAGESENDGIGA);
	Particle btn = (Particle) UiObjectRepo.get().get(BodyInfoG2gSend.BUTTONSENDGIGA);
	
	public SendG2GPageStep1Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderpage(String inputTitle) {
		toolLayout.verifyPresenceHeaderPage(inputTitle);
	}
	
	public void verifyLayoutPage() {
		
		SoftAssertion.get().getAssertions().assertThat(image.getElement().isDisplayed()).withFailMessage("Image non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(copy.getElement().isDisplayed()).withFailMessage("Copy non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(descrizione.getElement().isDisplayed()).withFailMessage("Descrizione non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(messaggio.getElement().isDisplayed()).withFailMessage("Messaggio non visibile").isEqualTo(true);

//		assertTrue(image.getElement().isDisplayed());
//		assertTrue(copy.getElement().isDisplayed());
//		assertTrue(descrizione.getElement().isDisplayed());
//		assertTrue(messaggio.getElement().isDisplayed());
		assertTrue(btn.getElement().isDisplayed());
	}
	
	public void clickOnProcedi() {
		btn.getElement().click();
		WaitManager.get().waitLongTime();
	}
	
	public void verifyImageCard(WebDriver driver, double discrepanza) {
		
		LayoutImage.get().verifyImage(driver, "g2g/g2g_paccoRegalo", discrepanza);
	}
}

