package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyExtraurbanPurchaseFinalSummary extends Molecule {
	public static final String NAME="M098";
	
	public static final String DIRECTIONJOURNEYINFOSUMMARY="directionJourneyInfoSummary";
public static final String INFOPASSENGERSCONTAINER="infoPassengersContainer";
public static final String CHECKBOXPRIVACY="checkBoxPrivacy";
public static final String MODIFYBUTTON="modifyButton";
public static final String INFOJOURNEYCONTAINER="infoJourneyContainer";
public static final String PAYBUTTON="payButton";
public static final String PRIVACYTERMSBOX="privacyTermsBox";
public static final String INFOPASSENGER="infoPassenger";


	public BodyExtraurbanPurchaseFinalSummary(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

