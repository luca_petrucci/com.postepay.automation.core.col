package com.postepay.automation.core.ui.pages.automaticrechargeonzerorechargepage;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.AutomaticRechargeOnZeroRechargePageMolecola;
import com.postepay.automation.core.ui.molecules.AutomaticRechargePostepayThankYouPageMolecola;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class AutomaticRechargeOnZeroRechargePageManager extends PageManager {

	public AutomaticRechargeOnZeroRechargePageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyLayout() {
		Particle header=(Particle) UiObjectRepo.get().get(AutomaticRechargeOnZeroRechargePageMolecola.HEADERAUTOMATICRECHARGEONZERORECHARGEPAGE);
		Particle annulbutton=(Particle) UiObjectRepo.get().get(AutomaticRechargeOnZeroRechargePageMolecola.ANNULBUTTONAUTOMATICRECHARGEONZERORECHARGEPAGE);
		Particle img=(Particle) UiObjectRepo.get().get(AutomaticRechargeOnZeroRechargePageMolecola.ICONEMPTYSTATEAUTOMATICRECHARGEONZERORECHARGEPAGE);
		Particle title=(Particle) UiObjectRepo.get().get(AutomaticRechargeOnZeroRechargePageMolecola.TITLEAUTOMATICRECHARGEONZERORECHARGEPAGE);
		Particle copyTitle=(Particle) UiObjectRepo.get().get(AutomaticRechargeOnZeroRechargePageMolecola.COPYAUTOMATICRECHARGEONZERORECHARGEPAGE);
		Particle startButton=(Particle) UiObjectRepo.get().get(AutomaticRechargeOnZeroRechargePageMolecola.STARTBUTTONAUTOMATICRECHARGEONZERORECHARGEPAGE);

		// Imagine
		assertTrue(header.getElement().isDisplayed());
		// Title
		assertTrue(annulbutton.getElement().isDisplayed());
		// Imagine
		assertTrue(img.getElement().isDisplayed());
		// Title
		assertTrue(title.getElement().isDisplayed());
		// Copy
		assertTrue(copyTitle.getElement().isDisplayed());
		// Bottone
		assertTrue(startButton.getElement().isDisplayed());

		try {Thread.sleep(1000);} catch (Exception err )  {}
	}
	
	public void clickOnBackButton() {
		Particle annulbutton=(Particle) UiObjectRepo.get().get(AutomaticRechargeOnZeroRechargePageMolecola.ANNULBUTTONAUTOMATICRECHARGEONZERORECHARGEPAGE);
		
		annulbutton.getElement().click();
	}

}

