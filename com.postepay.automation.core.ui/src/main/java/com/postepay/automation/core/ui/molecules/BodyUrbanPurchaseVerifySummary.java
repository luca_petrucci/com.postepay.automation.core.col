package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyUrbanPurchaseVerifySummary extends Molecule {
	public static final String NAME="M080";
	
	public static final String TIKETCARDURBANPURCHASE="tiketCardUrbanPurchase";
public static final String BUTTONPAYURBANPURCHASEVERIFYSUMMARY="buttonPayUrbanPurchaseVerifySummary";
public static final String TOTALPRICEURBANPURCHASE="totalPriceUrbanPurchase";
public static final String SINGLEPRICEURBANPURCHASE="singlePriceUrbanPurchase";
public static final String ICONTIKETURBANPURCHASE="iconTiketUrbanPurchase";
public static final String ICONPLUSURBANPURCHASE="iconPlusUrbanPurchase";
public static final String NUMBERTIKETURBANPURCHASE="numberTiketUrbanPurchase";
public static final String TITLETIKETURBANPURCHASE="titleTiketUrbanPurchase";
public static final String ICONMINUSURBANPURCHASE="iconMinusUrbanPurchase";


	public BodyUrbanPurchaseVerifySummary(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

