package com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep3;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.BodyDetailsExtraurbanPurchase;
import com.postepay.automation.core.ui.molecules.BodyTravelSolution;
import com.postepay.automation.core.ui.molecules.DiscoverMoreHomePage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import ui.core.support.waitutil.WaitManager;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class ExtraurbanPurchasePageStep3Manager extends PageManager {

	Particle detailContainer = (Particle) UiObjectRepo.get().get(BodyDetailsExtraurbanPurchase.DETAILJOURNEYCONTAINER);
	Particle destination = (Particle) UiObjectRepo.get().get(BodyDetailsExtraurbanPurchase.DIRECTIONJOURNEYINFO);
	Particle ticketAmount = (Particle) UiObjectRepo.get().get(BodyDetailsExtraurbanPurchase.CARDTICKETPRICE);
	Particle expandButton = (Particle) UiObjectRepo.get().get(BodyDetailsExtraurbanPurchase.EXPANDBUTTON);
	Particle chooseOrdinaryTicket = (Particle) UiObjectRepo.get().get(BodyDetailsExtraurbanPurchase.CHOOSERADIOBUTTON);
	Particle proceedBtn = (Particle) UiObjectRepo.get().get(BodyDetailsExtraurbanPurchase.PROCEEDBUTTONBODYDETAILSEXTRAURBAN);
	Particle chooseBaseTicket = (Particle) UiObjectRepo.get().get(BodyDetailsExtraurbanPurchase.CHOOSERADIOBUTTONBASE);

	
	public ExtraurbanPurchasePageStep3Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage(String inputTitle) {
		WaitManager.get().waitMediumTime();
		WaitManager.get().waitMediumTime();
		LayoutTools tool = new LayoutTools();
		tool.verifyPresenceHeaderPage(inputTitle);
	}
	
	public void verifyLayoutPage() {
		assertTrue(detailContainer.getElement().isDisplayed());
		assertTrue(destination.getElement().isDisplayed());
		assertTrue(ticketAmount.getElement().isDisplayed());
	}
	
	public void clickOnExpandArrow() {
		WaitManager.get().waitMediumTime();
		expandButton.getElement().click();
		WaitManager.get().waitShortTime();
	}
	
	public void scrollToOrdinario() {
		LayoutTools toolLayout=new LayoutTools();
			UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 1500);
			WaitManager.get().waitShortTime();
		}
	
	public void scrollToBase(String targetTitleString) {
		LayoutTools toolLayout=new LayoutTools();
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 1500);
			WaitManager.get().waitShortTime();
		}
	
	public void clickOnOrdinaryTicket() {
		
		scrollToOrdinario();
		List <WebElement> listaBiglietti = chooseOrdinaryTicket.getListOfElements();
		for (WebElement e: listaBiglietti ) {
			if (e.getText().equals("ORDINARIA") || e.getText().equals("BASE") ) {
				e.click();
				WaitManager.get().waitShortTime();
				break;
			}
		}	
	}
	
	public void clickOnProceed() {
		proceedBtn.getElement().click();
		WaitManager.get().waitShortTime();
	}

}

