package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class NewAccessPageMolecola extends Molecule {
	public static final String NAME="M248";

	public static final String AUTHORIZELISTBUTTONACCESS="authorizeListButtonAccess";
	public static final String DISCOUNTTITLEACCESS="discountTitleAccess";
	public static final String DISCOUNTSUBTITLEACCESS="discountSubTitleAccess";
	public static final String GENERICICONTILEACCESS="genericIconTileAccess";
	public static final String HELLOUSERTITLEACCESS="helloUserTitleAccess";
	public static final String NEWNOTYOULINKACCESS="newNotYouLinkAccess";
	public static final String AUTHORIZEVIAWEBBUTTONACCESS="authorizeViaWebButtonAccess";
	public static final String GENERICARROWTILEACCESS="genericArrowTileAccess";
	public static final String POSTEPAYIMAGEACCESS="postePayImageAccess";
	public static final String DISCOVERDISCOUNTACCESS="discoverDiscountAccess";
	public static final String NEWACCESSBUTTONACCESS="newAccessButtonAccess";
	public static final String QRIMAGEACCESS="qrImageAccess";


	public NewAccessPageMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

