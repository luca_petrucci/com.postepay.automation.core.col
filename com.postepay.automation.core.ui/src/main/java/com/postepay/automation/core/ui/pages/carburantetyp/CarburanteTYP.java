package com.postepay.automation.core.ui.pages.carburantetyp;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.CarburanteTYPMolecola;

import io.appium.java_client.android.AndroidDriver;


public class CarburanteTYP extends Page {
	public static final String NAME="T103";
	

	public CarburanteTYP(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(CarburanteTYPMolecola.NAME, UiObjectRepo.get().get(CarburanteTYPMolecola.NAME), false);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new CarburanteTYPManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new CarburanteTYPManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyLayoutPage(WebDriver driver, int indexPoints, double discrepanza) {
		((CarburanteTYPManager)this.manager).verifyLayoutPage(driver, indexPoints, discrepanza);
	}
	
	public void swipeTo(WebDriver driver, int i, String direction) {
		((CarburanteTYPManager)this.manager).swipeTo(driver, i, direction);
	}
	
	public void clickOnX() {
		((CarburanteTYPManager)this.manager).clickOnX();
	}

}

