package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class CarburanteTYPMolecola extends Molecule {
	public static final String NAME="M222";
	
	public static final String TYPCARBURANTEBOTTONEX="typCarburanteBottoneX";
public static final String TYPCARBURANTETITOLO="typCarburanteTitolo";
public static final String TYPCARBURANTEDESCRIZIONE="typCarburanteDescrizione";
public static final String TYPCARBURANTEIMAGE="typCarburanteImage";
public static final String TYPCARBURANTEPALLINI="typCarburantePallini";
public static final String TYPCARBURANTEBOTTONE="typCarburanteBottone";


	public CarburanteTYPMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

