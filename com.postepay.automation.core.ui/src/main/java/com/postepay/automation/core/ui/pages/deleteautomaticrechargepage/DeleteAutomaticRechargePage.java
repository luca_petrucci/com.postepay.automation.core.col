package com.postepay.automation.core.ui.pages.deleteautomaticrechargepage;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.DeleteAutomaticRechargePageMolecola;


public class DeleteAutomaticRechargePage extends Page {
	public static final String NAME="T092";
	

	public DeleteAutomaticRechargePage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(DeleteAutomaticRechargePageMolecola.NAME, UiObjectRepo.get().get(DeleteAutomaticRechargePageMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new DeleteAutomaticRechargePageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	public void verifyHeaderPopUp() {
		((DeleteAutomaticRechargePageManager)this.manager).verifyHeaderPopUp();
	}

	public void verifyCopyPopUp() {
		((DeleteAutomaticRechargePageManager)this.manager).verifyCopyPopUp();		
	}
	
	public void clickOnANNULLAbutton() {
		((DeleteAutomaticRechargePageManager)this.manager).clickOnANNULLAbutton();
	}
	
	public void clickOnOKbutton() {
		((DeleteAutomaticRechargePageManager)this.manager).clickOnOKbutton();
	}
}

