package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class AutorizzaOnbMolecola extends Molecule {
	public static final String NAME="M244";
	
	public static final String TITLEAUTORIZZAONP="titleAutorizzaOnp";
public static final String IMAGEAUTORIZZAONP="imageAutorizzaOnp";
public static final String LOADINGBARCARICAMENTOONP="loadingBarCaricamentoOnp";
public static final String TEXTCARICAMENTOONB="textCaricamentoOnb";
public static final String NEGAAUTORIZZAONP="negaAutorizzaOnp";
public static final String COPYAUTORIZZAONP="copyAutorizzaOnp";
public static final String BTNAUTORIZZAONP="btnAutorizzaOnp";


	public AutorizzaOnbMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

