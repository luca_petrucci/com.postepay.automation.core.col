package com.postepay.automation.core.ui.pages.genericerrorpopup;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.GenericErrorPopUpMolecola;

import io.appium.java_client.android.AndroidDriver;


public class GenericErrorPopUp extends Page {
	public static final String NAME="T086";
	

	public GenericErrorPopUp(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(GenericErrorPopUpMolecola.NAME, UiObjectRepo.get().get(GenericErrorPopUpMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new GenericErrorPopUpManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public boolean checkIfElementIsPresent() {
		return ((GenericErrorPopUpManager)this.manager).checkIfElementIsPresent();
	}
	
	public void verifyHeaderPopUp() {

		((GenericErrorPopUpManager)this.manager).verifyHeaderPopUp();
	}

	public void verifyCopyPopUp() {

		((GenericErrorPopUpManager)this.manager).verifyCopyPopUp();
	}

	public void clickOnOKPopUp() {

		((GenericErrorPopUpManager)this.manager).clickOnOKPopUp();
	}
	
	public void verifyLayoutOfPopup(WebDriver driver, String titleText, String bodyText, boolean isIOS) {
		((GenericErrorPopUpManager)this.manager).verifyLayoutOfPopup(driver, titleText, bodyText, isIOS);
	}
	
	public void verifyLayoutOfGpsPopupIos(WebDriver driver, String titleText, String bodyText, boolean isIOS) {
		((GenericErrorPopUpManager)this.manager).verifyLayoutOfGpsPopupIos(driver, titleText, bodyText, isIOS);
	}
	
	public void verifyLayoutOfPopupIOS(WebDriver driver, String titleText, String bodyText, boolean isIOS) {
		((GenericErrorPopUpManager)this.manager).verifyLayoutOfPopupIOS(driver, titleText, bodyText, isIOS);
	}
	
	public void clickOnNoGrazie() {
		((GenericErrorPopUpManager)this.manager).clickOnNoGrazie();
	}
	
	public void isRatingPopUp() {
		((GenericErrorPopUpManager)this.manager).isRatingPopUp();
	}
	
	public void isPermissionRequired() {
		((GenericErrorPopUpManager)this.manager).isPermissionRequired();
	}
	
	public void closeAtacPopup() {
		((GenericErrorPopUpManager)this.manager).closeAtacPopup();
	}
	
	public void isPermissionRequiredSamsung(String text) {
		((GenericErrorPopUpManager)this.manager).isPermissionRequiredSamsung(text);
	}

	public void isPermissionClickGPSUsa(String text) {
		((GenericErrorPopUpManager)this.manager).isPermissionRequiredSamsung(text);
	}
	
	public void isPermissionPositionSamsung() {
		((GenericErrorPopUpManager)this.manager).isPermissionPositionSamsung();
	}
	
	public void isPermissionGeneric() {
		((GenericErrorPopUpManager)this.manager).isPermissionGeneric();
	}
	
	public void GPayFunnel() {
		((GenericErrorPopUpManager)this.manager).GPayFunnel();
	} 
	
	public boolean isLocationVisible()
	{
		return ((GenericErrorPopUpManager)this.manager).isLocationVisible();
	}
	public void verifyEmptyFieldError(String emptyField) {
		((GenericErrorPopUpManager)this.manager).verifyEmptyFieldError(emptyField);
	}

	public void clickOnApplePayPopupCloseBtn() {
		((GenericErrorPopUpManager)this.manager).clickOnApplePayPopupCloseBtn();
	}
	
	public void clickOnNonMostrarePiu() {
		((GenericErrorPopUpManager)this.manager).clickOnNonMostrarePiu();
	}

	public void genericPermission(String permissionTextButton) {
		((GenericErrorPopUpManager)this.manager).genericPermission(permissionTextButton);
		
	}

	public void clickOnChiudiRicaricaDopoOnb() {
		((GenericErrorPopUpManager)this.manager).clickOnChiudiRicaricaDopoOnb();
		
	}

	public boolean isUpdate() {
		return ((GenericErrorPopUpManager)this.manager).isUpdate();
	}

	public void clickOnNotUpdte() {
		((GenericErrorPopUpManager)this.manager).clickOnNotUpdte();
		
	}
}
