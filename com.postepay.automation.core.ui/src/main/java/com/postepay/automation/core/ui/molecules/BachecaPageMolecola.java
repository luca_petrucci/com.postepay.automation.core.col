package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BachecaPageMolecola extends Molecule {
	public static final String NAME="M211";
	
	public static final String NOTIFICHETABBACHECAPAGE="notificheTabBachecaPage";
public static final String TITLENAMEOPERATIONBACHECAPAGE="titlenameOperationBachecaPage";
public static final String SEARCHFIELDBACHECAPAGE="searchFieldBachecaPage";
public static final String AMOUNTPAYEDBACHECAPAGE="amountPayedBachecaPage";
public static final String FILTERLINKBACHECAPAGE="filterLinkBachecaPage";
public static final String DATEPAYEDBACHECAPAGE="datePayedBachecaPage";
public static final String HEADERBACHECAPAGE="headerBachecaPage";
public static final String MESSAGETABBACHECAPAGE="messageTabBachecaPage";
public static final String SUBTITLEDESCRIPTIONPAYEDBACHECAPAGE="subTitleDescriptionPayedBachecaPage";
public static final String BACKBUTTONBACHECAPAGE="backButtonBachecaPage";
public static final String TITOLODINAMICONOTIFICAAUTORIZZATIVA = "titoloDinamicoNotificaAutorizzativa";
public static final String ROWDATA = "rowContainerData";
public static final String ROWDESCRIPTION = "rowContainerDescription";
public static final String ROWTITLE = "rowContainerType";
public static final String CONTAINER = "containerResultBacheca";

	public BachecaPageMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

