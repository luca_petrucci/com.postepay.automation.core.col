package com.postepay.automation.core.ui.pages.rechargemypostepaypagestep4;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.BodyRechargeMyPostepaySummary;


public class RechargeMyPostepayPageStep4 extends Page {
	public static final String NAME="T042";


	public RechargeMyPostepayPageStep4(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(BodyRechargeMyPostepaySummary.NAME, UiObjectRepo.get().get(BodyRechargeMyPostepaySummary.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {

		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {

		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {

		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new RechargeMyPostepayPageStep4Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		return new RechargeMyPostepayPageStep4ManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {

		return null;
	}

	public void verifyHeaderPage(String inputTitle) throws Error {
		((RechargeMyPostepayPageStep4Manager)this.manager).verifyHeaderPage(inputTitle);
	}

	public void verifySummaryRecharge(String targetOperation, String nameInput, String cardTargetInput, String amountInput, String reasonInput) {
		((RechargeMyPostepayPageStep4Manager)this.manager).verifySummaryRecharge(targetOperation, nameInput, cardTargetInput, amountInput, reasonInput);
	}
	
	public void clickOnPayButton() {
		((RechargeMyPostepayPageStep4Manager)this.manager).clickOnPayButton();
	}
}

