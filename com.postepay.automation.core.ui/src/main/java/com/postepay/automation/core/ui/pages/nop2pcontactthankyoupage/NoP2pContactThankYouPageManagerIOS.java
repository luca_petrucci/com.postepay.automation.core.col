package com.postepay.automation.core.ui.pages.nop2pcontactthankyoupage;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.NoP2pContactThankYouPageMolecola;

import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class NoP2pContactThankYouPageManagerIOS extends NoP2pContactThankYouPageManager {

	public NoP2pContactThankYouPageManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyLayoutOfTkp() {
		
		Particle imag = (Particle) UiObjectRepo.get().get(NoP2pContactThankYouPageMolecola.IMAGENOP2PCONTACTTHANKYOU);
		Particle text = (Particle) UiObjectRepo.get().get(NoP2pContactThankYouPageMolecola.TEXTNOP2PCONTACTTHANKYOU);
		
		assertTrue(imag.getElement()!=null);
		assertTrue(text.getElement()!=null);
	}
	
	public void clickOnChiudiInvitaAmici()
	{
		Particle chiudi = (Particle) UiObjectRepo.get().get(NoP2pContactThankYouPageMolecola.CHIUDIINVITAAMICI);
		chiudi.getElement().click();
	}

}
