package com.postepay.automation.core.ui.pages.thelephonerechargepagestep3;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.ThankYouPageForTelephoneRecharge;
import com.postepay.automation.core.ui.molecules.BodyTelephoneRechargePageSummary;


public class ThelephoneRechargePageStep3 extends Page {
	public static final String NAME="T074";
	

	public ThelephoneRechargePageStep3(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
this.addToTemplate(BodyTelephoneRechargePageSummary.NAME, UiObjectRepo.get().get(BodyTelephoneRechargePageSummary.NAME), true);
this.addToTemplate(ThankYouPageForTelephoneRecharge.NAME, UiObjectRepo.get().get(ThankYouPageForTelephoneRecharge.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new ThelephoneRechargePageStep3Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new ThelephoneRechargePageStep3ManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeader(String inputTitle) {
		((ThelephoneRechargePageStep3Manager)this.manager).verifyHeader(inputTitle);
	}
	
	public void verifyLayoutPage() {
		((ThelephoneRechargePageStep3Manager)this.manager).verifyLayoutPage();
	}
	
	public void clickOnPay() {
		((ThelephoneRechargePageStep3Manager)this.manager).clickOnPay();
	}
		
	public void verifySummary(String iPayWith, String iNumberTel, String iOperatore, String iAmountToRecharge) {
		((ThelephoneRechargePageStep3Manager)this.manager).verifySummary(iPayWith, iNumberTel, iOperatore, iAmountToRecharge);
	}

	public void verifySummaryTypTelephone() {
		((ThelephoneRechargePageStep3Manager)this.manager).verifySummaryTypTelephone();
	}
}

