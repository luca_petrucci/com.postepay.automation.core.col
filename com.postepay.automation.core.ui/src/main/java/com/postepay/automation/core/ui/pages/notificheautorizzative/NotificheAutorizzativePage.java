package com.postepay.automation.core.ui.pages.notificheautorizzative;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.AbilitaInAppMolecola;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.NotificheAutorizzativeM265;
import com.postepay.automation.core.ui.verifytools.LayoutTools;


public class NotificheAutorizzativePage extends Page {
	public static final String NAME="T141";
	

	public NotificheAutorizzativePage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(NotificheAutorizzativeM265.NAME, UiObjectRepo.get().get(NotificheAutorizzativeM265.NAME), true);
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new NotificheAutorizzativePageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeaderPage(String inputTitle) throws Error {
		((NotificheAutorizzativePageManager)this.manager).verifyHeaderPage(inputTitle);
	}
	
	public void verifyLayout(String copy, String copyTarget, String btnAutorizza, String btnNega) {
		((NotificheAutorizzativePageManager)this.manager).verifyLayout(copy, copyTarget, btnAutorizza, btnNega);
		
	}
	
	public void clickOnAutorizza() {
		((NotificheAutorizzativePageManager)this.manager).clickOnAutorizza();
	}	
	
	public void clickOnNegaAutorizzazione() {
		((NotificheAutorizzativePageManager)this.manager).clickOnNegaAutorizzazione();
	}
	
	
}

