package com.postepay.automation.core.ui.pages.hamburgermenuhomepage;

import com.postepay.automation.core.ui.molecules.SectionsAppHamburgerMenu;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class HamburgerMenuHomePageManager extends PageManager {

	public HamburgerMenuHomePageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void clickOnSettings() {
		Particle btn=(Particle) UiObjectRepo.get().get(SectionsAppHamburgerMenu.SETTINGLINKHAMBURGERMENU);
		
		btn.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnMyPurchase() {
		Particle btn=(Particle) UiObjectRepo.get().get(SectionsAppHamburgerMenu.MYPURCHASELINKHAMBURGERMENU);
		
		btn.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnMyProfile() {
		Particle btn=(Particle) UiObjectRepo.get().get(SectionsAppHamburgerMenu.PROFILELINKHAMBURGERMENU);
		
		btn.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnLogout() {
		Particle btn=(Particle) UiObjectRepo.get().get(SectionsAppHamburgerMenu.LOGOUTHAMBURGERMENU);
		
		btn.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnAssistence() {
		Particle btn=(Particle) UiObjectRepo.get().get(SectionsAppHamburgerMenu.ASSISTANCELINKHAMBURGERMENU);
		
		btn.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
}

