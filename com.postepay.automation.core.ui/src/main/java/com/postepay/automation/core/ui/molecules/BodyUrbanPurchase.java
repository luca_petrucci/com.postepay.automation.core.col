package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyUrbanPurchase extends Molecule {
	public static final String NAME="M077";
	
	public static final String SEARCHCITYURBANPURCHASE="searchCityUrbanPurchase";
public static final String SELECTVECTORURBANPURCHASE="selectVectorUrbanPurchase";
public static final String SELECTCITYURBANPURCHASE="selectCityUrbanPurchase";
public static final String CITYNAMEURBANPURCHASE="cityNameUrbanPurchase";
public static final String TITLEPAGEURBANPURCHASE="titlePageUrbanPurchase";


	public BodyUrbanPurchase(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

