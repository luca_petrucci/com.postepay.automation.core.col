package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyPageUrbanTicketPurchaseSummary extends Molecule {
	public static final String NAME="M082";
	
	public static final String OWNERURBANPURCHASE="ownerUrbanPurchase";
public static final String IMPORTURBANPURCHASE="importUrbanPurchase";
public static final String BUTTONPAYURBANPURCHASESUMMARY="buttonPayUrbanPurchaseSummary";
public static final String DATEURBANPURCHASE="dateUrbanPurchase";
public static final String TITLEPAGEURBANPURCHASESUMMARY="titlePageUrbanPurchaseSummary";
public static final String PAYMENTMODEURBANPURCHASE="paymentModeUrbanPurchase";
public static final String BUTTONCANCELURBANPURCHASE="buttonCancelUrbanPurchase";

public static final String BUTTONCAMBIATIPODIPAGAMENTO="buttonCambiaTipoDiPagamento";


	public BodyPageUrbanTicketPurchaseSummary(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

