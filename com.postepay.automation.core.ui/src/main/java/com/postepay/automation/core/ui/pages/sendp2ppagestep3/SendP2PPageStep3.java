package com.postepay.automation.core.ui.pages.sendp2ppagestep3;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.AmoutCreditToSend;


public class SendP2PPageStep3 extends Page {
	public static final String NAME="T010";


	public SendP2PPageStep3(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(AmoutCreditToSend.NAME, UiObjectRepo.get().get(AmoutCreditToSend.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {

		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {

		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {

		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SendP2PPageStep3Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {

		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {

		return null;
	}

	public void insetAmoutToSend(String amount) {

		((SendP2PPageStep3Manager)this.manager).insetAmoutToSend(amount);
	}

	public void clickOnConfirmButton() {

		((SendP2PPageStep3Manager)this.manager).clickOnConfirmButton();;
	}
}

