package com.postepay.automation.core.ui.pages.nop2pcontactthankyoupage;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.NoP2pContactThankYouPageMolecola;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class NoP2pContactThankYouPageManager extends PageManager {

	public NoP2pContactThankYouPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyLayoutOfTkp() {
		
		Particle imag = (Particle) UiObjectRepo.get().get(NoP2pContactThankYouPageMolecola.IMAGENOP2PCONTACTTHANKYOU);
		Particle text = (Particle) UiObjectRepo.get().get(NoP2pContactThankYouPageMolecola.TEXTNOP2PCONTACTTHANKYOU);
		
		assertTrue(imag.getElement().isDisplayed());
		assertTrue(text.getElement().isDisplayed());
	}
	
	public void clickOnInviteYouFriend() {

		Particle button = (Particle) UiObjectRepo.get().get(NoP2pContactThankYouPageMolecola.BUTTONNOP2PCONTACTTHANKYOU);
		
		button.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnBankButton() {

		Particle button = (Particle) UiObjectRepo.get().get(NoP2pContactThankYouPageMolecola.BACKNOP2PCONTACTTHANKYOU);
		
		button.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
}

