package com.postepay.automation.core.ui.pages.paymentpaymentpage;


import com.postepay.automation.core.ui.molecules.BodyPaymentPaymentSection;

import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class PaymentPaymentPageManagerIOS extends PaymentPaymentPageManager {

	public PaymentPaymentPageManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void clickOnPagaPA() {
		Particle btnPA=(Particle) UiObjectRepo.get().get(BodyPaymentPaymentSection.DINAMICTITLECARD);
		btnPA.getElement().click();
		WaitManager.get().waitShortTime();
	}
}
