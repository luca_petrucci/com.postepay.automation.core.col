package com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep6;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.ExtraurbaPurchaseTicketMolecola;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;


public class ExtraurbanPurchasePageStep6 extends Page {
	public static final String NAME="T097";
	

	public ExtraurbanPurchasePageStep6(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(ExtraurbaPurchaseTicketMolecola.NAME, UiObjectRepo.get().get(ExtraurbaPurchaseTicketMolecola.NAME), true);
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new ExtraurbanPurchasePageStep6Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderpage(String inputTitle) {
		((ExtraurbanPurchasePageStep6Manager)this.manager).verifyHeaderpage(inputTitle);
	}
	
	public void verifyLayoutPage() {
		((ExtraurbanPurchasePageStep6Manager)this.manager).verifyLayoutPage();
	}
	
	public void verifyDateOf() {
		((ExtraurbanPurchasePageStep6Manager)this.manager).verifyDateOf();
	}

	public void verifyPayWithCard(String cardInput) {
		((ExtraurbanPurchasePageStep6Manager)this.manager).verifyPayWithCard(cardInput);
	}
	
	public void clickOnPay() {
		((ExtraurbanPurchasePageStep6Manager)this.manager).clickOnPay();
	}
	
	public void clickOnAnnulla() {
		((ExtraurbanPurchasePageStep6Manager)this.manager).clickOnAnnulla();
	}
}

