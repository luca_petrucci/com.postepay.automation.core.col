package com.postepay.automation.core.ui.pages.creaposteid;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.CreaPosteIdMolecola;
import io.appium.java_client.android.AndroidDriver;


public class CreaPosteId extends Page {
	public static final String NAME="T128";
	

	public CreaPosteId(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(CreaPosteIdMolecola.NAME, UiObjectRepo.get().get(CreaPosteIdMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new CreaPosteIdManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderpage(String titlePageInput) {
		((CreaPosteIdManager)this.manager).verifyHeaderpage(titlePageInput);
	}

	public void verifyLayout() {
		((CreaPosteIdManager)this.manager).verifyLayout();
	}
	
	public void createPosteID(String posteID) {
		((CreaPosteIdManager)this.manager).createPosteID(posteID);
	}
	
	public void clickOnConferma() {
		((CreaPosteIdManager)this.manager).clickOnConferma();
	}
}

