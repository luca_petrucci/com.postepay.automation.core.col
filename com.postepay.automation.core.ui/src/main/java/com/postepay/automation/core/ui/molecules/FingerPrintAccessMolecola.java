package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class FingerPrintAccessMolecola extends Molecule {
	public static final String NAME="M213";

	public static final String FINGERPRINTACCESSDESCRIPTION="fingerPrintAccessDescription";
	public static final String FINGERPRINTACCESSBUTTON="fingerPrintAccessButton";
	public static final String FINGERPRINTACCESSTITLE="fingerPrintAccessTitle";
	public static final String FINGERPRINTACCESSTEXT="fingerPrintAccessText";
	public static final String FINGERPRINTACCESSICON="fingerPrintAccessIcon";
	public static final String FINGERPRINTENABLEACCESSDESCRIPTION="fingerPrintEnableAccessDescription";
	public static final String FINGERPRINTACCESSSETTINGICON="fingerPrintAccessSettingIcon";
	public static final String FINGERPRINTACCESSSETTINGTEXT="fingerPrintAccessSettingText";


	public FingerPrintAccessMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

