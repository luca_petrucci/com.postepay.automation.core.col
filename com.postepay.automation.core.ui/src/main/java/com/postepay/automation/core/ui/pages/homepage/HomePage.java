package com.postepay.automation.core.ui.pages.homepage;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.SpeedOperationHomePage;

import io.appium.java_client.android.AndroidDriver;

import com.postepay.automation.core.ui.molecules.FooterHomePage;
import com.postepay.automation.core.ui.molecules.CardDetailHomePage;
import com.postepay.automation.core.ui.molecules.DiscoverMoreHomePage;
import com.postepay.automation.core.ui.molecules.FastOperationHomePage;


public class HomePage extends Page {
	public static final String NAME="T003";


	public HomePage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(FooterHomePage.NAME, UiObjectRepo.get().get(FooterHomePage.NAME), true);
		this.addToTemplate(CardDetailHomePage.NAME, UiObjectRepo.get().get(CardDetailHomePage.NAME), true);
		this.addToTemplate(DiscoverMoreHomePage.NAME, UiObjectRepo.get().get(DiscoverMoreHomePage.NAME), false);
		this.addToTemplate(FastOperationHomePage.NAME, UiObjectRepo.get().get(FastOperationHomePage.NAME), true);
		this.addToTemplate(SpeedOperationHomePage.NAME, UiObjectRepo.get().get(SpeedOperationHomePage.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {

		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {

		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {

		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new HomePageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {

		return new HomePageManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {

		return null;
	}

	public void clickOnCard() {
		((HomePageManager)this.manager).clickOnCard();
	}

	public void clickOnHamburgerMenu() {
		((HomePageManager)this.manager).clickOnHamburgerMenu();
	}

	public void clickAllMessageNumber() {
		((HomePageManager)this.manager).clickAllMessageNumber();
	}

	public void clickOnMenuTabIcon() {
		((HomePageManager)this.manager).clickOnMenuTabIcon();
	}

	public void clickOnProductsTabIcon() {
		((HomePageManager)this.manager).clickOnProductsTabIcon();
	}

	public void clickOnCommunityTabIcon() {
		((HomePageManager)this.manager).clickOnCommunityTabIcon();
	}

	public void clickOnPaymentTabIcon() {
		((HomePageManager)this.manager).clickOnPaymentTabIcon();
	}

	public void clickOnMapsTabIcon() {
		((HomePageManager)this.manager).clickOnMapsTabIcon();
	}
	
	public void isP2PDisplayed() {
		((HomePageManager)this.manager).isP2PDisplayed();
	}
	
	public void clickOnSentP2PSpeedOperation() {
		((HomePageManager)this.manager).clickOnSentP2PSpeedOperation();
	}
	public void verifyHeaderLogoHomepage() {
		((HomePageManager)this.manager).verifyHeaderLogoHomepage();
	}
	
	public void scrollSbrodolo(WebDriver driver) {
		((HomePageManager)this.manager).scrollSbrodolo(driver);
	}
	
	public void scrollToSalvadanaio(WebDriver driver, double discrepanza) {
		((HomePageManager)this.manager).scrollToSalvadanaio(driver, discrepanza);	
	}
	
	public void clickOnBachecaIcon() {
		((HomePageManager)this.manager).clickOnBachecaIcon();
	}
	
	public void scrollToDawn(String targetTitleString) {
		((HomePageManager)this.manager).scrollToDawn(targetTitleString);
	}
	
	public void swithCardInEvdenza(String targetTitleString) {
		((HomePageManager)this.manager).swithCardInEvdenza(targetTitleString);
	}
}

