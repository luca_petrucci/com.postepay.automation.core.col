package com.postepay.automation.core.ui.pages.mapspaywithqrcode;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.BodyMapsGenericScreenFromHomePage;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.QrPayMolecola;
import com.postepay.automation.core.ui.molecules.QrPaySummaryMolecola;

import io.appium.java_client.AppiumDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.SoftAssertion;

public class MapsPayWithQRCodeManager extends PageManager {

	Particle insertAmount = (Particle) UiObjectRepo.get().get(QrPayMolecola.QRPAYAMOUNT);
	Particle insertPayButton = (Particle) UiObjectRepo.get().get(QrPayMolecola.QRPAYCONFIRMBUTTON);
	Particle insertDescription = (Particle) UiObjectRepo.get().get(QrPayMolecola.QRPAYDESDCRIZIONE);
	Particle insertTextAlert = (Particle) UiObjectRepo.get().get(QrPayMolecola.QRPAYTEXTALLERT);

	Particle insertXButton = (Particle) UiObjectRepo.get().get(HeaderGenericAllPage.RIGHTBUTTONGENERICICON);
	Particle insertBackButton = (Particle) UiObjectRepo.get().get(HeaderGenericAllPage.LEFTBUTTONGENERICICON);
	
	Particle summaryDescription = (Particle) UiObjectRepo.get().get(QrPaySummaryMolecola.QRPAYSUMMARYDESCRIPTION);
	Particle summaryActuaBilance = (Particle) UiObjectRepo.get().get(QrPaySummaryMolecola.QRPAYSUMMARYACTUALBILANCE);
	Particle summaryRequestDate = (Particle) UiObjectRepo.get().get(QrPaySummaryMolecola.QRPAYSUMMARYREQUESTDATE);
	Particle summaryMerchantAddress = (Particle) UiObjectRepo.get().get(QrPaySummaryMolecola.QRPAYSUMMARYMERCHANT);
	Particle summaryAmount = (Particle) UiObjectRepo.get().get(QrPaySummaryMolecola.QRPAYSUMMARYAMOUNT);
	Particle summaryPagaButton = (Particle) UiObjectRepo.get().get(QrPaySummaryMolecola.QRPAYSUMMARYBTNPAY);
	Particle summaryAnnullaButton = (Particle) UiObjectRepo.get().get(QrPaySummaryMolecola.QRPAYSUMMARYBTNANNUL);
	Particle summaryChangeCardButton = (Particle) UiObjectRepo.get().get(QrPaySummaryMolecola.QRPAYSUMMARYCHANGECARD);

	public MapsPayWithQRCodeManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyLayout() {
		SoftAssertion.get().getAssertions().assertThat(insertAmount.getElement().isDisplayed()).withFailMessage("Insert Amount non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(insertPayButton.getElement().isDisplayed()).withFailMessage("Pay BTN non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(insertDescription.getElement().isDisplayed()).withFailMessage("Insert Description non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(insertTextAlert.getElement().isDisplayed()).withFailMessage("Text Alert non visibile").isEqualTo(true);

//		assertTrue(insertAmount.getElement().isDisplayed());
//		assertTrue(insertPayButton.getElement().isDisplayed());
//		assertTrue(insertDescription.getElement().isDisplayed());
//		assertTrue(insertTextAlert.getElement().isDisplayed());
	}

	public void sendAmount(String amount) {
		insertAmount.getElement().sendKeys(amount);
		((AppiumDriver<?>)page.getDriver()).hideKeyboard();
		WaitManager.get().waitShortTime();
	}

	public void clickOnContinua() {
		insertPayButton.getElement().click();
		WaitManager.get().waitShortTime();
	}
	
	public void clickOnX() {
		insertXButton.getElement().click();
		WaitManager.get().waitShortTime();
	}

	public void verifyLayoutSummary() {
//		SoftAssertion.get().getAssertions().assertThat(summaryDescription.getElement().isDisplayed()).withFailMessage("Descrizione non visibile").isEqualTo(true);
//		SoftAssertion.get().getAssertions().assertThat(summaryActuaBilance.getElement().isDisplayed()).withFailMessage("Bilancio attuale non visibile").isEqualTo(true);
//		SoftAssertion.get().getAssertions().assertThat(summaryRequestDate.getElement().isDisplayed()).withFailMessage("Request Date non visibile").isEqualTo(true);
//		SoftAssertion.get().getAssertions().assertThat(summaryMerchantAddress.getElement().isDisplayed()).withFailMessage("Merchant Address non visibile").isEqualTo(true);
//		SoftAssertion.get().getAssertions().assertThat(summaryAmount.getElement().isDisplayed()).withFailMessage("Amount non visibile").isEqualTo(true);
//		SoftAssertion.get().getAssertions().assertThat(summaryPagaButton.getElement().isDisplayed()).withFailMessage("Paga BTN non visibile").isEqualTo(true);
//		SoftAssertion.get().getAssertions().assertThat(summaryAnnullaButton.getElement().isDisplayed()).withFailMessage("Annulla BTN non visibile").isEqualTo(true);

		assertTrue(summaryDescription.getElement().isDisplayed());
		assertTrue(summaryActuaBilance.getElement().isDisplayed());
		assertTrue(summaryRequestDate.getElement().isDisplayed());
		assertTrue(summaryMerchantAddress.getElement().isDisplayed());
		assertTrue(summaryAmount.getElement().isDisplayed());
		assertTrue(summaryPagaButton.getElement().isDisplayed());
		assertTrue(summaryAnnullaButton.getElement().isDisplayed());
	}
	
	public void verifySummaryData(String usedCard, String merchantAddress, String amount) {
		String aUsedCard = "Postepay " + usedCard;
		String eUsedCard = summaryActuaBilance.getElement().getText();
		
		String aMerchantAddress = merchantAddress;
		String eMerchantAddress = summaryMerchantAddress.getElement().getText();
		
		String aAmount = "€ " + amount;
		String eAmount = summaryAmount.getElement().getText();

		System.out.println("AR: "+ aUsedCard +"\tER: "+ eUsedCard);
		System.out.println("AR: "+ aMerchantAddress +"\tER: "+ eMerchantAddress);
		System.out.println("AR: "+ aAmount +"\tER: "+ eAmount);
		
//		assertTrue(aUsedCard.equals(eUsedCard));
//		assertTrue(aMerchantAddress.equals(eMerchantAddress));
		assertTrue(aAmount.equals(eAmount));
	}
	
	public void clickOnPaga() {
		summaryPagaButton.getElement().click();
		WaitManager.get().waitShortTime();
	}

	public void clickOnBackArrow() {
		insertBackButton.getElement().click();
		WaitManager.get().waitShortTime();
	}
	
	public void clickOnAnnulla() {
		summaryAnnullaButton.getElement().click();
		WaitManager.get().waitShortTime();
	}
	
	public void verifyChangeCard(String userCardNumber) {
		if (userCardNumber.equals("1")) {
			assertFalse(summaryChangeCardButton.getElement().isDisplayed());
		} else {
			assertTrue(summaryChangeCardButton.getElement().isDisplayed());
		}
	}
}

