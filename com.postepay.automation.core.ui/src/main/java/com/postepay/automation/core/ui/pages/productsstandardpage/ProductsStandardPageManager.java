package com.postepay.automation.core.ui.pages.productsstandardpage;

import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.CardDetailHomePage;
import com.postepay.automation.core.ui.molecules.DettaglioMovimentiCard;
import com.postepay.automation.core.ui.molecules.GenericOverviewsCardDetails;
import com.postepay.automation.core.ui.molecules.StandardCardFilterAndMoviment;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.SoftAssertion;

public class ProductsStandardPageManager extends PageManager {

	Particle cardAmount = (Particle) UiObjectRepo.get().get(GenericOverviewsCardDetails.AMOUNTOFCARDCASH);
	Particle cardStar = (Particle) UiObjectRepo.get().get(GenericOverviewsCardDetails.PREFEEREDCARDDETAILS);
	Particle cardNumber = (Particle) UiObjectRepo.get().get(GenericOverviewsCardDetails.NUMBERCARDDETAILS);
	Particle cardImpostazioni = (Particle) UiObjectRepo.get().get(GenericOverviewsCardDetails.SIMICONCARDDETAILS);
	Particle searchBar = (Particle) UiObjectRepo.get().get(StandardCardFilterAndMoviment.SPENDINGSERCHFIELDSTANDARD);
	Particle contabilizzate = (Particle) UiObjectRepo.get().get(StandardCardFilterAndMoviment.SPENDINGOUTRECORDSSTANDARD);
	Particle movimentiTitle = (Particle) UiObjectRepo.get().get(StandardCardFilterAndMoviment.OPERATIONTITLE);
	Particle movimentiDescription = (Particle) UiObjectRepo.get().get(StandardCardFilterAndMoviment.OPERATIONDESCRIPTION);
	Particle movimentiAmount = (Particle) UiObjectRepo.get().get(StandardCardFilterAndMoviment.OPERATIONAMOUNT);
	Particle movimentiDate = (Particle) UiObjectRepo.get().get(StandardCardFilterAndMoviment.OPERATIONDATE);
	Particle card = (Particle) UiObjectRepo.get().get(GenericOverviewsCardDetails.FRAMELAYOUTOFCARD);
	
	
	public ProductsStandardPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void clickOnCard() {
		
		WaitManager.get().waitShortTime();
		System.out.println("CLICK SU CARD");
		card.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
	
	public void verifyPresenceOfG2GCard() {
		Particle g2gLabel = (Particle) UiObjectRepo.get().get(StandardCardFilterAndMoviment.MANAGEG2G);
		assertTrue(g2gLabel.getElement() != null);
	}
	
	public void verifyLayoutPage() {

		SoftAssertion.get().getAssertions().assertThat(cardAmount.getElement().isDisplayed()).withFailMessage("Amount non visibile").isEqualTo(true);
//		SoftAssertion.get().getAssertions().assertThat(cardStar.getElement().isDisplayed()).withFailMessage("Star non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(cardNumber.getElement().isDisplayed()).withFailMessage("Card Number non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(cardImpostazioni.getElement().isDisplayed()).withFailMessage("Impostazioni non visibile").isEqualTo(true);
//		SoftAssertion.get().getAssertions().assertThat(searchBar.getElement().isDisplayed()).withFailMessage("Searchbar non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(contabilizzate.getElement().isDisplayed()).withFailMessage("Contabilizzate non visibile").isEqualTo(true);
//		SoftAssertion.get().getAssertions().assertThat(movimentiTitle.getElement().isDisplayed()).withFailMessage("Movimenti Title non visibile").isEqualTo(true);
//		SoftAssertion.get().getAssertions().assertThat(movimentiDescription.getElement().isDisplayed()).withFailMessage("Movimenti Description non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(movimentiAmount.getElement().isDisplayed()).withFailMessage("Movimenti Amount non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(movimentiDate.getElement().isDisplayed()).withFailMessage("Movimenti Date non visibile").isEqualTo(true);
		
		
//		assertTrue(cardAmount.getElement().isDisplayed());
//		assertTrue(cardStar.getElement().isDisplayed());
//		assertTrue(cardNumber.getElement().isDisplayed());
//		assertTrue(cardImpostazioni.getElement().isDisplayed());
		assertTrue(searchBar.getElement().isDisplayed());
//		assertTrue(contabilizzate.getElement().isDisplayed());
		assertTrue(movimentiTitle.getElement().isDisplayed());
		assertTrue(movimentiDescription.getElement().isDisplayed());
//		assertTrue(movimentiAmount.getElement().isDisplayed());
//		assertTrue(movimentiDate.getElement().isDisplayed());
	}
	
	public void verifyHeader(String inputTitle) {
		
		LayoutTools tool=new LayoutTools();
		tool.verifyHeaderPage(inputTitle);
	}
	
	public void clickOnG2GLabel() {
		Particle g2gLabel=null;
		boolean find=false;
		for(int i=0; i<2; i++) {
			try {
				g2gLabel = (Particle) UiObjectRepo.get().get(StandardCardFilterAndMoviment.MANAGEG2G);
				g2gLabel.getElement().click();
				find=true;
			} catch (Exception e) {
				System.out.println("Card non visibile - Swipe in corso");
				find=false;
			}
			if(find==false) {
			UIUtils.mobile().swipe((MobileDriver) page.getDriver(), SCROLL_DIRECTION.RIGHT, 300);
			}
		}
		try {Thread.sleep(1000);} catch (Exception err )  {}
		
		g2gLabel.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void searchMoviment(String movimento) {
		
		searchBar.getElement().sendKeys(movimento);
		WaitManager.get().waitMediumTime();
	}
	
	public void clickPrimoMovimento(String titoloMovimento, String importoMovimento)
	{
		List<WebElement> listaTitoli = page.getDriver().findElements(movimentiTitle.getXPath());
		List<WebElement> listaImporti = page.getDriver().findElements(movimentiAmount.getXPath());
		for(int i=0; i<listaTitoli.size(); i++)
		{
			WebElement titolo = listaTitoli.get(i);
			WebElement importo = listaImporti.get(i);
			System.out.println(titolo.getText()+ " " + importo.getText());
			if((titolo.getText().contains(titoloMovimento)) && (importo.getText().contains(importoMovimento)))
			{
				System.out.println("CLICK");
				titolo.click();
				WaitManager.get().waitMediumTime();
				break;
			}
		}
	}
	
	public void verifyDettaglioMovimenti(String titoloMovimento, String importoMovimento, String dataMovimentoValuta, String dataMovimentoContabile, String cardPostepay, String copyCardPostepay)
	{
		Particle titolo = (Particle) UiObjectRepo.get().get(DettaglioMovimentiCard.MOVIMENTOTITLE);
		Particle importo = (Particle) UiObjectRepo.get().get(DettaglioMovimentiCard.IMPORTO);
		Particle menu = (Particle) UiObjectRepo.get().get(DettaglioMovimentiCard.TREPUNTINIMENU);
		Particle valutaLabel = (Particle) UiObjectRepo.get().get(DettaglioMovimentiCard.DATAVALUTALABEL);
		Particle contabileLabel = (Particle) UiObjectRepo.get().get(DettaglioMovimentiCard.DATACONTABILELABEL);
		Particle valutaText = (Particle) UiObjectRepo.get().get(DettaglioMovimentiCard.DATAVALUTATEXT);
		Particle contabileText = (Particle) UiObjectRepo.get().get(DettaglioMovimentiCard.DATACONTABILETEXT);
		Particle card = (Particle) UiObjectRepo.get().get(DettaglioMovimentiCard.CARDPOSTEPAY);
		System.out.println("AR: "+titolo.getElement().getText()+" ER: "+titoloMovimento);
		assertTrue(titolo.getElement().getText().contains(titoloMovimento));
		assertTrue(importo.getElement().getText().contains(importoMovimento));
		assertTrue(menu.getElement().isDisplayed());
		assertTrue(valutaLabel.getElement().isDisplayed());
		assertTrue(contabileLabel.getElement().isDisplayed());
		System.out.println("AR: "+valutaText.getElement().getText()+" ER: "+dataMovimentoValuta);
		assertTrue(valutaText.getElement().getText().contains(dataMovimentoValuta));
		assertTrue(contabileText.getElement().getText().contains(dataMovimentoContabile));
		System.out.println("AR: "+ card.getElement().getText()+ " ER: "+cardPostepay);
		assertTrue(card.getElement().getText().contains(cardPostepay));
		System.out.println("AR: "+ card.getElement().getText()+ " ER: "+copyCardPostepay);
		assertTrue(card.getElement().getText().contains(copyCardPostepay));
	}
}

