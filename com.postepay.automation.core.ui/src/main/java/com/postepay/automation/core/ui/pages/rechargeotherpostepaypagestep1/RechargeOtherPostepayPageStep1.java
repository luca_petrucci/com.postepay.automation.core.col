package com.postepay.automation.core.ui.pages.rechargeotherpostepaypagestep1;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;

import io.appium.java_client.android.AndroidDriver;

import com.postepay.automation.core.ui.molecules.BodyRechargeOtherPostepay;


public class RechargeOtherPostepayPageStep1 extends Page {
	public static final String NAME="T045";


	public RechargeOtherPostepayPageStep1(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(BodyRechargeOtherPostepay.NAME, UiObjectRepo.get().get(BodyRechargeOtherPostepay.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {

		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {

		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {

		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new RechargeOtherPostepayPageStep1Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {

		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {

		return null;
	}

	public void verifyHeaderpage(String titlePageInput) {
		((RechargeOtherPostepayPageStep1Manager)this.manager).verifyHeaderpage(titlePageInput);
	}
	
	public void verifyImageCard(WebDriver driver, double discrepanza) {
		((RechargeOtherPostepayPageStep1Manager)this.manager).verifyImageCard(driver, discrepanza);
	}
	
	public void clickOnContactNumberMenu() {
		((RechargeOtherPostepayPageStep1Manager)this.manager).clickOnContactNumberMenu();
	}
	
	public void insertCardNumberTargetUser(String value) {
		((RechargeOtherPostepayPageStep1Manager)this.manager).insertCardNumberTargetUser(value);
	}
	
	public void insertNameTargetUser(String value) {
		((RechargeOtherPostepayPageStep1Manager)this.manager).insertNameTargetUser(value);
	}
	
	public void insertAmountToSendTargetUser(String value) {
		((RechargeOtherPostepayPageStep1Manager)this.manager).insertAmountToSendTargetUser(value);
	}
	
	public void insertReasonOfRecharge(String value) {
		((RechargeOtherPostepayPageStep1Manager)this.manager).insertReasonOfRecharge(value);
	}
	
	public void clickOnToggleAutomatickRecharge() {
		((RechargeOtherPostepayPageStep1Manager)this.manager).clickOnToggleAutomatickRecharge();
	}
	
	public void clickOnSaveContact() {
		((RechargeOtherPostepayPageStep1Manager)this.manager).clickOnSaveContact();
	}
	
	public void clickOnProceedButton() {
		((RechargeOtherPostepayPageStep1Manager)this.manager).clickOnProceedButton();
	}
	
	public void compileAllStandardForm(String valueCardTarget, String valueTargetUser, String valueAmount, String valueReason) {
		((RechargeOtherPostepayPageStep1Manager)this.manager).compileAllStandardForm(valueCardTarget, valueTargetUser, valueAmount, valueReason);
	}
	public void verifyDestinatarioAndCardNumber(String valueStatus, String targetOperation, String nameTarget, String cardNumberInput) {
		((RechargeOtherPostepayPageStep1Manager)this.manager).verifyDestinatarioAndCardNumber(valueStatus, targetOperation, nameTarget, cardNumberInput);
	}
	public void verifyTitle(WebDriver driver, String titleInput) {
		((RechargeOtherPostepayPageStep1Manager)this.manager).verifyTitle(driver, titleInput);
	}
}

