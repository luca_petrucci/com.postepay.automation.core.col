package com.postepay.automation.core.ui.pages.thelephonerechargepagestep1;

import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.appium.java_client.MobileBy;
import test.automation.core.UIUtils;
import ui.core.support.page.Page;
import ui.core.support.waitutil.WaitManager;

public class ThelephoneRechargePageStep1ManagerIOS extends ThelephoneRechargePageStep1Manager {

	private By pickers = MobileBy.className("XCUIElementTypePickerWheel");;

	public ThelephoneRechargePageStep1ManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void sendAmount(String amount)
	{
		fieldImporto.getElement().click();
		List<WebElement> pickerEls = UIUtils.ui().waitForCondition(page.getDriver(),ExpectedConditions.presenceOfAllElementsLocatedBy(pickers));
	     
        System.out.println(pickerEls.size());
        pickerEls.get(0).sendKeys(amount);
	}
	
	public void sendTelephoneNumber(String telephone, String adbExe) {
		fieldNumber.getElement().click();
		WaitManager.get().waitHighTime();

		fieldNumber.getElement().sendKeys(telephone);
		WaitManager.get().waitLongTime();
	}
}
