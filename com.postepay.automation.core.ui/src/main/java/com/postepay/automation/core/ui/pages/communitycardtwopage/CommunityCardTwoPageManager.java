package com.postepay.automation.core.ui.pages.communitycardtwopage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.CardsOfG2gCommunity;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import ui.core.support.waitutil.WaitManager;

import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class CommunityCardTwoPageManager extends PageManager {

	public CommunityCardTwoPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void clickOnSendG2G(WebDriver driver) {

		Particle sendP2p=(Particle) UiObjectRepo.get().get(CardsOfG2gCommunity.SENDG2GCOMMUNITY);
		String cadrG2GText = "Invia giga a SI...";
		String cadrG2GTextEsteso = "Invia giga a SIM PosteMobile";
		LayoutTools tool = new LayoutTools();
		
		WebElement newCard = null;
		
		try {
			newCard = tool.replaceGenericPathOfElement(driver, sendP2p, cadrG2GText);
		} catch (Exception e) {
			// TODO: handle exception
		}
		if (newCard == null ) {
			newCard = tool.replaceGenericPathOfElement(driver, sendP2p, cadrG2GTextEsteso);
		}
		newCard.click();
		
		WaitManager.get().waitMediumTime();
	}
}

