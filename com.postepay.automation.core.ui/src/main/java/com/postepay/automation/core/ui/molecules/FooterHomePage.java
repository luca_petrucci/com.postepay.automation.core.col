package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class FooterHomePage extends Molecule {
	public static final String NAME="M008";
	
	public static final String NAVIGATIONMENUHOMEVALORIZED="navigationMenuHomeValorized";
public static final String NAVIGATIONMENUHOMEMAPS="navigationMenuHomeMaps";
public static final String NAVIGATIONMENUHOMEPRODUCT="navigationMenuHomeProduct";
public static final String NAVIGATIONMENUHOMEPAYMENT="navigationMenuHomePayment";
public static final String NAVIGATIONMENUHOMECOMMUNITY="navigationMenuHomeCommunity";


	public FooterHomePage(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

