package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BolloAutoMotoCalcoloM262 extends Molecule {
	public static final String NAME="M262";
	
	public static final String ANNUALITAPREGRESSAM262="annualitaPregressaM262";
public static final String REGIONESELECTM262="regioneSelectM262";
public static final String REGIONEM262="regioneM262";
public static final String VEICOLOSELECTM262="veicoloSelectM262";
public static final String TARGAM262="targaM262";
public static final String COPYTEXTM262="copyTextM262";
public static final String VEICOLOM262="veicoloM262";
public static final String CALCOLABTNM262="calcolaBtnM262";


	public BolloAutoMotoCalcoloM262(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

