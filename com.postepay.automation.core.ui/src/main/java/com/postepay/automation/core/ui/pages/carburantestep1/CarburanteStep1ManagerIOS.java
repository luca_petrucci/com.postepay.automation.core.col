package com.postepay.automation.core.ui.pages.carburantestep1;

import static org.junit.Assert.assertTrue;

import ui.core.support.page.Page;
import ui.core.support.waitutil.WaitManager;

public class CarburanteStep1ManagerIOS extends CarburanteStep1Manager {

	public CarburanteStep1ManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public int getAmount() {
		String actualAmount = amount.getElement().getText().replace("€", "");
		int startAmount = toolNumber.convertStringToInt(actualAmount);
		return startAmount;
	}

	public void verifyOtherCardBehavior(int importoCard) {
		if (importoCard == 10) {
//			assertTrue(amount10.getElement().getAttribute("checked").equals("false"));
			amount10.getElement().click();
			WaitManager.get().waitShortTime();
			isCheckedAmountCard(10);
			
		} else if (importoCard == 50) {
//			assertTrue(amount50.getElement().getAttribute("checked").equals("false"));
			amount50.getElement().click();
			WaitManager.get().waitShortTime();
			isCheckedAmountCard(50);
			
		} else if (importoCard == 80) {
//			assertTrue(amount80.getElement().getAttribute("checked").equals("false"));
			amount80.getElement().click();
			WaitManager.get().waitShortTime();
			isCheckedAmountCard(80);
		}
	}
	
public void isCheckedAmountCard(int importoCard) {
	}
}
