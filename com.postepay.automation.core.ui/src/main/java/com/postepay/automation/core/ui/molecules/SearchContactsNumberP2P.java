package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SearchContactsNumberP2P extends Molecule {
	public static final String NAME="M023";

	public static final String SEARCHP2PCONTACT="searchP2PContact";
	public static final String TARGETIDP2PCONTACT="targetIdP2PContact";
	public static final String TABCONTACTP2PSEND="tabContactP2PSend";
	public static final String TABCONTACTSEND="tabContactSend";
	public static final String TARGETIDP2PCONTACTDINAMIC="targetIdP2PContactDinamic";

	public SearchContactsNumberP2P(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

