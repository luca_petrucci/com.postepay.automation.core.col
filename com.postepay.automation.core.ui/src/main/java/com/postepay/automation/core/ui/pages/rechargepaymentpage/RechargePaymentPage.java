package com.postepay.automation.core.ui.pages.rechargepaymentpage;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.TabsPaymentSection;

import io.appium.java_client.android.AndroidDriver;

import com.postepay.automation.core.ui.molecules.BodyRechargePaymentSection;
import com.postepay.automation.core.ui.molecules.FooterHomePage;


public class RechargePaymentPage extends Page {
	public static final String NAME="T026";


	public RechargePaymentPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(TabsPaymentSection.NAME, UiObjectRepo.get().get(TabsPaymentSection.NAME), true);
		this.addToTemplate(BodyRechargePaymentSection.NAME, UiObjectRepo.get().get(BodyRechargePaymentSection.NAME), true);
		this.addToTemplate(FooterHomePage.NAME, UiObjectRepo.get().get(FooterHomePage.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {

		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {

		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {

		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new RechargePaymentPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {

		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {

		return null;
	}

	public void clickOnTabRecharge() {

		((RechargePaymentPageManager)this.manager).clickOnTabRecharge();

	}

	public void clickOnTabPayment() {

		((RechargePaymentPageManager)this.manager).clickOnTabPayment();

	}

	public void clickOnTabService() {

		((RechargePaymentPageManager)this.manager).clickOnTabService();

	}
	
	public void clickOnRechargeOtherPostePay() {
		((RechargePaymentPageManager)this.manager).clickOnRechargeOtherPostePay();
	}

	public void clickOnRechargeTelephonePosteSim() {
		((RechargePaymentPageManager)this.manager).clickOnRechargeTelephonePosteSim();
		
	}

	public void clickOnRechargeTelephoneVodafone() {
		((RechargePaymentPageManager)this.manager).clickOnRechargeTelephoneVodafone();
		
	}

	public void clickOnSendP2p() {
		((RechargePaymentPageManager)this.manager).clickOnSendP2p();
		
	}

	public void clickOnRechargeMyPostePay() {
		((RechargePaymentPageManager)this.manager).clickOnRechargeMyPostePay();
		
	}
	
	public void clickOnCardAutomaticRechargeSection() {
		((RechargePaymentPageManager)this.manager).clickOnCardAutomaticRechargeSection();

	}
	
	public void verifyHeaderpage(String titlePageInput) {
		((RechargePaymentPageManager)this.manager).verifyHeaderpage(titlePageInput);
	}

	public void scrollToOtherSIM(WebDriver driver) {
		((RechargePaymentPageManager)this.manager).scrollToOtherSIM(driver);
	}
	
	public void clickOnRechargePostePay() {
		((RechargePaymentPageManager)this.manager).clickOnRechargePostePay();
	}
}

