package com.postepay.automation.core.ui.pages.salvadanaioversasuobiettivoricorrente;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;

import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;
import ui.core.support.page.Page;
import ui.core.support.waitutil.WaitManager;

public class SalvadanaioVersaSuObiettivoRicorrenteManagerIOS extends SalvadanaioVersaSuObiettivoRicorrenteManager {

	public SalvadanaioVersaSuObiettivoRicorrenteManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

public void verifyLayoutDinamicoAlRaggiungimento(WebDriver driver, String typeMessage, String importo, double discrepanza) {
	
	System.out.println("AMOUNT : " + page.getDriver().getPageSource());
		
		if (typeMessage.equals("2settimane")) {
			clickOnPlus(1);
			String amount = versamentoAmount.getElement().getText();
			assertTrue(amount.equals("5,00") );
			try {
				ricorrenteTermineVersamentoLabel.getElement().click();
//				assertTrue(raggiungeraiCardMessaggio2Settimana.getElement().isDisplayed());
			} catch(Exception e)
			{
				//TODO
			}
		} else if (typeMessage.equals("1giorno")) {
			clickOnPlus(10);
			String amount = versamentoAmount.getElement().getText();
			assertTrue(amount.equals("50,00") );
			try {
				ricorrenteTermineVersamentoLabel.getElement().click();
//				assertTrue(raggiungeraiCardMessaggio2SettimanaEntroLaScadenza.getElement().isDisplayed());
			} catch(Exception e)
			{
				//TODO
			}
		} else if (typeMessage.equals("oltre")) {
			sendAmountValue(importo);
			String amount = versamentoAmount.getElement().getText();
			assertTrue(amount.equals("0,10") );
			try {
				ricorrenteTermineVersamentoLabel.getElement().click();
//				assertTrue(raggiungeraiCardMessaggio2SettimanaDopoLaScadenza.getElement().isDisplayed());
			} catch(Exception e)
			{
				//TODO
			}
			
			try {
				scrollToElementDOWN(driver);
			} catch (Exception e) {
				// TODO: handle exception
			}
//			assertTrue(raggiungeraiMessaggioConsiglio.getElement().isDisplayed());
//			verifyImage(driver, "cassaforte_versa_ricorrente", discrepanza);
			
			WaitManager.get().waitShortTime();
			
			return;
		}
		
		try {
			scrollToElementDOWN(driver);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
//tolto		assertTrue(raggiungeraiMessaggioConsiglio.getElement().isDisplayed());
		verifyImage(driver, "cassaforte_versa_ricorrente", discrepanza);
		
		WaitManager.get().waitShortTime();
		
		try {
			scrollToElementUP(driver);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}	

public void verifyLayoutPageDefault(WebDriver driver, String nameInput, double discrepanza) {
	getElementName(driver, nameInput);
	assertTrue(versamentoTestoVersameto.getElement().isDisplayed());
	assertTrue(versamentoLimiteMassimo.getElement().isDisplayed());
	assertTrue(versamentoMeno.getElement()!=null);
	assertTrue(versamentoPiu.getElement()!=null);
	assertTrue(versamentoAmount.getElement().isDisplayed());
	assertTrue(versamentoRicorrenteLabel.getElement().isDisplayed());
	assertTrue(versamentoRicorrenteToggle.getElement().isDisplayed());
	UIUtils.ui().mobile().swipe((MobileDriver) page.getDriver(), SCROLL_DIRECTION.DOWN, 200);
	assertTrue(versamentoButton.getElement().isEnabled());
	
	assertTrue(ricorrenteFrequenzaLabel.getElement().isDisplayed());
	assertTrue(ricorrenteFrequenzaValue.getElement().getText().equals("Ogni giorno"));
	assertTrue(ricorrenteAPartireDaLabel.getElement().isDisplayed());
	assertTrue(ricorrenteAPartireDaValue.getElement().getText().equals("Oggi"));
	assertTrue(ricorrenteTermineVersamentoLabel.getElement().isDisplayed());
	assertTrue(ricorrenteTermineVersamentoLabelAlRaggiungimento.getElement().isDisplayed());
	assertTrue(ricorrenteTermineVersamentoLabelDataSpecifica.getElement().isDisplayed());
	assertTrue(ricorrenteDescrizioneUno.getElement().isDisplayed());
	assertTrue(ricorrenteDescrizioneDue.getElement().isDisplayed());
	
	verifyImage(driver, "toggle_enabled", discrepanza);
	verifyImage(driver, "bar_al_raggiungimento", discrepanza);
	verifyImage(driver, "matita_icon", discrepanza);
}
}
