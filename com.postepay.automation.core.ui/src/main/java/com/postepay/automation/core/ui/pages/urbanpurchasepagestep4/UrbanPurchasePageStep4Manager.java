package com.postepay.automation.core.ui.pages.urbanpurchasepagestep4;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.BodyPageUrbanTicketPurchaseSummary;
import com.postepay.automation.core.ui.molecules.ExtraurbaPurchaseTicketMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.CoreUtility;

public class UrbanPurchasePageStep4Manager extends PageManager {

	Particle amount = (Particle) UiObjectRepo.get().get(BodyPageUrbanTicketPurchaseSummary.IMPORTURBANPURCHASE);
	Particle date = (Particle) UiObjectRepo.get().get(BodyPageUrbanTicketPurchaseSummary.DATEURBANPURCHASE);
	Particle payWithCard = (Particle) UiObjectRepo.get().get(BodyPageUrbanTicketPurchaseSummary.PAYMENTMODEURBANPURCHASE);
	Particle btnAnnulla = (Particle) UiObjectRepo.get().get(BodyPageUrbanTicketPurchaseSummary.BUTTONCANCELURBANPURCHASE);
	Particle btnPaga = (Particle) UiObjectRepo.get().get(BodyPageUrbanTicketPurchaseSummary.BUTTONPAYURBANPURCHASESUMMARY);
	
	public UrbanPurchasePageStep4Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderpage(String inputTitle) {
		LayoutTools tool = new LayoutTools();
		tool.verifyPresenceHeaderPage(inputTitle);
	}
	
	public void verifyLayoutPage() {
		Particle title = (Particle) UiObjectRepo.get().get(BodyPageUrbanTicketPurchaseSummary.TITLEPAGEURBANPURCHASESUMMARY);
		Particle layoutPayWith = (Particle) UiObjectRepo.get().get(ExtraurbaPurchaseTicketMolecola.PAYWITHHEADEREXTRAURBAN);
		Particle cicero = (Particle) UiObjectRepo.get().get(BodyPageUrbanTicketPurchaseSummary.OWNERURBANPURCHASE);

		assertTrue(title.getElement().isDisplayed());
		assertTrue(date.getElement().isDisplayed());
		assertTrue(payWithCard.getElement().isDisplayed());
		assertTrue(btnAnnulla.getElement().isDisplayed());
		assertTrue(btnPaga.getElement().isDisplayed());
		assertTrue(layoutPayWith.getElement().isDisplayed());
		assertTrue(cicero.getElement().isDisplayed());
		assertTrue(amount.getElement().isDisplayed());
	}
	
	public void verifyDateOf() {
		StringAndNumberOperationTools tool = new StringAndNumberOperationTools();
		String toDay = tool.getDateExtraurban();
		
		assertTrue(date.getElement().getText().equals(toDay));
	}

	public void verifyPayWithCard(String cardInput) {
//		String cardIn = "Postepay Evolution" + cardInput;
		System.out.println("AR: "+ payWithCard.getElement().getText()+ " ER: "+ cardInput);
		assertTrue(">>>> ERRORE AR: "+ payWithCard.getElement().getText()+ " ER: "+ cardInput,
				payWithCard.getElement().getText().contains(cardInput));
	}
	
	public void clickOnPay() {
		btnPaga.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnAnnulla() {
		btnAnnulla.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public String getImporto() {
		return amount.getElement().getText();
	}
	
	public String getDate() {
		return date.getElement().getText();
	}

	public boolean isCurrentPayment(String cardNumber) 
	{
		Particle payWithCard = (Particle) UiObjectRepo.get().get(BodyPageUrbanTicketPurchaseSummary.PAYMENTMODEURBANPURCHASE);
		
		return payWithCard.getElement().getText().contains(cardNumber);
	}

	public void clickOnCambiaMetodoPagamento() {
		CoreUtility.visibilityOfElement(page.getDriver(), 
				(Particle) UiObjectRepo.get().get(BodyPageUrbanTicketPurchaseSummary.BUTTONCAMBIATIPODIPAGAMENTO), 10)
		.click();
	}
}

