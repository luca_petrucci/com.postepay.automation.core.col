package com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep2;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.molecules.BodyTravelSolution;


public class ExtraurbanPurchasePageStep2 extends Page {
	public static final String NAME="T061";
	

	public ExtraurbanPurchasePageStep2(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(BodyTravelSolution.NAME, UiObjectRepo.get().get(BodyTravelSolution.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new ExtraurbanPurchasePageStep2Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new ExtraurbanPurchasePageStep2ManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderpage(String inputTitle) {
		((ExtraurbanPurchasePageStep2Manager)this.manager).verifyHeaderpage(inputTitle);
	}
	
	public void verifyLayoutPage() {
		((ExtraurbanPurchasePageStep2Manager)this.manager).verifyLayoutPage();
	}
	
	public void clickOnChooseTicket() {
		((ExtraurbanPurchasePageStep2Manager)this.manager).clickOnChooseTicket();
	}
	
	public void verifyListOrderByPREZZO() {
		((ExtraurbanPurchasePageStep2Manager)this.manager).verifyListOrderByPREZZO();
	}
	
	public void selectVector(String vettore) {
		((ExtraurbanPurchasePageStep2Manager)this.manager).selectVector(vettore);
	}
	
	public void selectFilter(String filtro) {
		((ExtraurbanPurchasePageStep2Manager)this.manager).selectFilter(filtro);
	}
}

