package com.postepay.automation.core.ui.pages.loginwithposteid;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.EditTextNotIAmConfirmBtnOldPosteId;
import com.postepay.automation.core.ui.molecules.FingerPrintAccessMolecola;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.cmd.adb.AdbCommandPrompt;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class LoginWithPosteIdManager extends PageManager {

	Particle btnConferma=(Particle) UiObjectRepo.get().get(EditTextNotIAmConfirmBtnOldPosteId.CONFIRMBUTTONOLDPOSTEID);
	Particle btnCambiaAccount=(Particle) UiObjectRepo.get().get(EditTextNotIAmConfirmBtnOldPosteId.CHANGEACCOUNT);
	Particle editPosteId=(Particle) UiObjectRepo.get().get(EditTextNotIAmConfirmBtnOldPosteId.POSTEIDEDITTEXT);
	Particle helloDescription=(Particle) UiObjectRepo.get().get(EditTextNotIAmConfirmBtnOldPosteId.HELLODESCRIPTION);
	Particle helloTitle=(Particle) UiObjectRepo.get().get(EditTextNotIAmConfirmBtnOldPosteId.HELLOTITLE);
	Particle forgetPosteId=(Particle) UiObjectRepo.get().get(EditTextNotIAmConfirmBtnOldPosteId.LINKFORGETPOSTEID);
	
	public LoginWithPosteIdManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyLayout() {
		LayoutTools.isPresentKeyboard((AppiumDriver<?>)page.getDriver());	
		WaitManager.get().waitMediumTime();
		assertTrue(editPosteId.getElement() != null);
		assertTrue(helloDescription.getElement ()!= null);
		assertTrue(helloTitle.getElement() != null );
		assertTrue(forgetPosteId.getElement() != null);
		assertTrue(btnConferma.getElement() != null);
		assertTrue(btnCambiaAccount.getElement() != null);
	}
	
	public void insertPosteId(String posteId) {
		LayoutTools.isPresentKeyboard((AppiumDriver<?>)page.getDriver());	

		// Inserisci il PosteIDc
		editPosteId.getElement().sendKeys(posteId);
try {
	((AppiumDriver<?>)page.getDriver()).hideKeyboard();
} catch (Exception e) {
 System.out.println("Non l'ho veramente chiusa la tastiera");
 helloTitle.getElement().click();
}
		

	}

	public void clickOnConfirmButton() {
		// Click su Conferma
		btnConferma.getElement().click();
		WaitManager.get().waitLongTime();
	}

	public void verifyHeaderPage(String inputTitle) throws Error {

		LayoutTools layOutTool= new LayoutTools();
		layOutTool.verifyPresenceHeaderPage(inputTitle);

	}

	// Popup di inserimento finger print
	public void verifyLayoutPopupSendFingerprint() {
		Particle title = (Particle) UiObjectRepo.get().get(FingerPrintAccessMolecola.FINGERPRINTACCESSTITLE);
		Particle body = (Particle) UiObjectRepo.get().get(FingerPrintAccessMolecola.FINGERPRINTACCESSDESCRIPTION);
		Particle iconFinger = (Particle) UiObjectRepo.get().get(FingerPrintAccessMolecola.FINGERPRINTACCESSICON);
		Particle textFinger = (Particle) UiObjectRepo.get().get(FingerPrintAccessMolecola.FINGERPRINTACCESSTEXT);
		Particle disableBtn = (Particle) UiObjectRepo.get().get(FingerPrintAccessMolecola.FINGERPRINTACCESSBUTTON);

		assertTrue(title.getElement().isDisplayed());
		assertTrue(body.getElement().isDisplayed());
		assertTrue(iconFinger.getElement().isDisplayed());
		assertTrue(textFinger.getElement().isDisplayed());
		assertTrue(disableBtn.getElement().isDisplayed());
	}

	public void sendFingerprint(String adbExe, String deviceName, int fingerId) throws Exception {
		AdbCommandPrompt cmd=new AdbCommandPrompt(adbExe);
		cmd.setDeviceName(deviceName);
		cmd.fingerTouch(fingerId);
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnCambiaAccount() {
		btnCambiaAccount.getElement().click();
		WaitManager.get().waitLongTime();
	}
	
	public void verifyHelloName(String nameInput) {
		String tName = "Ciao " + nameInput + ",";
		String eName = helloTitle.getElement().getText();
		System.out.println("ER:\t" + tName + "\tAR:\t" + eName);
		assertTrue(eName.equals(tName));
	}
}

