package com.postepay.automation.core.ui.pages.ministerotrasportipage;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.AbilitaInAppMolecola;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.MinisteroTrasportiMolecola;
import com.postepay.automation.core.ui.pages.paymentpaymentpage.PaymentPaymentPageManager;


public class MinisteroTrasportiPage extends Page {
	public static final String NAME="T146";
	

	public MinisteroTrasportiPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(MinisteroTrasportiMolecola.NAME, UiObjectRepo.get().get(MinisteroTrasportiMolecola.NAME), true);
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new MinisteroTrasportiPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeaderpage(String titlePageInput)
	{
		((MinisteroTrasportiPageManager)this.manager).verifyHeaderpage(titlePageInput);
	}
	
	public void verifyLayout()
	{
		((MinisteroTrasportiPageManager)this.manager).verifyLayout();
	}
	
	public void inserisciTipologiaPratica()
	{
		((MinisteroTrasportiPageManager)this.manager).inserisciTipologiaPratica();
	}

	public void inserisciPratica()
	{
		((MinisteroTrasportiPageManager)this.manager).inserisciPratica();
	}
	
	public void clickOnCalcola()
	{
		((MinisteroTrasportiPageManager)this.manager).clickOnCalcola();
	}

	public void clickOnTrento() {
		((MinisteroTrasportiPageManager)this.manager).clickOnTrento();
	}
}

