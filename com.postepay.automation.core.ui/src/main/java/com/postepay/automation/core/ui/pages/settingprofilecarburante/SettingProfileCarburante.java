package com.postepay.automation.core.ui.pages.settingprofilecarburante;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.SettingProfileCarburanteMolecola;

import io.appium.java_client.android.AndroidDriver;


public class SettingProfileCarburante extends Page {
	public static final String NAME="T099";
	

	public SettingProfileCarburante(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), false);
		this.addToTemplate(SettingProfileCarburanteMolecola.NAME, UiObjectRepo.get().get(SettingProfileCarburanteMolecola.NAME), false);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SettingProfileCarburanteManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeaderpage(String arg) {
		((SettingProfileCarburanteManager)this.manager).verifyHeaderpage(arg);
	}
	
	public void verifyLayoutPage(String arg, WebDriver driver) {
		((SettingProfileCarburanteManager)this.manager).verifyLayoutPage(arg, driver);
	}
	
	public void checkIncrementoDecremento() {
		((SettingProfileCarburanteManager)this.manager).checkIncrementoDecremento();
	}
	
	public void setAmount(String inputSettingAmount) {
		((SettingProfileCarburanteManager)this.manager).setAmount(inputSettingAmount);
	}
	
	public void checkAddSubBtn() {
		((SettingProfileCarburanteManager)this.manager).checkAddSubBtn();
	}
}

