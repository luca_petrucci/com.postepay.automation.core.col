package com.postepay.automation.core.ui.pages.sendg2gpagestep2;

import com.postepay.automation.core.ui.molecules.SearchContactsNumberG2G;
import com.postepay.automation.core.ui.molecules.SearchContactsNumberP2P;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class SendG2GPageStep2Manager extends PageManager {

	public SendG2GPageStep2Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void searchContact(String targetUser) {
		Particle search=(Particle) UiObjectRepo.get().get(SearchContactsNumberG2G.SEARCHCONTACTS);
		
		search.getElement().sendKeys(targetUser);
		
		WaitManager.get().waitMediumTime();
		
		Particle userTargetCard=(Particle) UiObjectRepo.get().get(SearchContactsNumberG2G.TARGETIDCONTACT);
		
		userTargetCard.getElement().click();
		
		WaitManager.get().waitLongTime();
	}
	
	public void clickOnContattiTab() {
		Particle tab=(Particle) UiObjectRepo.get().get(SearchContactsNumberG2G.TABCONTACTSSENDGIGA);
		
		tab.getElement().click();
		
		WaitManager.get().waitMediumTime();
		
	}
}

