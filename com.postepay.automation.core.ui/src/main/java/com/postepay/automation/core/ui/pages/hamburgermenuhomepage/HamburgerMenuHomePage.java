package com.postepay.automation.core.ui.pages.hamburgermenuhomepage;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.UserDetailsHamburgerMenu;
import com.postepay.automation.core.ui.molecules.SectionsAppHamburgerMenu;
import com.postepay.automation.core.ui.molecules.OtherAppsAndInfoHamburgerMenu;


public class HamburgerMenuHomePage extends Page {
	public static final String NAME="T004";


	public HamburgerMenuHomePage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	protected void onInit() {
		this.addToTemplate(UserDetailsHamburgerMenu.NAME, UiObjectRepo.get().get(UserDetailsHamburgerMenu.NAME), true);
		this.addToTemplate(SectionsAppHamburgerMenu.NAME, UiObjectRepo.get().get(SectionsAppHamburgerMenu.NAME), true);
		this.addToTemplate(OtherAppsAndInfoHamburgerMenu.NAME, UiObjectRepo.get().get(OtherAppsAndInfoHamburgerMenu.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {

		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {

		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {

		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new HamburgerMenuHomePageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {

		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {

		return null;
	}

	public void clickOnSettings() {
		((HamburgerMenuHomePageManager)this.manager).clickOnSettings();
	}
	
	public void clickOnMyPurchase() {
		((HamburgerMenuHomePageManager)this.manager).clickOnMyPurchase();
	}
	
	public void clickOnMyProfile() {
		((HamburgerMenuHomePageManager)this.manager).clickOnMyProfile();
	}
	
	public void clickOnLogout() {
		((HamburgerMenuHomePageManager)this.manager).clickOnLogout();
	}
	
	public void clickOnAssistence() {
		((HamburgerMenuHomePageManager)this.manager).clickOnAssistence();
	}
}

