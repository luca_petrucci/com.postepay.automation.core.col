package com.postepay.automation.core.ui.pages.homepage;

import static io.appium.java_client.touch.offset.PointOption.point;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;
import com.postepay.automation.core.ui.molecules.CardDetailHomePage;
import com.postepay.automation.core.ui.molecules.DiscoverMoreHomePage;
import com.postepay.automation.core.ui.molecules.FooterHomePage;
import com.postepay.automation.core.ui.molecules.GenericErrorPopUpMolecola;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.SpeedOperationHomePage;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import org.openqa.selenium.WebDriver;
import ch.qos.logback.core.Layout;
import io.appium.java_client.TouchAction;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class HomePageManager extends PageManager {
	LayoutTools toolLayout=new LayoutTools();

	public HomePageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderLogoHomepage() {
		Particle logoHeader = (Particle) UiObjectRepo.get().get(HeaderGenericAllPage.TITLEPOSTEPAYLOGOHOMEPAGE);
		logoHeader.getElement();
		//assertTrue(logoHeader.getElement().isDisplayed());
		
	}
	
	public void clickOnCard() {
		Particle card=null;
		boolean find=false;
		for(int i=0; i<2; i++) {
			try {
				card=(Particle) UiObjectRepo.get().get(CardDetailHomePage.CLICKONCARD);
				card.getElement().click();
				find=true;
			} catch (Exception e) {
				System.out.println("Card non visibile - Swipe in corso");
				find=false;
			}
			if(find==false) {
			UIUtils.mobile().swipe((MobileDriver) page.getDriver(), SCROLL_DIRECTION.RIGHT, 300);
			}
		}
		try {Thread.sleep(1000);} catch (Exception err )  {}
	}

	public String getAmountCashOfCard() {
		Particle cardBelance=(Particle) UiObjectRepo.get().get(CardDetailHomePage.CARDBALANCEHOMEPAGE);

		String cardBelanceString = cardBelance.getElement().getText();
		
		System.out.println(cardBelanceString);
		
		return cardBelanceString;
	}
	
	public void clickOnHamburgerMenu() {

		Particle menuIcon=(Particle) UiObjectRepo.get().get(HeaderGenericAllPage.LEFTBUTTONGENERIC);

		// Click sull'icona del hamburger menu
		menuIcon.getElement().click();
		
		try {Thread.sleep(1000);} catch (Exception err )  {}
	}

	public void clickAllMessageNumber() {

		Particle wallMessageIcon=(Particle) UiObjectRepo.get().get(HeaderGenericAllPage.RIGHTBUTTONGENERICICON);
        Particle bachecaIconaHomepage=(Particle) UiObjectRepo.get().get(HeaderGenericAllPage.BACHECAICONAHOMEPAGE);
		// Click sull'icona della campana
		try {
//			wallMessageIcon.getElement().click();
			bachecaIconaHomepage.getElement().click();
		} catch (Exception e) {
//			bachecaIconaHomepage.getElement().click();
		}
		
		try {Thread.sleep(3000);} catch (Exception err )  {}
	}

	public void clickOnMenuTabIcon() {

		Particle menuTabIcon=(Particle) UiObjectRepo.get().get(FooterHomePage.NAVIGATIONMENUHOMEVALORIZED);

		// Click sul tab Menu
		menuTabIcon.getElement().click();
		
		try {Thread.sleep(1000);} catch (Exception err )  {}
	}

	public void clickOnProductsTabIcon() {

		Particle productTabIcon=(Particle) UiObjectRepo.get().get(FooterHomePage.NAVIGATIONMENUHOMEPRODUCT);

		// Click sul tab Prodotti
		productTabIcon.getElement().click();
		
		try {Thread.sleep(1000);} catch (Exception err )  {}
	}

	public void clickOnCommunityTabIcon() {

		Particle communityTabIcon=(Particle) UiObjectRepo.get().get(FooterHomePage.NAVIGATIONMENUHOMECOMMUNITY);

		// Click sul tab Community
		communityTabIcon.getElement().click();
		
		try {Thread.sleep(1000);} catch (Exception err )  {}
	}

	public void clickOnPaymentTabIcon() {

		Particle paymentTabIcon=(Particle) UiObjectRepo.get().get(FooterHomePage.NAVIGATIONMENUHOMEPAYMENT);

		// Click sul tab Paga
		paymentTabIcon.getElement().click();
		
		WaitManager.get().waitShortTime();
		
		try {
			Particle atac=(Particle) UiObjectRepo.get().get(GenericErrorPopUpMolecola.ATACBUTTONOKM202);
			UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.visibilityOfElementLocated(atac.getXPath()));
			atac.getElement().click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		WaitManager.get().waitLongTime();
	}

	public void clickOnMapsTabIcon() {

		Particle mapsTabIcon=(Particle) UiObjectRepo.get().get(FooterHomePage.NAVIGATIONMENUHOMEMAPS);

		// Click sul tab Mappa
		mapsTabIcon.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
	
	public void isP2PDisplayed() {
		for (int i = 0; i < 10; i++) {
			Dimension size = page.getDriver().manage().window().getSize();
			
			Particle p2pOperation = null;
			boolean find=false;
			try {
				p2pOperation=(Particle) UiObjectRepo.get().get(SpeedOperationHomePage.SPEEDOPERATIONP2P);
				p2pOperation.getElement();
				find=true;
			} catch (Exception e) {
				System.out.println("P2P Veloce non visibile");
				find=false;
//				e.printStackTrace();
				// TODO: handle exception
			}
			if (find == false) {
				System.out.println("p2pOperation == null");
				Particle quickAction = (Particle) UiObjectRepo.get().get(SpeedOperationHomePage.QUICKACTIONBAR);
				
				WebElement secondAction = quickAction.getElement();
				System.out.println("secondAction "+ secondAction);
				
				float x = secondAction.getLocation().getX() + secondAction.getRect().width * 0.5f; 
				float y = secondAction.getLocation().getY() + secondAction.getRect().height * 0.5f; 
				System.out.println("secondActionget.Location().getX() " + secondAction.getLocation().getX() + "\t secondAction.getRect().width" + secondAction.getRect().width);
				System.out.println("secondActionget.Location().getY() " + secondAction.getLocation().getY() + "\t secondAction.getRect().height" + secondAction.getRect().height);

				System.out.println("x " + x);
				System.out.println("y " + y);
				
				float X = x / size.width; 
				float Y = y / size.height;
				
				System.out.println("X " + X);
				System.out.println("Y " + Y);
				
				System.out.println("Scroll - Start");
				UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.RIGHT, 
						X, 
						Y, 
						X - (X * 0.4f),
						Y,
						300);
				System.out.println("Scroll - End");
				WaitManager.get().waitShortTime();
			} else {
				System.out.println("p2pOperation != null");
				return;
			}
		}
	}
	
	public void clickOnSentP2PSpeedOperation() {

		Particle p2pOperation=(Particle) UiObjectRepo.get().get(SpeedOperationHomePage.SPEEDOPERATIONP2P);

		// Click sul tab Mappa
		p2pOperation.getElement().click();
		
		try {Thread.sleep(1000);} catch (Exception err )  {}
	}
	
	public void scrollSbrodolo(WebDriver driver) {
		Particle sbr=(Particle) UiObjectRepo.get().get(DiscoverMoreHomePage.SBRODOLO);
		WebElement eSbr=driver.findElement(By.xpath(sbr.getLocator()));
		// TouchAction e imposto il driver
		int coordinateX; int coordinateY; int coordinateYnew;

		coordinateX=eSbr.getLocation().getX()+100;
		coordinateY=eSbr.getLocation().getY();
		coordinateYnew=coordinateY+500;
		
		// TouchAction e imposto il driver
		TouchAction touchAction = new TouchAction((MobileDriver<?>) page.getDriver());
//		System.out.println("Prova lo Swipe");
		touchAction.longPress(point(coordinateX,coordinateY)).moveTo(point(coordinateX,coordinateYnew)).release().perform();
		
		WaitManager.get().waitLongTime();
	}
	
	public WebElement lastRootCard(WebDriver driver) {
		List<WebElement> rootCard = (List<WebElement>) driver.findElements(By.xpath("//*[@resource-id='posteitaliane.posteapp.apppostepay:id/root']"));
		int last = rootCard.size() - 1;
		WebElement lastRoot = rootCard.get(last);
		return lastRoot;
	}
	
	public void scrollToElement(WebDriver driver, WebElement salvadanaioTitle) {
		
		
			WebElement lastRoot = lastRootCard(driver);
			toolLayout.scrollWithCoordinate(driver,
					lastRoot.getLocation().getX()+200, lastRoot.getLocation().getX()+200,
					lastRoot.getLocation().getY(), lastRoot.getLocation().getY()-1500);
			WaitManager.get().waitHighTime();
		
	}
	
	public void verifyImageCard(WebDriver driver, double discrepanza) {
		
		LayoutImage.get().verifyImage(driver, "homepage/salvadanaio_home", discrepanza);
	}

	public void scrollToSalvadanaio(WebDriver driver, double discrepanza) {
		Particle title=(Particle) UiObjectRepo.get().get(DiscoverMoreHomePage.CARDDINAMICTITLE);
		Particle descrizione=(Particle) UiObjectRepo.get().get(DiscoverMoreHomePage.CARDDINAMICDESCRIPTION);
		Particle buttonScopri=(Particle) UiObjectRepo.get().get(DiscoverMoreHomePage.CARDDINAMICSCOPRI);
		Particle lastCard=(Particle) UiObjectRepo.get().get(DiscoverMoreHomePage.LASTCARDONPAGE);
		
		LayoutTools toolLayout=new LayoutTools();
		WebElement wTitle = null;

		while (wTitle==null) {
			try {
				wTitle = toolLayout.replaceGenericPathOfElement(driver, title, "Salvadanaio");
				break;
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			WebElement lastRoot = lastRootCard(driver);
			scrollToElement(driver, wTitle);
		}
		
		WebElement wDescription = toolLayout.replaceGenericPathOfElement(driver, descrizione, "Scopri il servizio con cui puoi accumulare i tuoi risparmi per raggiungere più facilmente i tuoi obiettivi!");
		WebElement wButton = buttonScopri.getElement();	
		
		// swipe su ultima carta
		toolLayout.scrollWithCoordinate(driver, 
										wTitle.getLocation().getX(), 
										wTitle.getLocation().getX()-500, 
										wTitle.getLocation().getY(), 
										wTitle.getLocation().getY());
		assertTrue(wTitle.isDisplayed());
		assertTrue(wDescription.isDisplayed());
		assertTrue(wButton.isDisplayed());
		
		System.out.println("Verifica sull'immagine di salvadanaio");
//		verifyImageCard(driver, discrepanza);
		
		WaitManager.get().waitShortTime();
		
		wTitle.click();
		WaitManager.get().waitHighTime();		
	}
	
	public void clickOnBachecaIcon() {
		System.out.println(page.getDriver().getPageSource());
		Particle bachecaIcon=(Particle) UiObjectRepo.get().get(HeaderGenericAllPage.BACHECAICONAHOMEPAGE);
		// Click sul tab Menu
//		bachecaIcon.getElement().click();
		String s = "//*[@content-desc='Bacheca']";
		
		page.getDriver().findElement(By.xpath(s)).click();
//		if(bachecaIcon != null) {
//			System.out.println("Click con Notifiche");
//			bachecaIcon.getElement().click();
//		} else {
//			Particle bIB = (Particle) UiObjectRepo.get().get(HeaderGenericAllPage.RIGHTBUTTONGENERIC);
//			try {
//				bIB.getElement().click();	
//			} catch (Exception e) {
//				// TODO: handle exception
//			}
//		}

		WaitManager.get().waitMediumTime();
	}
	
	public void scrollToDawn(String targetTitleString) {
		LayoutTools toolLayout=new LayoutTools();
		Particle title=(Particle) UiObjectRepo.get().get(DiscoverMoreHomePage.CARDDINAMICTITLE);
		
		WebElement wTitle = null;
		
		while (wTitle==null) {
			try {
				wTitle = toolLayout.replaceGenericPathOfElement(page.getDriver(), title, targetTitleString);
				break;
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			// Scroll su tutto il cassetto
			Particle cassetto=(Particle) UiObjectRepo.get().get(DiscoverMoreHomePage.INTEROCASSETTOHOMEPAGE);
			toolLayout.scrollWithCoordinate(page.getDriver(),
					cassetto.getElement().getLocation().getX()+400, cassetto.getElement().getLocation().getX()+400,
					cassetto.getElement().getLocation().getY()+800, cassetto.getElement().getLocation().getY());
			WaitManager.get().waitShortTime();
		}
	}
	
	public void swithCardInEvdenza(String targetTitleString) {
		LayoutTools toolLayout=new LayoutTools();
		Particle title=(Particle) UiObjectRepo.get().get(DiscoverMoreHomePage.CARDDINAMICTITLE);
		
		WebElement coordinateEl = toolLayout.replaceGenericPathOfElement(page.getDriver(), title, "IN EVIDENZA");
		
		WebElement wTitle = null;
		
		while (wTitle==null) {
			try {
				wTitle = toolLayout.replaceGenericPathOfElement(page.getDriver(), title, targetTitleString);
				break;
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			// Scroll su tutto il cassetto
			Particle cassetto=(Particle) UiObjectRepo.get().get(DiscoverMoreHomePage.INTEROCASSETTOHOMEPAGE);
			toolLayout.scrollWithCoordinate(page.getDriver(),
					coordinateEl.getLocation().getX()+600, coordinateEl.getLocation().getX(),
					coordinateEl.getLocation().getY()+200, coordinateEl.getLocation().getY()+200);
			WaitManager.get().waitShortTime();
		}
		wTitle.click();
	}
}

