package com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep3;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.molecules.BodyDetailsExtraurbanPurchase;


public class ExtraurbanPurchasePageStep3 extends Page {
	public static final String NAME="T062";
	

	public ExtraurbanPurchasePageStep3(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(BodyDetailsExtraurbanPurchase.NAME, UiObjectRepo.get().get(BodyDetailsExtraurbanPurchase.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new ExtraurbanPurchasePageStep3Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new ExtraurbanPurchasePageStep3ManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderPage(String inputTitle) {
		((ExtraurbanPurchasePageStep3Manager)this.manager).verifyHeaderpage(inputTitle);
	}
	
	public void verifyLayoutPage() {
		((ExtraurbanPurchasePageStep3Manager)this.manager).verifyLayoutPage();
	}
	
	public void clickOnExpandArrow() {
		((ExtraurbanPurchasePageStep3Manager)this.manager).clickOnExpandArrow();
	}
	
	public void clickOnOrdinaryTicket() {
		((ExtraurbanPurchasePageStep3Manager)this.manager).clickOnOrdinaryTicket();
	}
	
	public void clickOnProceed() {
		((ExtraurbanPurchasePageStep3Manager)this.manager).clickOnProceed();
	}
}

