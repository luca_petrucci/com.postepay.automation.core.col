package com.postepay.automation.core.ui.pages.communitycardtwopage;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.CardsOfG2gCommunity;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;

import io.appium.java_client.android.AndroidDriver;

import com.postepay.automation.core.ui.molecules.FooterHomePage;


public class CommunityCardTwoPage extends Page {
	public static final String NAME="T007";
	

	public CommunityCardTwoPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(CardsOfG2gCommunity.NAME, UiObjectRepo.get().get(CardsOfG2gCommunity.NAME), true);
this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
this.addToTemplate(FooterHomePage.NAME, UiObjectRepo.get().get(FooterHomePage.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new CommunityCardTwoPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void clickOnSendG2G(WebDriver driver) {
		((CommunityCardTwoPageManager)this.manager).clickOnSendG2G(driver);
	}

}

