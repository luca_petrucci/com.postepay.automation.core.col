package com.postepay.automation.core.ui.pages.rubricabollettinopage;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.AbilitaInAppMolecola;
import com.postepay.automation.core.ui.molecules.DatiBeneficiarioM256;
import com.postepay.automation.core.ui.molecules.RubricaBollettinoMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class RubricaBollettinoPageManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();


	public RubricaBollettinoPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}
	
	public void verifyLayout() {
		Particle title = (Particle) UiObjectRepo.get().get(RubricaBollettinoMolecola.BENEFICIARIOTITLERUBRICA);
		assertTrue("Beneficiario non visibile",title.getElement().isDisplayed());
		Particle subTitle = (Particle) UiObjectRepo.get().get(RubricaBollettinoMolecola.CONTOCORRENTERUBRICA);
		assertTrue("Conto Corrente non visibile",subTitle.getElement().isDisplayed());
	}
	
	public void scegliBeneficiario(String titoloInput, String contoCorrente, String importo, String causale)
	{
		Particle title = (Particle) UiObjectRepo.get().get(RubricaBollettinoMolecola.BENEFICIARIOTITLERUBRICA);
		String nuovoLocator =Utility.replacePlaceHolders(title.getLocator(), new Entry("title", titoloInput));
		System.out.println(nuovoLocator);
		page.getDriver().findElement(By.xpath(nuovoLocator)).isDisplayed();
		WaitManager.get().waitShortTime();
		
		Particle subTitle = (Particle) UiObjectRepo.get().get(RubricaBollettinoMolecola.CONTOCORRENTERUBRICA);
		String locatorSubTitle =Utility.replacePlaceHolders(subTitle.getLocator(), new Entry("title", contoCorrente));
		page.getDriver().findElement(By.xpath(locatorSubTitle)).click();
		WaitManager.get().waitShortTime();
		
		Particle amount = (Particle) UiObjectRepo.get().get(DatiBeneficiarioM256.M256IMPORTO);
		Particle text = (Particle) UiObjectRepo.get().get(DatiBeneficiarioM256.M256CAUSALE);
		amount.getElement().sendKeys(importo);
		WaitManager.get().waitShortTime();
		text.getElement().sendKeys(causale);
		WaitManager.get().waitShortTime();	
	}

}

