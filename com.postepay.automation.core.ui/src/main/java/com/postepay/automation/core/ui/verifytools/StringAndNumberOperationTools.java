package com.postepay.automation.core.ui.verifytools;

import java.util.Date;
import java.util.Locale;

import org.opencv.core.Mat;

import static org.junit.Assert.assertTrue;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.GregorianCalendar;


public class StringAndNumberOperationTools {

	public StringAndNumberOperationTools() {
		// TODO Auto-generated constructor stub
	}

	public static double convertStringToDouble(String amountString) {
		NumberFormat format = NumberFormat.getInstance(Locale.ITALY);
		double d=0.0;
		try {
			Number number = format.parse(amountString);
			double a = number.doubleValue();
			d = d+a;

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return d;
	}

	public String convertNumberToHiddeNumber(String cardNumber, String targetOperation) {
		
		if (targetOperation.equals("MyPP")) {
			String prefix="**** **** ";
			String newString=prefix.concat(cardNumber);
			return newString;
		}
		else {
			String string=cardNumber.substring(12);
			String prefix="**** **** **** ";
			String newString=prefix.concat(string);
			return newString;
		}
	}

	public String convertAmountWithEuro(String amountInput) {
		String euroPrefix="€ ";
		String newAmountInput=euroPrefix.concat(amountInput).replace(".", ",");
		
		return newAmountInput;
	}
	
	public double convertWithoutPAGAlabel(String stringInput) {
		String newAmountButton=stringInput.replace("PAGA € ", "");
		double newAmountButtonInteger=convertStringToDouble(newAmountButton);
		
		return newAmountButtonInteger;
	}
	
	public double convertWithoutEUROlabel(String stringInput) {
		String newAmountButton=stringInput.replace("€ ", "").replace("€", "");
		double newAmountButtonInteger=convertStringToDouble(newAmountButton);
		
		return newAmountButtonInteger;
	}
	
	public String stringDATEforBachecaMessageTab(String dateOfOperation) {
	    
		Date d = new Date();
		DateFormat formatoData = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.ITALY);
		
		
		if (dateOfOperation.equals("toDay")) {
			String s = formatoData.format(d);
			System.out.println(s);
			String date=s.replace("-", " ").toUpperCase();
			System.out.println(date);
			System.out.println(date.substring(0,1));
			
			switch (date.substring(0,1)) {
			case "1":
				if (date.substring(0,1) != "1") {
					break;
				}
				date = "0".concat(date);
				System.out.println(date);
				break;
			case "2": 
				if (date.substring(0,2) != "2") {
					break;
				}
				date = "0".concat(date);
				System.out.println(date);
				break;
			case "3":
				date = "0".concat(date);
				System.out.println(date);
				break;
			case "4":
				date = "0".concat(date);
				System.out.println(date);
				break;
			case "5":
				date = "0".concat(date);
				System.out.println(date);
				break;
			case "6":
				date = "0".concat(date);
				System.out.println(date);
				break;
			case "7":
				date = "0".concat(date);
				System.out.println(date);
				break;
			case "8":
				date = "0".concat(date);
				System.out.println(date);
				break;
			case "9":
				date = "0".concat(date);
				System.out.println(date);
				break;	
			}			
			return date;
		}
		else {
			String getDate = dateOfOperation;
			System.out.println(getDate);
			String date=getDate.replace("-", " ").toUpperCase();
			
			System.out.println(date);
			
			return date;
		}
	}
	
	public String stringDATEforBachecaNotificheTab(String dateOfOperation) {
		Date d = new Date();
		DateFormat formatoData = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.ITALY);
		
		
		if (dateOfOperation.equals("toDay")) {
			String s = formatoData.format(d);
			System.out.println(s);
			String date=s.replace("-", " ");
			System.out.println(date);
			System.out.println(date.substring(0,1));
			
			switch (date.substring(0,1)) {
			case "1":
				date = "0".concat(date);
				System.out.println(date);
				break;
			case "2": 
				if (date.substring(0,2) != "2") {
					break;
				}
				date = "0".concat(date);
				System.out.println(date);
				break;
			case "3":
				date = "0".concat(date);
				System.out.println(date);
				break;
			case "4":
				date = "0".concat(date);
				System.out.println(date);
				break;
			case "5":
				date = "0".concat(date);
				System.out.println(date);
				break;
			case "6":
				date = "0".concat(date);
				System.out.println(date);
				break;
			case "7":
				date = "0".concat(date);
				System.out.println(date);
				break;
			case "8":
				date = "0".concat(date);
				System.out.println(date);
				break;
			case "9":
				date = "0".concat(date);
				System.out.println(date);
				break;	
			}			
	
			return date;
		}
		else {
			String getDate = dateOfOperation;
			System.out.println(getDate);
			String date=getDate.replace("-", " ");
			
			System.out.println(date);
			
			return date;
		}
	}
	
	public double convertStringG2G(String g2gStringAmount) {
		String str=g2gStringAmount.replace("GB", "");
		
		double number = convertStringToDouble(str);
		
		return number;
	}
	
	public void verifyG2GAmount(String oldAmount, String newAmount, String toSendAmount) {
		double dOldAmount = convertStringG2G(oldAmount);
		double dNewAmount = convertStringG2G(newAmount);
		double dToSendAmount = convertStringG2G(toSendAmount);
		
		double differenceGb = dOldAmount - dToSendAmount;
		double diffEnd = Math.round(differenceGb * 100) / 100.0;
		
		System.out.println("Converted toSendAmount --> " + dToSendAmount);
		System.out.println("Converted newAmount --> " + dNewAmount);
		System.out.println("Converted oldAmount --> " + dOldAmount);
		System.out.println("Converted differenceGb --> " + diffEnd);
		
		assertTrue(diffEnd == dNewAmount);
	}
	
	public double converAmountDisponibileAndContabile(String stringInput) {
		String amount=stringInput.replace(" €", "");
		double newAmount=convertStringToDouble(amount);
		
		return newAmount;
	}
	
	public String getDateExtraurban() {
		Date d = new Date();
		DateFormat formatoData = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.GERMANY);
		String s = formatoData.format(d);
		System.out.println(s);
		String date=s.replace(".", "/");
		System.out.println(date);
		
		return date;
	}
	
	public int convertStringToInt(String amountString) {
		NumberFormat format = NumberFormat.getInstance(Locale.ITALY);
		int d=0;
		try {
			Number number = format.parse(amountString);
			int a = number.intValue();
			d = d+a;

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return d;
	}
	
	public static String convertDateForMieiAcquisti(String oldDateString) {
		final String OLD_FORMAT = "dd/MM/yyyy";
		final String NEW_FORMAT = "dd MMM";

		// August 12, 2010
//		String oldDateString = "12/08/2010";
		String newDateString;
		SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
		Date d = null;
		try {
			d = sdf.parse(oldDateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		sdf.applyPattern(NEW_FORMAT);
		newDateString = sdf.format(d);
		
		System.out.println(newDateString);
		
		return newDateString.toUpperCase();
	}
}
