package com.postepay.automation.core.ui.pages.detailsconnectsimpurchaseandsendgigapage;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.BodyOfPurchaseGiga;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.ToolbarTitlePurchaseGiga;
import ui.core.support.waitutil.WaitManager;
import utility.SoftAssertion;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class DetailsConnectSimPurchaseAndSendGigaPageManager extends PageManager {

	Particle btnSendG2G = (Particle) UiObjectRepo.get().get(BodyOfPurchaseGiga.G2GSENDBUTTONGIGASUBCARD);
	Particle btnPurchaseGiga = (Particle) UiObjectRepo.get().get(BodyOfPurchaseGiga.G2GPURCHASEBUTTONGIGASUBCARD);
	
	public DetailsConnectSimPurchaseAndSendGigaPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	public void verifyLayoutPage() {
		Particle title = (Particle) UiObjectRepo.get().get(ToolbarTitlePurchaseGiga.TITLECONNECTSIMPURCHASEANDSENDGIGA);
		Particle simDetail = (Particle) UiObjectRepo.get().get(ToolbarTitlePurchaseGiga.SIMDETAILSSUBCARD);
		Particle phoneNumber = (Particle) UiObjectRepo.get().get(ToolbarTitlePurchaseGiga.PHONENUMBERSUBCARD);
		Particle creditSim = (Particle) UiObjectRepo.get().get(ToolbarTitlePurchaseGiga.CREDITSIMSUBCARD);
		Particle assistenzaIcon = (Particle) UiObjectRepo.get().get(ToolbarTitlePurchaseGiga.ASSISTENTCONNECTSIMPURCHASEANDSENDGIGA);
		Particle gigaLabel = (Particle) UiObjectRepo.get().get(BodyOfPurchaseGiga.TITLEGIGASUBCARD);
		Particle gigaAmountAvaible = (Particle) UiObjectRepo.get().get(BodyOfPurchaseGiga.G2GAMOUNTAVAIBLEGIGASUBCARD);
		Particle creditLabel = (Particle) UiObjectRepo.get().get(BodyOfPurchaseGiga.TITLECREDITSUBCARD);
		Particle creditLimit = (Particle) UiObjectRepo.get().get(BodyOfPurchaseGiga.COPYCREDITSUBCARD);
	
		
		SoftAssertion.get().getAssertions().assertThat(title.getElement().isDisplayed()).withFailMessage("Title non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(simDetail.getElement().isDisplayed()).withFailMessage("SimDetail non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(phoneNumber.getElement().isDisplayed()).withFailMessage("PhoneNumber non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(creditSim.getElement().isDisplayed()).withFailMessage("CreditSim non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(assistenzaIcon.getElement().isDisplayed()).withFailMessage("AssistenzaIcon non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(gigaLabel.getElement().isDisplayed()).withFailMessage("Giga Label non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(gigaAmountAvaible.getElement().isDisplayed()).withFailMessage("Giga Amounrt non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(creditLabel.getElement().isDisplayed()).withFailMessage("Crediti Label non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(creditLimit.getElement().isDisplayed()).withFailMessage("Credit Limit non visibile").isEqualTo(true);

//		assertTrue(title.getElement().isDisplayed());
//		assertTrue(simDetail.getElement().isDisplayed());
//		assertTrue(phoneNumber.getElement().isDisplayed());
//		assertTrue(creditSim.getElement().isDisplayed());
//		assertTrue(assistenzaIcon.getElement().isDisplayed());
//		assertTrue(gigaLabel.getElement().isDisplayed());
//		assertTrue(gigaAmountAvaible.getElement().isDisplayed());
//		assertTrue(creditLabel.getElement().isDisplayed());
//		assertTrue(creditLimit.getElement().isDisplayed());
	}
	
	public void clickOnSendG2G() {
		System.out.println(page.getDriver().getPageSource());
		WaitManager.get().waitMediumTime();
		btnSendG2G.getElement().click();
		WaitManager.get().waitMediumTime();
	}

	public void clickOnPurchaseGiga() {
		btnPurchaseGiga.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnBack() {
		Particle back = (Particle) UiObjectRepo.get().get(HeaderGenericAllPage.TASTOBACKALTERNATIVO);
		back.getElement().click();
		WaitManager.get().waitShortTime();
		
	}
}

