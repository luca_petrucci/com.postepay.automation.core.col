package com.postepay.automation.core.ui.pages.salvadanaioversasuobiettivo;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;

public class SalvadanaioVersaSuObiettivoManagerIOS extends SalvadanaioVersaSuObiettivoManager {

	public SalvadanaioVersaSuObiettivoManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	
	public void verifyLayoutPage(WebDriver driver, String nameInput, String typeObiettivoInput, String isRicorrente, double discrepanza) {
		assertTrue(iconaYellow.getElement().isDisplayed());
		getElementName(driver, nameInput);
		assertTrue(versamentoTestoVersameto.getElement().isDisplayed());
		assertTrue(versamentoLimiteMassimo.getElement().isDisplayed());
		assertTrue(versamentoMeno.getElement()!=null);
		assertTrue(versamentoPiu.getElement()!=null);
		assertTrue(versamentoAmount.getElement().isDisplayed());
		assertTrue(versamentoRicorrenteLabel.getElement().isDisplayed());
		assertTrue(versamentoRicorrenteToggle.getElement().isDisplayed());
		assertTrue(versamentoSuggestionImage.getElement()!=null);
		getElementTextOnCard(driver, isRicorrente);
		assertTrue(versamentoButton.getElement().isEnabled());
		
		switch (typeObiettivoInput) {
		case "viaggi":
			System.out.println("Vedifica del tipo immagini - VIAGGI");
			verifyImage(driver, "viaggi_icon_versa", discrepanza);
			break;
		case "salute":
			System.out.println("Vedifica del tipo immagini - SALUTE E BENESSERE");
			verifyImage(driver, "salute_icon_versa", discrepanza);
			break;
		case "risparmio":
			System.out.println("Vedifica del tipo immagini - RISPARMIO");
			verifyImage(driver, "risparmio_icon_versa", discrepanza);
			break;
		default:
			System.out.println("Errore nel tipo immagini");
			break;
		}
		System.out.println("Vedifica del tipo immagini - ICONA VERSA IN CASSAFORTE");
		verifyImage(driver, "cassaforte_versa", discrepanza);
		System.out.println("Vedifica del tipo immagini - ICONA MENO");
		verifyImage(driver, "versa_minus", discrepanza);
		System.out.println("Vedifica del tipo immagini - ICONA PIU");
		verifyImage(driver, "versa_plus", discrepanza);
		System.out.println("Vedifica delle immagini - FINITA");
		
	}
}
