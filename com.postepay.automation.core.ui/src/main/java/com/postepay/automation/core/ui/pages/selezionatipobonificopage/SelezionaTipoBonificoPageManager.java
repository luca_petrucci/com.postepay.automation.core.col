package com.postepay.automation.core.ui.pages.selezionatipobonificopage;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.common.datatable.UserDataCredential;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.SelezionaTipoBonificoMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class SelezionaTipoBonificoPageManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();
	String fromDataCorrente;
	String toDataCorrente;
	
	Particle titleM270=(Particle) UiObjectRepo.get().get(SelezionaTipoBonificoMolecola.TITLEM270);
	Particle bonificoSepaPostagiroM270=(Particle) UiObjectRepo.get().get(SelezionaTipoBonificoMolecola.BONIFICOSEPAPOSTAGIROM270);
	Particle bonificoSepaIstantaneoM270=(Particle) UiObjectRepo.get().get(SelezionaTipoBonificoMolecola.BONIFICOSEPAISTANTANEOM270);
	Particle titleBonificoSepaPostagiroM270=(Particle) UiObjectRepo.get().get(SelezionaTipoBonificoMolecola.TITLEBONIFICOSEPAPOSTAGIROM270);
	Particle titleBonificoSepaIstantaneoM270=(Particle) UiObjectRepo.get().get(SelezionaTipoBonificoMolecola.TITLEBONIFICOSEPAISTANTANEOM270);
	Particle copyBonificoSepaPostagiroM270=(Particle) UiObjectRepo.get().get(SelezionaTipoBonificoMolecola.COPYBONIFICOSEPAPOSTAGIROM270);
	Particle copyBonificoSepaIstantaneoM270=(Particle) UiObjectRepo.get().get(SelezionaTipoBonificoMolecola.COPYBONIFICOSEPAISTANTANEOM270);
	
	
	public SelezionaTipoBonificoPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}
	
	public void verifyLayout() {
		Particle titleM270=(Particle) UiObjectRepo.get().get(SelezionaTipoBonificoMolecola.TITLEM270);
		System.out.println(titleM270);
		Particle bonificoSepaPostagiroM270=(Particle) UiObjectRepo.get().get(SelezionaTipoBonificoMolecola.BONIFICOSEPAPOSTAGIROM270);
		System.out.println(bonificoSepaPostagiroM270);
		Particle bonificoSepaIstantaneoM270=(Particle) UiObjectRepo.get().get(SelezionaTipoBonificoMolecola.BONIFICOSEPAISTANTANEOM270);
		Particle titleBonificoSepaPostagiroM270=(Particle) UiObjectRepo.get().get(SelezionaTipoBonificoMolecola.TITLEBONIFICOSEPAPOSTAGIROM270);
		Particle titleBonificoSepaIstantaneoM270=(Particle) UiObjectRepo.get().get(SelezionaTipoBonificoMolecola.TITLEBONIFICOSEPAISTANTANEOM270);
		Particle copyBonificoSepaPostagiroM270=(Particle) UiObjectRepo.get().get(SelezionaTipoBonificoMolecola.COPYBONIFICOSEPAPOSTAGIROM270);
		Particle copyBonificoSepaIstantaneoM270=(Particle) UiObjectRepo.get().get(SelezionaTipoBonificoMolecola.COPYBONIFICOSEPAISTANTANEOM270);
	try {
		titleM270.getElement().isDisplayed();
		bonificoSepaPostagiroM270.getElement().isDisplayed();
		bonificoSepaIstantaneoM270.getElement().isDisplayed();
		titleBonificoSepaPostagiroM270.getElement().isDisplayed();
		titleBonificoSepaIstantaneoM270.getElement().isDisplayed();
		copyBonificoSepaPostagiroM270.getElement().isDisplayed();
		copyBonificoSepaIstantaneoM270.getElement().isDisplayed();
	} catch (Exception e) {
		
	}

	
	}
	
	public void clickOnBonificoPostagiro(String operazione) {
		switch (operazione) {
		
		case "SEPA":
			bonificoSepaPostagiroM270.getElement().click();
			break;
		case "Postagiro":
			bonificoSepaPostagiroM270.getElement().click();
			break;
		case "Istantaneo":
			bonificoSepaIstantaneoM270.getElement().click();
			break;
		}
	}
	
}