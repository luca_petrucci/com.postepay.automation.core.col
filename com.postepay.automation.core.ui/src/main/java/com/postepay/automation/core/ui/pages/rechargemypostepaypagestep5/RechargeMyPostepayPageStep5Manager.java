package com.postepay.automation.core.ui.pages.rechargemypostepaypagestep5;

import com.postepay.automation.core.ui.molecules.BodyOfNewPosteID;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.AppiumDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.CoreUtility;

public class RechargeMyPostepayPageStep5Manager extends PageManager {

	public RechargeMyPostepayPageStep5Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void insertPosteId(String posteId) {

		Particle posteIdEditText=(Particle) UiObjectRepo.get().get(BodyOfNewPosteID.ENTERPOSTID);

		CoreUtility.visibilityOfElement(page.getDriver(), posteIdEditText, 10).sendKeys(posteId);

//		((AppiumDriver<?>)page.getDriver()).hideKeyboard();	
		
		//chiusura tastiera
//		Particle tastiera=(Particle) UiObjectRepo.get().get(BodyOfNewPosteID.TITLEHEADERSENDG2G);
//		tastiera.getElement().click();
		
		try {Thread.sleep(1000);} catch (Exception err )  {}
	}

	public void clickOnConfirmButton() {
		
		WaitManager.get().waitShortTime();
		try {((AppiumDriver<?>)page.getDriver()).hideKeyboard();} catch (Exception err )  {}
		
		
		Particle btn=(Particle) UiObjectRepo.get().get(BodyOfNewPosteID.BUTTONCONFIRM);

		//btn.getElement().click();
		boolean confermaClicked=false;
		
		try {
			CoreUtility.visibilityOfElement(page.getDriver(), btn, 10)
			.click();
			confermaClicked=true;
		} catch (Exception e) {
			Particle header=(Particle) UiObjectRepo.get().get(BodyOfNewPosteID.TITLEHEADERSENDG2G);
			
			CoreUtility.visibilityOfElement(page.getDriver(), header, 10)
			.click();
		}
		
		if(confermaClicked == false)
		{
			CoreUtility.visibilityOfElement(page.getDriver(), btn, 10)
			.click();
		}
		
		WaitManager.get().waitShortTime();
	}

	public void verifyHeaderPage(String titleHeader) throws Error 	{

		LayoutTools layOutTool= new LayoutTools();
		layOutTool.verifyHeaderPage(titleHeader);
	}
	
	public void clickOnBackArrow() {
		Particle backArrow=(Particle) UiObjectRepo.get().get(HeaderGenericAllPage.LEFTBUTTONGENERICICON);
		backArrow.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	

}

