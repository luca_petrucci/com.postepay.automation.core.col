package com.postepay.automation.core.ui.pages.selectpostepayrecharge;

import com.postepay.automation.core.ui.molecules.SelectPostepayRechargeMolecola;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class SelectPostepayRechargeManager extends PageManager {

	public SelectPostepayRechargeManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void clickMyPostePay() {
		Particle myPoste = (Particle) UiObjectRepo.get().get(SelectPostepayRechargeMolecola.M249RECHARGEMYPOSTEPAY);
		
		myPoste.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOtherPostePay() {
		Particle otherPoste = (Particle) UiObjectRepo.get().get(SelectPostepayRechargeMolecola.M249RECHARGEOTHERPOSTEPAY);
		
		otherPoste.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
}

