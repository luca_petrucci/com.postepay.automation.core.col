package com.postepay.automation.core.ui.pages.abilitaquestodispositivopage;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.postepay.automation.core.ui.molecules.AbilitaQuestoDispositivoMolecola;
import com.postepay.automation.core.ui.molecules.NotificheSettingMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.CoreUtility;

public class AbilitaQuestoDispositivoPageManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();

	Particle imageAbilitaDispositivo = (Particle) UiObjectRepo.get().get(AbilitaQuestoDispositivoMolecola.IMAGEABILITADISPOSITIVO);
	Particle titleAbilitaDispositivo = (Particle) UiObjectRepo.get().get(AbilitaQuestoDispositivoMolecola.TITLEABILITADISPOSITIVO);
	Particle copyAbilitaDispositivo = (Particle) UiObjectRepo.get().get(AbilitaQuestoDispositivoMolecola.COPYABILITADISPOSITIVO);
	Particle scopriAbilitaDispositivo = (Particle) UiObjectRepo.get().get(AbilitaQuestoDispositivoMolecola.SCOPRIABILITADISPOSITIVO);
	Particle iniziaAbilitaDispositivo = (Particle) UiObjectRepo.get().get(AbilitaQuestoDispositivoMolecola.INIZIAABILITADISPOSITIVO);

	public AbilitaQuestoDispositivoPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}

	public void verifyLayout() {
		assertTrue(imageAbilitaDispositivo.getElement().isDisplayed());
		assertTrue(titleAbilitaDispositivo.getElement().isDisplayed());
		assertTrue(copyAbilitaDispositivo.getElement().isDisplayed());
		assertTrue(scopriAbilitaDispositivo.getElement().isDisplayed());
		assertTrue(iniziaAbilitaDispositivo.getElement().isDisplayed());
	}
	
	public void clickOnInizia() {
		CoreUtility.visibilityOfElement(page.getDriver(), iniziaAbilitaDispositivo, 20).click();
		WaitManager.get().waitLongTime();
	}

	public boolean checkOnb() {
		boolean i = false;
		try {
			WaitManager.get().waitMediumTime();
			WebDriverWait w = new WebDriverWait(page.getDriver(), 20);
			w.until(ExpectedConditions.presenceOfElementLocated(iniziaAbilitaDispositivo.getXPath()));
			i=true;
		} catch (Exception e) {
			i = false;
		}
		return i;
		
	}

}

