package com.postepay.automation.core.ui.pages.webviewgenericpage;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.WebviewGenericPageMolecola;

import io.appium.java_client.android.AndroidDriver;


public class WebviewGenericPage extends Page {
	public static final String NAME="T085";
	

	public WebviewGenericPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(WebviewGenericPageMolecola.NAME, UiObjectRepo.get().get(WebviewGenericPageMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new WebviewGenericPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void isLinkToBancopostaStore() {
		((WebviewGenericPageManager)this.manager).isLinkToBancopostaStore();
	}
	
	public void verifyHeaderBancopostaApp(WebDriver driver) {
		((WebviewGenericPageManager)this.manager).verifyHeaderBancopostaApp(driver);
	}
}

