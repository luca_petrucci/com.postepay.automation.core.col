package com.postepay.automation.core.ui.pages.notifichesettingpage;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.NotificheSettingMolecola;

import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class NotificheSettingPageManagerIOS extends NotificheSettingPageManager {

	public NotificheSettingPageManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyLayoutPreferito() {
		assertTrue(dispositivoLabel.getElement().isDisplayed());
		assertTrue(questoEPreferito.getElement().isDisplayed());
		assertTrue(ricevendoNotifiche.getElement().isDisplayed());
		assertTrue(notificheLabel.getElement().isDisplayed());
		assertTrue(movimentiEntrata.getElement().isDisplayed());
		assertTrue(movimentiUscita.getElement().isDisplayed());
		assertTrue(arrowSetting.getElement()!=null);
		assertTrue(siSetting.getElement()!=null);
	}
	
	public void clickOnback()
	{
		Particle btn = (Particle) UiObjectRepo.get().get(HeaderGenericAllPage.LEFTBUTTONGENERICICON);
		btn.getElement().click();
	}
	
	public void clickOnChiudi()
	{
		Particle btn = (Particle) UiObjectRepo.get().get(HeaderGenericAllPage.CHIUDIBUTTONGENERICICON);
		btn.getElement().click();
	}
}
