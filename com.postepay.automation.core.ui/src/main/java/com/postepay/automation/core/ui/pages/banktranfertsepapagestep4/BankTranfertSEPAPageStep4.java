package com.postepay.automation.core.ui.pages.banktranfertsepapagestep4;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;

import io.appium.java_client.AppiumDriver;

import com.postepay.automation.core.ui.molecules.BodyOfNewPosteID;


public class BankTranfertSEPAPageStep4 extends Page {
	public static final String NAME="T034";
	

	public BankTranfertSEPAPageStep4(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
this.addToTemplate(BodyOfNewPosteID.NAME, UiObjectRepo.get().get(BodyOfNewPosteID.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new BankTranfertSEPAPageStep4Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void insertPosteId(String posteID) {

		((BankTranfertSEPAPageStep4Manager)this.manager).insertPosteId(posteID);
	}

	public void clickOnConfirmButton() {

		((BankTranfertSEPAPageStep4Manager)this.manager).clickOnConfirmButton();
	}
	public void verifyHeaderpage(String titlePageInput) {
		((BankTranfertSEPAPageStep4Manager)this.manager).verifyHeaderpage(titlePageInput);
	}
	
	public void verifyHeaderAutorizzaWEB(String inputTitle) {
		((BankTranfertSEPAPageStep4Manager)this.manager).verifyHeaderAutorizzaWEB(inputTitle);
	}

	public void insertPosteIdAutorizzaWEB(String posteID) {
		((BankTranfertSEPAPageStep4Manager)this.manager).insertPosteIdAutorizzaWEB(posteID);
		
	}

	public void clickOnConfirmButtonAutorizzaWEB() {
		((BankTranfertSEPAPageStep4Manager)this.manager).clickOnConfirmButtonAutorizzaWEB();
	}
}

