package com.postepay.automation.core.ui.pages.sendp2ppagestep5;

import com.postepay.automation.core.ui.molecules.ThankYouPageSendP2P;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class SendP2PPageStep5Manager extends PageManager {

	public SendP2PPageStep5Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void clickOnCloseButton() {
		System.out.println(page.getDriver().getPageSource());
		Particle btn=(Particle) UiObjectRepo.get().get(ThankYouPageSendP2P.BUTTONCLOSEP2PSEND);
		
		btn.getElement().click();
		
		WaitManager.get().waitLongTime();
	}
	
	
}

