package com.postepay.automation.core.ui.pages.notifichesettingpage;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.NotificheSettingMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.SoftAssertion;

import org.openqa.selenium.WebDriver;

public class NotificheSettingPageManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();

	Particle dispositivoLabel = (Particle) UiObjectRepo.get().get(NotificheSettingMolecola.DISPOSITIVOLABELSETTINGNOTIFICHE);
	Particle questoEPreferito = (Particle) UiObjectRepo.get().get(NotificheSettingMolecola.QUESTOEPREFERITOSETTINGNOTIFICHE);
	Particle ricevendoNotifiche = (Particle) UiObjectRepo.get().get(NotificheSettingMolecola.RICEVENDONOTIFICHESETTINGNOTIFICHE);
	Particle notificheLabel = (Particle) UiObjectRepo.get().get(NotificheSettingMolecola.NOTIFICHELABELSETTINGNOTIFICHE);
	Particle movimentiEntrata = (Particle) UiObjectRepo.get().get(NotificheSettingMolecola.MOVIMENTIENTRATASETTINGNOTIFICHE);
	Particle movimentiUscita = (Particle) UiObjectRepo.get().get(NotificheSettingMolecola.MOVIMENTIUSCITASETTINGNOTIFICHE);
	Particle arrowSetting = (Particle) UiObjectRepo.get().get(NotificheSettingMolecola.ARROWSETTINGNOTIFICHE);
	Particle siSetting = (Particle) UiObjectRepo.get().get(NotificheSettingMolecola.SISETTINGNOTIFICHE);
	Particle notificaPush = (Particle) UiObjectRepo.get().get(NotificheSettingMolecola.NOTIFICHEPUSHBIGTEXT);
	
	public NotificheSettingPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		WaitManager.get().waitShortTime();
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}

	public void verifyLayoutPreferito() {
		SoftAssertion.get().getAssertions().assertThat(dispositivoLabel.getElement().isDisplayed()).withFailMessage("DispositivoLabel non visibile").isEqualTo(true);
		
		SoftAssertion.get().getAssertions().assertThat(questoEPreferito.getElement().isDisplayed()).withFailMessage("IsPreferito non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(ricevendoNotifiche.getElement().isDisplayed()).withFailMessage("Ricevendo Notifiche non visibile").isEqualTo(true);
//		SoftAssertion.get().getAssertions().assertThat(notificheLabel.getElement().isDisplayed()).withFailMessage("Label Notifiche non visibile").isEqualTo(true);
//		SoftAssertion.get().getAssertions().assertThat(movimentiEntrata.getElement().isDisplayed()).withFailMessage("Movimenti entrata non visibile").isEqualTo(true);
//		SoftAssertion.get().getAssertions().assertThat(movimentiUscita.getElement().isDisplayed()).withFailMessage("Movimenti uscita non visibile").isEqualTo(true);
//		SoftAssertion.get().getAssertions().assertThat(arrowSetting.getElement().isDisplayed()).withFailMessage("Arrow Setting non visibile").isEqualTo(true);
//		SoftAssertion.get().getAssertions().assertThat(siSetting.getElement().isDisplayed()).withFailMessage("SI setting non visibile").isEqualTo(true);

//		assertTrue(dispositivoLabel.getElement().isDisplayed());
//		assertTrue(questoEPreferito.getElement().isDisplayed());
//		assertTrue(ricevendoNotifiche.getElement().isDisplayed());
//		assertTrue(notificheLabel.getElement().isDisplayed());
//		assertTrue(movimentiEntrata.getElement().isDisplayed());
//		assertTrue(movimentiUscita.getElement().isDisplayed());
//		assertTrue(arrowSetting.getElement().isDisplayed());
//		assertTrue(siSetting.getElement().isDisplayed());
	}
	
	public WebElement getElementNotificaPush(WebDriver driver, String bigText) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, notificaPush, bigText);
		assertTrue(elm.isDisplayed());
		return elm;
	}
	
	public void clickOnNotificaPush(WebDriver driver, String bigText) {
		WebElement elm = getElementNotificaPush(driver, bigText);
		elm.click();
		WaitManager.get().waitHighTime();
	}
	
	public void verifyLayoutNonPreferito() {
		Particle impostaComePreferitoButton = (Particle) UiObjectRepo.get().get(NotificheSettingMolecola.IMPOSTACOMEPREFERITOBUTTON);
		Particle impostaComePreferitoText = (Particle) UiObjectRepo.get().get(NotificheSettingMolecola.IMPOSTACOMEPREFERITOTEXT);
		SoftAssertion.get().getAssertions().assertThat(impostaComePreferitoText.getElement() != null).withFailMessage("Preferito text non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(impostaComePreferitoButton.getElement() != null).withFailMessage("Preferito btn non visibile").isEqualTo(true);

//		assertTrue(impostaComePreferitoText.getElement().isDisplayed());
//		assertTrue(impostaComePreferitoButton.getElement().isDisplayed());
	}

	public void clickOnImpostaComePreferito() {
		Particle impostaComePreferitoButton = (Particle) UiObjectRepo.get().get(NotificheSettingMolecola.IMPOSTACOMEPREFERITOBUTTON);
		impostaComePreferitoButton.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public boolean isPreferito() {
		boolean isPref=false; 
		try {
			Particle impostaComePreferitoButton = (Particle) UiObjectRepo.get().get(NotificheSettingMolecola.IMPOSTACOMEPREFERITOBUTTON);
			impostaComePreferitoButton.getElement();
			isPref = true;
			
		} catch (Exception e) {
			isPref=false;
		}
		return isPref;
	}
}

