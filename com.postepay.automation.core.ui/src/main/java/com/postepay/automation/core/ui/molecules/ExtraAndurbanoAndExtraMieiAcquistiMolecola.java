package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class ExtraAndurbanoAndExtraMieiAcquistiMolecola extends Molecule {
	public static final String NAME="M225";
	
	public static final String URBANOANDEXTRAMIEIACQUISTITITOLO="urbanoAndExtraMieiAcquistiTitolo";
public static final String URBANOANDEXTRAMIEIACQUISTIICONABUS="urbanoAndExtraMieiAcquistiIconaBus";
public static final String URBANOANDEXTRAMIEIACQUISTICOMPAGNIA="urbanoAndExtraMieiAcquistiCompagnia";
public static final String URBANOANDEXTRAMIEIACQUISTILABELDATA="urbanoAndExtraMieiAcquistiLabelData";
public static final String URBANOANDEXTRAMIEIACQUISTIMESSAGGIODINAMICO="urbanoAndExtraMieiAcquistiMessaggioDinamico";
public static final String URBANOANDEXTRAMIEIACQUISTIBUTTONESCARICARICEVUTA="urbanoAndExtraMieiAcquistiButtoneScaricaRicevuta";
public static final String URBANOANDEXTRAMIEIACQUISTILABELSERVIZIFINANZIARI="urbanoAndExtraMieiAcquistiLabelServiziFinanziari";
public static final String URBANOANDEXTRAMIEIACQUISTIMESSAGGIOSTATICO="urbanoAndExtraMieiAcquistiMessaggioStatico";
public static final String URBANOANDEXTRAMIEIACQUISTITITOLOCORSA="urbanoAndExtraMieiAcquistiTitoloCorsa";


	public ExtraAndurbanoAndExtraMieiAcquistiMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

