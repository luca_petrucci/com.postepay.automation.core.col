package com.postepay.automation.core.ui.pages.scontipostedettagliot137;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.ScontiPosteM259;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import utility.SoftAssertion;

public class ScontiPosteDettaglioT137Manager extends PageManager {

	public ScontiPosteDettaglioT137Manager(Page page) {
		super(page);

	}

	public void verifyLayout() {
		Particle scontiCardTitleM259 = (Particle) UiObjectRepo.get().get(ScontiPosteM259.SCONTICARDTITLEM259);
		Particle scontiCardDescrM259 = (Particle) UiObjectRepo.get().get(ScontiPosteM259.SCONTICARDDESCRM259);
		Particle scontiCardLinkM259 = (Particle) UiObjectRepo.get().get(ScontiPosteM259.SCONTICARDLINKM259);
		Particle scontiCardImageM259 = (Particle) UiObjectRepo.get().get(ScontiPosteM259.SCONTICARDIMAGEM259);
		Particle scontiAccreditatiM259 = (Particle) UiObjectRepo.get().get(ScontiPosteM259.SCONTIACCREDITATOLABELM259);
		Particle scontiAccreditatoImportoM259 = (Particle) UiObjectRepo.get()
				.get(ScontiPosteM259.SCONTIACCREDITATOIMPORTOM259);
		Particle scontiCercaM259 = (Particle) UiObjectRepo.get().get(ScontiPosteM259.SCONTICERCAM259);
		Particle scontiCercaFiltroM259 = (Particle) UiObjectRepo.get().get(ScontiPosteM259.SCONTICERCAFILTROM259);
		Particle scontiCercaCounterM259 = (Particle) UiObjectRepo.get().get(ScontiPosteM259.SCONTICERCACOUNTERM259);
		Particle scontiStoricoM259 = (Particle) UiObjectRepo.get().get(ScontiPosteM259.SCONTISTORICOM259);

		assertTrue(scontiCardTitleM259.getElement().getText().equals("SCONTI DA ACCREDITARE"));
		assertTrue(scontiCardDescrM259.getElement().getText()
				.equals("Accumula sconti per riceverli direttamente sulla tua carta"));
		assertTrue(scontiCardLinkM259.getElement().getText().equals("Scegli dove accreditare i tuoi sconti"));
		assertTrue(scontiCardImageM259.getElement() != null);
		assertTrue(scontiAccreditatiM259.getElement().getText().equals("SCONTI ACCREDITATI"));
		assertTrue(scontiAccreditatoImportoM259.getElement().getText().contains("€"));
		assertTrue(scontiCercaM259.getElement().getText().equals("Cerca sconti"));
		assertTrue(scontiCercaFiltroM259.getElement().getText().equals("FILTRI"));
		assertTrue(scontiCercaCounterM259.getElement() != null);
		assertTrue(scontiStoricoM259.getElement().getText().equals("STORICO SCONTI"));

	}

}
