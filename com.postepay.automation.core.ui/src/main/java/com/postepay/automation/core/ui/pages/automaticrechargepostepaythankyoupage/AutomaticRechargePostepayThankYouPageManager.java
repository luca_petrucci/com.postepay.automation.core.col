package com.postepay.automation.core.ui.pages.automaticrechargepostepaythankyoupage;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.AutomaticRechargePostepayThankYouPageMolecola;
import com.postepay.automation.core.ui.molecules.ThankYouPageForTelephoneRecharge;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class AutomaticRechargePostepayThankYouPageManager extends PageManager {

	public AutomaticRechargePostepayThankYouPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void clickOnCloseButton() {
		
		Particle btn=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepayThankYouPageMolecola.CLOSEBUTTONAUTOMATICRECHARGEPOSTEPAYTHANKYOUPAGE);
		
		btn.getElement().click();
		
		try {Thread.sleep(3000);} catch (Exception err )  {}
	}
	
	public void verifyLayout() {
		Particle img=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepayThankYouPageMolecola.IMAGEAUTOMATICRECHARGEPOSTEPAYTHANKYOUPAGE);
		Particle title=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepayThankYouPageMolecola.TITLEAUTOMATICRECHARGEPOSTEPAYTHANKYOUPAGE);
		Particle copyTitle=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepayThankYouPageMolecola.COPYAUTOMATICRECHARGEPOSTEPAYTHANKYOUPAGE);
		Particle btn=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepayThankYouPageMolecola.CLOSEBUTTONAUTOMATICRECHARGEPOSTEPAYTHANKYOUPAGE);
		
		// Imagine
		assertTrue(img.getElement().isDisplayed());
		// Title
		assertTrue(title.getElement().isDisplayed());
		// Copy
		assertTrue(copyTitle.getElement().isDisplayed());
		// Bottone
		assertTrue(btn.getElement().isDisplayed());
		
		try {Thread.sleep(1000);} catch (Exception err )  {}
	}
}

