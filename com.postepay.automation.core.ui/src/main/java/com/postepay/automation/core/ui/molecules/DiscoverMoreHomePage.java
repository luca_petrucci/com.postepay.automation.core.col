package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class DiscoverMoreHomePage extends Molecule {
	public static final String NAME="M010";
	
	public static final String ENGAGMENTDISCOVERMOREABOUTHOMEPAGE="engagmentDiscoverMoreAboutHomePage";
	public static final String CARDDINAMICTITLE="cardDinamicTitle";
	public static final String CARDDINAMICDESCRIPTION="cardDinamicDescription";
	public static final String CARDDINAMICBUTTON="cardDinamicButton";
	public static final String SBRODOLO="sbrodolo";
	public static final String CARDDINAMICSCOPRI="cardDinamicScopri";
	public static final String LASTCARDONPAGE="lastCardOnPage";
	public static final String INTEROCASSETTOHOMEPAGE = "interoCassettoHomePage";
	
	public DiscoverMoreHomePage(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

