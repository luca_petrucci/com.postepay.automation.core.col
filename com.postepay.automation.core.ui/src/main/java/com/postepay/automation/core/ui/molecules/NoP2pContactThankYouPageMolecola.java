package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class NoP2pContactThankYouPageMolecola extends Molecule {
	public static final String NAME="M205";

	public static final String TEXTNOP2PCONTACTTHANKYOU="textNoP2pContactThankYou";
	public static final String BUTTONNOP2PCONTACTTHANKYOU="buttonNoP2pContactThankYou";
	public static final String BACKNOP2PCONTACTTHANKYOU="backNoP2pContactThankYou";
	public static final String IMAGENOP2PCONTACTTHANKYOU="imageNoP2pContactThankYou";
	public static final String CHIUDIINVITAAMICI="chiudiInvitaAmici";


	public NoP2pContactThankYouPageMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

