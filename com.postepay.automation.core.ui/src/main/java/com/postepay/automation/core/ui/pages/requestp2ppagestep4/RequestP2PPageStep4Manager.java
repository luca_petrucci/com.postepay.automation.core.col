package com.postepay.automation.core.ui.pages.requestp2ppagestep4;

import com.postepay.automation.core.ui.molecules.AmoutCreditToSend;

import io.appium.java_client.AppiumDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class RequestP2PPageStep4Manager extends PageManager {

	public RequestP2PPageStep4Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void insetAmoutToRequest(String amount) {

		Particle amountEditText=(Particle) UiObjectRepo.get().get(AmoutCreditToSend.AMOUNTSENDP2P);

		amountEditText.getElement().sendKeys(amount);
		
//		((AppiumDriver<?>)page.getDriver()).hideKeyboard();	
		
		WaitManager.get().waitShortTime();
	}

	public void clickOnConfirmButton() {

		Particle btn=(Particle) UiObjectRepo.get().get(AmoutCreditToSend.BUTTONSENDP2P);

		btn.getElement().click();	

		WaitManager.get().waitMediumTime();

	}
}

