package com.postepay.automation.core.ui.pages.genericerrorpopup;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.postepay.automation.core.ui.molecules.GenericErrorPopUpMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import test.automation.core.UIUtils;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.CoreUtility;

public class GenericErrorPopUpManager extends PageManager {

	public GenericErrorPopUpManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public boolean checkIfElementIsPresent() {

		Particle txt = (Particle) UiObjectRepo.get().get(GenericErrorPopUpMolecola.TEXTTITLEGENERICERRORPOPUP);

		boolean isVisibel;

		try {
			if (ExpectedConditions.visibilityOf(txt.getElement()) != null) {
				assertTrue(txt.getElement().isDisplayed());
				isVisibel = true;
			} else {
				isVisibel = false;
			}
		} catch (Exception e) {
			isVisibel = false;
		}
		return isVisibel;
	}

	public void verifyHeaderPopUp() {

		Particle txt = (Particle) UiObjectRepo.get().get(GenericErrorPopUpMolecola.TEXTTITLEGENERICERRORPOPUP);

		assertTrue(txt.getElement().isDisplayed());
	}

	public void verifyCopyPopUp() {

		Particle txt = (Particle) UiObjectRepo.get().get(GenericErrorPopUpMolecola.TEXTBODYGENERICERRORPOPUP);

		assertTrue(txt.getElement().isDisplayed());
	}

	public void clickOnOKPopUp() {

		Particle btn = (Particle) UiObjectRepo.get().get(GenericErrorPopUpMolecola.OKPOSTEACCOUNTGENERICERRORPOPUP);
		
		
		WebDriverWait wait = new WebDriverWait(page.getDriver(), 10);
		wait.until(ExpectedConditions.presenceOfElementLocated(btn.getXPath())).click();
//		btn.getElement().click();
     	System.out.println("ok");

		WaitManager.get().waitMediumTime();
	}

	public void verifyLayoutOfPopup(WebDriver driver, String titleText, String bodyText, boolean isIOS) {
		LayoutTools tool = new LayoutTools();

		tool.verifyLayoutOfPopupGeneric(driver, titleText, bodyText, isIOS);
	}

	public void verifyLayoutOfGpsPopupIos(WebDriver driver, String titleText, String bodyText, boolean isIOS) {
		LayoutTools tool = new LayoutTools();

		tool.verifyLayoutOfGpsPopupIos(driver, titleText, bodyText, isIOS);
	}

	public void verifyLayoutOfPopupIOS(WebDriver driver, String titleText, String bodyText, boolean isIOS) {
		LayoutTools tool = new LayoutTools();

		tool.verifyLayoutOfPopupGeneric(driver, titleText, bodyText, isIOS);
	}

	public void clickOnNoGrazie() {
		Particle noGrazie = (Particle) UiObjectRepo.get().get(GenericErrorPopUpMolecola.NOGRAZIERATINGPOPUP);
		noGrazie.getElement().click();
		WaitManager.get().waitShortTime();
	}

	public void isRatingPopUp() {
//		Particle ratingPopUp = (Particle) UiObjectRepo.get().get(GenericErrorPopUpMolecola.TITLERATINGPOPUP);
		Particle noGrazie = (Particle) UiObjectRepo.get().get(GenericErrorPopUpMolecola.NOGRAZIERATINGPOPUP);

		if (noGrazie != null) {
			noGrazie.getElement().click();
			WaitManager.get().waitMediumTime();
		}
	}

	public void isPermissionRequired() {
		Particle permissionPopUp = (Particle) UiObjectRepo.get().get(GenericErrorPopUpMolecola.ALLOWPERMISSIONBUTTON);
		if (permissionPopUp != null) {
			permissionPopUp.getElement().click();
			WaitManager.get().waitShortTime();
		}
	}

	public void closeAtacPopup() {
		Particle buttonOK = (Particle) UiObjectRepo.get().get(GenericErrorPopUpMolecola.ATACBUTTONOKM202);
		if (buttonOK != null) {
			buttonOK.getElement().click();
			WaitManager.get().waitMediumTime();

			try {
				Particle buttonOKpostMsg = (Particle) UiObjectRepo.get().get(GenericErrorPopUpMolecola.ATACPOSTOKM202);
				buttonOKpostMsg.getElement().click();
				WaitManager.get().waitShortTime();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

	public void isPermissionRequiredSamsung(String text) {
		WebDriver driver = UIUtils.ui().getCurrentDriver();

		Particle nuovoLocator = (Particle) UiObjectRepo.get().get(GenericErrorPopUpMolecola.ALLOWPERMISSIONBUTTONSMG);
		String permissionPopUp = Utility.replacePlaceHolders(nuovoLocator.getLocator(), new Entry("title", text));
		System.out.println(permissionPopUp);

//		if (permissionPopUp != null) {
//			driver.findElement(By.xpath(permissionPopUp)).click();
		CoreUtility.visibilityOfElement(page.getDriver(), By.xpath(permissionPopUp), 20).click();

//			permissionPopUp.getElement().click();
			WaitManager.get().waitMediumTime();
//		}
	}

	public void isPermissionPositionSamsung() {
		Particle permissionPopUp = (Particle) UiObjectRepo.get()
				.get(GenericErrorPopUpMolecola.ALLOWPERMISSIONPOSITIONSMG);
		if (permissionPopUp != null) {
			permissionPopUp.getElement().click();
			WaitManager.get().waitMediumTime();
		}
	}

	public void isPermissionGeneric() {
		Particle permissionPopUp = (Particle) UiObjectRepo.get().get(GenericErrorPopUpMolecola.GENERICAUTHORIZATION);
			permissionPopUp.getElement().click();
			WaitManager.get().waitMediumTime();
	}

	public void GPayFunnel() {
		Particle p = (Particle) UiObjectRepo.get().get(GenericErrorPopUpMolecola.ALLOWPERMISSIONBUTTONSMG);

		String txt1 = "Continua";
		String txt2 = "Espandi";
		String txt3 = "Accetta e continua";
		String txt4 = "OK";

		String continuaL = Utility.replacePlaceHolders(p.getLocator(), new Entry("title", txt1));
		String espandiL = Utility.replacePlaceHolders(p.getLocator(), new Entry("title", txt2));
		String accettaL = Utility.replacePlaceHolders(p.getLocator(), new Entry("title", txt3));
		String okL = Utility.replacePlaceHolders(p.getLocator(), new Entry("title", txt4));
		// Click
		page.getDriver().findElement(By.xpath(continuaL)).click();
		;
		WaitManager.get().waitMediumTime();
		page.getDriver().findElement(By.xpath(continuaL)).click();
		;
		WaitManager.get().waitMediumTime();
		page.getDriver().findElement(By.xpath(espandiL)).click();
		;
		WaitManager.get().waitMediumTime();
		page.getDriver().findElement(By.xpath(accettaL)).click();
		;
		WaitManager.get().waitMediumTime();
		page.getDriver().findElement(By.xpath(okL)).click();
		;
		WaitManager.get().waitMediumTime();
		page.getDriver().findElement(By.xpath("//*[@resource-id='com.google.android.gms:id/cancel_button']")).click();
		;
		WaitManager.get().waitMediumTime();
	}

	public boolean isLocationVisible() {
		Particle p = (Particle) UiObjectRepo.get().get(GenericErrorPopUpMolecola.ALLOWPERMISSIONBUTTONSMG);
		String geoLocation = Utility.replacePlaceHolders(p.getLocator(), new Entry("title", "Geolocalizzazione"));
		String location = Utility.replacePlaceHolders(p.getLocator(), new Entry("title", "Location"));
		String posizione = Utility.replacePlaceHolders(p.getLocator(), new Entry("title", "Posizione"));
		try {
			page.getDriver().findElement(By.xpath(geoLocation));
			return true;
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			page.getDriver().findElement(By.xpath(location));
			return true;
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			page.getDriver().findElement(By.xpath(posizione));
			return true;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return false;
	}

	public void verifyEmptyFieldError(String emptyField) {
		Particle titolo = (Particle) UiObjectRepo.get().get(GenericErrorPopUpMolecola.TEXTTITLEGENERICERRORPOPUP);
		String elTitles = titolo.getElement().getText();
		assertTrue(elTitles.trim().equals("Avviso"));
		Particle titolo1 = (Particle) UiObjectRepo.get().get(GenericErrorPopUpMolecola.TEXTBODYGENERICERRORPOPUP);
		String elTitles1 = titolo1.getElement().getText();
		switch (emptyField) {
		
		case "iban":
			if(page.getDriver() instanceof IOSDriver)
			{
				assertTrue(elTitles1.trim().equals("Verifica i dati inseriti"));
			} else {
			assertTrue(elTitles1.trim().equals("L'IBAN è obbligatorio."));
			}
			break;
		case "beneficiario":
			assertTrue(elTitles1.trim().equals("L'intestatario è obbligatorio."));
			break;
		case "importo":
			assertTrue(elTitles1.trim().equals("L'importo è obbligatorio."));
			break;
		case "causale":
			assertTrue(elTitles1.trim().equals("La causale è obbligatoria."));
			break;
		default:
			break;
		}
	 Particle okButton = (Particle)UiObjectRepo.get().get(GenericErrorPopUpMolecola.OKPOSTEACCOUNTGENERICERRORPOPUP);
	 okButton.getElement().click();
	 WaitManager.get().waitMediumTime();
   }

	public void clickOnApplePayPopupCloseBtn() 
	{
		
		try {
			Particle okButton = (Particle)UiObjectRepo.get().get(GenericErrorPopUpMolecola.APPLEPAYCLOSEBTN);
			System.out.println("apple pay check");
			UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.presenceOfElementLocated(okButton.getXPath()),10);
			okButton.getElement().click();
			System.out.println("apple pay click");
		}catch(Exception err)
		{
			
		}
	}
	
	public void clickOnNonMostrarePiu()
	{
		try {
			Particle nonMostrareButton = (Particle)UiObjectRepo.get().get(GenericErrorPopUpMolecola.NONMOSTRAREPIUBUTTON);
			System.out.println("Non mostrare più");
			UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.presenceOfElementLocated(nonMostrareButton.getXPath()),10);
			nonMostrareButton.getElement().click();
			System.out.println("Non mostrare più");
		}catch(Exception err) { 
			
		}
		
		try {
			Particle nonMostrareButton = (Particle)UiObjectRepo.get().get(GenericErrorPopUpMolecola.NONMOSTRAREPIUBUTTON);
			nonMostrareButton.getElement().click();
			System.out.println("Non mostrare più");
		}catch(Exception err) {
			
		}
	}
	//*[contains(@resource-id,'permission_allow')]
	public void genericPermission(String permissionTextButton) {
		String loc = "//*[contains(@resource-id,'"+permissionTextButton+"')]";
		page.getDriver().findElement(By.xpath(loc)).click();
		WaitManager.get().waitShortTime();
	}

	public void clickOnChiudiRicaricaDopoOnb() {
		Particle chiudi = (Particle)UiObjectRepo.get().get(GenericErrorPopUpMolecola.CHIUDIRICARICAPOSTONB);
		try {
			System.out.println("Non mostrare più");
			UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.presenceOfElementLocated(chiudi.getXPath()),20)
			.click();
			System.out.println("Non mostrare più");
		}catch(Exception err) {}
		
	}

	public boolean isUpdate() {
		boolean isUp = true;
		
		// Prendo il pottone e verifico se c'è la popup con aggiorna
		Particle btn = (Particle) UiObjectRepo.get().get(GenericErrorPopUpMolecola.OKPOSTEACCOUNTGENERICERRORPOPUP);
				
		WebDriverWait wait = new WebDriverWait(page.getDriver(), 10);
		String btnText = wait.until(ExpectedConditions.presenceOfElementLocated(btn.getXPath())).getText();

		
		if(btnText.toLowerCase().contains("aggiorna")) {
			isUp = false;
		}
		return isUp;
	}

	public void clickOnNotUpdte() {
		Particle btn = (Particle) UiObjectRepo.get().get(GenericErrorPopUpMolecola.NONORARATINGPOPUP);
		
		WebDriverWait wait = new WebDriverWait(page.getDriver(), 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(btn.getXPath())).click();

	}
}
