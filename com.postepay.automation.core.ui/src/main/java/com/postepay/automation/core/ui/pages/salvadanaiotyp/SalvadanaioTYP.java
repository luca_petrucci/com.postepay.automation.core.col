package com.postepay.automation.core.ui.pages.salvadanaiotyp;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.SalvadanaioTYPMolecola;

import io.appium.java_client.android.AndroidDriver;


public class SalvadanaioTYP extends Page {
	public static final String NAME="T118";
	

	public SalvadanaioTYP(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(SalvadanaioTYPMolecola.NAME, UiObjectRepo.get().get(SalvadanaioTYPMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SalvadanaioTYPManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyLayout(WebDriver driver, String type, double discrepanza) {
		((SalvadanaioTYPManager)this.manager).verifyLayout(driver, type, discrepanza);
	}
	
	public void clickOnClose() {
		((SalvadanaioTYPManager)this.manager).clickOnClose();
	}
}

