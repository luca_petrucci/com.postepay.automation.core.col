package com.postepay.automation.core.ui.pages.sendp2ppagestep4;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.AmoutCreditToSendSummary;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;


public class SendP2PPageStep4 extends Page {
	public static final String NAME="T011";


	public SendP2PPageStep4(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	protected void onInit() {
		this.addToTemplate(AmoutCreditToSendSummary.NAME, UiObjectRepo.get().get(AmoutCreditToSendSummary.NAME), true);
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {

		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {

		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {

		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SendP2PPageStep4Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {

		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {

		return null;
	}

	public void clickOnSendButton() {
		((SendP2PPageStep4Manager)this.manager).clickOnSendButton();
	}
}

