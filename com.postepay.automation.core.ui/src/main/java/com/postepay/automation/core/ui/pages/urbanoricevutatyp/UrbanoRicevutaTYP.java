package com.postepay.automation.core.ui.pages.urbanoricevutatyp;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.UrbanoRicevutaTYPMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutImage;

import io.appium.java_client.android.AndroidDriver;


public class UrbanoRicevutaTYP extends Page {
	public static final String NAME="T105";
	

	public UrbanoRicevutaTYP(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(UrbanoRicevutaTYPMolecola.NAME, UiObjectRepo.get().get(UrbanoRicevutaTYPMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new UrbanoRicevutaTYPManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderPage(String inputTitle) {
		((UrbanoRicevutaTYPManager)this.manager).verifyHeaderPage(inputTitle);
	}
	
	public void verifyLayoutPage() {
		((UrbanoRicevutaTYPManager)this.manager).verifyLayoutPage();
	}
	
	public void verifyDataInRicevuta(String importoInput, String dataInput, String cardInput) {
		((UrbanoRicevutaTYPManager)this.manager).verifyDataInRicevuta(importoInput, dataInput, cardInput);
	}
	
	public void verifyImageCard(WebDriver driver, double discrepanza) {

		((UrbanoRicevutaTYPManager)this.manager).verifyImageCard(driver, discrepanza);
	}
	
	public void clickOnMieiAcquisti() {
		((UrbanoRicevutaTYPManager)this.manager).clickOnMieiAcquisti();
	}
}

