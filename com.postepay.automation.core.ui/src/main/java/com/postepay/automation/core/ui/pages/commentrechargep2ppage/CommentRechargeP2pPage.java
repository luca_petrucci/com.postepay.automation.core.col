package com.postepay.automation.core.ui.pages.commentrechargep2ppage;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.CommentRechargeP2pMolecola;


public class CommentRechargeP2pPage extends Page {
	public static final String NAME="T087";
	

	public CommentRechargeP2pPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(CommentRechargeP2pMolecola.NAME, UiObjectRepo.get().get(CommentRechargeP2pMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new CommentRechargeP2pPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void inserComment(String comment) {
		
	 ((CommentRechargeP2pPageManager)this.manager).inserComment(comment);
		
	}
	
	public void clickOnConfirm() {
		((CommentRechargeP2pPageManager)this.manager).clickOnConfirm();
	}

}

