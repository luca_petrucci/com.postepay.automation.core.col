package com.postepay.automation.core.ui.pages.rechargemypostepaypagestep3;

import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.SelectACardToPayTheRechargeMyPostepay;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;


public class RechargeMyPostepayPageStep3 extends Page {
	public static final String NAME="T041";
	

	public RechargeMyPostepayPageStep3(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(SelectACardToPayTheRechargeMyPostepay.NAME, UiObjectRepo.get().get(SelectACardToPayTheRechargeMyPostepay.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new RechargeMyPostepayPageStep3Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		((RechargeMyPostepayPageStep3Manager)this.manager).verifyHeaderpage(titlePageInput);
	}
	public void verifyLayoutPage(WebDriver driver, String titleInput, String typeInput, String numberInput, double discrepanza) {
		((RechargeMyPostepayPageStep3Manager)this.manager).verifyLayoutPage(driver, titleInput, typeInput, numberInput, discrepanza);
	}

	public void selectCardToRecharge(WebDriver driver, String numberInput) {
		((RechargeMyPostepayPageStep3Manager)this.manager).selectCardToRecharge(driver, numberInput);
	}

	public void clickOnBancoPostaButton() {
		((RechargeMyPostepayPageStep3Manager)this.manager).clickOnBancoPostaButton();
	}
	
	public void clickOnOtherCards() {
		((RechargeMyPostepayPageStep3Manager)this.manager).clickOnOtherCards();
	}
	
	public void verifyLayoutScontiPoste(WebDriver driver, String titleInput, String typeInput, String numberInput) {
		((RechargeMyPostepayPageStep3Manager)this.manager).verifyLayoutScontiPoste(driver, titleInput, typeInput, numberInput);
	}
	
	public void clickOnCardToSetAsPreferred(WebDriver driver, String numberInput) {
		((RechargeMyPostepayPageStep3Manager)this.manager).clickOnCardToSetAsPreferred(driver, numberInput);
	}
	
	public String whoIsPreferredCard() {
		return ((RechargeMyPostepayPageStep3Manager)this.manager).whoIsPreferredCard();
	}
	
	public void selectAsPreferredOtherCard() {
		((RechargeMyPostepayPageStep3Manager)this.manager).selectAsPreferredOtherCard();
	}
	
	public String whoIsNotPreferredCard() {
		return ((RechargeMyPostepayPageStep3Manager)this.manager).whoIsNotPreferredCard();
	}
	public void clickOnBackArrow() {
		((RechargeMyPostepayPageStep3Manager)this.manager).clickOnBackArrow();
	}
}

