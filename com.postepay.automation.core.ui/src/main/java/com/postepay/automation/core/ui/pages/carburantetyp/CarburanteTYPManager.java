package com.postepay.automation.core.ui.pages.carburantetyp;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.CarburanteTYPMolecola;
import com.postepay.automation.core.ui.molecules.ThankYouPageSendG2G;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import ui.core.support.waitutil.WaitManager;

import io.appium.java_client.android.AndroidDriver;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class CarburanteTYPManager extends PageManager {
	
	LayoutTools toolLayout = new LayoutTools();
	
	Particle icon = (Particle) UiObjectRepo.get().get(CarburanteTYPMolecola.TYPCARBURANTEIMAGE);
	Particle title = (Particle) UiObjectRepo.get().get(CarburanteTYPMolecola.TYPCARBURANTETITOLO);
	Particle description = (Particle) UiObjectRepo.get().get(CarburanteTYPMolecola.TYPCARBURANTEDESCRIZIONE);
	Particle points = (Particle) UiObjectRepo.get().get(CarburanteTYPMolecola.TYPCARBURANTEPALLINI);
	Particle button = (Particle) UiObjectRepo.get().get(CarburanteTYPMolecola.TYPCARBURANTEBOTTONE);
	Particle buttonX = (Particle) UiObjectRepo.get().get(CarburanteTYPMolecola.TYPCARBURANTEBOTTONEX);
	
	String title1 = "Transazione avvenuta con successo, ora solleva l'erogatore!";
	String description1 = "L’erogatore 1 è pronto. Hai 2 minuti per iniziare il tuo rifornimento";
	String title2 = "Fai il tuo rifornimento";
	String description2 = "Una volta raggiunto l’importo definito, l’erogazione verrà terminata";
	String title3 = "La tua ricevuta di pagamento";
	String description3 = "Al termine dell’erogazione, riceverai la conferma dell’avvenuto pagamento";
	
	String img1 = "tut_1";
	String img2 = "tut_2";
	String img3 = "tut_3";
	
	
	public CarburanteTYPManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyText(WebDriver driver, int i) {
		WebElement elmTitolo;
		WebElement elmDescrizione;
		switch (i) {
		case 1:
			elmTitolo = toolLayout.replaceGenericPathOfElement(driver, title, title1);
			elmDescrizione = toolLayout.replaceGenericPathOfElement(driver, description, description1);
			break;
		case 2:
			elmTitolo = toolLayout.replaceGenericPathOfElement(driver, title, title2);
			elmDescrizione = toolLayout.replaceGenericPathOfElement(driver, description, description2);
			break;
		case 3:
			elmTitolo = toolLayout.replaceGenericPathOfElement(driver, title, title3);
			elmDescrizione = toolLayout.replaceGenericPathOfElement(driver, description, description3);
			break;
		default:
			elmTitolo = toolLayout.replaceGenericPathOfElement(driver, title, title1);
			elmDescrizione = toolLayout.replaceGenericPathOfElement(driver, description, description1);
			break;
		}
		assertTrue(elmTitolo.isDisplayed());
		assertTrue(elmDescrizione.isDisplayed());
	}
	
	public void verifyImageCard(WebDriver driver, int i, double discrepanza) {
		String img; 
		switch (i) {
		case 1:
			img = "carburante/"+img1;	
			break;
		case 2:	
			img = "carburante/"+img2;				
			break;
		case 3:
			img = "carburante/"+img3;					
			break;
		default:
			img = "carburante/"+img1;	
			break;
		}
		LayoutImage.get().verifyImage(driver, img, discrepanza);
	}
	
	public void verifyActionBar(WebDriver driver, int i) {
		String pnt1L=Utility.replacePlaceHolders(points.getLocator(), new Entry("index", "0"));
		String pnt2L=Utility.replacePlaceHolders(points.getLocator(), new Entry("index", "1"));
		String pnt3L=Utility.replacePlaceHolders(points.getLocator(), new Entry("index", "2"));
		
		WebElement pnt1E =  driver.findElement(By.xpath(pnt1L));
		WebElement pnt2E =  driver.findElement(By.xpath(pnt2L));
		WebElement pnt3E =  driver.findElement(By.xpath(pnt3L));

		if (i == 1) {			
			assertTrue(pnt1E.isSelected());
			assertFalse(pnt2E.isSelected());
			assertFalse(pnt3E.isSelected());
		} 
		else if (i == 2) {
			assertFalse(pnt1E.isSelected());
			assertTrue(pnt2E.isSelected());
			assertFalse(pnt3E.isSelected());			
		} 
		else if (i == 3) {
			assertFalse(pnt1E.isSelected());
			assertFalse(pnt2E.isSelected());			
			assertTrue(pnt3E.isSelected());			
		}
	}
	
	public void verifyLayoutPage(WebDriver driver, int indexPoints, double discrepanza) {
		assertTrue(icon.getElement().isDisplayed());
		verifyImageCard(driver, indexPoints, discrepanza);
		
		verifyText(driver, indexPoints);
		
		verifyActionBar(driver, indexPoints);
		
		assertTrue(button.getElement().isDisplayed());
		assertTrue(buttonX.getElement().isDisplayed());
		
	}
	
	public void swipeTo(WebDriver driver, int i, String direction) {
		WebElement elmTitolo;
		switch (i) {
		case 1:
			elmTitolo = toolLayout.replaceGenericPathOfElement(driver, title, title1);
			break;
		case 2:
			elmTitolo = toolLayout.replaceGenericPathOfElement(driver, title, title2);
			break;
		case 3:
			elmTitolo = toolLayout.replaceGenericPathOfElement(driver, title, title3);
			break;
		default:
			elmTitolo = toolLayout.replaceGenericPathOfElement(driver, title, title1);
			break;
		}
		
		int y = elmTitolo.getSize().getHeight() / 2;
		int x = elmTitolo.getSize().getWidth() / 2;
		
		int nY = y;
		int nX;
		switch (direction) {
		case "RIGHT":
			nX = x - 500;
			break;
		case "LEFT":
			nX = x + 500;
			break;
		default:
			nX = x - 500;
			break;
		}		
		toolLayout.scrollWithCoordinate(driver, x, nX, y, nY);
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnX() {
		buttonX.getElement().click();
		WaitManager.get().waitLongTime();
	}
	

}

