package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class DettailsMessageConfirmRemovingAutomaticRechargeMolecola extends Molecule {
	public static final String NAME="M212";

	public static final String MESSAGEDETTAILSMESSAGECONFIRMREMOVINGAUTOMATICRECHARGE="messageDettailsMessageConfirmRemovingAutomaticRecharge";
	public static final String HEADERDETTAILSMESSAGECONFIRMREMOVINGAUTOMATICRECHARGE="headerDettailsMessageConfirmRemovingAutomaticRecharge";
	public static final String CLOSEDETTAILSMESSAGECONFIRMREMOVINGAUTOMATICRECHARGE="closeDettailsMessageConfirmRemovingAutomaticRecharge";
	public static final String TYPEMESSAGE="typeMessage";
	public static final String DATEMESSAGE="dateMessage";
	public static final String NOMEMESSAGE="nomeMessage";
	public static final String TXTMESSAGE="txtMessage";
	public static final String PIEMESSAGE="pieMessage";
	public static final String DINAMICMESSAGE="dinamicMessage";

	public static final String DATEM212="dateMessageM212";
	public static final String NOMEM212="nomeMessageM212";
	public static final String TXTM212="txtMessageM212";
	public static final String TYPEM212="typeMessageM212";
	public static final String ELIMINAM212="btnEliminaM211";

	

	public DettailsMessageConfirmRemovingAutomaticRechargeMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

