package com.postepay.automation.core.ui.pages.preloginpostepage;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.PreLoginPostePageMolecola;
import com.postepay.automation.core.ui.verifytools.BasicLayoutControl;

import io.appium.java_client.android.AndroidDriver;


public class PreLoginPostePage extends Page {
	public static final String NAME="T130";


	public PreLoginPostePage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	protected void onInit() {
		this.addToTemplate(PreLoginPostePageMolecola.NAME, UiObjectRepo.get().get(PreLoginPostePageMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {

		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {

		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {

		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new PreLoginPostePageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {

		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {

		return null;
	}


	public void verifyLayout(WebDriver driver, double discrepanza) {

		((PreLoginPostePageManager)this.manager).verifyLayout(driver, discrepanza);

	}

	public void clickOnAccedi() {

		((PreLoginPostePageManager)this.manager).clickOnAccedi();

	}

	public void clickOnScopri() {

		((PreLoginPostePageManager)this.manager).clickOnScopri();

	}
}

