package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class ServiziTYPMolecola extends Molecule {
	public static final String NAME="M223";
	
	public static final String SERVIZITYPILTUOBIGLIETTO="serviziTYPIlTuoBiglietto";
public static final String SERVIZITYPVAIALLARICEVUTADIPAGAMENTO="serviziTYPVaiAllaRicevutaDiPagamento";
public static final String SERVIZITYPTITOLO="serviziTYPTitolo";
public static final String SERVIZITYPIMAGE="serviziTYPImage";
public static final String SERVIZITYPDESCRIZIONE="serviziTYPDescrizione";
public static final String SERVIZITITOLOTALLONCINO ="serviziTitoloTalloncino";
public static final String SERVIZIIMAGETALLONCINO ="serviziImageTalloncino";
public static final String SERVIZITITLETALLONCINO ="serviziTitleTalloncino";
public static final String SERVIZIDESCRIPTIONTALLONCINO ="serviziDescriptionTalloncino";
public static final String SERVIZIMESSAGGIOTALLONCINO ="serviziMessaggioTalloncino";
public static final String SERVIZISTAMPABUTTONTALLONCINO ="serviziStampaButtonTalloncino";
public static final String SERVIZICLOSEBUTTONTALLONCINO ="serviziCloseButtonTalloncino";
public static final String SERVIZIINTERROMPIBUTTONBIGLIETTO ="serviziInterrompiButtonBiglietto";
public static final String SERVIZIVAIMIEIACQUISTIBUTTON ="serviziVaiAiMieiAcquistiButton";
public static final String SERVIZIBACKARROWIMIEIACQUISTI ="serviziBackArrowIMieiAcquisti";
public static final String SERVIZICLOSEBUTTONRICEVUTA ="serviziCloseButtonRicevuta";

	public ServiziTYPMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

