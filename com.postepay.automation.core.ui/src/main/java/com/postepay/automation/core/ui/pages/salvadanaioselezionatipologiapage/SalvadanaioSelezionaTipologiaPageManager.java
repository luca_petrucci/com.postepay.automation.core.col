package com.postepay.automation.core.ui.pages.salvadanaioselezionatipologiapage;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.SalvadanaioSelezionaTipologiaMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class SalvadanaioSelezionaTipologiaPageManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();

	Particle titleView = (Particle) UiObjectRepo.get().get(SalvadanaioSelezionaTipologiaMolecola.SALVADANAIOSELEZIONAVERSAMENTOTITOLO);
	Particle labelView = (Particle) UiObjectRepo.get().get(SalvadanaioSelezionaTipologiaMolecola.SALVADANAIOSELEZIONAVERSAMENTOLABEL);
	Particle singoloTitle = (Particle) UiObjectRepo.get().get(SalvadanaioSelezionaTipologiaMolecola.SALVADANAIOVERSAMENTOSINGOLOTITLE);
	Particle singoloSubTitle = (Particle) UiObjectRepo.get().get(SalvadanaioSelezionaTipologiaMolecola.SALVADANAIOVERSAMENTOSINGOLOSUBTITLE);
	Particle arrotondamentoTitle = (Particle) UiObjectRepo.get().get(SalvadanaioSelezionaTipologiaMolecola.SALVADANAIOVERSAMENTOARROTONDAMENTOTITLE);
	Particle arrotondamentoSubTitle = (Particle) UiObjectRepo.get().get(SalvadanaioSelezionaTipologiaMolecola.SALVADANAIOVERSAMENTOARROTONDAMENTOSUBTITLE);
	Particle budgetTitle = (Particle) UiObjectRepo.get().get(SalvadanaioSelezionaTipologiaMolecola.SALVADANAIOVERSAMENTOBUDGETTITLE);
	Particle budgetSubTitle = (Particle) UiObjectRepo.get().get(SalvadanaioSelezionaTipologiaMolecola.SALVADANAIOVERSAMENTOBUDGETSUBTITLE);
	Particle ricorrenteTitle = (Particle) UiObjectRepo.get().get(SalvadanaioSelezionaTipologiaMolecola.SALVADANAIOVERSAMENTORICORRENTETITLE);
	Particle ricorrenteSubTitle = (Particle) UiObjectRepo.get().get(SalvadanaioSelezionaTipologiaMolecola.SALVADANAIOVERSAMENTORICORRENTESUBTITLE);
	
	public SalvadanaioSelezionaTipologiaPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}

	public void verifyLayoutPage() {

		assertTrue(titleView.getElement().isDisplayed());
		assertTrue(labelView.getElement().isDisplayed());
		assertTrue(singoloSubTitle.getElement().isDisplayed());
		assertTrue(arrotondamentoTitle.getElement().isDisplayed());
		assertTrue(arrotondamentoSubTitle.getElement().isDisplayed());
		assertTrue(budgetTitle.getElement().isDisplayed());
		assertTrue(budgetSubTitle.getElement().isDisplayed());
		assertTrue(ricorrenteTitle.getElement().isDisplayed());
		assertTrue(ricorrenteSubTitle.getElement().isDisplayed());
	}
	
	public void clickOnRicorrente() {
		ricorrenteTitle.getElement().click();
		WaitManager.get().waitMediumTime();
	}
}

