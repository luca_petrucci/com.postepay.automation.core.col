package com.postepay.automation.core.ui.pages.accessandauthorizationpage;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;

import com.postepay.automation.core.ui.molecules.EnableFingerPrintPopup;
import com.postepay.automation.core.ui.molecules.FingerPrintAccessMolecola;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.ToggleAccessAndAuthorizationSettingPosteId;


public class AccessAndAuthorizationPage extends Page {
	public static final String NAME="T066";
	

	public AccessAndAuthorizationPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(ToggleAccessAndAuthorizationSettingPosteId.NAME, UiObjectRepo.get().get(ToggleAccessAndAuthorizationSettingPosteId.NAME), true);
		this.addToTemplate(EnableFingerPrintPopup.NAME, UiObjectRepo.get().get(EnableFingerPrintPopup.NAME), false);
		this.addToTemplate(FingerPrintAccessMolecola.NAME, UiObjectRepo.get().get(FingerPrintAccessMolecola.NAME), false);
	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new AccessAndAuthorizationPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void clickOnAccessWithFingerprint() {
		((AccessAndAuthorizationPageManager)this.manager).clickOnAccessWithFingerprint();
	}
	
	public void clickOnAuthorizationWithPosteId() {
		((AccessAndAuthorizationPageManager)this.manager).clickOnAuthorizationWithFingerprint();
	}
	
	public void verifyIfAccessToggle(String statusToggle) {
		((AccessAndAuthorizationPageManager)this.manager).verifyIfAccessToggle(statusToggle);
	}
	
	public void verifyIfAuthorizationToggle(String statusToggle) {
		((AccessAndAuthorizationPageManager)this.manager).verifyIfAuthorizationToggle(statusToggle);
	}
	
	public void verifyLayoutPopupEnableFingerprint() {
		((AccessAndAuthorizationPageManager)this.manager).verifyLayoutPopupEnableFingerprint();
	}
	
	public void clickOnEnableOnPopupFingerprint() {
		((AccessAndAuthorizationPageManager)this.manager).clickOnEnableOnPopupFingerprint();
	}
	
	// Popup di inserimento finger print
		public void verifyLayoutPopupSendFingerprint() {
			((AccessAndAuthorizationPageManager)this.manager).verifyLayoutPopupSendFingerprint();
		}
		
		public void sendFingerprint(String adbExe, String deviceName, int fingerId) throws Exception {
			((AccessAndAuthorizationPageManager)this.manager).sendFingerprint(adbExe, deviceName, fingerId);
		}
}

