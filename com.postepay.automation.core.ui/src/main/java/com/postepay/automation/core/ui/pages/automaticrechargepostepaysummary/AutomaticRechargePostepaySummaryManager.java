package com.postepay.automation.core.ui.pages.automaticrechargepostepaysummary;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;

import com.postepay.automation.core.ui.molecules.AutomaticRechargePostepaySummaryMolecola;
import com.postepay.automation.core.ui.molecules.BodyRechargeMyPostepaySummary;
import com.postepay.automation.core.ui.molecules.SelectRecurrenceAutomaticRechargePostepayMolecula;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class AutomaticRechargePostepaySummaryManager extends PageManager {

	public AutomaticRechargePostepaySummaryManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifySubTitleOfAutomaticRechargeSummary() {
		Particle p=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySummaryMolecola.SUBTITLERECHARGESUMMARYAUTOMATICRECHARGEPOSTEPAYSUMMARY);
		String nuovoLocator=Utility.replacePlaceHolders(p.getLocator(), new Entry("title","Riepilogo ricarica automatica"));

		((AppiumDriver<?>)page.getDriver()).findElement(By.xpath(nuovoLocator)).click();

		try {Thread.sleep(1000);} catch (Exception err )  {}
	}
	
	public void verifyHeaderPage(String inputTitle) throws Error {
		
		LayoutTools layOutTool= new LayoutTools();
		layOutTool.verifyHeaderPage(inputTitle);
	}
	
	public String getNameTargetUser() {

		Particle intestatario=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySummaryMolecola.SUMMARYHOLDERRECHARGEPAYPPAUTOMATICRECHARGEPOSTEPAYSUMMARY);

		return intestatario.getElement().getText();
	}

	public String getCardNumberTargetUser() {

		Particle iban=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySummaryMolecola.SUMMARYPAYTOPPAUTOMATICRECHARGEPOSTEPAYSUMMARY);

		return iban.getElement().getText();
	}

	public String getAmountToPay() {

		Particle amount=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySummaryMolecola.SUMMARYAMOUNTTOPAYPPAUTOMATICRECHARGEPOSTEPAYSUMMARY);

		return amount.getElement().getText();
	}

	public String getMyCardNumber() {

		Particle cardNumebr=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySummaryMolecola.SUBTITLERECHARGESUMMARYAUTOMATICRECHARGEPOSTEPAYSUMMARY);

		return cardNumebr.getElement().getText();
	}

	public String getReason() {

		Particle reason=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySummaryMolecola.SUMMARYREASONOFRECHARGEAUTOMATICRECHARGEPOSTEPAYSUMMARY);

		return reason.getElement().getText();
	}

	public String getAmountSum() {

		Particle sum=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySummaryMolecola.TOTALPAYMENTTOSENDAUTOMATICRECHARGEPOSTEPAYSUMMARY);

		return sum.getElement().getText();
	}
	
	public String getAmountCommition() {

		Particle commition=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySummaryMolecola.SUMMARYAMOUTOFCOMMISSIONAUTOMATICRECHARGEPOSTEPAYSUMMARY);

		return commition.getElement().getText();
	}
	
	public String getNameOfRecharge() {

		Particle name=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySummaryMolecola.NAMEOFAUTOMATICRECHARGEPOSTEPAYSUMMARY);

		return name.getElement().getText();
		
	}
	
	public void clickLinkOfDetails() {

		Particle link=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySummaryMolecola.DETAILSMOUNTHLINKAUTOMATICRECHARGEPOSTEPAYSUMMARY);

		link.getElement().click();
	}
	
	public void verifyCopyMessage() {

		Particle text=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySummaryMolecola.COPYOFAUTOMATICRECHARGEPOSTEPAYSUMMARY);
		
		text.getElement().isDisplayed();
//		String nuovoLocatorText=Utility.replacePlaceHolders(text.getLocator(), new Entry("title","Autorizzo l'addebito della ricarica e del canone mensile sopra indicati. Le successive ricariche del medesimo importo verranno eseguite ed addebitate automaticamente ogni settimana fino al \"11/04/2021\""));
//
//		((AppiumDriver<?>)page.getDriver()).findElement(By.xpath(nuovoLocatorText)).isDisplayed();		
	}
	
	public void verifyRealTimeMessage() {

		Particle text=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySummaryMolecola.MESSAGEREALTIMEAUTOMATICRECHARGEPOSTEPAYSUMMARY);

		text.getElement().isDisplayed();

//		String nuovoLocatorText=Utility.replacePlaceHolders(text.getLocator(), new Entry("title","Il beneficiario riceverà l'importo in tempo reale"));
//
//		((AppiumDriver<?>)page.getDriver()).findElement(By.xpath(nuovoLocatorText)).isDisplayed();
	}

	public void clickAutorizationButton() {

		Particle btn=(Particle) UiObjectRepo.get().get(AutomaticRechargePostepaySummaryMolecola.AUTORIZATIONBUTTONAUTOMATICRECHARGEPOSTEPAYSUMMARY);

		btn.getElement().click();
		
		try {Thread.sleep(2000);} catch (Exception err )  {}
	}
	
	public void verifySummaryRecharge(String targetOperation,String nameInput, String cardTargetInput, String amountInput, String reasonInput, String nameOfAutomaticRechargeNameInput) {
		
		String nameTarget=getNameTargetUser();
		String cardTarget=getCardNumberTargetUser();
//		String cardNumber=getMyCardNumber();     DA IMPLEMENTARE CONTROLLO SU QUESTA VARIABILE
		String reason=getReason();
		String amount=getAmountToPay();
		String amountSum=getAmountSum();
		String commition=getAmountCommition();
		
		// Tool per Stringhe e numeri
		StringAndNumberOperationTools operationTool=new StringAndNumberOperationTools();				
		
		// Aggiustamento nomeTargetInput
		String newNameInput=nameInput.toUpperCase();
//		System.out.println("newNameInput " + newNameInput);				
		// Aggiustamento cardTargetInput
		String newCardTargetInput= operationTool.convertNumberToHiddeNumber(cardTargetInput, targetOperation);
//		System.out.println("newCardTargetInput "+newCardTargetInput);				
		// Aggiustamento input SOLDI INVIATI
//		System.out.println("amountInput " + amountInput);
		String newAmount=operationTool.convertAmountWithEuro(amountInput);
//		System.out.println("newAmount"+newAmount);
		// Aggiustamento verifica su IMPORTO BOTTONE PAGA
		// Otteniamo il valore numerico del bottone PAGA
//		System.out.println("amountSum"+amountSum);
		double newAmountSumInteger=operationTool.convertWithoutEUROlabel(amountSum);
//		System.out.println("newAmountButtonInteger " + newAmountButtonInteger);
		// Otteniamo la somma di COMMISSIONE e IMPORTO
		double newAmountInteger=operationTool.convertWithoutEUROlabel(amount);
//		System.out.println("newAmountInteger "+newAmountInteger);
		double newCommissionInteger=operationTool.convertWithoutEUROlabel(commition);
//		System.out.println(newCommissionInteger);
		// Somma
		double sumCommImp= newCommissionInteger+newAmountInteger;
//		System.out.println(sumCommImp);		
				
		// Verifiche
		System.out.println("Expected: "+ newNameInput +" ---> Actual: "+nameTarget);
		assertEquals(newNameInput, nameTarget);
//		System.out.println("newNameInput OK");
		System.out.println("Expected: "+ newCardTargetInput +" ---> Actual: "+cardTarget);
		assertEquals(newCardTargetInput, cardTarget);
//		System.out.println("newCardTargetInput OK");
		System.out.println("Expected: "+ newAmount +" ---> Actual: "+amount);
		assertEquals(newAmount, amount);
//		System.out.println("newAmount OK");
//		assertEquals(cardNumberInput, cardNumber);
		System.out.println("Expected: "+ reasonInput +" ---> Actual: "+reason);
		assertEquals(reasonInput, reason);
//		System.out.println("reasonInput OK");
		System.out.println("Expected: € 1,00" +" ---> Actual: "+commition);
		assertEquals("€ 1,00", commition);
//		System.out.println("commition OK");
		System.out.println("Expected: "+ sumCommImp +" ---> Actual: "+newAmountSumInteger);
		assertEquals(sumCommImp, newAmountSumInteger, 0.0);
//		System.out.println("sumCommImp OK");

		try {Thread.sleep(1000);}catch(Exception e) {}
		
		// SCROLL PER RENDERE VISIVILI I SUCCESSIVI OGGETTI
		LayoutTools layOutTool=new LayoutTools();
		layOutTool.makeAScrollOnPage(page, 500);		
		
		// Nome Inserito per chiamare la ricarica automatica
		String nameOfAutomaticRecharge=getNameOfRecharge();
		System.out.println("Expected: "+ nameOfAutomaticRechargeNameInput +" ---> Actual: "+nameOfAutomaticRecharge);
		assertEquals(nameOfAutomaticRechargeNameInput, nameOfAutomaticRecharge);

	}
	
	public void verifySummariRicaricaAutomaticaSOGLIA(String targetUser, String targetUserCardNumber, String targetReason, String targetImporto) {
		
		String aNameTarget=getNameTargetUser();
		String eName = targetUser.toUpperCase();
		System.out.println("AR: " + aNameTarget + "ER: " + eName);
		assertTrue(aNameTarget.equals(eName));
		
		String aCardTarget=getCardNumberTargetUser();
		String eCard = "**** **** " + targetUserCardNumber;
		System.out.println("AR: " + aCardTarget + "ER: " + eCard);
		assertTrue(aCardTarget.equals(eCard));
		
		String aReason=getReason();
		System.out.println("AR: " + aReason + "ER: " + targetReason);
		assertTrue(aReason.equals(targetReason));
		
		String aCommissione=getAmountCommition();
		System.out.println("AR: " + aCommissione + "ER: € 1,00");
		assertTrue(aCommissione.equals("€ 1,00"));
			
		String aAmount=getAmountToPay();
		String eAmount = "€ " + targetImporto;
		eAmount = eAmount.replace(".", ",");
		System.out.println("AR: " + aAmount + "ER: " + eAmount);
		assertTrue(aAmount.equals(eAmount));
		
	}
}

