package com.postepay.automation.core.ui.pages.rechargemypostepaypagestep5;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.BodyOfNewPosteID;


public class RechargeMyPostepayPageStep5 extends Page {
	public static final String NAME="T043";


	public RechargeMyPostepayPageStep5(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(BodyOfNewPosteID.NAME, UiObjectRepo.get().get(BodyOfNewPosteID.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {

		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {

		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {

		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new RechargeMyPostepayPageStep5Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {

		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {

		return null;
	}
	public void insertPosteId(String posteId) {

		((RechargeMyPostepayPageStep5Manager)this.manager).insertPosteId(posteId);
	}

	public void clickOnConfirmButton() {

		((RechargeMyPostepayPageStep5Manager)this.manager).clickOnConfirmButton();
	}
	
	public void verifyHeaderPage(String titleHeader) {
		
		((RechargeMyPostepayPageStep5Manager)this.manager).verifyHeaderPage(titleHeader);
	}
	
	public void clickOnBackArrow() {
		((RechargeMyPostepayPageStep5Manager)this.manager).clickOnBackArrow();
	}
}

