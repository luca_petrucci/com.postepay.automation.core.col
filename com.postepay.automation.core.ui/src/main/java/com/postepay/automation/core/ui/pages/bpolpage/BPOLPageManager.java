package com.postepay.automation.core.ui.pages.bpolpage;

import static io.appium.java_client.touch.offset.PointOption.point;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.AbilitaInAppMolecola;
import com.postepay.automation.core.ui.molecules.BPOLMolecola;
import com.postepay.automation.core.ui.molecules.BodyMapsGenericScreenFromHomePage;
import com.postepay.automation.core.ui.molecules.GenericErrorPopUpMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.HidesKeyboard;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.StartsActivity;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.android.nativekey.PressesKey;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.UiObject;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class BPOLPageManager extends PageManager {


	private WebDriver driver;

	public BPOLPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub


	}
	
	public void inserisciCredenziali(String username, String password, String NumeroCarta)
	{
		try {
			Particle btnCookie=(Particle) UiObjectRepo.get().get(BPOLMolecola.BTNCOOKIE);
			btnCookie.setDriver(page.getDriver());
			btnCookie.getElement().click();
		}catch(Exception e)
		{
			
		}
	
		WaitManager.get().waitShortTime();
		Particle user=(Particle) UiObjectRepo.get().get(BPOLMolecola.USERNAMEBPOL);
		user.setDriver(page.getDriver());
		user.getElement().sendKeys(username);
		WaitManager.get().waitShortTime();
		Particle pwd=(Particle) UiObjectRepo.get().get(BPOLMolecola.PASSWORDBPOL);
		pwd.setDriver(page.getDriver());
		pwd.getElement().sendKeys(password);
		WaitManager.get().waitShortTime();
		
		Particle btnAccedi=(Particle) UiObjectRepo.get().get(BPOLMolecola.ACCEDILOGINBPOL);
		btnAccedi.setDriver(page.getDriver());
		btnAccedi.getElement().click();
		WaitManager.get().waitLongTime();
		
		try {
			Particle btnCookie=(Particle) UiObjectRepo.get().get(BPOLMolecola.BTNCOOKIE);
			btnCookie.setDriver(page.getDriver());
			btnCookie.getElement().click();
		}catch(Exception e)
		{
			
		}
		WaitManager.get().waitShortTime();
//		Particle btnNotNow=(Particle) UiObjectRepo.get().get(BPOLMolecola.BTNNONORABPOL);
//		btnNotNow.setDriver(page.getDriver());
//		UIUtils.ui().scrollAndReturnElementLocatedBy(page.getDriver(), By.xpath(btnNotNow.getLocator())).click();
		WaitManager.get().waitShortTime();
		
		Particle accediButton=(Particle) UiObjectRepo.get().get(BPOLMolecola.CARDPOSTEPAY);
		accediButton.setDriver(page.getDriver());
		System.out.println(accediButton.getXPath());
//		UIUtils.ui().scrollAndReturnElementLocatedBy(page.getDriver(), By.xpath(accediButton.getLocator()));
//		accediButton.getElement().findElement(By.xpath("//a[@class='btn-card']//span[contains(.,'Accedi')]")).click();
		//accediButton.setDriver(page.getDriver());
		//System.out.println(accediButton.getLocator());
		try {
			Particle btnCookie=(Particle) UiObjectRepo.get().get(BPOLMolecola.BTNCOOKIE);
			btnCookie.setDriver(page.getDriver());
			btnCookie.getElement().click();
		}catch(Exception e)
		{
			
		}
		System.out.println("SCROLL");
		UIUtils.ui().scrollAndReturnElementLocatedBy(page.getDriver(), By.xpath(accediButton.getLocator()));
		WaitManager.get().waitLongTime();
		accediButton.getElement().click();
		
		
		WaitManager.get().waitMediumTime();
		
		
	}
	public void utenteCliccaProfilo() 
	{
		Particle postepay=(Particle) UiObjectRepo.get().get(BPOLMolecola.PROFILOMENU);
		System.out.println(postepay.getLocator());
		postepay.setDriver(page.getDriver());
		//System.out.println(postepay.getXPath());
		postepay.getElement().click(); 
		WaitManager.get().waitMediumTime();
		
	}
	
	public void utenteAccedeAPostepay(String numeroCarta) 
	{

	/*	Particle servizi=(Particle) UiObjectRepo.get().get(BPOLMolecola.SERVIZIONLINE);
		servizi.setDriver(page.getDriver());
		servizi.getElement().click();
		WaitManager.get().waitMediumTime();*/
		Particle btnX=(Particle) UiObjectRepo.get().get(BPOLMolecola.COOKIEXBTN);
		btnX.setDriver(page.getDriver());
		try {
			btnX.getElement().click();

			WaitManager.get().waitShortTime();	
		} catch(Exception err) {
			
		}

//		Particle sdaLabel=(Particle) UiObjectRepo.get().get(BPOLMolecola.MOBILESDA);
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 500);
		
		
		Particle btnAccediCard=(Particle) UiObjectRepo.get().get(BPOLMolecola.MOBILEACCEDIBUTTONCARD);
		
		String locatorAccedi = Utility.replacePlaceHolders(btnAccediCard.getLocator(),new Entry("title", numeroCarta));

		WaitManager.get().waitMediumTime();
		try {
			btnX.getElement().click();

			WaitManager.get().waitShortTime();	
		} catch(Exception err) {
			
		}
		page.getDriver().findElement(By.xpath(locatorAccedi)).click();
		WaitManager.get().waitShortTime();
	}
	
	public void invioAutorizzazionePostepay()
	{
		WaitManager.get().waitMediumTime();
		Particle checkBox=(Particle) UiObjectRepo.get().get(BPOLMolecola.CHECKBOXAUTORIZZAPOSTEPAY);
		checkBox.setDriver(page.getDriver());
		try {
			checkBox.getElement().click();

		} catch(Exception e) {
			
		}
		WaitManager.get().waitShortTime();
		page.getDriver().getPageSource();	
		
		WaitManager.get().waitMediumTime();
		Particle prosegui=(Particle) UiObjectRepo.get().get(BPOLMolecola.BTNPROSEGUIBPOL);
		prosegui.setDriver(page.getDriver());
		prosegui.getElement().click();
		WaitManager.get().waitMediumTime(); 
	}

	public void layoutAreaPersonalePostePay(String cardNumber) {
	
		WaitManager.get().waitLongTime(); 
		WaitManager.get().waitMediumTime();
		Particle areaPersonale=(Particle) UiObjectRepo.get().get(BPOLMolecola.AREAPERSONALEPOSTEPAY);
		areaPersonale.setDriver(page.getDriver());
		String locatorAccedi = Utility.replacePlaceHolders(areaPersonale.getLocator(),new Entry("title", cardNumber));
		assertTrue(page.getDriver().findElement(By.xpath(locatorAccedi)) != null);
		WaitManager.get().waitMediumTime();
	}

	public void openMobileChrome() {
		((StartsActivity) page.getDriver()).startActivity(new Activity(
				"com.android.chrome", 
				"com.google.android.apps.chrome.Main "));

		WaitManager.get().waitMediumTime();
		
		String url = "https://securelogin.test.poste.it/jod-fcc/fcc-authentication.html";
		
//		Particle title=(Particle) UiObjectRepo.get().get(BPOLMolecola.MOBILELOGINTITLEBPOL);
//		Particle urlBox=(Particle) UiObjectRepo.get().get(BPOLMolecola.MOBILEURLBARBPOL);
//		urlBox.getElement().click();
//		WaitManager.get().waitShortTime();
//		urlBox.getElement().sendKeys(url);
//		WaitManager.get().waitShortTime();
//		((PressesKey) page.getDriver()).pressKey(new KeyEvent(AndroidKey.ENTER));

		
		
		
	}

	public void loginMobile(String username, String password, String cardNumber) {
		Particle box = (Particle) UiObjectRepo.get().get(BPOLMolecola.MOBILEURLBARBPOL);
		box.getElement().click();
		box.getElement().sendKeys("https://postepay2.test.poste.it/ppay/private/pages/index.html");
		((PressesKey) page.getDriver()).pressKey(new KeyEvent(AndroidKey.ENTER));
		WaitManager.get().waitShortTime();
		
		
		Particle user=(Particle) UiObjectRepo.get().get(BPOLMolecola.MOBILEUSERNAMEBPOL);
		Particle pass=(Particle) UiObjectRepo.get().get(BPOLMolecola.MOBILEPASSWORDBPOL);
		Particle accediBtn=(Particle) UiObjectRepo.get().get(BPOLMolecola.MOBILEACCEDIBUTTON);
//		urlBox.getElement().click();
		WaitManager.get().waitShortTime();
		System.out.println("INSERISCI CREDENZIALI");
		System.out.println(page.getDriver().getPageSource());
		
		user.getElement().click();
		user.getElement().sendKeys(username);
		((HidesKeyboard) page.getDriver()).hideKeyboard();
		WaitManager.get().waitShortTime();
		
		pass.getElement().click();
		pass.getElement().sendKeys(password);
		((HidesKeyboard) page.getDriver()).hideKeyboard();
		WaitManager.get().waitShortTime();
		
		accediBtn.getElement().click();
		
	}

	public void scrollChipLabel()
	{
		WaitManager.get().waitShortTime();
		Dimension size = page.getDriver().manage().window().getSize();
//		Particle chip =(Particle) UiObjectRepo.get().get(BPOLMolecola.MOBILESDA);
//
//		WebElement coordinate = chip.getElement();
//		float x = coordinate.getLocation().getX() + coordinate.getRect().width * 0.5f; 
//		float y = coordinate.getLocation().getY() + coordinate.getRect().height * 0.5f; 

		float x = 400; 
		float y = 600; 

		System.out.println("x " + x);
		System.out.println("y " + y);

		float X = x / size.width; 
		float Y = y / size.height;
		System.out.println("X " + X);
		System.out.println("Y " + Y);
		System.out.println("Y- (Y * 0.4f) " + (Y- (Y * 0.7f)));
		System.out.println("Scroll - Start");
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 
				X, 
				Y, 
				X,
				Y- (Y * 0.7f),
				1500);
		System.out.println("Scroll - End");
	}
	
	public void isVisible(String cardNumber) {
		Particle accediCard=(Particle) UiObjectRepo.get().get(BPOLMolecola.MOBILEACCEDIBUTTONCARD);
		String locatorAccedi = Utility.replacePlaceHolders(accediCard.getLocator(),new Entry("title", cardNumber));
		
		WebElement el = null;
		
		try {
			el = page.getDriver().findElement(By.xpath(locatorAccedi));
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		for (int i = 0; i < 6; i++) {
			scrollChipLabel();
			try {
				el = page.getDriver().findElement(By.xpath(locatorAccedi));
			} catch (Exception e) {
				// TODO: handle exception
			}
			if(el != null) {
				el.click();
				return;
			}
		}
	}
	
	public void utenteInviaLautorizzativa(String cardNumber) {
		WaitManager.get().waitShortTime();
//		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 2000);

//		scrollChipLabel();
		isVisible(cardNumber);
		
		WaitManager.get().waitMediumTime();
//		Particle accediCard=(Particle) UiObjectRepo.get().get(BPOLMolecola.MOBILEACCEDIBUTTONCARD);
//		
//		String locatorAccedi = Utility.replacePlaceHolders(accediCard.getLocator(),new Entry("title", cardNumber));
//		page.getDriver().findElement(By.xpath(locatorAccedi)).click();
//		WaitManager.get().waitShortTime();
		
		Particle proseguiCard=(Particle) UiObjectRepo.get().get(BPOLMolecola.MOBILEPROSEGUIBUTTON);
		proseguiCard.getElement().click();
		
		WaitManager.get().waitMediumTime();
		
	}

	public void goToPosteApp() {
		((PressesKey) page.getDriver()).pressKey(new KeyEvent(AndroidKey.APP_SWITCH));
		WaitManager.get().waitShortTime();
		
		page.getDriver().findElement(By.xpath("//*[contains(@resource-id,'icon')]/preceding-sibling::*")).click();
		
		WaitManager.get().waitShortTime();
		
	}
}

