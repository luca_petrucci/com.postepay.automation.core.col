package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyChooseHourAndZoneParkingPurchaseSummary extends Molecule {
	public static final String NAME="M088";
	
	public static final String CANCELBUTTONPARKINGPURCHASE="cancelButtonParkingPurchase";
public static final String IMPORTPARKINGPURCHASE="importParkingPurchase";
public static final String DATAPARKINGPURCHASE="dataParkingPurchase";
public static final String PAYBUTTONPARKINGPURCHASESUMMARY="payButtonParkingPurchaseSummary";
public static final String CARTPARKINGPURCHASE="cartParkingPurchase";
public static final String TITLEPAGEPARKINGPURCHASE="titlePageParkingPurchase";
public static final String OWNERPARKINGPURCHASE="ownerParkingPurchase";


	public BodyChooseHourAndZoneParkingPurchaseSummary(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

