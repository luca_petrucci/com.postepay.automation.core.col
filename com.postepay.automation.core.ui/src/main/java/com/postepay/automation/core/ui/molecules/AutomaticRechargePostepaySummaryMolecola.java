package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class AutomaticRechargePostepaySummaryMolecola extends Molecule {
	public static final String NAME="M206";

	public static final String SUMMARYPAYTOPPAUTOMATICRECHARGEPOSTEPAYSUMMARY="summaryPayToPPAutomaticRechargePostepaySummary";
	public static final String COPYOFAUTOMATICRECHARGEPOSTEPAYSUMMARY="copyOfAutomaticRechargePostepaySummary";
	public static final String NAMEOFAUTOMATICRECHARGEPOSTEPAYSUMMARY="nameOfAutomaticRechargePostepaySummary";
	public static final String SUMMARYHOLDERPAYPPAUTOMATICRECHARGEPOSTEPAYSUMMARY="summaryHolderPayPPAutomaticRechargePostepaySummary";
	public static final String SUMMARYHOLDERRECHARGEPAYPPAUTOMATICRECHARGEPOSTEPAYSUMMARY="summaryHolderRechargePayPPAutomaticRechargePostepaySummary";
	public static final String TOTALPAYMENTTOSENDAUTOMATICRECHARGEPOSTEPAYSUMMARY="totalPaymentToSendAutomaticRechargePostepaySummary";
	public static final String SUMMARYPAYWITHPPAUTOMATICRECHARGEPOSTEPAYSUMMARY="summaryPayWithPPAutomaticRechargePostepaySummary";
	public static final String SUMMARYAMOUTOFCOMMISSIONAUTOMATICRECHARGEPOSTEPAYSUMMARY="summaryAmoutOfCommissionAutomaticRechargePostepaySummary";
	public static final String DETAILSMOUNTHLINKAUTOMATICRECHARGEPOSTEPAYSUMMARY="detailsMounthLinkAutomaticRechargePostepaySummary";
	public static final String SUBTITLERECHARGESUMMARYAUTOMATICRECHARGEPOSTEPAYSUMMARY="subTitleRechargeSummaryAutomaticRechargePostepaySummary";
	public static final String SUMMARYREASONOFRECHARGEAUTOMATICRECHARGEPOSTEPAYSUMMARY="summaryReasonOfRechargeAutomaticRechargePostepaySummary";
	public static final String MESSAGEREALTIMEAUTOMATICRECHARGEPOSTEPAYSUMMARY="messageRealTimeAutomaticRechargePostepaySummary";
	public static final String SUMMARYAMOUNTTOPAYPPAUTOMATICRECHARGEPOSTEPAYSUMMARY="summaryAmountToPayPPAutomaticRechargePostepaySummary";
	public static final String TITLERECHARGESUMMARYAUTOMATICRECHARGEPOSTEPAYSUMMARY="titleRechargeSummaryAutomaticRechargePostepaySummary";
	public static final String AUTORIZATIONBUTTONAUTOMATICRECHARGEPOSTEPAYSUMMARY="autorizationButtonAutomaticRechargePostepaySummary";


	public AutomaticRechargePostepaySummaryMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

