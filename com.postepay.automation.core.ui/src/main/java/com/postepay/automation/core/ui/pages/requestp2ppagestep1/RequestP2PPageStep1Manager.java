package com.postepay.automation.core.ui.pages.requestp2ppagestep1;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.SearchContactsNumberP2P;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class RequestP2PPageStep1Manager extends PageManager {

	public RequestP2PPageStep1Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void searchContact(String targetUser) {
		Particle search=(Particle) UiObjectRepo.get().get(SearchContactsNumberP2P.SEARCHP2PCONTACT);

		search.getElement().sendKeys(targetUser);

		WaitManager.get().waitLongTime();

		Particle userTargetCard=(Particle) UiObjectRepo.get().get(SearchContactsNumberP2P.TARGETIDP2PCONTACT);

		userTargetCard.getElement().click();

		WaitManager.get().waitShortTime();
	}

	public WebElement getTargetUser(WebDriver driver, String targetUser) {
		LayoutTools toolLayout = new LayoutTools();
		Particle userTargetCard=(Particle) UiObjectRepo.get().get(SearchContactsNumberP2P.TARGETIDP2PCONTACTDINAMIC);
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, userTargetCard, targetUser);
		WaitManager.get().waitShortTime();
		assertTrue(elm.isDisplayed());
		return elm;
	}
	
	public void searchContactNoP2p(WebDriver driver, String targetUser) {
		Particle search=(Particle) UiObjectRepo.get().get(SearchContactsNumberP2P.SEARCHP2PCONTACT);
		
		search.getElement().click();
		WaitManager.get().waitShortTime();
		
		search.getElement().sendKeys(targetUser);
		WaitManager.get().waitShortTime();

		WebElement user = getTargetUser(driver, targetUser);
		user.click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnContactTab() {
		Particle btnContacts=(Particle) UiObjectRepo.get().get(SearchContactsNumberP2P.TABCONTACTSEND);

		btnContacts.getElement().click();
	}
}

