package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class IOSDeviceGegericMolecola extends Molecule {
	public static final String NAME="M261";
	
	public static final String AIRPLANETOGGLE="SettingAirPlaneToggleM261";
	public static final String WIFILABEL="SettingWifiLabelM261";
	public static final String WIFIBACKBTN="SettingWifiBackM261";
	public static final String WIFITOGGLE="SettingWifiToggleM261";
	


	public IOSDeviceGegericMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {
		
	}

}

