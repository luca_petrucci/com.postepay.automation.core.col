package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SalvadanaioSelezionaTipologiaMolecola extends Molecule {
	public static final String NAME="M266";
	
public static final String SALVADANAIOSELEZIONAVERSAMENTOTITOLO="salvadanaioSelezionaVersamentoTitolo";
public static final String SALVADANAIOSELEZIONAVERSAMENTOLABEL="salvadanaioSelezionaVersamentoLabel";
public static final String SALVADANAIOVERSAMENTOSINGOLOTITLE="salvadanaioVersamentoSingoloTitle";
public static final String SALVADANAIOVERSAMENTOSINGOLOSUBTITLE="salvadanaioVersamentoSingoloSubTitle";
public static final String SALVADANAIOVERSAMENTOARROTONDAMENTOTITLE="salvadanaioVersamentoArrotondamentoTitle";
public static final String SALVADANAIOVERSAMENTOARROTONDAMENTOSUBTITLE="salvadanaioVersamentoArrotondamentoSubTitle";
public static final String SALVADANAIOVERSAMENTOBUDGETTITLE="salvadanaioVersamentoBudgetTitle";
public static final String SALVADANAIOVERSAMENTOBUDGETSUBTITLE="salvadanaioVersamentoBudgetSubTitle";
public static final String SALVADANAIOVERSAMENTORICORRENTETITLE="salvadanaioVersamentoRicorrenteTitle";
public static final String SALVADANAIOVERSAMENTORICORRENTESUBTITLE="salvadanaioVersamentoRicorrenteSubTitle";


	public SalvadanaioSelezionaTipologiaMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

