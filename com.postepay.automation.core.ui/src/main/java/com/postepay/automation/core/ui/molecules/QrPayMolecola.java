package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class QrPayMolecola extends Molecule {
	public static final String NAME="M250";
	
	public static final String QRPAYCONFIRMBUTTON="qrPayConfirmButton";
public static final String QRPAYTEXTALLERT="qrPayTextAllert";
public static final String QRPAYDESDCRIZIONE="qrPayDesdcrizione";
public static final String QRPAYAMOUNT="qrPayAmount";


	public QrPayMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

