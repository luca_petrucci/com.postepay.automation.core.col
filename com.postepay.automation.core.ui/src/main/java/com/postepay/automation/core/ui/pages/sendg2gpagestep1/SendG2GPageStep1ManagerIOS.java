package com.postepay.automation.core.ui.pages.sendg2gpagestep1;

import static org.junit.Assert.assertTrue;

import ui.core.support.page.Page;

public class SendG2GPageStep1ManagerIOS extends SendG2GPageStep1Manager {

	public SendG2GPageStep1ManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	public void verifyLayoutPage()
	{
		assertTrue(image.getElement()!=null);
		assertTrue(copy.getElement().isDisplayed());
		assertTrue(descrizione.getElement().isDisplayed());
		assertTrue(messaggio.getElement().getText().contains("Attualmente hai a disposizione"));
		assertTrue(btn.getElement().isDisplayed());
	}

}
