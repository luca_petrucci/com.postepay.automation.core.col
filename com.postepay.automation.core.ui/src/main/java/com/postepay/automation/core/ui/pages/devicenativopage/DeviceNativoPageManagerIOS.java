package com.postepay.automation.core.ui.pages.devicenativopage;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.AbilitaInAppMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class DeviceNativoPageManagerIOS extends PageManager {

	LayoutTools toolLayout = new LayoutTools();

	public DeviceNativoPageManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}
	
	public void verifyImage() {
		
	}
}

