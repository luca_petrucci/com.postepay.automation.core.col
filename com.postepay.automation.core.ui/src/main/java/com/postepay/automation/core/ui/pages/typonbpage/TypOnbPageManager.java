package com.postepay.automation.core.ui.pages.typonbpage;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.AutorizzaOnbMolecola;
import com.postepay.automation.core.ui.molecules.TypOnbMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class TypOnbPageManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();

	Particle imageTypOnp = (Particle) UiObjectRepo.get().get(TypOnbMolecola.IMAGETYPONP);
	Particle titleTypOnp = (Particle) UiObjectRepo.get().get(TypOnbMolecola.TITLETYPONP);
	Particle copyTypOnp = (Particle) UiObjectRepo.get().get(TypOnbMolecola.COPYTYPONP);
	Particle closeTypOnp = (Particle) UiObjectRepo.get().get(TypOnbMolecola.CLOSETYPONP);

	public TypOnbPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyImage(WebDriver driver, String imageNameInput, double discrepanza) {
		String imageName = "typGeneric/" + imageNameInput;
		LayoutImage.get().verifyImage(driver, imageName, discrepanza);
	}

	public void verifyLayout(WebDriver driver, double discrepanza) {
		assertTrue(imageTypOnp.getElement().isDisplayed());
		assertTrue(titleTypOnp.getElement().isDisplayed());
		assertTrue(copyTypOnp.getElement().isDisplayed());
		assertTrue(closeTypOnp.getElement().isDisplayed());
		System.out.println("\nVerifica dell'icona");
		verifyImage(driver, "icona_conferma_blu", discrepanza);
		System.out.println("\nVerifica completata");
	}
	
	public void clickOnChiudi() {
		closeTypOnp.getElement().click();
		WaitManager.get().waitLongTime();
	}
}

