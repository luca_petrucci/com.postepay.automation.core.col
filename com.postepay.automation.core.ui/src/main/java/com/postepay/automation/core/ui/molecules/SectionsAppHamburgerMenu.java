package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SectionsAppHamburgerMenu extends Molecule {
	public static final String NAME="M013";
	
	public static final String MYPURCHASELINKHAMBURGERMENU="myPurchaseLinkHamburgerMenu";
public static final String ASSISTANCELINKHAMBURGERMENU="assistanceLinkHamburgerMenu";
public static final String LOGOUTHAMBURGERMENU="logoutHamburgerMenu";
public static final String PROFILELINKHAMBURGERMENU="profileLinkHamburgerMenu";
public static final String SETTINGLINKHAMBURGERMENU="settingLinkHamburgerMenu";


	public SectionsAppHamburgerMenu(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

