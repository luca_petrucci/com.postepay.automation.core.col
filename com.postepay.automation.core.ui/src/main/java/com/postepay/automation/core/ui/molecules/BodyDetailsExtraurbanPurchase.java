package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyDetailsExtraurbanPurchase extends Molecule {
	public static final String NAME="M094";
	
	public static final String CHOOSETICKETCONTAINER="chooseTicketContainer";
public static final String CARDTICKETCONTAINER="cardTicketContainer";
public static final String PROCEEDBUTTONBODYDETAILSEXTRAURBAN="proceedButtonBodyDetailsExtraurban";
public static final String EXPANDBUTTON="expandButton";
public static final String CHOOSERADIOBUTTON="chooseRadioButton";
public static final String DIRECTIONJOURNEYINFO="directionJourneyInfo";
public static final String DETAILJOURNEYCONTAINER="detailJourneyContainer";
public static final String CARDTICKETPRICE="cardTicketPrice";
public static final String CHOOSERADIOBUTTONBASE ="chooseRadioButtonBase";


	public BodyDetailsExtraurbanPurchase(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

