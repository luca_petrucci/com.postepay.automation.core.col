package com.postepay.automation.core.ui.pages.rechargemypostepaypagestep2;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;

import io.appium.java_client.AppiumDriver;

import com.postepay.automation.core.ui.molecules.CompileFormToRechargeMyPostepay;


public class RechargeMyPostepayPageStep2 extends Page {
	public static final String NAME="T040";
	

	public RechargeMyPostepayPageStep2(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
this.addToTemplate(CompileFormToRechargeMyPostepay.NAME, UiObjectRepo.get().get(CompileFormToRechargeMyPostepay.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new RechargeMyPostepayPageStep2Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	public void clickOnProceedButton() {
		((RechargeMyPostepayPageStep2Manager)this.manager).clickOnProceedButton();
	}
	
	public void clickOnToggleAutomatickRecharge() {
		((RechargeMyPostepayPageStep2Manager)this.manager).clickOnToggleAutomatickRecharge();
	}
	
	public void insertReasonOfRecharge(String value) {
		((RechargeMyPostepayPageStep2Manager)this.manager).insertReasonOfRecharge(value);
	}
	
	public void insertAmountToSendTargetUser(String value) {
		((RechargeMyPostepayPageStep2Manager)this.manager).insertAmountToSendTargetUser(value);
	}
}

