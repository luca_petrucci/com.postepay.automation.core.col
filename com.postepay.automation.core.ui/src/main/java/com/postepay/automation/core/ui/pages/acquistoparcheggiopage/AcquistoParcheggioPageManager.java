package com.postepay.automation.core.ui.pages.acquistoparcheggiopage;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.AbilitaInAppMolecola;
import com.postepay.automation.core.ui.molecules.AcquistoParcheggioMolecola;
import com.postepay.automation.core.ui.molecules.BodyTravelSolution;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class AcquistoParcheggioPageManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();


	public AcquistoParcheggioPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyData(String titolo, String tipo, String luogo, String indirizzo, String msgStatico, String msgDinamico) {
		
		Particle elTitolo = (Particle) UiObjectRepo.get().get(AcquistoParcheggioMolecola.TITOLOM280);
		String arTitle = elTitolo.getElement().getText().toUpperCase().trim();
		String errTitolo = "Titolo diverso AR: " + arTitle + "  ER: " + titolo.toUpperCase().trim();
		assertTrue(errTitolo, arTitle.equals(titolo.toUpperCase().trim()));
		
		Particle elTipo = (Particle) UiObjectRepo.get().get(AcquistoParcheggioMolecola.TYPEOPERAZIONEM280);
		String arTipo = elTipo.getElement().getText().toUpperCase().trim();
		String errTipo = "Tipo diverso AR: " + arTipo + "  ER: " + tipo.toUpperCase().trim();
		assertTrue(errTipo, arTipo.equals(tipo.toUpperCase().trim()));
		
		Particle elLuogo = (Particle) UiObjectRepo.get().get(AcquistoParcheggioMolecola.LUOGOM280);
		String arLuogo = elLuogo.getElement().getText().toUpperCase().trim();
		String errLuogo = "Luogo diverso AR: " + arLuogo + "  ER: " + luogo.toUpperCase().trim();
		assertTrue(errLuogo, arLuogo.equals(luogo.toUpperCase().trim()));
		
		Particle elIndirizzo = (Particle) UiObjectRepo.get().get(AcquistoParcheggioMolecola.INDIRIZZOM280);
		String arIndirizzo = elIndirizzo.getElement().getText().toUpperCase().trim();
		String errIndirizzo = "Indirizzo diverso AR: " + arIndirizzo + "  ER: " + indirizzo.toUpperCase().trim();
		assertTrue(errIndirizzo, arIndirizzo.equals(indirizzo.toUpperCase().trim()));
		
		Particle elStatico = (Particle) UiObjectRepo.get().get(AcquistoParcheggioMolecola.MSGSTATICOM280);
		String arStatico = elStatico.getElement().getText().toUpperCase().trim();
		String errStatico = "MsgStatico diverso AR: " + arStatico + "  ER: " + msgStatico.toUpperCase().trim();
		assertTrue(errStatico, arStatico.equals(msgStatico.toUpperCase().trim()));
		
		Particle elDinamico = (Particle) UiObjectRepo.get().get(AcquistoParcheggioMolecola.MSGDINAMICOM280);
		String arDinamico = elDinamico.getElement().getText().toUpperCase().trim();
		String errDinamico = "MsgDinamico diverso AR: " + arDinamico + "  ER: " + msgDinamico.toUpperCase().trim();
		assertTrue(errDinamico, arDinamico.equals(msgDinamico.toUpperCase().trim()));
		
		
	}
}

