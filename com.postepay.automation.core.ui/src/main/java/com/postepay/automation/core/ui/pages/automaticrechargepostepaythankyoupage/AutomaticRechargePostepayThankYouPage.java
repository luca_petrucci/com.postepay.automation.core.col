package com.postepay.automation.core.ui.pages.automaticrechargepostepaythankyoupage;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.AutomaticRechargePostepayThankYouPageMolecola;
import com.postepay.automation.core.ui.pages.rechargemypostepaypagestep6.RechargeMyPostepayPageStep6Manager;


public class AutomaticRechargePostepayThankYouPage extends Page {
	public static final String NAME="T090";
	

	public AutomaticRechargePostepayThankYouPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(AutomaticRechargePostepayThankYouPageMolecola.NAME, UiObjectRepo.get().get(AutomaticRechargePostepayThankYouPageMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new AutomaticRechargePostepayThankYouPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyLayout() {
		((AutomaticRechargePostepayThankYouPageManager)this.manager).verifyLayout();
	}
	
	public void clickOnButtonClose() {
		((AutomaticRechargePostepayThankYouPageManager)this.manager).clickOnCloseButton();
	}

}

