package com.postepay.automation.core.ui.pages.sendg2gpagestep5;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.SummaryG2gSend;
import com.postepay.automation.core.ui.molecules.ThankYouPageGeneric;
import com.postepay.automation.core.ui.molecules.ThankYouPageSendG2G;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.android.nativekey.PressesKey;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class SendG2GPageStep5Manager extends PageManager {

	public SendG2GPageStep5Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	Particle iconG2g = (Particle) UiObjectRepo.get().get(ThankYouPageSendG2G.ICONTHANKYOUPAGEG2G);
	Particle iconSender = (Particle) UiObjectRepo.get().get(ThankYouPageSendG2G.ICONSENDTHANKYOUG2G);
	Particle iconReceiver = (Particle) UiObjectRepo.get().get(ThankYouPageSendG2G.ICONRECEIVETHANKYOUG2G);
	Particle g2gAmount = (Particle) UiObjectRepo.get().get(ThankYouPageSendG2G.AMOUNTTHANKYOUG2G);
	Particle g2gValue = (Particle) UiObjectRepo.get().get(ThankYouPageSendG2G.VALUETHANKYOUG2G);
	Particle text = (Particle) UiObjectRepo.get().get(ThankYouPageSendG2G.TITLETHANKYOUG2G);
	Particle description = (Particle) UiObjectRepo.get().get(ThankYouPageSendG2G.SUBTITLETHANKYOUG2G);
	Particle closeBtn = (Particle) UiObjectRepo.get().get(ThankYouPageSendG2G.BUTTONCLOSETHANKYOUG2G);
	
	public void verifyLayoutPage() {
		
		
		assertTrue(iconG2g.getElement().isDisplayed());
		assertTrue(iconSender.getElement().isDisplayed());
		assertTrue(iconReceiver.getElement().isDisplayed());
		assertTrue(g2gAmount.getElement().isDisplayed());
		assertTrue(g2gValue.getElement().isDisplayed());
		assertTrue(text.getElement().isDisplayed());
		assertTrue(description.getElement().isDisplayed());
		assertTrue(closeBtn.getElement().isDisplayed());
		
	}
	
	public void clickOnClose() {
		Particle closeBtn = (Particle) UiObjectRepo.get().get(ThankYouPageSendG2G.BUTTONCLOSETHANKYOUG2G);
		
		closeBtn.getElement().click();
		
		WaitManager.get().waitLongTime();
	}
	
	public void verifyImageCard(WebDriver driver, double discrepanza) {
		
		LayoutImage.get().verifyImage(driver, "g2g/g2g_typ_send", discrepanza);
		LayoutImage.get().verifyImage(driver, "g2g/g2g_typ_sender", discrepanza);
		LayoutImage.get().verifyImage(driver, "g2g/g2g_typ_ricevente", discrepanza);
		LayoutImage.get().verifyImage(driver, "g2g/g2g_typ_onegiga", discrepanza);
	}
	
	public void verifyTYPNoG2g() {
		Particle img = (Particle) UiObjectRepo.get().get(ThankYouPageGeneric.NOG2GIMG);
		Particle title = (Particle) UiObjectRepo.get().get(ThankYouPageGeneric.NOG2GTITLE);
		Particle btn = (Particle) UiObjectRepo.get().get(ThankYouPageGeneric.NOG2GSEND);
		
		assertTrue(img.getElement().isDisplayed());
		assertTrue(title.getElement().getText().equals("Invita i tuoi amici!"));
		assertTrue(btn.getElement().getText().equals("INVITA I TUOI AMICI"));
		
		WaitManager.get().waitShortTime();
		btn.getElement().click();
		WaitManager.get().waitMediumTime();
		((PressesKey) page.getDriver()).pressKey(new KeyEvent(AndroidKey.HOME));
//		page.getDriver().pressKey(new KeyEvent(AndroidKey.HOME));
		WaitManager.get().waitMediumTime();
		
	}
	
}

