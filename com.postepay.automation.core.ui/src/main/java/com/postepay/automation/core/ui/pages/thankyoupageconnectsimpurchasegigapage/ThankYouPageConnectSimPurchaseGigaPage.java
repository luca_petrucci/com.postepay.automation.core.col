package com.postepay.automation.core.ui.pages.thankyoupageconnectsimpurchasegigapage;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.ThankYouPagePurchaseG2G;


public class ThankYouPageConnectSimPurchaseGigaPage extends Page {
	public static final String NAME="T025";
	

	public ThankYouPageConnectSimPurchaseGigaPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(ThankYouPagePurchaseG2G.NAME, UiObjectRepo.get().get(ThankYouPagePurchaseG2G.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new ThankYouPageConnectSimPurchaseGigaPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new ThankYouPageConnectSimPurchaseGigaPageManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyLayoutPage() {
		((ThankYouPageConnectSimPurchaseGigaPageManager)this.manager).verifyLayoutPage();
	}
	
	public void clickOnClose() {
		((ThankYouPageConnectSimPurchaseGigaPageManager)this.manager).clickOnClose();
	}
}

