package com.postepay.automation.core.ui.pages.rechargepostepayusingtelephoncontactspage;

import javax.servlet.http.Part;

import org.openqa.selenium.By;

import com.postepay.automation.core.ui.molecules.BodyRechargePostepayUsingTelephonContacts;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.SelectACardToRechargeMyPostepay;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;

import io.appium.java_client.AppiumDriver;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class RechargePostepayUsingTelephonContactsPageManager extends PageManager {

	public RechargePostepayUsingTelephonContactsPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void selectUserToSendRecharge(String targetUser) {
		// Tool per Stringhe e numeri
		StringAndNumberOperationTools operationTool=new StringAndNumberOperationTools();

		// Aggiustamento nomeTargetInput
		String newNameInput=targetUser.toUpperCase();

		Particle p=(Particle) UiObjectRepo.get().get(BodyRechargePostepayUsingTelephonContacts.LISTOFCONTACTSCARDS);
		String nuovoLocator=Utility.replacePlaceHolders(p.getLocator(), new Entry("title",newNameInput));

		((AppiumDriver<?>)page.getDriver()).findElement(By.xpath(nuovoLocator)).click();

		WaitManager.get().waitShortTime();
	}

	public void clickOnSaveButton() {
		Particle btn=(Particle) UiObjectRepo.get().get(HeaderGenericAllPage.SALVABUTTONGENERICICON);

		btn.getElement().click();

		WaitManager.get().waitShortTime();
	}
	
	public void clickOnTabMyCARD() {
		Particle p=(Particle) UiObjectRepo.get().get(BodyRechargePostepayUsingTelephonContacts.TABMYCARDS);
		p.getElement().click();
		WaitManager.get().waitShortTime();
	}
	
	public void selectUserToSendRechargeUsingCard(String targetCard) {
		// Tool per Stringhe e numeri
		StringAndNumberOperationTools operationTool=new StringAndNumberOperationTools();

		Particle p=(Particle) UiObjectRepo.get().get(BodyRechargePostepayUsingTelephonContacts.LISTOFCONTACTSBYCARDNUMBER);
		String nuovoLocator=Utility.replacePlaceHolders(p.getLocator(), new Entry("title", targetCard));

		((AppiumDriver<?>)page.getDriver()).findElement(By.xpath(nuovoLocator)).click();

		WaitManager.get().waitShortTime();
	}
	
}

