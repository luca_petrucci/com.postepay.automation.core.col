package com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep4;

import com.postepay.automation.core.ui.molecules.BodyDetailsOfTravelersExtraurbanPurchase;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import ui.core.support.waitutil.WaitManager;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class ExtraurbanPurchasePageStep4Manager extends PageManager {

	public ExtraurbanPurchasePageStep4Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage(String inputTitle) {
		LayoutTools tool = new LayoutTools();
		tool.verifyPresenceHeaderPage(inputTitle);
	}
	
	public void sendName(String name) {
		Particle input = (Particle) UiObjectRepo.get().get(BodyDetailsOfTravelersExtraurbanPurchase.INPUTNAME);
		input.getElement().clear();
		input.getElement().sendKeys(name);
		WaitManager.get().waitShortTime();
	}
	
	public void sendSurname(String surname) {
		Particle input = (Particle) UiObjectRepo.get().get(BodyDetailsOfTravelersExtraurbanPurchase.INPUTSURNAME);
		input.getElement().clear();
		input.getElement().sendKeys(surname);
		WaitManager.get().waitShortTime();
	}
	
	public void sendCard(String card) {
		Particle input = (Particle) UiObjectRepo.get().get(BodyDetailsOfTravelersExtraurbanPurchase.INPUTCARD);
		input.getElement().clear();
		input.getElement().sendKeys(card);
		WaitManager.get().waitShortTime();
	}
	
	public void sendEmail(String email) {
		Particle input = (Particle) UiObjectRepo.get().get(BodyDetailsOfTravelersExtraurbanPurchase.INPUTEMAIL);
		input.getElement().clear();
		input.getElement().sendKeys(email);
		WaitManager.get().waitShortTime();
	}
	
	public void sendTelephono(String telephone) {
		Particle input = (Particle) UiObjectRepo.get().get(BodyDetailsOfTravelersExtraurbanPurchase.INPUTPHONE);
		input.getElement().clear();
		input.getElement().sendKeys(telephone);
		WaitManager.get().waitShortTime();
	}
	
	public void clickOnProceed() {
		Particle btn = (Particle) UiObjectRepo.get().get(BodyDetailsOfTravelersExtraurbanPurchase.PROCEEDBUTTONBODYDETAILSOFTRAVELERS);
		btn.getElement().click();
		WaitManager.get().waitLongTime();
	}

}

