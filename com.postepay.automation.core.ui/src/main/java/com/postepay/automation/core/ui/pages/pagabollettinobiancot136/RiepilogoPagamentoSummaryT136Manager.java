package com.postepay.automation.core.ui.pages.pagabollettinobiancot136;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.postepay.automation.core.ui.molecules.RiepilogoPagamanetoM258;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class RiepilogoPagamentoSummaryT136Manager extends PageManager {

	public RiepilogoPagamentoSummaryT136Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderPage(String inputTitle) throws Error {	
		LayoutTools layOutTool= new LayoutTools();
		layOutTool.verifyHeaderPage(inputTitle);
	}
	
	public void verifyFilledData(String contoCorrente, String targetUser, String importo, String causale, String nome, String cognome, String indirizzo, String cap, String citta, String provincia, String cardNumber) {
		Particle cc = (Particle) UiObjectRepo.get().get(RiepilogoPagamanetoM258.M258CONTOCORRENTE);
		String aCc = cc.getElement().getText();
		Particle user = (Particle) UiObjectRepo.get().get(RiepilogoPagamanetoM258.M258INTESTATARIO);
		String aUser = user.getElement().getText();
		Particle amount = (Particle) UiObjectRepo.get().get(RiepilogoPagamanetoM258.M258IMPORTO);
		String aAmount = amount.getElement().getText();
		Particle commissione = (Particle) UiObjectRepo.get().get(RiepilogoPagamanetoM258.M258COMMISSIONE);
		String aCommissione = commissione.getElement().getText();
		Particle text = (Particle) UiObjectRepo.get().get(RiepilogoPagamanetoM258.M258CAUSALE);
		String aText = text.getElement().getText();
		Particle executedBy = (Particle) UiObjectRepo.get().get(RiepilogoPagamanetoM258.M258ESEGUITODA);
		String aExecutedBy = executedBy.getElement().getText();
		
		Particle pagaConLabel = (Particle) UiObjectRepo.get().get(RiepilogoPagamanetoM258.M258PAGACONLABEL);
		String aPagaConLabel = pagaConLabel.getElement().getText();
		Particle pagaConCard = (Particle) UiObjectRepo.get().get(RiepilogoPagamanetoM258.M258PAGACONVALORE);
		String aPagaConCard = pagaConCard.getElement().getText();
		
//		Expected result
		String eCc = contoCorrente;
		String eUser = targetUser.toUpperCase();
		String eAmount = "€ " + importo.replace(".", ",");
		String eCommissione = "€ 1,50";
		String eText = causale;
		String eExecutedBy = nome + " " + cognome + "\r\n" + indirizzo + " - " + cap + " " + citta + " (" + provincia + ")"; 
		
		String ePagaConLabel = "PAGA CON";

		String ePagaConCard = "Postepay Evolution " + cardNumber;
		if(cognome.contains("ROMANI")) {
			ePagaConCard = "Postepay " + cardNumber;
		}

		System.out.println("CC | ER: " + eCc + " AR:"+ aCc);
		assertTrue("Controllo summary CC | ER --> " + eCc, aCc.equals(eCc));
		
		System.out.println("User | ER: " + eUser + " AR:"+ aUser);
		assertTrue("Controllo summary Intestatario | ER --> " + eUser, aUser.equals(eUser));
		
		System.out.println("Amount | ER: " + eAmount + " AR:"+ aAmount);
		assertTrue("Controllo summary Importo | ER --> " + eAmount, aAmount.equals(eAmount));
		
		System.out.println("CC | ER: " + eCommissione + " AR:"+ aCommissione);
		assertTrue("Controllo summary Commissioni | ER --> " + eCommissione, aCommissione.equals(eCommissione));
	
		System.out.println("Causale | ER: " + eText + " AR:"+ aText);
		assertTrue("Controllo summary Causale | ER --> " + eText, aText.equals(eText));
//		assertTrue("Controllo summary EseguitoDa | ER --> " + eExecutedBy, aExecutedBy.equals(eExecutedBy));
		
		System.out.println("PagaConLabel | ER: " + ePagaConLabel + " AR:"+ aPagaConLabel);
		assertTrue("Controllo summary PagaLabel | ER --> " + ePagaConLabel, aPagaConLabel.equals(ePagaConLabel));
		
		System.out.println("CC | ER: " + ePagaConCard + " AR:"+ aPagaConCard);
		assertTrue("Controllo summary PagaNumeroCarta | ER --> " + ePagaConCard, aPagaConCard.equals(ePagaConCard));

		System.out.println("Start Scroll");
		WaitManager.get().waitShortTime();
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 500);
		WaitManager.get().waitShortTime();
		
		Particle pagaBtn = (Particle) UiObjectRepo.get().get(RiepilogoPagamanetoM258.M258BTNPAGA);
		String aPagaBtn = pagaBtn.getElement().getText();
		System.out.println("importo: " + importo);
		double somma = Double.parseDouble(importo) + 1.5;
		System.out.println("somma: " + somma);
		String sommaString = Double.toString(somma).replace(".0", ",00");
		sommaString = sommaString.replace(".", ",");
		System.out.println("sommaString: " + sommaString);
		String ePagaBtn = "PAGA € " + sommaString;
		System.out.println("ePagaBtn: " + ePagaBtn);
		System.out.println("aPagaBtn: " + aPagaBtn);
		assertTrue("Controllo summary PagaBtnSomma | ER --> " + ePagaBtn, aPagaBtn.equals(ePagaBtn));
		
	/* Particle annullaBtn = (Particle) UiObjectRepo.get().get(RiepilogoPagamanetoM258.M258BTNANNULLA);
		String aAnnullaBtn = annullaBtn.getElement().getText();
		String eAnnullaBtn = "ANNULLA";
		assertTrue("Controllo summary BtnAnnulla | ER --> " + eAnnullaBtn, aAnnullaBtn.equals(eAnnullaBtn)); */
	}
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = BigDecimal.valueOf(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	public void clickOnPaga() {
		Particle pagaBtn = (Particle) UiObjectRepo.get().get(RiepilogoPagamanetoM258.M258BTNPAGA);
		pagaBtn.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnAnnulla() {
		Particle annullaBtn = (Particle) UiObjectRepo.get().get(RiepilogoPagamanetoM258.M258BTNANNULLA);
		annullaBtn.getElement().click();
		WaitManager.get().waitMediumTime();
	}
}

