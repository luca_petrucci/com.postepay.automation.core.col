package com.postepay.automation.core.ui.pages.sendg2gpagestep3;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.SummaryG2gSend;

import io.appium.java_client.android.AndroidDriver;


public class SendG2GPageStep3 extends Page {
	public static final String NAME="T020";
	

	public SendG2GPageStep3(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(SummaryG2gSend.NAME, UiObjectRepo.get().get(SummaryG2gSend.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SendG2GPageStep3Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new SendG2GPageStep3ManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeaderpage(String inputTitle) {
		((SendG2GPageStep3Manager)this.manager).verifyHeaderpage(inputTitle);
	}
	
	public void verifyLayoutPage() {
		((SendG2GPageStep3Manager)this.manager).verifyLayoutPage();
	}
	
	public void verifyGigaDifferentAmount() {
		((SendG2GPageStep3Manager)this.manager).verifyGigaDifferentAmount();
	}
	
	public void clickOnSendG2G() {
		((SendG2GPageStep3Manager)this.manager).clickOnSendG2G();
	}
	
	public void verifyImageCard(WebDriver driver, double discrepanza) {
		((SendG2GPageStep3Manager)this.manager).verifyImageCard(driver, discrepanza);
	}
}

