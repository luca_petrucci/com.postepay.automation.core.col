package com.postepay.automation.core.ui.pages.sendg2gpagestep3;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import com.postepay.automation.core.ui.molecules.SummaryG2gSend;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.ImageUtils;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import org.openqa.selenium.WebDriver;

public class SendG2GPageStep3Manager extends PageManager {

	Particle targetUser = (Particle) UiObjectRepo.get().get(SummaryG2gSend.ICONCONTACT);
	Particle gigaCard = (Particle) UiObjectRepo.get().get(SummaryG2gSend.AMOUNTGIGATOSEND);
	Particle newGigaAmount = (Particle) UiObjectRepo.get().get(SummaryG2gSend.INDICATORGIGANEW);
	Particle oldGigaAmount = (Particle) UiObjectRepo.get().get(SummaryG2gSend.INDICATORGIGAOLD);
	Particle gigaBarStatus = (Particle) UiObjectRepo.get().get(SummaryG2gSend.BARINDICATOR);
	Particle gigaDescriptionStatus = (Particle) UiObjectRepo.get().get(SummaryG2gSend.DESCRIPTIONSENDGIGA);
	Particle sendBtn = (Particle) UiObjectRepo.get().get(SummaryG2gSend.BUTTONSENDGIGASUMMARY);
	
	public SendG2GPageStep3Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage(String inputTitle) {
		LayoutTools tool = new LayoutTools();
		tool.verifyPresenceHeaderPage(inputTitle);
	}
	
	public void verifyLayoutPage() {	
		assertTrue(targetUser.getElement().isDisplayed());
		assertTrue(gigaCard.getElement().isDisplayed());
		assertTrue(newGigaAmount.getElement().isDisplayed());
		assertTrue(oldGigaAmount.getElement().isDisplayed());
		assertTrue(gigaBarStatus.getElement().isDisplayed());
		assertTrue(gigaDescriptionStatus.getElement().isDisplayed());
		assertTrue(sendBtn.getElement().isDisplayed());
	}
	
	public void verifyGigaDifferentAmount() {
		
		String toSendAmount = gigaCard.getElement().getText();
		String newAmount = newGigaAmount.getElement().getText();
		String oldAmount = oldGigaAmount.getElement().getText();
		
		System.out.println("toSendAmount --> " + toSendAmount);
		System.out.println("newAmount --> " + newAmount);
		System.out.println("oldAmount --> " + oldAmount);
		
		StringAndNumberOperationTools tool = new StringAndNumberOperationTools();
		tool.verifyG2GAmount(oldAmount, newAmount, toSendAmount);
	}
	
	public void clickOnSendG2G() {
				
		sendBtn.getElement().click();
		
		WaitManager.get().waitLongTime();
	}
	
	public void verifyImageCard(WebDriver driver, double discrepanza) {
		
		LayoutImage.get().verifyImage(driver, "g2g/g2g_targetIcon", discrepanza);
	}
}

