package com.postepay.automation.core.ui.pages.carburantestep2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;

import test.automation.core.UIUtils;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import utility.SoftAssertion;

public class CarburanteStep2ManagerIOS extends CarburanteStep2Manager {

	public CarburanteStep2ManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage() {
		Particle p=(Particle) UiObjectRepo.get().get(HeaderGenericAllPage.TITLEGENERICWITHTITLE);
		String nuovoLocator=Utility.replacePlaceHolders(p.getLocator(), new Entry("title","XCUIElementTypeStaticText"));
		WebDriver driver=UIUtils.ui().getCurrentDriver();
		WebElement newHeader = driver.findElement(By.xpath(nuovoLocator));
		SoftAssertion.get().getAssertions().assertThat(newHeader.getText()).withFailMessage("Controllo header carrburante Step2 Fallito").isEqualTo("Acquista Carburante");
	}

}
