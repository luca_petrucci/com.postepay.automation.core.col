package com.postepay.automation.core.ui.pages.paymentpaymentpage;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.TabsPaymentSection;
import com.postepay.automation.core.ui.molecules.FooterHomePage;
import com.postepay.automation.core.ui.molecules.BodyPaymentPaymentSection;


public class PaymentPaymentPage extends Page {
	public static final String NAME="T027";


	public PaymentPaymentPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(TabsPaymentSection.NAME, UiObjectRepo.get().get(TabsPaymentSection.NAME), true);
		this.addToTemplate(FooterHomePage.NAME, UiObjectRepo.get().get(FooterHomePage.NAME), true);
		this.addToTemplate(BodyPaymentPaymentSection.NAME, UiObjectRepo.get().get(BodyPaymentPaymentSection.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {

		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {

		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {

		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new PaymentPaymentPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {

		return new PaymentPaymentPageManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {

		return null;
	}

	public void clickOnSendP2p() {
		((PaymentPaymentPageManager)this.manager).clickOnSendP2p();
	}
	
	public void clickOnPayWithQR() {
		((PaymentPaymentPageManager)this.manager).clickOnPayWithQR();;
	}
	
	public void clickOnTransfertSepa() {
		((PaymentPaymentPageManager)this.manager).clickOnTransfertSepa();
	}
	
	public void clickOnPayBullettin() {
		((PaymentPaymentPageManager)this.manager).clickOnPayBullettin();
	}
	
	public void clickOnMenuTabIcon() {
		((PaymentPaymentPageManager)this.manager).clickOnMenuTabIcon();
	}

	public void clickOnProductsTabIcon() {

		((PaymentPaymentPageManager)this.manager).clickOnProductsTabIcon();
	}

	public void clickOnCommunityTabIcon() {

		((PaymentPaymentPageManager)this.manager).clickOnCommunityTabIcon();
	}

	public void clickOnPaymentTabIcon() {

		((PaymentPaymentPageManager)this.manager).clickOnPaymentTabIcon();
	}

	public void clickOnMapsTabIcon() {

		((PaymentPaymentPageManager)this.manager).clickOnMapsTabIcon();
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		((PaymentPaymentPageManager)this.manager).verifyHeaderpage(titlePageInput);
	}
	
	public void clickOnGenericOperation(String titleCard) {
		((PaymentPaymentPageManager)this.manager).clickOnGenericOperation(titleCard);
	}
	
	public void clickOnMinisteroTrasporti()
	{
		((PaymentPaymentPageManager)this.manager).clickOnMinisteroTrasporti();
	}
	
	public void clickOnPagaPA() {
		((PaymentPaymentPageManager)this.manager).clickOnPagaPA();	
	}
}

