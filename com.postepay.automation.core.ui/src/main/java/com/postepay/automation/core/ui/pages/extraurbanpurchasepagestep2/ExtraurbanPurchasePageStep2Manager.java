package com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep2;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.BodyTravelSolution;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;

import ui.core.support.waitutil.WaitManager;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class ExtraurbanPurchasePageStep2Manager extends PageManager {

	Particle startDate = (Particle) UiObjectRepo.get().get(BodyTravelSolution.STARTDATEEXTRAURBAN);
	Particle destination = (Particle) UiObjectRepo.get().get(BodyTravelSolution.DESTINATIONEXTRAUBAN);
	Particle modifyDestination = (Particle) UiObjectRepo.get().get(BodyTravelSolution.MODIFYDESTINATIONICON);
	Particle allFilter = (Particle) UiObjectRepo.get().get(BodyTravelSolution.DROPMENUALLFILTER);
	Particle principalFilter = (Particle) UiObjectRepo.get().get(BodyTravelSolution.DROPMENUPRINCIPALFILTER);
	Particle trainFilter = (Particle) UiObjectRepo.get().get(BodyTravelSolution.TRAINICONEXTRAURBANFILTER);
	Particle busFilter = (Particle) UiObjectRepo.get().get(BodyTravelSolution.BUSICONEXTRAURBANFILTER);
	Particle trenitaliIcon = (Particle) UiObjectRepo.get().get(BodyTravelSolution.TICKETCARDTENITALIALOGO);
	Particle travelFrom = (Particle) UiObjectRepo.get().get(BodyTravelSolution.TICKETCARDFROM);
	Particle travelTo = (Particle) UiObjectRepo.get().get(BodyTravelSolution.TICKETCARDTO);
	Particle travelAmount = (Particle) UiObjectRepo.get().get(BodyTravelSolution.TICKETCARDAMOUNT);
	Particle travelBtnChoose = (Particle) UiObjectRepo.get().get(BodyTravelSolution.BUTTONEXTRAURBANPURCHASE);
	Particle filtroVettori = (Particle) UiObjectRepo.get().get(BodyTravelSolution.FILTROVETTORI);
	Particle filtroOrdinamento = (Particle) UiObjectRepo.get().get(BodyTravelSolution.FILTROORDINAMENTO);
	Particle filtroValue = (Particle) UiObjectRepo.get().get(BodyTravelSolution.FILTROVALUE);
	
	StringAndNumberOperationTools tool = new StringAndNumberOperationTools();

	
	public ExtraurbanPurchasePageStep2Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage(String inputTitle) {
		WaitManager.get().waitMediumTime();
		WaitManager.get().waitMediumTime();
		WaitManager.get().waitLongTime();
		LayoutTools tool = new LayoutTools();
		tool.verifyPresenceHeaderPage(inputTitle);
	}
	
	public void verifyLayoutPage() {
		assertTrue(startDate.getElement().isDisplayed());
		assertTrue(destination.getElement().isDisplayed());
		assertTrue(modifyDestination.getElement().isDisplayed());
		assertTrue(allFilter.getElement().isDisplayed());
		assertTrue(principalFilter.getElement().isDisplayed());
		assertTrue(trainFilter.getElement().isDisplayed());
		assertTrue(busFilter.getElement().isDisplayed());
		assertTrue(trenitaliIcon.getElement().isDisplayed());
		assertTrue(travelFrom.getElement().isDisplayed());
		assertTrue(travelTo.getElement().isDisplayed());
		assertTrue(travelAmount.getElement().isDisplayed());
	}
	
	public void clickOnChooseTicket() {
//		Particle travelBtnChoose = (Particle) UiObjectRepo.get().get(BodyTravelSolution.BUTTONEXTRAURBANPURCHASE);
//		System.out.println("Clicca su scegli");
//		System.out.println(travelFrom.getElement().getText());
		travelBtnChoose.getElement().click();
		WaitManager.get().waitMediumTime();
	}

	public void selectVector(String vettore) {
		filtroVettori.getElement().click();
		WaitManager.get().waitShortTime();
		
		List<WebElement> values = page.getDriver().findElements(filtroValue.getXPath());
		
		for (int i = 0; i < values.size(); i++) {
			WebElement el = values.get(i);
			if(el.getText().toLowerCase().trim().contains(vettore.toLowerCase().trim())) {
				el.click();
				WaitManager.get().waitShortTime();
				return;
			}	
		}
		
		assertTrue("Il filtro dei vettori non contiene il vettore target", filtroVettori.getElement().getText().toLowerCase().trim().contains(vettore.toLowerCase().trim()));
		
	}

	public void selectFilter(String filtro) {
		filtroOrdinamento.getElement().click();
		WaitManager.get().waitShortTime();
		
		List<WebElement> values = page.getDriver().findElements(filtroValue.getXPath());
		
		for (int i = 0; i < values.size(); i++) {
			WebElement el = values.get(i);
			if(el.getText().toLowerCase().trim().contains(filtro.toLowerCase().trim())) {
				el.click();
				WaitManager.get().waitShortTime();
				return;
			}	
		}
		
		assertTrue("Il filtro dei vettori non contiene il vettore target", filtroOrdinamento.getElement().getText().toLowerCase().trim().contains(filtro.toLowerCase().trim()));
		
	}
	
	public void verifyListOrderByPREZZO() {
		
		Particle amount = (Particle) UiObjectRepo.get().get(BodyTravelSolution.PRICEEXTRAURBANPURCHASE);
		List<WebElement> values = page.getDriver().findElements(amount.getXPath());
		
		double lastAmount = 0.0;
		
		for (int i = 0; i < values.size(); i++) {
			WebElement el = values.get(i);
			System.out.println("Risultato numero: " + i + " importo " + el.getText());
			double actualAmount = tool.convertStringToDouble(el.getText().trim().replace("€", "").trim());
			if(actualAmount < lastAmount) {
				assertTrue("L'importo del biglietto attuale è piu basso del precedente", false);
				return;
			}
			lastAmount = actualAmount;
		}
	}
	
	
}

