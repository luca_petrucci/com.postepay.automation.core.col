package com.postepay.automation.core.ui.pages.urbanpurchasepagestep2;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.BodyTicketUrbanPurchase;
import com.postepay.automation.core.ui.molecules.BodyUrbanPurchase;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class UrbanPurchasePageStep2Manager extends PageManager {

	Particle cardText = (Particle) UiObjectRepo.get().get(BodyTicketUrbanPurchase.DESCRIPTIONTIKETURBANPURCHASE);
	Particle cardAmount = (Particle) UiObjectRepo.get().get(BodyTicketUrbanPurchase.PRICETIKETURBANPURCHASE);
	
	public UrbanPurchasePageStep2Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderpage() {
		LayoutTools tool = new LayoutTools();
		tool.verifyPresenceHeaderPage("Trasporto Urbano");
	}
	
	public void verifyLayoutPage() {
		assertTrue(cardText.getElement().isDisplayed());
		assertTrue(cardAmount.getElement().isDisplayed());
	}
	
	public void clickOnTicketCard() {
		cardText.getElement().click();
		WaitManager.get().waitMediumTime();
	}
}

