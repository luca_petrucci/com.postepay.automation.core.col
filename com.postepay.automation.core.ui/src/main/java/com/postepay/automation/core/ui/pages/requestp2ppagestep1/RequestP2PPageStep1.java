package com.postepay.automation.core.ui.pages.requestp2ppagestep1;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.SearchContactsNumberP2P;

import io.appium.java_client.android.AndroidDriver;


public class RequestP2PPageStep1 extends Page {
	public static final String NAME="T014";


	public RequestP2PPageStep1(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(SearchContactsNumberP2P.NAME, UiObjectRepo.get().get(SearchContactsNumberP2P.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {

		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {

		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {

		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new RequestP2PPageStep1Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {

		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {

		return null;
	}
	
	public void searchContact(String targetUser) {
		((RequestP2PPageStep1Manager)this.manager).searchContact(targetUser);
	}
	
	public void searchContactNoP2p(WebDriver driver, String targetUser) {
		((RequestP2PPageStep1Manager)this.manager).searchContactNoP2p(driver, targetUser);
	}
	
	public void clickOnContactTab() {
		((RequestP2PPageStep1Manager)this.manager).clickOnContactTab();
	}
}

