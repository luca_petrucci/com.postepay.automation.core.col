package com.postepay.automation.core.ui.pages.parkingpurchasepagestep2;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.BodyChooseHourAndZoneParkingPurchase;

import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.SoftAssertion;

public class ParkingPurchasePageStep2ManagerIOS extends ParkingPurchasePageStep2Manager {

	public ParkingPurchasePageStep2ManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	public void verifyLayout()
	{
		Particle iconP=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.ICONPARKINGPURCHASECHOOSEHOUR);
		Particle zona=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.ZONEPARKINGPURCHASECHOOSEHOUR);
		Particle targa=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.TARGCARPARKINGPURCHASE);
		Particle copy=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.LABELCOPYPARKINGPURCHASE);
		Particle minus=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.MINUSPARKINGPURCHASE);
		Particle plus=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.PLUSPARKINGPUCHASE);
		Particle price=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.PLUSPARKINGPUCHASE);
		Particle endTime=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.ENDTIMEPARKINGPURCHASE);
		Particle endData=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.ENDDATEPARKINGPURCHASE);
		Particle btnPaga=(Particle) UiObjectRepo.get().get(BodyChooseHourAndZoneParkingPurchase.PAYBUTTONPARKINGPURCHASE);

		WaitManager.get().waitShortTime();
		SoftAssertion.get().getAssertions().assertThat(iconP.getElement().isEnabled()).withFailMessage("Icona P non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(zona.getElement().isEnabled()).withFailMessage("Zona non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(targa.getElement().isEnabled()).withFailMessage("Targa non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(copy.getElement().isEnabled()).withFailMessage("Copy non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(minus.getElement().isEnabled()).withFailMessage("Tasto minus non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(plus.getElement().isEnabled()).withFailMessage("Tasto plus non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(price.getElement().isEnabled()).withFailMessage("Prezzo non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(endTime.getElement().isEnabled()).withFailMessage("End Time non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(endData.getElement().isEnabled()).withFailMessage("End Data non visibile").isEqualTo(true);

		assertTrue(btnPaga.getElement().isEnabled());
	}

}
