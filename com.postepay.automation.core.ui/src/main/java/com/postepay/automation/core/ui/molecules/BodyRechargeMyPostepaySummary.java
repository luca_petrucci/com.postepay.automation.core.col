package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyRechargeMyPostepaySummary extends Molecule {
	public static final String NAME="M064";
	
	public static final String SUMMARYAMOUTOFCOMMISSION="summaryAmoutOfCommission";
public static final String SUMMARYAMOUNTTOPAYPP="summaryAmountToPayPP";
public static final String CONFIRMPAYMENTTOSEND="confirmPaymentToSend";
public static final String ANTIFRAUDMESSAGEFORUSER="antiFraudMessageForUser";
public static final String SUMMARYREASONOFRECHARGE="summaryReasonOfRecharge";
public static final String SUMMARYPAYWITHPP="summaryPayWithPP";
public static final String SUMMARYHOLDERPAYPP="summaryHolderPayPP";
public static final String SUMMARYHOLDERRECHARGEPAYPP="summaryHolderRechargePayPP";
public static final String SUMMARYPAYTOPP="summaryPayToPP";
public static final String SUBTITLERECHARGESUMMARY="subTitleRechargeSummary";
public static final String TOTALAMOUNTRECHARGESUMMARY="totalAmointSummaryRecharge";

	public BodyRechargeMyPostepaySummary(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

