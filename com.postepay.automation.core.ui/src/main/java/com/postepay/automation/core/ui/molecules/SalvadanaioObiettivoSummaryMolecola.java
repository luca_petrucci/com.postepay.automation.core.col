package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SalvadanaioObiettivoSummaryMolecola extends Molecule {
	public static final String NAME="M236";
	
	public static final String SALVADANAIOSUMMARYINTESTATOAVALORE="salvadanaioSummaryIntestatoAValore";
public static final String SALVADANAIOSUMMARYMODIFICA="salvadanaioSummaryModifica";
public static final String SALVADANAIOSUMMARYTERMINEVERSAMENTORICORRENTE="salvadanaioSummaryTermineVersamentoRicorrente";
public static final String SALVADANAIOSUMMARYFREQUENZA="salvadanaioSummaryFrequenza";
public static final String SALVADANAIOSUMMARYINTESTATOA="salvadanaioSummaryIntestatoA";
public static final String SALVADANAIOSUMMARYAUTORIZZA="salvadanaioSummaryAutorizza";
public static final String SALVADANAIOSUMMARYIMPORTO="salvadanaioSummaryImporto";
public static final String SALVADANAIOSUMMARYAPARTIREDA="salvadanaioSummaryAPartireDa";
public static final String SALVADANAIOSUMMARYIMPORTOVALORE="salvadanaioSummaryImportoValore";
public static final String SALVADANAIOSUMMARYTERMINEVERSAMENTORICORRENTEVALORE="salvadanaioSummaryTermineVersamentoRicorrenteValore";
public static final String SALVADANAIOSUMMARYAPARTIREDAVALORE="salvadanaioSummaryAPartireDaValore";
public static final String SALVADANAIOSUMMARYVERSACONVALORE="salvadanaioSummaryVersaConValore";
public static final String SALVADANAIOSUMMARYVERSACONLABEL="salvadanaioSummaryVersaConLabel";
public static final String SALVADANAIOSUMMARYFREQUENZAVALORE="salvadanaioSummaryFrequenzaValore";
public static final String SALVADANAIOSUMMARYTITOLO="salvadanaioSummaryTitolo";
public static final String SALVADANAIOSUMMARYCONTINUA="salvadanaioSummaryContinua";


	public SalvadanaioObiettivoSummaryMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

