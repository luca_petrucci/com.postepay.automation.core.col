package com.postepay.automation.core.ui.pages.creaposteid;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import com.postepay.automation.core.ui.molecules.CreaPosteIdMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import ui.core.support.waitutil.WaitManager;

import io.appium.java_client.AppiumDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class CreaPosteIdManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();

	Particle creaLabel = (Particle) UiObjectRepo.get().get(CreaPosteIdMolecola.CREALABEL);
	Particle creaTextLabel = (Particle) UiObjectRepo.get().get(CreaPosteIdMolecola.CREATEXTLABEL);
	Particle insertFirstPosteId = (Particle) UiObjectRepo.get().get(CreaPosteIdMolecola.INSERTFIRSTPOSTEID);
	Particle insertSecondPosteId = (Particle) UiObjectRepo.get().get(CreaPosteIdMolecola.INSERTSECONDPOSTEID);
	Particle confermaBtnCreaPosteIs = (Particle) UiObjectRepo.get().get(CreaPosteIdMolecola.CONFERMABTNCREAPOSTEIS);
	
	public CreaPosteIdManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	
	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}

	public void verifyLayout() {
		assertTrue(creaLabel.getElement().isDisplayed());
		assertTrue(creaTextLabel.getElement().isDisplayed());
		assertTrue(insertFirstPosteId.getElement().isDisplayed());
		assertTrue(insertSecondPosteId.getElement().isDisplayed());
//		
//		assertTrue(confermaBtnCreaPosteIs.getElement().isDisplayed());
	}
	
	public void insertPosteID(Particle elm, String posteID) {
		elm.getElement().sendKeys(posteID);
	}
	
	public void createPosteID(String posteID) {
		insertPosteID(insertFirstPosteId, posteID);
		WaitManager.get().waitFor(TimeUnit.MILLISECONDS, 500);
		insertPosteID(insertSecondPosteId, posteID);
		WaitManager.get().waitFor(TimeUnit.MILLISECONDS, 500);
		((AppiumDriver<?>)page.getDriver()).hideKeyboard();	
		WaitManager.get().waitFor(TimeUnit.MILLISECONDS, 500);
	}
	
	public void clickOnConferma() {
		confermaBtnCreaPosteIs.getElement().click();
		WaitManager.get().waitLongTime();
	}
}

