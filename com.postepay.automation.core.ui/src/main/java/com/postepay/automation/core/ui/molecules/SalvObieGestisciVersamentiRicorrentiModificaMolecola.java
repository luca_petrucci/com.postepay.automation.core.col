package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SalvObieGestisciVersamentiRicorrentiModificaMolecola extends Molecule {
	public static final String NAME="M239";
	
	public static final String SALVADANAIOMODIFICAAUTOMATISMIFREQUENZAVALORE="salvadanaioModificaAutomatismiFrequenzaValore";
public static final String SALVADANAIOMODIFICAAUTOMATISMIDESCRIZIONEUNO="salvadanaioModificaAutomatismiDescrizioneUno";
public static final String SALVADANAIOMODIFICAAUTOMATISMIAPARTIREDA="salvadanaioModificaAutomatismiAPartireDa";
public static final String SALVADANAIOMODIFICAAUTOMATISMIIMPORTO="salvadanaioModificaAutomatismiImporto";
public static final String SALVADANAIOMODIFICAAUTOMATISMIIMPORTOVALORE="salvadanaioModificaAutomatismiImportoValore";
public static final String SALVADANAIOMODIFICAAUTOMATISMIAPARTIREDAVALORE="salvadanaioModificaAutomatismiAPartireDaValore";
public static final String SALVADANAIOMODIFICAAUTOMATISMILIMITE="salvadanaioModificaAutomatismiLimite";
public static final String SALVADANAIOMODIFICAAUTOMATISMITERMINEVERSAMENTOLABELDATASPECIFICA="salvadanaioModificaAutomatismiTermineVersamentoLabelDataSpecifica";
public static final String SALVADANAIOMODIFICAAUTOMATISMIVERSACONVALORE="salvadanaioModificaAutomatismiVersaConValore";
public static final String SALVADANAIOMODIFICAAUTOMATISMITERMINEVERSAMENTOLABELALRAGGIUNGIMENTO="salvadanaioModificaAutomatismiTermineVersamentoLabelAlRaggiungimento";
public static final String SALVADANAIOMODIFICAAUTOMATISMITERMINEVERSAMENTOLABEL="salvadanaioModificaAutomatismiTermineVersamentoLabel";
public static final String SALVADANAIOMODIFICAAUTOMATISMICONFERMA="salvadanaioModificaAutomatismiConferma";
public static final String SALVADANAIOMODIFICAAUTOMATISMIELIMINAAUTOMATISMO="salvadanaioModificaAutomatismiEliminaAutomatismo";
public static final String SALVADANAIOMODIFICAAUTOMATISMIFREQUENZA="salvadanaioModificaAutomatismiFrequenza";
public static final String SALVADANAIOMODIFICAAUTOMATISMIDESCRIZIONEDUE="salvadanaioModificaAutomatismiDescrizioneDue";
public static final String SALVADANAIOMODIFICAAUTOMATISMIVERSACON="salvadanaioModificaAutomatismiVersaCon";


	public SalvObieGestisciVersamentiRicorrentiModificaMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

