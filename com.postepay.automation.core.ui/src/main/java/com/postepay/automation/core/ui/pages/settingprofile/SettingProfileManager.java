package com.postepay.automation.core.ui.pages.settingprofile;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.ExtraurbaPurchaseTicketMolecola;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.SettingProfileMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class SettingProfileManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();
	
	Particle addressLabel = (Particle) UiObjectRepo.get().get(SettingProfileMolecola.PROFILEADDRESSSECTIONLABEL);
	Particle email = (Particle) UiObjectRepo.get().get(SettingProfileMolecola.PROFILEINDIRIZZILATUAEMAIL);
	Particle tuaCitta = (Particle) UiObjectRepo.get().get(SettingProfileMolecola.PROFILEINDIRIZZILATUACITTAPREFERITA);
	Particle spedizione = (Particle) UiObjectRepo.get().get(SettingProfileMolecola.PROFILEINDIRIZZIDISPEDIZIONE);
	Particle fatturazione = (Particle) UiObjectRepo.get().get(SettingProfileMolecola.PROFILEINDIRIZZIDIFATTURAZIONE);
	Particle parcheggioLabel = (Particle) UiObjectRepo.get().get(SettingProfileMolecola.PROFILEPARCHEGGIOSECTIONLABEL);
	Particle talloncino = (Particle) UiObjectRepo.get().get(SettingProfileMolecola.PROFILEPARCHEGGIOTALLONCINOHEADER);
	Particle targa = (Particle) UiObjectRepo.get().get(SettingProfileMolecola.PROFILEPARCHEGGIOLETUETARGHE);
	Particle carburanteLabel = (Particle) UiObjectRepo.get().get(SettingProfileMolecola.PROFILECARBURANTESECTIONLABEL);
	Particle pieno = (Particle) UiObjectRepo.get().get(SettingProfileMolecola.PROFILECARBURANTEILTUOPIENO);
	Particle importoPreferito = (Particle) UiObjectRepo.get().get(SettingProfileMolecola.PROFILECARBURANTEILTUOIMPORTOPREFERITO);
	Particle trasportiLabel = (Particle) UiObjectRepo.get().get(SettingProfileMolecola.PROFILETRASPORTISECTIONLABEL);
	Particle cartaFreccia = (Particle) UiObjectRepo.get().get(SettingProfileMolecola.PROFILETRASPORTILATUACARTAFRECCIA);
	Particle genericConteiner = (Particle) UiObjectRepo.get().get(SettingProfileMolecola.PROFILEGENERICCLICKABLEROWCONTEINS);
	
	public SettingProfileManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderpage(String inputTitle) {
		toolLayout.verifyPresenceHeaderPage(inputTitle);
	}
	
	public void verifyLayoutPage() {
		assertTrue(addressLabel.getElement().isDisplayed());
		assertTrue(email.getElement().isDisplayed());
		assertTrue(tuaCitta.getElement().isDisplayed());
		assertTrue(spedizione.getElement().isDisplayed());
		assertTrue(fatturazione.getElement().isDisplayed());
		assertTrue(parcheggioLabel.getElement().isDisplayed());
		assertTrue(talloncino.getElement().isDisplayed());
		assertTrue(targa.getElement().isDisplayed());
		
		toolLayout.makeAScrollOnPage(page, 500);
		WaitManager.get().waitMediumTime();
		
		assertTrue(carburanteLabel.getElement().isDisplayed());
		assertTrue(pieno.getElement().isDisplayed());
		assertTrue(importoPreferito.getElement().isDisplayed());
		assertTrue(trasportiLabel.getElement().isDisplayed());
		assertTrue(cartaFreccia.getElement().isDisplayed());
	}
	
	public void clickOnIlTuoPieno() {
		pieno.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnIlTuoRifornimentoPreferito() {
		importoPreferito.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnCHIUDI() {
		Particle btn = (Particle) UiObjectRepo.get().get(HeaderGenericAllPage.LEFTBUTTONGENERICICON);
		
		btn.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnCHIUDIRight() {
		Particle btn = (Particle) UiObjectRepo.get().get(HeaderGenericAllPage.RIGHTBUTTONGENERICICON);
		
		btn.getElement().click();
		WaitManager.get().waitMediumTime();
	}
}

