package com.postepay.automation.core.ui.pages.rechargepostepayusingtelephoncontactspage;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;

import io.appium.java_client.AppiumDriver;

import com.postepay.automation.core.ui.molecules.BodyRechargePostepayUsingTelephonContacts;


public class RechargePostepayUsingTelephonContactsPage extends Page {
	public static final String NAME="T047";
	

	public RechargePostepayUsingTelephonContactsPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(BodyRechargePostepayUsingTelephonContacts.NAME, UiObjectRepo.get().get(BodyRechargePostepayUsingTelephonContacts.NAME), false);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new RechargePostepayUsingTelephonContactsPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void selectUserToSendRecharge(String targetUser) {
		((RechargePostepayUsingTelephonContactsPageManager)this.manager).selectUserToSendRecharge(targetUser);		
	}

	public void clickOnSaveButton() {
		((RechargePostepayUsingTelephonContactsPageManager)this.manager).clickOnSaveButton();
	}
	
	public void clickOnTabMyCARD() {
		((RechargePostepayUsingTelephonContactsPageManager)this.manager).clickOnTabMyCARD();
	}
	
	public void selectUserToSendRechargeUsingCard(String targetCard) {
		((RechargePostepayUsingTelephonContactsPageManager)this.manager).selectUserToSendRechargeUsingCard(targetCard);
	}
}

