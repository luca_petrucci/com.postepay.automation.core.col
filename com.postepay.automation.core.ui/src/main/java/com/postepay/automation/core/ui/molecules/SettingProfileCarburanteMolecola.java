package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SettingProfileCarburanteMolecola extends Molecule {
	public static final String NAME="M217";
	
	public static final String AMOUNTMINUSSETTINGCARBURANTE="amountMinusSettingCarburante";
public static final String PIENOLABELSETTINGCARBURANTE="pienoLabelSettingCarburante";
public static final String AMOUNTPIENOSETTINGCARBURANTE="amountPienoSettingCarburante";
public static final String AMOUNTADDSETTINGCARBURANTE="amountAddSettingCarburante";
public static final String SAVEBUTTONSETTINGCARBURANTE="saveButtonSettingCarburante";
public static final String AMOUNTLABELSETTINGCARBURANTE="amountLabelSettingCarburante";
public static final String AMOUNTSETTEDSETTINGCARBURANTE="amountSettedSettingCarburante";
public static final String POMPAIMAGESETTINGCARBURANTE="pompaImageSettingCarburante";


	public SettingProfileCarburanteMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

