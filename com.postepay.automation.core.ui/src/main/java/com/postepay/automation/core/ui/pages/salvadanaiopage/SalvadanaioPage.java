package com.postepay.automation.core.ui.pages.salvadanaiopage;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.SalvadanaioPageMolecola;

import io.appium.java_client.android.AndroidDriver;


public class SalvadanaioPage extends Page {
	public static final String NAME="T110";
	

	public SalvadanaioPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(SalvadanaioPageMolecola.NAME, UiObjectRepo.get().get(SalvadanaioPageMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SalvadanaioPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new SalvadanaioPageManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		((SalvadanaioPageManager)this.manager).verifyHeaderpage(titlePageInput);
	}
	
	public void verifyLayoutPage(WebDriver driver, String nameInput, double discrepanza) {
		((SalvadanaioPageManager)this.manager).verifyLayoutPage(driver, nameInput, discrepanza);
	}

	public void clickOnCard(WebDriver driver, String nameInput) {
		((SalvadanaioPageManager)this.manager).clickOnCard(driver, nameInput);
	}
	
	public void verifyAutomatismoIcon(WebDriver driver, double discrepanza) {
		((SalvadanaioPageManager)this.manager).verifyAutomatismoIcon(driver, discrepanza);
	}
	
	public void controlloLista(WebDriver driver) {
		((SalvadanaioPageManager)this.manager).controlloLista(driver);
	}
}

