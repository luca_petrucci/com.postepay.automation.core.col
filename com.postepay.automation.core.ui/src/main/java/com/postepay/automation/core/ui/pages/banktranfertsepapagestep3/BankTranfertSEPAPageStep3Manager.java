package com.postepay.automation.core.ui.pages.banktranfertsepapagestep3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.text.NumberFormat;
import java.util.Locale;

import org.assertj.core.api.Assert;

import com.postepay.automation.core.ui.molecules.WhomWontSendABonificoSepaSummary;
import com.postepay.automation.core.ui.molecules.WhomWontSendAPostagiroSummary;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;

import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class BankTranfertSEPAPageStep3Manager extends PageManager {

	public BankTranfertSEPAPageStep3Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public String getNameIntestatario() {

		Particle intestatario=(Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepaSummary.SUMMARYHOLDERTORECHARGEPAYBANKTRANFERTSEPA);
		return intestatario.getElement().getText();
	}

	public String getIban() {

		Particle iban=(Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepaSummary.SUMMARYPAYTOIBANBANKTRANFERTSEPA);

		return iban.getElement().getText();
	}

	public String getAmountToPay() {

		Particle amount=(Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepaSummary.SUMMARYAMOUNTTOPAYBANKTRANFERTSEPA);

		return amount.getElement().getText();
	}

	public String getCountry() {

		Particle country=(Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepaSummary.COUNTRYRESIDENCEHOLDERBANKTRANFERTSEPA);

		return country.getElement().getText();
	}

	public String getMyIban() {

		try {
			UIUtils.mobile().swipe((MobileDriver<?>) page.getDriver(), SCROLL_DIRECTION.DOWN, 500);
		} catch (Exception err )  {}

		Particle myIban=(Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepaSummary.SUMMARYPAYWITHBANKTRANFERTSEPA);

		return myIban.getElement().getText();
	}

	public String getReason() {

		Particle reason=(Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepaSummary.SUMMARYREASONOFBANKTRANFERTSEPASUMMARY);

		return reason.getElement().getText();
	}

	public String getAmountSumFromButton() {

		Particle sum=(Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepaSummary.CONFIRMBANKTRANFERTSEPATOSEND);

		return sum.getElement().getText();
	}
	
	public String getAmountCommition() {

		Particle commition=(Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepaSummary.SUMMARYAMOUTOFCOMMISSIONBANKTRANFERTSEPA);

		return commition.getElement().getText();
	}
	public void verifySummarySepa(String nameInput, String ibanInput, String amountInput, String reasonInput, String myIbanInput) {

		String name=getNameIntestatario();
		String iban=getIban();
		String country=getCountry();
		String myIban=getMyIban();
		String reason=getReason();
		String amount=getAmountToPay();
		String amountSum=getAmountSumFromButton();
		String commition=getAmountCommition();
		
		System.out.println(name);
		System.out.println(iban);
		System.out.println(amount);
		System.out.println(country);
		System.out.println(myIban);
		System.out.println(reason);
		System.out.println(amountSum);

		// Aggiustamento input SOLDI INVIATI
		String euroPrefix="€ ";
		String newAmountInput=euroPrefix.concat(amountInput).replace(".", ",");
		System.out.println(newAmountInput);

		// Aggiustamento verifica su IMPORTO BOTTONE PAGA
		StringAndNumberOperationTools operationTool=new StringAndNumberOperationTools();
		// Otteniamo il valore numerico del bottone PAGA
		String newAmountButton=amountSum.replace("PAGA € ", "");
		double newAmountButtonInteger=operationTool.convertStringToDouble(newAmountButton);
		// Otteniamo la somma di COMMISSIONE e IMPORTO
		String newAmount=amount.replace("€ ", "");
		double newAmountInteger=operationTool.convertStringToDouble(newAmount);
		String newCommission=commition.replace("€ ", "");
		double newCommissionInteger=operationTool.convertStringToDouble(newCommission);
		// Somma
		double sumCommImp= newCommissionInteger+newAmountInteger;
		
		// Verifiche
		assertEquals(nameInput, name);
		assertEquals(ibanInput, iban);
		assertEquals(newAmountInput, amount);
		assertEquals("Italia", country);
		assertEquals(myIbanInput, myIban);
		assertEquals(reasonInput, reason);
		assertEquals("€ 1,00", commition);
		assertEquals(sumCommImp, newAmountButtonInteger, 0.0);
	}

	public void clickOnPay() {

		Particle buttonPay=(Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepaSummary.CONFIRMBANKTRANFERTSEPATOSEND);

		buttonPay.getElement().click();

		WaitManager.get().waitShortTime();
	}

	
	public void verifyHeaderPage(String inputTitle) throws Error {
		
		LayoutTools layOutTool= new LayoutTools();
		layOutTool.verifyHeaderPage(inputTitle);
	}
}

