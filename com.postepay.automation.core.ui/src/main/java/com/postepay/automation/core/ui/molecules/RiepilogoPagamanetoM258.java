package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class RiepilogoPagamanetoM258 extends Molecule {
	public static final String NAME="M258";
	
	public static final String M258BTNPAGA="M258btnPaga";
public static final String M258PAGACONLABEL="M258pagaConLabel";
public static final String M258CAUSALE="M258causale";
public static final String M258PAGACONVALORE="M258pagaConValore";
public static final String M258BTNANNULLA="M258btnAnnulla";
public static final String M258IMPORTO="M258importo";
public static final String M258INTESTATARIO="M258intestatario";
public static final String M258CONTOCORRENTE="M258contoCorrente";
public static final String M258COMMISSIONE="M258commissione";
public static final String M258ESEGUITODA="M258eseguitoDa";
public static final String M258TITLE="M258title";


	public RiepilogoPagamanetoM258(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

