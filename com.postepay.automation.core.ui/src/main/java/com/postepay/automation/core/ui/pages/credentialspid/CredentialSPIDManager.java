package com.postepay.automation.core.ui.pages.credentialspid;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.PosteSpidPopUpError;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class CredentialSPIDManager extends PageManager {

	public CredentialSPIDManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void clickOnOk() {
		
		Particle okBtn=(Particle) UiObjectRepo.get().get(PosteSpidPopUpError.OKPOSTEACCOUNT);
		
		okBtn.getElement().click();
		
		try {Thread.sleep(2000);} catch (Exception err )  {}
	}

	public void clickOnIDonHaveAccount() {
		
		Particle btn=(Particle) UiObjectRepo.get().get(PosteSpidPopUpError.NOPOSTEACCOUNT);
		
		btn.getElement().click();
		
		try {Thread.sleep(2000);} catch (Exception err )  {}
	}
	
	public void verifyHeaderPopUp() {
		Particle txt=(Particle) UiObjectRepo.get().get(PosteSpidPopUpError.TEXTTITLE);
		
		assertTrue(txt.getElement().isDisplayed());
	}
	
	public void verifyCopyPopUp() {
		Particle txt=(Particle) UiObjectRepo.get().get(PosteSpidPopUpError.TEXTBODY);
		
		assertTrue(txt.getElement().isDisplayed());
	}
	
	public void verifyLayoutPopUp() {
		
		verifyHeaderPopUp();
		verifyCopyPopUp();
		
		try {Thread.sleep(1000);} catch (Exception err )  {}
	}
}

