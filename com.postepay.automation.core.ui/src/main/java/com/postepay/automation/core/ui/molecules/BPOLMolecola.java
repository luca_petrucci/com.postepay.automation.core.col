package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BPOLMolecola extends Molecule {
	public static final String NAME="M265";

	public static final String USERNAMEBPOL="usernameBPOL";
	public static final String PASSWORDBPOL="passwordBPOL";
	public static final String ACCEDILOGINBPOL="accediLoginBPOL";
	public static final String BTNNONORABPOL="btnNonOraBPOL";
	public static final String ACCEDICARDSPECIFICPOSTEPAYBPOL="accediCardSpecificPostePayBPOL";
	public static final String ACCEDIVERIFYIDENTITYBPOL="accediVerifyIdentityBPOL";
	public static final String BTNPROSEGUIBPOL="btnProseguiBPOL";
	public static final String BTNCOOKIE="btnCookie";
	public static final String SERVIZIONLINE="serviziOnline";
	public static final String PROFILOMENU="profiloMenu";
	public static final String PROFILOMENULOGGED="profiloMenuLogged";
	public static final String ACCEDIBUTTONPOSTEPAYCRUSCOTTO="accediButtonPostepayCruscotto";
	public static final String COOKIEXBTN="cookieXbtn";
	public static final String ACCEDISPECIFICCARD="accediSpecificCard";
	public static final String CHECKBOXAUTORIZZAPOSTEPAY="checkBoxAutorizzaPostepay";
	public static final String AREAPERSONALEPOSTEPAY="areaPersonalePostePay";
	public static final String CARDPOSTEPAY="cardPostePay";
	public static final String MOBILEURLBARBPOL="mobileUrlBarBPOL";
	public static final String MOBILELOGINTITLEBPOL="mobileLoginTitleBPOL";
	public static final String MOBILEUSERNAMEBPOL="mobileUsernameBPOL";
	public static final String MOBILEPASSWORDBPOL="mobilePasswordBPOL";
	public static final String MOBILEACCEDIBUTTON="mobileAccediButton";
	public static final String MOBILESDA ="mobileSDA";
	public static final String MOBILEACCEDIBUTTONCARD = "mobileAccediButtonCard";
	public static final String MOBILEPROSEGUIBUTTON = "mobileProseguiButton";

	public BPOLMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

