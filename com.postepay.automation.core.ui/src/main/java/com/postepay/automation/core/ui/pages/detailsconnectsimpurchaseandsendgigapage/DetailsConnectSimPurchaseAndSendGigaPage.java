package com.postepay.automation.core.ui.pages.detailsconnectsimpurchaseandsendgigapage;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.ToolbarTitlePurchaseGiga;
import com.postepay.automation.core.ui.molecules.BodyOfPurchaseGiga;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;


public class DetailsConnectSimPurchaseAndSendGigaPage extends Page {
	public static final String NAME="T023";
	

	public DetailsConnectSimPurchaseAndSendGigaPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(ToolbarTitlePurchaseGiga.NAME, UiObjectRepo.get().get(ToolbarTitlePurchaseGiga.NAME), true);
		this.addToTemplate(BodyOfPurchaseGiga.NAME, UiObjectRepo.get().get(BodyOfPurchaseGiga.NAME), true);
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		
	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new DetailsConnectSimPurchaseAndSendGigaPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void clickOnSendG2G() {
		((DetailsConnectSimPurchaseAndSendGigaPageManager)this.manager).clickOnSendG2G();
	}

	public void clickOnPurchaseGiga() {
		((DetailsConnectSimPurchaseAndSendGigaPageManager)this.manager).clickOnPurchaseGiga();
	}
	
	public void verifyLayoutPage() {
		((DetailsConnectSimPurchaseAndSendGigaPageManager)this.manager).verifyLayoutPage();
	}
	
	public void clickOnBack() {
		((DetailsConnectSimPurchaseAndSendGigaPageManager)this.manager).clickOnBack();
	}

}

