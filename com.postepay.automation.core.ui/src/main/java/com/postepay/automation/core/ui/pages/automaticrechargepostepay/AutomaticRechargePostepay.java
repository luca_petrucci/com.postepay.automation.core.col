package com.postepay.automation.core.ui.pages.automaticrechargepostepay;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.BodyAutomaticRechargePostepay;


public class AutomaticRechargePostepay extends Page {
	public static final String NAME="T048";
	

	public AutomaticRechargePostepay(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(BodyAutomaticRechargePostepay.NAME, UiObjectRepo.get().get(BodyAutomaticRechargePostepay.NAME), false);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new AutomaticRechargePostepayManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void compileFormAutomaticRecharge(String valueRechargeAutomatic, String valueRechargeAutomaticDate, String valueRechargeAutomaticName) {
		((AutomaticRechargePostepayManager)this.manager).compileFormAutomaticRecharge(valueRechargeAutomatic, valueRechargeAutomaticDate, valueRechargeAutomaticName);
	}
	

	public void selectRecurrencyRechargeDate(String valueRechargeAutomaticDate) {
		((AutomaticRechargePostepayManager)this.manager).selectRecurrencyRechargeDate(valueRechargeAutomaticDate);
	}
	
	public void selectRecurrencyRechargeName(String valueRechargeAutomaticName) {
		((AutomaticRechargePostepayManager)this.manager).selectRecurrencyRechargeName(valueRechargeAutomaticName);
	}
	
	public void clickOnRechargSOGLIA() {
		((AutomaticRechargePostepayManager)this.manager).clickOnRechargSOGLIA();
	}
	
	public void sendSaldoMinimo(String saldoMinimo) {
		((AutomaticRechargePostepayManager)this.manager).sendSaldoMinimo(saldoMinimo);
	}
}

