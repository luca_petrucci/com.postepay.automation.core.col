package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class TipologiaBollettinoM255 extends Molecule {
	public static final String NAME="M255";
	
	public static final String M255TIPOLOGIA="M255Tipologia";
public static final String M255MODIFICATIPOLOGIA="M255ModificaTipologia";
public static final String M255TITLE="M255title";


	public TipologiaBollettinoM255(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

