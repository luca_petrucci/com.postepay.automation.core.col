package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyDetailsOfTravelersExtraurbanPurchase extends Molecule {
	public static final String NAME="M096";
	
	public static final String INPUTNAME="inputName";
public static final String INPUTEMAIL="inputEmail";
public static final String INPUTSURNAME="inputSurname";
public static final String INPUTCARD="inputCard";
public static final String INFOPASSENGERCONTAINER="infoPassengerContainer";
public static final String PROCEEDBUTTONBODYDETAILSOFTRAVELERS="proceedButtonBodyDetailsOfTravelers";
public static final String INPUTPHONE="inputPhone";


	public BodyDetailsOfTravelersExtraurbanPurchase(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

