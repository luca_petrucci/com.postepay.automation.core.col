package com.postepay.automation.core.ui.pages.paymentpaymentpage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.BodyPaymentPaymentSection;
import com.postepay.automation.core.ui.molecules.BodyRechargePaymentSection;
import com.postepay.automation.core.ui.molecules.FooterHomePage;
import com.postepay.automation.core.ui.molecules.TabsPaymentSection;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import test.automation.core.UIUtils;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class PaymentPaymentPageManager extends PageManager {

	public PaymentPaymentPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	LayoutTools toolLayout = new LayoutTools();
	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}
	
	public void clickOnSendP2p() {
		Particle card=(Particle) UiObjectRepo.get().get(BodyPaymentPaymentSection.SENDP2P);
		
		card.getElement().click();
		
		WaitManager.get().waitShortTime();
	}
	
	public void clickOnPayWithQR() {
		Particle card=(Particle) UiObjectRepo.get().get(BodyPaymentPaymentSection.PAYMENTWITHQRCODE);
		
		card.getElement().click();
		
		WaitManager.get().waitShortTime();
	}
	
	public void clickOnTransfertSepa() {
		Particle card=(Particle) UiObjectRepo.get().get(BodyPaymentPaymentSection.TRANSFERTSEPAANDPOSTAGIRO);
		
		card.getElement().click();
		
		WaitManager.get().waitShortTime();
	}
	
	public void clickOnPayBullettin() {
		Particle card=(Particle) UiObjectRepo.get().get(BodyPaymentPaymentSection.PAYMENTBULLETIN);
		System.out.println(card);
		card.getElement().click();
		
		WaitManager.get().waitShortTime();
	}
	
	public void clickOnMenuTabIcon() {

		Particle menuTabIcon=(Particle) UiObjectRepo.get().get(FooterHomePage.NAVIGATIONMENUHOMEVALORIZED);

		// Click sul tab Menu
		menuTabIcon.getElement().click();
		
		WaitManager.get().waitShortTime();
	}

	public void clickOnProductsTabIcon() {

		Particle productTabIcon=(Particle) UiObjectRepo.get().get(FooterHomePage.NAVIGATIONMENUHOMEPRODUCT);

		// Click sul tab Prodotti
		productTabIcon.getElement().click();
		
		WaitManager.get().waitShortTime();
	}

	public void clickOnCommunityTabIcon() {

		Particle communityTabIcon=(Particle) UiObjectRepo.get().get(FooterHomePage.NAVIGATIONMENUHOMECOMMUNITY);

		// Click sul tab Community
		communityTabIcon.getElement().click();
		
		WaitManager.get().waitShortTime();
	}

	public void clickOnPaymentTabIcon() {

		Particle paymentTabIcon=(Particle) UiObjectRepo.get().get(FooterHomePage.NAVIGATIONMENUHOMEPAYMENT);

		// Click sul tab Paga
		paymentTabIcon.getElement().click();
		
		WaitManager.get().waitShortTime();
	}

	public void clickOnMapsTabIcon() {

		Particle mapsTabIcon=(Particle) UiObjectRepo.get().get(FooterHomePage.NAVIGATIONMENUHOMEMAPS);

		// Click sul tab Mappa
		mapsTabIcon.getElement().click();
		
		WaitManager.get().waitShortTime();
	}
	
	public void clickOnGenericOperation(String titleCard) {
		Particle card=(Particle) UiObjectRepo.get().get(BodyPaymentPaymentSection.DINAMICTITLECARD);
		
		String nuovoLocator=Utility.replacePlaceHolders(card.getLocator(), new Entry("title",titleCard));
				
		page.getDriver().findElement(By.xpath(nuovoLocator)).click();
		
		WaitManager.get().waitShortTime();
	}
	
	public void clickOnMinisteroTrasporti()
	{
		Particle ministero=(Particle) UiObjectRepo.get().get(BodyPaymentPaymentSection.PAYMENTMINISTEROTRASPORTI);
		ministero.getElement().click();		
		WaitManager.get().waitShortTime();
	}
	
	public void clickOnPagaPA() {
		Particle btnPA=(Particle) UiObjectRepo.get().get(BodyPaymentPaymentSection.DINAMICTITLECARD);
		String nuovoLocator=Utility.replacePlaceHolders(btnPA.getLocator(), new Entry("title","Bollette e pagamenti"));
		page.getDriver().findElement(By.xpath(nuovoLocator)).click();
		
		WaitManager.get().waitShortTime();
	}
}

