package com.postepay.automation.core.ui.verifytools;

import static io.appium.java_client.touch.offset.PointOption.point;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.postepay.automation.core.ui.molecules.DiscoverMoreHomePage;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.HowToPayGeneral;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.HasOnScreenKeyboard;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.CoreUtility;

public class LayoutTools {

	public LayoutTools() {
		// TODO Auto-generated constructor stub
	}
	
	public int verifyHeaderPage(String titleInput) throws Error 
	{
		// specifico per la gestione della login
		Particle p=(Particle) UiObjectRepo.get().get(HeaderGenericAllPage.TITLEGENERICWITHTITLE);
		String nuovoLocator=Utility.replacePlaceHolders(p.getLocator(), new Entry("title",titleInput));
		
		WebDriver driver=UIUtils.ui().getCurrentDriver();
		int index=0;
		
		try {
			driver.findElement(By.xpath(nuovoLocator)).isDisplayed();
			
			System.out.println("Il titolo: " + titleInput + " è presente");
			index=1;
			
		} catch (Exception err) {
			
			System.out.println("[##ERROR]Il titolo: " + titleInput + " NON è presente");
			index=0;
		}

//		try {Thread.sleep(1000);} catch (Exception err )  {}
		return index;
	}
	
	public void makeAScrollOnPage(Page page, int i) {
		try {
			UIUtils.mobile().swipe((MobileDriver<?>) page.getDriver(), SCROLL_DIRECTION.DOWN, i);
		} catch (Exception err )  {}
		
		try {Thread.sleep(2000);} catch (Exception err )  {}
	}
	
	public void verifyPresenceHeaderPage(String titleInput) throws Error {
		// Implementa l'errore nello step di Junit
		Particle p=(Particle) UiObjectRepo.get().get(HeaderGenericAllPage.TITLEGENERICWITHTITLE);
		String nuovoLocator=Utility.replacePlaceHolders(p.getLocator(), new Entry("title",titleInput));
		
		WebDriver driver=UIUtils.ui().getCurrentDriver();
		
		WebElement newHeader = driver.findElement(By.xpath(nuovoLocator));
		
		assertTrue(newHeader.isDisplayed());
	}
	
	public void verifyLayoutOfPopupGeneric(WebDriver driver, String titleText, String bodyText, boolean isIOS) {
		String titleS = "//*[@resource-id='posteitaliane.posteapp.apppostepay:id/md_title' and @text='"+titleText+"']";
		String bodyS = "//*[@resource-id='posteitaliane.posteapp.apppostepay:id/md_content' and @text="+'"'+ bodyText +'"'+"]";
		
		if(isIOS) {
			titleS = "//*[@*='"+titleText+"']";
			bodyS = "//*[@*="+'"'+ bodyText +'"'+"]";
		}
				//*[@resource-id="posteitaliane.posteapp.apppostepay:id/md_content" and @text="'+bodyText+'"]';
		if(titleText !=null)
		{
				WebElement title = (WebElement) driver.findElement(By.xpath(titleS));
				assertTrue(title.isDisplayed());
				}
		WebElement body = (WebElement) driver.findElement(By.xpath(bodyS));
		assertTrue(body.isDisplayed());
		}
	
	public void verifyLayoutOfGpsPopupIos(WebDriver driver, String titleText, String bodyText, boolean isIOS) {
		String titleS =  "//*[@*='"+titleText+"'"+" " +"and"+" "+"@*="+"'"+"XCUIElementTypeStaticText"+"'"+"]";
		String bodyS = "//*[@*=concat("+bodyText+")]";
		String bodyS1 = "//*[@*=concat('Per trovare un ufficio postale sulla mappa è necessaria l', \"'\", 'autorizzazione alla localizzazione')]";
		System.out.println(bodyS);
		System.out.println(bodyS1);
		
		if(titleText !=null) 
		{
				WebElement title = (WebElement) driver.findElement(By.xpath(titleS));
				assertTrue(title.isDisplayed());
				}
		WebElement body = (WebElement) driver.findElement(By.xpath(bodyS));
		assertTrue(body.isDisplayed());
	}
	
	public void replaceCardToUseForPay(WebDriver driver, String cardTarget) {
		
		Particle p=(Particle) UiObjectRepo.get().get(HowToPayGeneral.NUMBERCARDSENDERP2P);
		String nuovoLocator=Utility.replacePlaceHolders(p.getLocator(), new Entry("title",cardTarget));
		
//		WebElement cardElement = (WebElement) driver.findElement(By.xpath(nuovoLocator));
//		
//		cardElement.click();
		
		CoreUtility.visibilityOfElement(driver, By.xpath(nuovoLocator), 10)
		.click();
		
		try {Thread.sleep(2000);} catch (Exception err )  {}
	}

	public WebElement replaceGenericPathOfElement(WebDriver driver, Particle particle, String newValue) {
		
		Particle p=particle;
		
		String nuovoLocator=Utility.replacePlaceHolders(p.getLocator(), new Entry("title", newValue));
		
		WebElement newElement = (WebElement) driver.findElement(By.xpath(nuovoLocator));
		
		return newElement;
	}
	
	public void scrollWithCoordinate(WebDriver driver, int x, int nX, int y, int nY) {
		// TouchAction e imposto il driver
		TouchAction touchAction = new TouchAction((MobileDriver<?>) driver);
//		System.out.println("Prova lo Swipe");
		touchAction.longPress(point(x,y)).moveTo(point(nX,nY)).release().perform();
		
		WaitManager.get().waitLongTime();
	}
	
	public static void closeKeyboard(AppiumDriver<?> driverAndroid) {
		driverAndroid.hideKeyboard();
		WaitManager.get().waitMediumTime();
	}
	
	public static void isPresentKeyboard(AppiumDriver<?> driverAndroid) {
		boolean statusKey = ((HasOnScreenKeyboard) driverAndroid).isKeyboardShown();
		if (statusKey) {
			System.out.println("La tastiera è aperta");
			closeKeyboard(driverAndroid);
			System.out.println("La tastiera viene chiusa");
		}else {
			System.out.println("La tastiera è già chiusa");
		}

	}
}
