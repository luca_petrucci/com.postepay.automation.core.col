package com.postepay.automation.core.ui.pages.rechargeotherpostepaypagestep2;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.HowWontPayRechargeOtherPostepay;
import com.postepay.automation.core.ui.molecules.SelectACardToRechargeMyPostepay;

import io.appium.java_client.android.AndroidDriver;


public class RechargeOtherPostepayPageStep2 extends Page {
	public static final String NAME="T046";
	

	public RechargeOtherPostepayPageStep2(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(HowWontPayRechargeOtherPostepay.NAME, UiObjectRepo.get().get(HowWontPayRechargeOtherPostepay.NAME), false);
		this.addToTemplate(SelectACardToRechargeMyPostepay.NAME, UiObjectRepo.get().get(SelectACardToRechargeMyPostepay.NAME), false);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new RechargeOtherPostepayPageStep2Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderpage(String titlePageInput) {
		((RechargeOtherPostepayPageStep2Manager)this.manager).verifyHeaderpage(titlePageInput);
	}
	
	public void verifyLayoutPage(WebDriver driver, String username, String titleInput, String typeInput, String numberInput, double discrepanza) {
		((RechargeOtherPostepayPageStep2Manager)this.manager).verifyLayoutPage(driver, username, titleInput, typeInput, numberInput, discrepanza);
	}
	
	public void clickOnCard(WebDriver driver, String numberInput) {
		((RechargeOtherPostepayPageStep2Manager)this.manager).clickOnCard(driver, numberInput);
	}
	
	public void clickOnLinkBancoPosta() {
		((RechargeOtherPostepayPageStep2Manager)this.manager).clickOnLinkBancoPosta();
	}

}

