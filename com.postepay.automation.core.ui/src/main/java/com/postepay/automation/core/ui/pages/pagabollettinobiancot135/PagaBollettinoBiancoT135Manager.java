package com.postepay.automation.core.ui.pages.pagabollettinobiancot135;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.DatiBeneficiarioM256;
import com.postepay.automation.core.ui.molecules.DatiPaganteM257;
import com.postepay.automation.core.ui.molecules.TipologiaBollettinoM255;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.SoftAssertion;

public class PagaBollettinoBiancoT135Manager extends PageManager {

	public PagaBollettinoBiancoT135Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderPage(String inputTitle) throws Error {	
		LayoutTools layOutTool= new LayoutTools();
		layOutTool.verifyHeaderPage(inputTitle);
	}
	
	public void isBollettinoBianco() {
		Particle type = (Particle) UiObjectRepo.get().get(TipologiaBollettinoM255.M255MODIFICATIPOLOGIA);
		String sType = type.getElement().getText();
		
		assertTrue("Controllo tipo di Bollettino AR --> Bianco",sType.equals("Bianco"));
		
		Particle beneficiarioLabel = (Particle) UiObjectRepo.get().get(DatiBeneficiarioM256.M256SALVARUBRICA);
		//String labelText = beneficiarioLabel.getElement().getText();
		SoftAssertion.get().getAssertions().assertThat(beneficiarioLabel.getElement().getText()).withFailMessage("Label BENEFICIARIO errata").isEqualTo("DATI DEL BENEFICIARIO");
		//assertTrue("Controllo label sezione beneficiario AR --> DATI DEL BENEFICIARIO", labelText.equals("DATI DEL BENEFICIARIO"));
		
		Particle chiPagaLabel = (Particle) UiObjectRepo.get().get(DatiBeneficiarioM256.M256SALVARUBRICA);
		//String chiPagaText = beneficiarioLabel.getElement().getText();
		SoftAssertion.get().getAssertions().assertThat(chiPagaLabel.getElement().getText()).withFailMessage("Label CHI PAGA errata").isEqualTo("DATI DI CHI PAGA");
		//assertTrue("Controllo label sezione chi paga AR --> DATI DI CHI PAGA", chiPagaText.equals("DATI DI CHI PAGA"));
	}
	
	
	public void fillBeneficiarioField(String contoCorrente, String targetUser, String importo, String causale) {
		
		Particle cc = (Particle) UiObjectRepo.get().get(DatiBeneficiarioM256.M256CONTOCORRENTE);
		Particle user = (Particle) UiObjectRepo.get().get(DatiBeneficiarioM256.M256INTESTATARIO);
		Particle amount = (Particle) UiObjectRepo.get().get(DatiBeneficiarioM256.M256IMPORTO);
		Particle text = (Particle) UiObjectRepo.get().get(DatiBeneficiarioM256.M256CAUSALE);
		
		cc.getElement().sendKeys(contoCorrente);
		WaitManager.get().waitShortTime();
		user.getElement().sendKeys(targetUser);
		WaitManager.get().waitShortTime();
		amount.getElement().sendKeys(importo);
		WaitManager.get().waitShortTime();
		text.getElement().sendKeys(causale);
		WaitManager.get().waitShortTime();
		
		Particle title = (Particle) UiObjectRepo.get().get(DatiPaganteM257.M257TITLE);
		title.getElement().click();

	}
	
	public void scrollDown() {
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 500);
		WaitManager.get().waitShortTime();
	}
	
	public void scrollUp() {
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.UP, 500);
		WaitManager.get().waitShortTime();
	}
	
	public void verifyChiPagaSection(String nome, String cognome, String indirizzo, String citta, String cap, String provincia) {

		Particle eNome = (Particle) UiObjectRepo.get().get(DatiPaganteM257.M257VALORENOME);
		Particle eCognome = (Particle) UiObjectRepo.get().get(DatiPaganteM257.M257VALORECOGNOME);
		Particle eIndirizzo = (Particle) UiObjectRepo.get().get(DatiPaganteM257.M257VALOREINDIRIZZO);
		Particle eCitta = (Particle) UiObjectRepo.get().get(DatiPaganteM257.M257VALORECITTA);
		Particle eCap = (Particle) UiObjectRepo.get().get(DatiPaganteM257.M257VALORECAP);
		Particle eProvincia = (Particle) UiObjectRepo.get().get(DatiPaganteM257.M257VALOREPROVINCIA);
		
		String sNome = eNome.getElement().getText();
		System.out.println("sNome: " + sNome);
		
		String sCognome = eCognome.getElement().getText();
		System.out.println("sCognome: " + sCognome);
		
		String sIndirizzo = eIndirizzo.getElement().getText();
		System.out.println("sIndirizzo: " + sIndirizzo);
		
		String sCitta = eCitta.getElement().getText();
		System.out.println("sCitta: " + sCitta);
		
		String sCap = eCap.getElement().getText();
		System.out.println("sCap: " + sCap);
		
		String sProvincia = eProvincia.getElement().getText();
		System.out.println("sProvincia: " + sProvincia);
		
		assertTrue("Controllo label sezione Chi paga | Nome AR --> " + nome, sNome.equals(nome));
		assertTrue("Controllo label sezione Chi paga | Cognome AR --> " + cognome, sCognome.equals(cognome));
		assertTrue("Controllo label sezione Chi paga | Indirizzo AR --> " + indirizzo, sIndirizzo.equals(indirizzo));
		assertTrue("Controllo label sezione Chi paga | Citta AR --> " + citta, sCitta.equals(citta));
		assertTrue("Controllo label sezione Chi paga | Cap --> " + cap, sCap.equals(cap));
		assertTrue("Controllo label sezione Chi paga | Provincia AR --> " + provincia, sProvincia.equals(provincia));		
	}
	
	public void clickOnContinua() {
		Particle eProvincia = (Particle) UiObjectRepo.get().get(DatiPaganteM257.M257BTNCONFERMA);
		eProvincia.getElement().click();
		WaitManager.get().waitMediumTime();
		
		try {
			page.getDriver().findElement(By.xpath("//*[@text='CONTINUA']")).click();
			WaitManager.get().waitMediumTime();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void clickOnRubrica()
	{
		Particle rubrica = (Particle) UiObjectRepo.get().get(DatiBeneficiarioM256.M256BTNSELEZIONABENEFICIARIO);
		rubrica.getElement().click();
		WaitManager.get().waitShortTime();
	}
}

