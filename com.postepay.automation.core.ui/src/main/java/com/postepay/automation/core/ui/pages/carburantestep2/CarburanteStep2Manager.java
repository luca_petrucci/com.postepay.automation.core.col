package com.postepay.automation.core.ui.pages.carburantestep2;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.BannerCarburanteMolecola;
import com.postepay.automation.core.ui.molecules.CarburanteStep1Molecola;
import com.postepay.automation.core.ui.molecules.CarburanteStep2Molecola;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;

import test.automation.core.UIUtils;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.SoftAssertion;

public class CarburanteStep2Manager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();
	StringAndNumberOperationTools toolNumber = new StringAndNumberOperationTools();
	
	Particle banner = (Particle) UiObjectRepo.get().get(BannerCarburanteMolecola.BANNER);
	Particle bannerIcon = (Particle) UiObjectRepo.get().get(BannerCarburanteMolecola.BANNERICONPROVIDER);
	Particle bannerHeader = (Particle) UiObjectRepo.get().get(BannerCarburanteMolecola.BANNERHEADER);
	Particle bannerAddress = (Particle) UiObjectRepo.get().get(BannerCarburanteMolecola.BANNERADDRESS);
	
	Particle selectErogatoreLabel = (Particle) UiObjectRepo.get().get(CarburanteStep2Molecola.SELECTEROFATORELABEL);
	Particle erogatorePoint = (Particle) UiObjectRepo.get().get(CarburanteStep2Molecola.EROGATOREPOINTER);
	Particle erogatoreIcon = (Particle) UiObjectRepo.get().get(CarburanteStep2Molecola.EROGATOREICON);
	Particle erogatoreAvaible = (Particle) UiObjectRepo.get().get(CarburanteStep2Molecola.EROGATOREAVIBLE);
	Particle erogatoreNumber = (Particle) UiObjectRepo.get().get(CarburanteStep2Molecola.EROGATORENUMBER);
	Particle btn = (Particle) UiObjectRepo.get().get(CarburanteStep2Molecola.BUTTONPAGA);
	
	public CarburanteStep2Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderpage() {
		toolLayout.verifyPresenceHeaderPage("Acquista Carburante");
	}
	
	public void verifyLayoutPage() {
		assertTrue(banner.getElement().isDisplayed());
		assertTrue(bannerIcon.getElement().isDisplayed());
		assertTrue(bannerHeader.getElement().isDisplayed());
		assertTrue(bannerAddress.getElement().isDisplayed());
		assertTrue(selectErogatoreLabel.getElement().isDisplayed());
		assertTrue(erogatorePoint.getElement().isDisplayed());
		assertTrue(erogatoreIcon.getElement().isDisplayed());
		assertTrue(erogatoreAvaible.getElement().isDisplayed());
		assertTrue(erogatoreNumber.getElement().isDisplayed());	
	}
	
	public void clickOnErogatore() {
		erogatoreAvaible.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnPaga() {
		btn.getElement().click();
		WaitManager.get().waitLongTime();
	}
}

