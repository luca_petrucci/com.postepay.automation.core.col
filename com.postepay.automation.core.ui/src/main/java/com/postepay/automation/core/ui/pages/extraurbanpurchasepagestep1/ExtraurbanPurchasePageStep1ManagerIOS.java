package com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep1;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.BodyExtraurbanPurchase;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;

import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.SoftAssertion;

public class ExtraurbanPurchasePageStep1ManagerIOS extends ExtraurbanPurchasePageStep1Manager {

	public ExtraurbanPurchasePageStep1ManagerIOS(Page page) {
		super(page);
		
	}

	
	public void sendStartPlace(WebDriver driver, String startPlace) {
		searchBarPlace.getElement().sendKeys(startPlace);
		WaitManager.get().waitMediumTime();
		
		// Cerchiamo nelle opzioni di ricerca l'oggetto con il nome destinationPlace
		String loc = "//*[@*='XCUIElementTypeTable']//following::*[@*='" + startPlace + "']";
		System.out.println(loc);
		WebElement row = (WebElement) driver.findElement(By.xpath(loc));
		row.click();
		WaitManager.get().waitMediumTime();
	}
	
	
	public void sendDestinationPlace(WebDriver driver, String destinationPlace) {
		searchBarPlace.getElement().sendKeys(destinationPlace);
		WaitManager.get().waitMediumTime();
		
		// Cerchiamo nelle opzioni di ricerca l'oggetto con il nome destinationPlace
		String loc = "//*[@*='XCUIElementTypeTable']//following::*[@*='" + destinationPlace + "']";
		WebElement row = (WebElement) driver.findElement(By.xpath(loc));
		row.click();
		WaitManager.get().waitMediumTime();
	}
	
	public void verifyData(String moreInfo) {
		
		if (moreInfo.equals("si")) {
			// TODO Implementare controlli per quando si introduce un test con modifiche su numero passeggeri 
		}else {
			// Verifichiamo che di default la data sia quella di oggi
			StringAndNumberOperationTools tool = new StringAndNumberOperationTools();
			String toDay = tool.getDateExtraurban();
			System.out.println("Actual "+ date.getElement().getText() + " toDay " +toDay);
			SoftAssertion.get().getAssertions().assertThat(date.getElement().getText()).withFailMessage("Campo data non valorizzato").isEqualTo(toDay);
//			assertTrue(date.getElement().getText().equals(toDay));
			date.getElement().click();
			Particle frecciaDestra = (Particle) UiObjectRepo.get().get(BodyExtraurbanPurchase.FRECCIADESTRACALENDARIO);
			frecciaDestra.getElement().click();
			WaitManager.get().waitMediumTime();
			Particle calendario = (Particle) UiObjectRepo.get().get(BodyExtraurbanPurchase.CALENDARIO);
			calendario.getElement().click();
		}
	}
	
	public void verifyLayoutPage() {
		//assertTrue(partenza.getElement().getAttribute("visible").equals("true"));
		assertTrue(partenza.getElement()!=null);
		assertTrue(destinazione.getElement()!=null);
		assertTrue(toggleAndata.getElement()!=null);
		assertTrue(toggleAndataRitorno.getElement()!=null);
		assertTrue(date.getElement()!=null);
		assertTrue(hours.getElement()!=null);
		assertTrue(passengersContains.getElement()!=null);
		assertTrue(passengerAdultLabel.getElement()!=null);
		assertTrue(passengerKindLabel.getElement()!=null);
		assertTrue(btnSearchTicket.getElement()!=null);
	}
}
