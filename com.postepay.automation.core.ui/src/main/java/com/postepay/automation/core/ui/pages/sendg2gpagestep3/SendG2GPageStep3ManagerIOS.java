package com.postepay.automation.core.ui.pages.sendg2gpagestep3;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;

import ui.core.support.page.Page;

public class SendG2GPageStep3ManagerIOS extends SendG2GPageStep3Manager {

	public SendG2GPageStep3ManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyLayoutPage()
	{
		assertTrue(targetUser.getElement()!=null);
		assertTrue(gigaCard.getElement()!=null);
		assertTrue(newGigaAmount.getElement()!=null);
		assertTrue(oldGigaAmount.getElement()!=null);
		assertTrue(gigaBarStatus.getElement()!=null);
		assertTrue(gigaDescriptionStatus.getElement().getText().contains("Inviando 1 giga a"));
		assertTrue(sendBtn.getElement()!=null);
	}
	
public void verifyGigaDifferentAmount() {
		
		String toSendAmount = "1,00GB";
		String newAmount = newGigaAmount.getElement().getText();
		String oldAmount = oldGigaAmount.getElement().getText();
		
		System.out.println("toSendAmount --> " + toSendAmount);
		System.out.println("newAmount --> " + newAmount);
		System.out.println("oldAmount --> " + oldAmount);
		
		StringAndNumberOperationTools tool = new StringAndNumberOperationTools();
		tool.verifyG2GAmount(oldAmount, newAmount, toSendAmount);
	}

}
