package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SalvadanaioAccantonamentoCondividiObiettivoMolecola extends Molecule {
	public static final String NAME="M232";
	
	public static final String SALVADANAIOCONDIVIDIBUTTON="salvadanaioCondividiButton";
public static final String SALVADANAIOCONDIVIDISTRUMENTOACCREDITOUSATO="salvadanaioCondividiStrumentoAccreditoUsato";
public static final String SALVADANAIOCONDIVIDISTRUMENTOACCREDITOLABEL="salvadanaioCondividiStrumentoAccreditoLabel";
public static final String SALVADANAIOCONDIVIDIIMAGE="salvadanaioCondividiImage";
public static final String SALVADANAIOCONDIVIDITESTO="salvadanaioCondividiTesto";


	public SalvadanaioAccantonamentoCondividiObiettivoMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

