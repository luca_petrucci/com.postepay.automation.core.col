package com.postepay.automation.core.ui.pages.mapsgenericscreenfromhomepage;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.MapListBody;
import com.postepay.automation.core.ui.molecules.BodyMapsGenericScreenFromHomePage;


public class MapsGenericScreenFromHomePage extends Page {
	public static final String NAME="T071";
	

	public MapsGenericScreenFromHomePage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(BodyMapsGenericScreenFromHomePage.NAME, UiObjectRepo.get().get(BodyMapsGenericScreenFromHomePage.NAME), true);
		this.addToTemplate(MapListBody.NAME, UiObjectRepo.get().get(MapListBody.NAME), false);
	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new MapsGenericScreenFromHomePageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderpage(String titlePageInput)
	{
		((MapsGenericScreenFromHomePageManager)this.manager).verifyHeaderpage(titlePageInput);
	}
	public void clickOnApriDettaglio() {
		((MapsGenericScreenFromHomePageManager)this.manager).clickOnApriDettaglio();
	}
	
	public void verifyScontiPostePage() {
		((MapsGenericScreenFromHomePageManager)this.manager).verifyScontiPostePage();
	}
	
	public void clickOnCodiceQR() {
		((MapsGenericScreenFromHomePageManager)this.manager).clickOnCodiceQR();
	}
	
	public void isGeoAuthorized(String text) {
		((MapsGenericScreenFromHomePageManager)this.manager).isGeoAuthorized(text);
	}
	
	public void clickOnIntantPayFirstMerchant() {
		((MapsGenericScreenFromHomePageManager)this.manager).clickOnIntantPayFirstMerchant();
	}
	
	public String getQRCategory() {
		return ((MapsGenericScreenFromHomePageManager)this.manager).getQRCategory();
	}
	
	public String getQRName() {
		return ((MapsGenericScreenFromHomePageManager)this.manager).getQRName();
	}
	
	public String getQRAddress() {
		return ((MapsGenericScreenFromHomePageManager)this.manager).getQRAddress();
	}
	
	public void clickOnScontiDaAccreditareCard() {
		((MapsGenericScreenFromHomePageManager)this.manager).clickOnScontiDaAccreditareCard();
	}
	
	public void clickOnTabMappa()
	{
		((MapsGenericScreenFromHomePageManager)this.manager).clickOnTabMappa();
	}
	public void clickOnTabLista()
	{
		((MapsGenericScreenFromHomePageManager)this.manager).clickOnTabLista();
	}
	
	
	public void clickOnUfficiPostali()
	{
		((MapsGenericScreenFromHomePageManager)this.manager).clickOnUfficiPostali();
	}
	
	public void clickOnATM()
	{
		((MapsGenericScreenFromHomePageManager)this.manager).clickOnATM();
	}
	public void clickOnSosta()
	{
		((MapsGenericScreenFromHomePageManager)this.manager).clickOnSosta();
	}
	public void clickOnCarburante()
	{
		((MapsGenericScreenFromHomePageManager)this.manager).clickOnCarburante();
	}
	
	public void scrollChipLabel()
	{
		((MapsGenericScreenFromHomePageManager)this.manager).scrollChipLabel();
	}
	public void verifyPin()
	{
		((MapsGenericScreenFromHomePageManager)this.manager).verifyPin();
	}
	
	public void clickOnPin(int inputPin)
	{
		((MapsGenericScreenFromHomePageManager)this.manager).clickOnPin(inputPin);
	}
	
	public void verifyCategorie(String inputCategoria)
	{
		((MapsGenericScreenFromHomePageManager)this.manager).verifyCategorie(inputCategoria);
	}
	
	public void verifyGenericLayout()
	{
		((MapsGenericScreenFromHomePageManager)this.manager).verifyGenericLayout();
	}

	public void clickOnCashback() {
		((MapsGenericScreenFromHomePageManager)this.manager).clickOnCashback();
	}
	
	public void verifyLayoutNewPageMaps(boolean isIOS){
		((MapsGenericScreenFromHomePageManager)this.manager).verifyLayoutNewPageMaps(isIOS);
	}

	public void clickOnCerca() {
		((MapsGenericScreenFromHomePageManager)this.manager).clickOnCerca();
	}
	
}

