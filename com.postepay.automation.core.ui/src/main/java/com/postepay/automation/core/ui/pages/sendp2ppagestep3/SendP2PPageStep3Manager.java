package com.postepay.automation.core.ui.pages.sendp2ppagestep3;

import com.postepay.automation.core.ui.molecules.AmoutCreditToSend;

import io.appium.java_client.AppiumDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class SendP2PPageStep3Manager extends PageManager {

	public SendP2PPageStep3Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void insetAmoutToSend(String amount) {

		Particle amountEditText=(Particle) UiObjectRepo.get().get(AmoutCreditToSend.AMOUNTSENDP2P);

		amountEditText.getElement().sendKeys(amount);
		
//		((AppiumDriver<?>)page.getDriver()).hideKeyboard();	
	}

	public void clickOnConfirmButton() {

		Particle btn=(Particle) UiObjectRepo.get().get(AmoutCreditToSend.BUTTONSENDP2P);

		btn.getElement().click();	
	}
}

