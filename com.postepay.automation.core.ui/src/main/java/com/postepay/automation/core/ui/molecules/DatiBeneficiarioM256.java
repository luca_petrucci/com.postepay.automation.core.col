package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class DatiBeneficiarioM256 extends Molecule {
	public static final String NAME="M256";
	
	public static final String M256CAUSALE="M256causale";
public static final String M256TITLE="M256title";
public static final String M256SALVARUBRICA="M256salvaRubrica";
public static final String M256CONTOCORRENTE="M256contoCorrente";
public static final String M256BTNSELEZIONABENEFICIARIO="M256btnSelezionaBeneficiario";
public static final String M256IMPORTO="M256importo";
public static final String M256INTESTATARIO="M256intestatario";


	public DatiBeneficiarioM256(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

