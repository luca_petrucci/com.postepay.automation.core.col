package com.postepay.automation.core.ui.pages.mieiacquistipage;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.MieiAcquistiPageMolecole;
import com.postepay.automation.core.ui.molecules.UrbanoRicevutaTYPMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class MieiAcquistiPageManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();

	Particle toolbar = (Particle) UiObjectRepo.get().get(MieiAcquistiPageMolecole.TOOLBARMIEIACQUISTI);
	Particle cerca = (Particle) UiObjectRepo.get().get(MieiAcquistiPageMolecole.SEARCHTEXTMIEIACQUISTI);
	Particle filtri = (Particle) UiObjectRepo.get().get(MieiAcquistiPageMolecole.FILTERMIEIACQUISTI);
	Particle filtriCount = (Particle) UiObjectRepo.get().get(MieiAcquistiPageMolecole.FILTERCOUNTMIEIACQUISTI);
	Particle iconBiglietto = (Particle) UiObjectRepo.get().get(MieiAcquistiPageMolecole.IMAGECARDMIEIACQUISTI);
	Particle titoloBiglietto = (Particle) UiObjectRepo.get().get(MieiAcquistiPageMolecole.TITLEBIGLIETTOMIEIACQUISTI);
	Particle descrizioneBiglietto = (Particle) UiObjectRepo.get().get(MieiAcquistiPageMolecole.DESCRIPTIONBIGLIETTOMIEIACQUISTI);
	Particle dataBiglietto = (Particle) UiObjectRepo.get().get(MieiAcquistiPageMolecole.DATABIGLIETTOMIEIACQUISTI);
	Particle importoBiglietto = (Particle) UiObjectRepo.get().get(MieiAcquistiPageMolecole.IMPORTOBIGLIETTOMIEIACQUISTI);

	public MieiAcquistiPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderPage(String inputTitle) {
		toolLayout.verifyHeaderPage(inputTitle);
	}

	public void verifyLayoutPage(WebDriver driver) {
		WebElement toolBarE = toolLayout.replaceGenericPathOfElement(driver, toolbar, "TUTTE");
		assertTrue(toolBarE.isDisplayed());

		assertTrue(cerca.getElement().isDisplayed());
		assertTrue(filtri.getElement().isDisplayed());
		assertTrue(filtriCount.getElement().isDisplayed());
		assertTrue(iconBiglietto.getElement().isDisplayed());
		assertTrue(descrizioneBiglietto.getElement().isDisplayed());
	}

	public void clickToElementInToolbar(WebDriver driver, String toElement) {
			
		switch (toElement) {
		case "tutte":
			WebElement toolBarTutti = toolLayout.replaceGenericPathOfElement(driver, toolbar, "TUTTE");
			toolBarTutti.click();
			WaitManager.get().waitMediumTime();
			break;
			
		case "carburante":
			WebElement toolBarCarburante = toolLayout.replaceGenericPathOfElement(driver, toolbar, "ACQUISTA CARBURANTE");
			toolBarCarburante.click();
			WaitManager.get().waitMediumTime();
			break;
			
		case "parcheggio":
			WebElement toolBarParcheggio = toolLayout.replaceGenericPathOfElement(driver, toolbar, "ACQUISTA PARCHEGGIO");
			toolBarParcheggio.click();
			WaitManager.get().waitMediumTime();
			break;
			
		case "urbano":
			WebElement toolBarUrbano = toolLayout.replaceGenericPathOfElement(driver, toolbar, "ACQUISTO TRASPORTO URBANO");
			toolBarUrbano.click();
			WaitManager.get().waitMediumTime();
			break;
			
		case "extraurbano":
			WebElement toolBarExtraurbano = toolLayout.replaceGenericPathOfElement(driver, toolbar, "ACQUISTA TRASPORTO EXTRAURBANO");
			toolBarExtraurbano.click();
			WaitManager.get().waitMediumTime();
			break;
			
		default:
			WaitManager.get().waitMediumTime();
			break;
		}
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnSelectedToolbar(WebDriver driver, String toElement) {
		WebElement toolBarE = toolLayout.replaceGenericPathOfElement(driver, toolbar, toElement);
		toolBarE.click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnCardSpecific(WebDriver driver, String titleImput, String dataImput, String importoImput) {
		
		String dateString = StringAndNumberOperationTools.convertDateForMieiAcquisti(dataImput);
		System.out.println(dataImput + " -----> " + dateString);
		WebElement dateE = toolLayout.replaceGenericPathOfElement(driver, dataBiglietto, dateString);
		assertTrue(dateE.isDisplayed());
		
		WebElement titoloE = toolLayout.replaceGenericPathOfElement(driver, titoloBiglietto, titleImput);
		assertTrue(titoloE.isDisplayed());
		
		System.out.println(importoImput);
		String importoString1 = importoImput.replace("€ ", "€");
		String importoString = importoString1.replace(",", ".");
		System.out.println(importoImput + " -----> " + importoString);
		WebElement importoE = toolLayout.replaceGenericPathOfElement(driver, importoBiglietto, importoString);
		assertTrue(importoE.isDisplayed());
		
		int x = (titoloE.getLocation().getX() + dateE.getLocation().getX()) / 2;
		int y = (dateE.getLocation().getY() + importoE.getLocation().getY()) / 2;
		
		toolLayout.scrollWithCoordinate(driver, x, x, y, y);
		WaitManager.get().waitLongTime();
		
	}
}

