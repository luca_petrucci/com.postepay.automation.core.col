package com.postepay.automation.core.ui.pages.ministerotrasportisummarypage;

import static org.junit.Assert.assertTrue;

import org.apache.commons.lang.StringUtils;
import com.postepay.automation.core.ui.molecules.MinisteroTrasportiSummaryMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.DinamicData;
import utility.SoftAssertion;

public class MinisteroTrasportiSummaryPageManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();


	public MinisteroTrasportiSummaryPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}
	
	public void verifyLayoutMITSummary(String cardNumber, String codiceFiscale)
	{
		Particle title=(Particle) UiObjectRepo.get().get(MinisteroTrasportiSummaryMolecola.TITLEMINISTEROSUMMARY);
		Particle pagaCon=(Particle) UiObjectRepo.get().get(MinisteroTrasportiSummaryMolecola.LABELPAGACONMINISTEROSUMMARY);
		Particle numeroPratica=(Particle) UiObjectRepo.get().get(MinisteroTrasportiSummaryMolecola.NUMEROPRATICAMINISTEROSUMMARY);
		Particle ambito=(Particle) UiObjectRepo.get().get(MinisteroTrasportiSummaryMolecola.AMBITOMINISTEROSUMMARY);
		Particle tipologiaPratica=(Particle) UiObjectRepo.get().get(MinisteroTrasportiSummaryMolecola.TIPOLOGIAPRATICASUMMARY);
		Particle labelPratica=(Particle) UiObjectRepo.get().get(MinisteroTrasportiSummaryMolecola.LABELPRATICASUMMARY);
		Particle importo=(Particle) UiObjectRepo.get().get(MinisteroTrasportiSummaryMolecola.IMPORTOMINISTEROSUMMARY);
		Particle commissione=(Particle) UiObjectRepo.get().get(MinisteroTrasportiSummaryMolecola.COMMISSIONEMINISTEROSUMMARY);
		Particle dettaglioNum=(Particle) UiObjectRepo.get().get(MinisteroTrasportiSummaryMolecola.DETTAGLIOBOLLETTINOMINISTEROSUMMARY);
		Particle btnPlus=(Particle) UiObjectRepo.get().get(MinisteroTrasportiSummaryMolecola.BTNPLUSMINISTEROSUMMARY);
		Particle eseguitoDa=(Particle) UiObjectRepo.get().get(MinisteroTrasportiSummaryMolecola.ESEGUITODAMINISTEROSUMMARY);
		Particle fattura=(Particle) UiObjectRepo.get().get(MinisteroTrasportiSummaryMolecola.INVIOFATTURAMINISTEROSUMMARY);

		assertTrue(title.getElement() != null);
		assertTrue(pagaCon.getElement().isDisplayed());
		assertTrue(numeroPratica.getElement() != null);
		assertTrue(ambito.getElement().isDisplayed());
		assertTrue(tipologiaPratica.getElement().isDisplayed());
		assertTrue(labelPratica.getElement().isDisplayed());
		String ePagaCon= "Postepay "+cardNumber;
		assertTrue("AR: "+pagaCon.getElement().getText()+ " ER: "+ePagaCon,pagaCon.getElement().getText().equals(ePagaCon));
		UIUtils.mobile().swipe((MobileDriver<?>) page.getDriver(), SCROLL_DIRECTION.DOWN , 200);
		assertTrue("Importo non visibile", importo.getElement().isDisplayed());
		assertTrue("Commissione non visibile", commissione.getElement().isDisplayed());
		SoftAssertion.get().getAssertions().assertThat(dettaglioNum.getElement().isDisplayed()).withFailMessage("Dettaglio numero non visibile").isEqualTo(true);
		//assertTrue(dettaglioNum.getElement().isDisplayed());
		SoftAssertion.get().getAssertions().assertThat(btnPlus.getElement().isDisplayed()).withFailMessage("BTN plus non visibile").isEqualTo(true);
		//assertTrue(btnPlus.getElement().isDisplayed());
		assertTrue("Codice fiscale non visibile",eseguitoDa.getElement().isEnabled());
		String eFattura= "Richiedo l'invio della fattura";
		SoftAssertion.get().getAssertions().assertThat(fattura.getElement().getText().equals(eFattura)).withFailMessage("Fattura non visibile").isEqualTo(true);
		//assertTrue(fattura.getElement().isDisplayed());
		
		String tipoPraticaMIT= (String) DinamicData.getIstance().get("tipoPraticaMIT");
		String praticaMIT= (String) DinamicData.getIstance().get("praticaMIT");
		assertTrue("AR: "+tipologiaPratica.getElement().getText()+ "ER: "+tipoPraticaMIT, tipologiaPratica.getElement().getText().equals(tipoPraticaMIT) );
		assertTrue("AR: "+labelPratica.getElement().getText()+ "ER: "+praticaMIT, labelPratica.getElement().getText().equals(praticaMIT) );

		assertTrue(StringUtils.isNotBlank(ambito.getElement().getText()));
		assertTrue(StringUtils.isNotBlank(importo.getElement().getText()));
		assertTrue(StringUtils.isNotBlank(commissione.getElement().getText()));
		String eEseguitoDa= codiceFiscale;
		System.out.println(eseguitoDa.getElement().getText());
		System.out.println(eEseguitoDa);
		assertTrue("AR: "+eseguitoDa.getElement().getText()+ " ER: "+eEseguitoDa,eseguitoDa.getElement().getText().trim().equals(eEseguitoDa.trim()));
	}
	
	public void clickOnPagaMinistero()
	{
		System.out.println(page.getDriver().getPageSource());
		Particle btnPaga=(Particle) UiObjectRepo.get().get(MinisteroTrasportiSummaryMolecola.BTNPAGAMINISTEROSUMMARY);
		btnPaga.getElement().click();
		WaitManager.get().waitShortTime();
	}
}

