package com.postepay.automation.core.ui.pages.rubricabollettinopage;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.AbilitaInAppMolecola;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.RubricaBollettinoMolecola;


public class RubricaBollettinoPage extends Page {
	public static final String NAME="T148";
	
	

	public RubricaBollettinoPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(RubricaBollettinoMolecola.NAME, UiObjectRepo.get().get(RubricaBollettinoMolecola.NAME), true);
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(AbilitaInAppMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new RubricaBollettinoPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		((RubricaBollettinoPageManager)this.manager).verifyHeaderpage(titlePageInput);
	}

	public void verifyLayout()
	{
		((RubricaBollettinoPageManager)this.manager).verifyLayout();
	}
	
	public void scegliBeneficiario(String titoloInput, String contoCorrente, String importo, String causale)
	{
		((RubricaBollettinoPageManager)this.manager).scegliBeneficiario(titoloInput, contoCorrente, importo, causale);
	}
}

