package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyChooseManualPayBill extends Molecule {
	public static final String NAME="M122";
	
	public static final String ICONIMAGEPAYBILLSTEP2="iconImagePayBillStep2";
public static final String COPYPAYBILLSTEP2="copyPayBillStep2";
public static final String MANUALCOMPILEPAYBILLSTEP2="manualCompilePayBillStep2";
public static final String INSERTCODEBUTTONPAYBILLSTEP2="insertCodeButtonPayBillStep2";
public static final String TITLEPAYBILLSTEP2="TitlePayBillStep2";


	public BodyChooseManualPayBill(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

