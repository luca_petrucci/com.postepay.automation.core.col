package com.postepay.automation.core.ui.pages.abilitaquestodispositivopage;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.AbilitaQuestoDispositivoMolecola;


public class AbilitaQuestoDispositivoPage extends Page {
	public static final String NAME="T122";
	

	public AbilitaQuestoDispositivoPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(AbilitaQuestoDispositivoMolecola.NAME, UiObjectRepo.get().get(AbilitaQuestoDispositivoMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new AbilitaQuestoDispositivoPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderpage(String titlePageInput) {
		((AbilitaQuestoDispositivoPageManager)this.manager).verifyHeaderpage(titlePageInput);
	}

	public void verifyLayout() {
		((AbilitaQuestoDispositivoPageManager)this.manager).verifyLayout();
	}
	
	public void clickOnInizia() {
		((AbilitaQuestoDispositivoPageManager)this.manager).clickOnInizia();
	}

	public boolean checkOnb() {
		return ((AbilitaQuestoDispositivoPageManager)this.manager).checkOnb();
	}
}

