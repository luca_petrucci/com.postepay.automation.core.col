package com.postepay.automation.core.ui.pages.selezionabollettinot134;

import org.openqa.selenium.By;

import com.postepay.automation.core.ui.molecules.SelezionaBollettinoM254;

import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class SelezionaBollettinoT134Manager extends PageManager {

	public SelezionaBollettinoT134Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void onClickBollettino(String tipo)
	{
		Particle bollettino= (Particle) UiObjectRepo.get().get(SelezionaBollettinoM254.M254ELEMENTTITLE);
			Particle compilaManualmente = (Particle) UiObjectRepo.get().get(SelezionaBollettinoM254.M254COMPILAMANUALMENTE);
		System.out.println(compilaManualmente.getLocator());
		try {
			compilaManualmente.getElement().click();
		} catch (Exception e) {
			
		}
		String nuovoLocator= Utility.replacePlaceHolders(bollettino.getLocator(), new Entry("title", tipo));
		System.out.println(nuovoLocator);
		page.getDriver().findElement(By.xpath(nuovoLocator)).click();
		WaitManager.get().waitMediumTime();
	}

}

