package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class PagoPaMolecola extends Molecule {
	public static final String NAME = "M281";

	public static final String BTNCOMILAMANUALM281 = "btnComilaManualM281";
	public static final String BENEFICIARIOLABELM281 = "beneficiarioLabelM281";
	public static final String ICONFOTOM281 = "iconFotoM281";
	public static final String CANALEPAGAMENTOM281 = "canalePagamentoM281";
	public static final String TABPOSTALEM281 = "tabPostaleM281";
	public static final String TABBANCHEM281 = "tabBancheM281";
	public static final String CODICEAVVISOM281 = "codiceAvvisoM281";
	public static final String CCM281 = "ccM281";
	public static final String CODICEFISCALECREDITOREM281 = "codiceFiscaleCreditoreM281";
	public static final String CHIPAGALABELM281 = "chiPagaLabelM281";
	public static final String BTNCANCELLAM281 = "btnCancellaM281";
	public static final String NOMECHIPAGAM281 = "nomeChiPagaM281";
	public static final String CODICEFISCALECHIPAGAM281 = "codiceFiscaleChiPagaM281";
	public static final String BTNCALCOLAM281 = "btnCalcolaM281";

	public PagoPaMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}
