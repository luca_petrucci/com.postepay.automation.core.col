package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class ServiceExtraurbanoBigliettoMostratoMolecola extends Molecule {
	public static final String NAME="M227";
	
	public static final String SERVICEEXTRAURBANOORARIO="serviceExtraurbanoOrario";
public static final String SERVICEEXTRAURBANOPREZZO="serviceExtraurbanoPrezzo";
public static final String SERVICEEXTRAURBANOVALIDOILLABEL="serviceExtraurbanoValidoIlLabel";
public static final String SERVICEEXTRAURBANOVALIDOIL="serviceExtraurbanoValidoIl";
public static final String SERVICEEXTRAURBANOLOGOTRENITALIA="serviceExtraurbanoLogoTrenitalia";
public static final String SERVICEEXTRAURBANOVIAGGIOFROMTO="serviceExtraurbanoViaggioFromTo";
public static final String SERVICEEXTRAURBANOPNR="serviceExtraurbanoPNR";


	public ServiceExtraurbanoBigliettoMostratoMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

