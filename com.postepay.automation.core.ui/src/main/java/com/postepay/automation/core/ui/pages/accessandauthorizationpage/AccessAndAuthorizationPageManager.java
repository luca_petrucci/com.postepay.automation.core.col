package com.postepay.automation.core.ui.pages.accessandauthorizationpage;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.BodySettingGeneral;
import com.postepay.automation.core.ui.molecules.EnableFingerPrintPopup;
import com.postepay.automation.core.ui.molecules.FingerPrintAccessMolecola;
import com.postepay.automation.core.ui.molecules.ToggleAccessAndAuthorizationSettingPosteId;

import test.automation.core.cmd.adb.AdbCommandPrompt;
import ui.core.support.waitutil.WaitManager;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class AccessAndAuthorizationPageManager extends PageManager {

	public AccessAndAuthorizationPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void clickOnAccessWithFingerprint() {
		Particle btn=(Particle) UiObjectRepo.get().get(ToggleAccessAndAuthorizationSettingPosteId.ENABLELOGINFINGEPRINTTOGGLE);
		
		btn.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnAuthorizationWithFingerprint() {
		Particle btn=(Particle) UiObjectRepo.get().get(ToggleAccessAndAuthorizationSettingPosteId.ENABLEPAYMENTFINGEPRINTTOGGLE);
		
		btn.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
	
	public void verifyIfAccessToggle(String statusToggle) {
		Particle btn=(Particle) UiObjectRepo.get().get(ToggleAccessAndAuthorizationSettingPosteId.ENABLELOGINFINGEPRINTTOGGLE);
		WebElement btnWeb = btn.getElement();
		
		String checked=btnWeb.getAttribute("checked").toString();
		System.out.println(checked);
		
		if (statusToggle.equals("enable")) {

			System.out.println("Verifica che è abilitato");
			assertTrue(checked.equals("true"));
			
		} else if (statusToggle.equals("disable")) {
			
			System.out.println("Verifica che è disabilitato");
			assertTrue(checked.equals("false"));	
		}
	}
	
	public void verifyIfAuthorizationToggle(String statusToggle) {
		Particle btn=(Particle) UiObjectRepo.get().get(ToggleAccessAndAuthorizationSettingPosteId.ENABLEPAYMENTFINGEPRINTTOGGLE);
		
		WebElement btnWeb = btn.getElement();
		
		String checked=btnWeb.getAttribute("checked").toString();
		System.out.println(checked);
		
		if (statusToggle.equals("enable")) {

			System.out.println("Verifica che è abilitato");
			assertTrue(checked.equals("true"));
			
		} else if (statusToggle.equals("disable")) {
			
			System.out.println("Verifica che è disabilitato");
			assertTrue(checked.equals("false"));	
		}
	}
	
	public void verifyLayoutPopupEnableFingerprint() {
		Particle title = (Particle) UiObjectRepo.get().get(EnableFingerPrintPopup.TEXTENABLEFINGERPOPUP);
		Particle body = (Particle) UiObjectRepo.get().get(EnableFingerPrintPopup.BODYENABLEFINGERPOPUP);
		Particle enableBtn = (Particle) UiObjectRepo.get().get(EnableFingerPrintPopup.ENABLEENABLEFINGERPOPUP);
		Particle disableBtn = (Particle) UiObjectRepo.get().get(EnableFingerPrintPopup.DELETEENABLEFINGERPOPUP);
		
		assertTrue(title.getElement().isDisplayed());
		assertTrue(body.getElement().isDisplayed());
		assertTrue(enableBtn.getElement().isDisplayed());
		assertTrue(disableBtn.getElement().isDisplayed());
	}
	
	public void clickOnEnableOnPopupFingerprint() {
		Particle enableBtn = (Particle) UiObjectRepo.get().get(EnableFingerPrintPopup.ENABLEENABLEFINGERPOPUP);
		
		enableBtn.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
	
	// Popup di inserimento finger print
	public void verifyLayoutPopupSendFingerprint() {
		Particle title = (Particle) UiObjectRepo.get().get(FingerPrintAccessMolecola.FINGERPRINTACCESSTITLE);
		Particle body = (Particle) UiObjectRepo.get().get(FingerPrintAccessMolecola.FINGERPRINTENABLEACCESSDESCRIPTION);
		Particle iconFinger = (Particle) UiObjectRepo.get().get(FingerPrintAccessMolecola.FINGERPRINTACCESSSETTINGICON);
		Particle textFinger = (Particle) UiObjectRepo.get().get(FingerPrintAccessMolecola.FINGERPRINTACCESSSETTINGTEXT);
		Particle disableBtn = (Particle) UiObjectRepo.get().get(FingerPrintAccessMolecola.FINGERPRINTACCESSBUTTON);
		
		assertTrue(title.getElement().isDisplayed());
		assertTrue(body.getElement().isDisplayed());
		assertTrue(iconFinger.getElement().isDisplayed());
		assertTrue(textFinger.getElement().isDisplayed());
		assertTrue(disableBtn.getElement().isDisplayed());
	}
	
	public void sendFingerprint(String adbExe, String deviceName, int fingerId) throws Exception {
//		String exc = "adb -e emu finger touch " + fingerId;
//		Runtime.getRuntime().exec(exc);
		AdbCommandPrompt cmd=new AdbCommandPrompt(adbExe);
		cmd.setDeviceName(deviceName);
		cmd.fingerTouch(fingerId);
		
		WaitManager.get().waitMediumTime();
	}
}

