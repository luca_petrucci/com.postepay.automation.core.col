package com.postepay.automation.core.ui.pages.typonbpage;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.TypOnbMolecola;

import io.appium.java_client.android.AndroidDriver;


public class TypOnbPage extends Page {
	public static final String NAME="T126";
	

	public TypOnbPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(TypOnbMolecola.NAME, UiObjectRepo.get().get(TypOnbMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new TypOnbPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyLayout(WebDriver driver, double discrepanza) {
		((TypOnbPageManager)this.manager).verifyLayout(driver, discrepanza);
	}
	
	public void clickOnChiudi() {
		((TypOnbPageManager)this.manager).clickOnChiudi();
	}
}

