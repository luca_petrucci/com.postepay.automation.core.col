package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyInsertCodePosteIdDuringDeleteId extends Molecule {
	public static final String NAME="M108";
	
	public static final String CODENOTARRIVEDINSERTCODE="codeNotArrivedInsertCode";
public static final String INSERTCODETEXTINSERTCODE="insertCodeTextInsertCode";
public static final String SAFETYCODEFIELDINSERTCODE="SafetyCodeFieldInsertCode";
public static final String SAFETYCODELABELINSERTCODE="SafetyCodeLabelInsertCode";
public static final String NEXTBUTTONINSERTCODE="nextButtonInsertCode";


	public BodyInsertCodePosteIdDuringDeleteId(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

