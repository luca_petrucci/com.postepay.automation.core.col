package com.postepay.automation.core.ui.pages.automaticrechargepostepaysummary;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.AutomaticRechargePostepaySummaryMolecola;

import io.appium.java_client.AppiumDriver;


public class AutomaticRechargePostepaySummary extends Page {
	public static final String NAME="T089";
	

	public AutomaticRechargePostepaySummary(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(AutomaticRechargePostepaySummaryMolecola.NAME, UiObjectRepo.get().get(AutomaticRechargePostepaySummaryMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new AutomaticRechargePostepaySummaryManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	public void verifySummaryRecharge(String targetOperation,String nameInput, String cardTargetInput, String amountInput, String reasonInput, String nameOfAutomaticRechargeNameInput) {
		((AutomaticRechargePostepaySummaryManager)this.manager).verifySummaryRecharge(targetOperation, nameInput, cardTargetInput, amountInput, reasonInput, nameOfAutomaticRechargeNameInput);
	}

	public void clickAutorizationButton() {
		((AutomaticRechargePostepaySummaryManager)this.manager).clickAutorizationButton();
	}

	public void clickLinkOfDetails() {

		((AutomaticRechargePostepaySummaryManager)this.manager).clickLinkOfDetails();
	}
	
	public void verifyCopyMessage() {

		((AutomaticRechargePostepaySummaryManager)this.manager).verifyCopyMessage();
	}
	
	public void verifyRealTimeMessage() {
		((AutomaticRechargePostepaySummaryManager)this.manager).verifyRealTimeMessage();
	}
	
	public void verifyHeaderPage(String inputTitle) throws Error {
		((AutomaticRechargePostepaySummaryManager)this.manager).verifyHeaderPage(inputTitle);
	}
	
	public void verifySummariRicaricaAutomaticaSOGLIA(String targetUser, String targetUserCardNumber, String targetReason, String targetImporto) {
		((AutomaticRechargePostepaySummaryManager)this.manager).verifySummariRicaricaAutomaticaSOGLIA(targetUser, targetUserCardNumber, targetReason, targetImporto);
		
	}
}

