package com.postepay.automation.core.ui.verifytools;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

public interface BasicLayoutControl {

	public void verifyHeaderpage(String titlePageInput);
	
	public void verifyImage(AppiumDriver<?> driver, String imageNameInput, double discrepanza);
	
	public void verifyLayout(AppiumDriver<?> driver, double discrepanza);
}
