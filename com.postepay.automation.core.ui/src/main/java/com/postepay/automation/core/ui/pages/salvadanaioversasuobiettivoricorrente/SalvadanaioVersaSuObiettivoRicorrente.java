package com.postepay.automation.core.ui.pages.salvadanaioversasuobiettivoricorrente;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.SalvadanaioVersaSuObiettivoRicorrenteMolecola;
import com.postepay.automation.core.ui.pages.salvadanaioversasuobiettivo.SalvadanaioVersaSuObiettivoManager;

import io.appium.java_client.android.AndroidDriver;


public class SalvadanaioVersaSuObiettivoRicorrente extends Page {
	public static final String NAME="T115";
	

	public SalvadanaioVersaSuObiettivoRicorrente(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(SalvadanaioVersaSuObiettivoRicorrenteMolecola.NAME, UiObjectRepo.get().get(SalvadanaioVersaSuObiettivoRicorrenteMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SalvadanaioVersaSuObiettivoRicorrenteManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new SalvadanaioVersaSuObiettivoRicorrenteManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderpage(String titlePageInput) {
		((SalvadanaioVersaSuObiettivoRicorrenteManager)this.manager).verifyHeaderpage(titlePageInput);
	}
	
	public void verifyLayoutPageDefault(WebDriver driver, String nameInput, double discrepanza) {
		((SalvadanaioVersaSuObiettivoRicorrenteManager)this.manager).verifyLayoutPageDefault(driver, nameInput, discrepanza);
	}
	
	public void clickOnMinus(int times) {
		((SalvadanaioVersaSuObiettivoRicorrenteManager)this.manager).clickOnMinus(times);
	}
	
	public void clickOnPlus(int times) {
		((SalvadanaioVersaSuObiettivoRicorrenteManager)this.manager).clickOnPlus(times);
	}
	
	public void sendAmountValue(String importo) {
		((SalvadanaioVersaSuObiettivoRicorrenteManager)this.manager).sendAmountValue(importo);
	}
	
	public void verifyLayoutDinamicoAlRaggiungimento(WebDriver driver, String typeMessage, String importo, double discrepanza) {
		((SalvadanaioVersaSuObiettivoRicorrenteManager)this.manager).verifyLayoutDinamicoAlRaggiungimento(driver, typeMessage, importo, discrepanza);
	}
	
	public void scrollToElementUP(WebDriver driver) {
		((SalvadanaioVersaSuObiettivoRicorrenteManager)this.manager).scrollToElementUP(driver);
	}
	public void scrollToElementDOWN(WebDriver driver) {
		((SalvadanaioVersaSuObiettivoRicorrenteManager)this.manager).scrollToElementDOWN(driver);
	}
	
	public void scrollToElementDOWNDinamic(WebDriver driver, int size) {
		((SalvadanaioVersaSuObiettivoRicorrenteManager)this.manager).scrollToElementDOWNDinamic(driver, size);
	}
	public void clickOnCONTINUA() {
		((SalvadanaioVersaSuObiettivoRicorrenteManager)this.manager).clickOnCONTINUA();
	}
	
	public void clickOnFrequenza() {
		((SalvadanaioVersaSuObiettivoRicorrenteManager)this.manager).clickOnFrequenza();
	}
	
	public void clickOnApartireDa() {
		((SalvadanaioVersaSuObiettivoRicorrenteManager)this.manager).clickOnApartireDa();
	}
	
	public String getFrequenzaText() {
		return ((SalvadanaioVersaSuObiettivoRicorrenteManager)this.manager).getFrequenzaText();
	}
	
	public String getAPartireDaText() {
		return ((SalvadanaioVersaSuObiettivoRicorrenteManager)this.manager).getAPartireDaText();
	}
	
	public void clickOnDataSpecifica() {
		((SalvadanaioVersaSuObiettivoRicorrenteManager)this.manager).clickOnDataSpecifica();
	}

	public void varifyLayoutDinamicoDataSpecifica() {
		((SalvadanaioVersaSuObiettivoRicorrenteManager)this.manager).varifyLayoutDinamicoDataSpecifica();
	}
	
	public String getDataTermine() {
		return ((SalvadanaioVersaSuObiettivoRicorrenteManager)this.manager).getDataTermine();
	}
	
	public void clickOnSelectDataSpecifica() {
		((SalvadanaioVersaSuObiettivoRicorrenteManager)this.manager).clickOnSelectDataSpecifica();
	}
}

