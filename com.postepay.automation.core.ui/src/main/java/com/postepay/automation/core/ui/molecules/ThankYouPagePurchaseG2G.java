package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class ThankYouPagePurchaseG2G extends Molecule {
	public static final String NAME="M040";
	
	public static final String IMAGEOFPURCHASEGIGATHANKYOUPAGE="imageOfPurchaseGigaThankYouPage";
public static final String COPYOFPURCHASESUCCESSGIGATHANKYOUPAGE="copyOfPurchaseSuccessGigaThankYouPage";
public static final String CLOSEPURCHASEGIGATHANKYOUPAGE="closePurchaseGigaThankYouPage";
public static final String HEADEROFPURCHASESUCCESSGIGATHANKYOUPAGE="headerOfPurchaseSuccessGigaThankYouPage";


	public ThankYouPagePurchaseG2G(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

