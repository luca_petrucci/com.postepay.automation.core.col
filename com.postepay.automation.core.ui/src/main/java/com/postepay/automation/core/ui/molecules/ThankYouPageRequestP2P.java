package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class ThankYouPageRequestP2P extends Molecule {
	public static final String NAME="M029";
	
	public static final String AMOUNTP2PREQUESTTHANKYOUPAGE="amountP2PRequestThankYouPage";
public static final String ICONRECEIVERP2PREQUEST="iconReceiverP2PRequest";
public static final String TITLEP2PREQUEST="titleP2PRequest";
public static final String ICONP2PREQUESTTHANKYOUPAGE="iconP2PRequestThankYouPage";
public static final String BUTTONCLOSEP2PREQUEST="buttonCloseP2PRequest";
public static final String ICONSENDERP2PREQUEST="iconSenderP2PRequest";


	public ThankYouPageRequestP2P(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

