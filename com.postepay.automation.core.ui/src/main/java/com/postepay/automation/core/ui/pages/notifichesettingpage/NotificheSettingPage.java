package com.postepay.automation.core.ui.pages.notifichesettingpage;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.NotificheSettingMolecola;

import io.appium.java_client.android.AndroidDriver;


public class NotificheSettingPage extends Page {
	public static final String NAME="T127";
	

	public NotificheSettingPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(NotificheSettingMolecola.NAME, UiObjectRepo.get().get(NotificheSettingMolecola.NAME), true);
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new NotificheSettingPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new NotificheSettingPageManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		((NotificheSettingPageManager)this.manager).verifyHeaderpage(titlePageInput);
	}

	public void verifyLayoutPreferito() {
		((NotificheSettingPageManager)this.manager).verifyLayoutPreferito();
	}
	
	public void verifyLayoutNonPreferito() {
		((NotificheSettingPageManager)this.manager).verifyLayoutNonPreferito();
	}

	public void clickOnImpostaComePreferito() {
		((NotificheSettingPageManager)this.manager).clickOnImpostaComePreferito();
	}
	
	public boolean isPreferito() {
		return ((NotificheSettingPageManager)this.manager).isPreferito();
	}

	public void clickOnNotificaPush(WebDriver driver, String bigText) {
		((NotificheSettingPageManager)this.manager).clickOnNotificaPush(driver, bigText);
	}
	public void clickOnback()
	{
		((NotificheSettingPageManagerIOS)this.manager).clickOnback();
	}
	
	public void clickOnChiudi()
	{
		((NotificheSettingPageManagerIOS)this.manager).clickOnChiudi();
	}
}

