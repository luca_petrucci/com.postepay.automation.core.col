package com.postepay.automation.core.ui.pages.carburantestep2;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.BannerCarburanteMolecola;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.CarburanteStep2Molecola;


public class CarburanteStep2 extends Page {
	public static final String NAME="T101";
	

	public CarburanteStep2(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(BannerCarburanteMolecola.NAME, UiObjectRepo.get().get(BannerCarburanteMolecola.NAME), false);
this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), false);
this.addToTemplate(CarburanteStep2Molecola.NAME, UiObjectRepo.get().get(CarburanteStep2Molecola.NAME), false);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new CarburanteStep2Manager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new CarburanteStep2ManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderpage() {
		((CarburanteStep2Manager)this.manager).verifyHeaderpage();
	}
	
	public void verifyLayoutPage() {
		((CarburanteStep2Manager)this.manager).verifyLayoutPage();
	}
	
	public void clickOnErogatore() {
		((CarburanteStep2Manager)this.manager).clickOnErogatore();
	}
	
	public void clickOnPaga() {
		((CarburanteStep2Manager)this.manager).clickOnPaga();
	}
}

