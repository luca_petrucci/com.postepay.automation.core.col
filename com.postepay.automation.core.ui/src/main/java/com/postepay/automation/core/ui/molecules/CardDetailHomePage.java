package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class CardDetailHomePage extends Molecule {
	public static final String NAME="M009";
	
	public static final String CARDBALANCEHOMEPAGE="cardBalanceHomePage";
public static final String CLICKONCARD="clickOnCard";
public static final String HIDEBALANCEBUTTONCARDDETAILHOMEPAGE="hideBalanceButtonCardDetailHomePage";


	public CardDetailHomePage(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

