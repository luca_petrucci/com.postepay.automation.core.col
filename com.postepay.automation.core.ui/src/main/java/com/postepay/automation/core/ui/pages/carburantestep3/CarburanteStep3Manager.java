package com.postepay.automation.core.ui.pages.carburantestep3;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.CarburanteStep3Molecola;
import com.postepay.automation.core.ui.molecules.ExtraurbaPurchaseTicketMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;
import ui.core.support.waitutil.WaitManager;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class CarburanteStep3Manager extends PageManager {

	Particle amount = (Particle) UiObjectRepo.get().get(CarburanteStep3Molecola.CARBURANTEIMPORTO);
	Particle date = (Particle) UiObjectRepo.get().get(CarburanteStep3Molecola.CARBURANTEDATA);
	Particle payWithCard = (Particle) UiObjectRepo.get().get(CarburanteStep3Molecola.CARBURANTEPAYWITH);
	Particle btnAnnulla = (Particle) UiObjectRepo.get().get(CarburanteStep3Molecola.CARBURANTEANNULLABUTTON);
	Particle btnPaga = (Particle) UiObjectRepo.get().get(CarburanteStep3Molecola.CARBURANTEPAYBUTTON);
	
	public CarburanteStep3Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderpage() {
		LayoutTools tool = new LayoutTools();
		tool.verifyPresenceHeaderPage("Acquista Carburante");
	}
	
	public void verifyLayoutPage() {
		Particle title = (Particle) UiObjectRepo.get().get(CarburanteStep3Molecola.CARBURANTESUMMARYHEADER);
		Particle layoutPayWith = (Particle) UiObjectRepo.get().get(CarburanteStep3Molecola.CARBURANTELABEPAYWITH);
		Particle api = (Particle) UiObjectRepo.get().get(CarburanteStep3Molecola.CARBURANTEPROVIDER);

		assertTrue(title.getElement().isDisplayed());
		assertTrue(date.getElement().isDisplayed());
		assertTrue(payWithCard.getElement().isDisplayed());
		assertTrue(btnAnnulla.getElement().isDisplayed());
		assertTrue(btnPaga.getElement().isDisplayed());
		assertTrue(layoutPayWith.getElement().isDisplayed());
		assertTrue(api.getElement().isDisplayed());
		assertTrue(amount.getElement().isDisplayed());
	}
	
	public void verifyDateOf() {
		StringAndNumberOperationTools tool = new StringAndNumberOperationTools();
		String toDay = tool.getDateExtraurban();
		
		assertTrue(date.getElement().getText().equals(toDay));
	}

	public void verifyPayWithCard(String cardInput) {
		String cardIn = "Postepay Evolution" + cardInput;
		assertTrue("Errore La carta per il pagamento non viene mostrata",payWithCard.getElement().isDisplayed());
	}
	
	public void clickOnPay() {
		btnPaga.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnAnnulla() {
		btnAnnulla.getElement().click();
		WaitManager.get().waitMediumTime();
	}
}

