package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class CardDetailTotal extends Molecule {
	public static final String NAME="M015";
	
	public static final String CARDBALANCECARDDETAILTOTAL="cardBalanceCardDetailTotal";
public static final String DETAILCARDPANEL="detailCardPanel";
public static final String HIDEBALANCEBUTTONCARDDETAILTOTAL="hideBalanceButtonCardDetailTotal";
public static final String NUMBERDETAILCARDPANEL="numberDetailCardPanel";
public static final String PREFEREEDDETAILCARDPANEL="prefereedDetailCardPanel";


	public CardDetailTotal(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

