package com.postepay.automation.core.ui.pages.settingpage;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.BodySettingGeneral;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.SectionsAppHamburgerMenu;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class SettingPageManager extends PageManager {

	public SettingPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderPage(WebDriver driver, String inputTitle) {
		Particle title=(Particle) UiObjectRepo.get().get(HeaderGenericAllPage.TITLEGENERICWITHTITLE);
		
		LayoutTools tool=new LayoutTools();
		WebElement newTitle = tool.replaceGenericPathOfElement(driver, title, inputTitle);
		
		assertTrue(newTitle.isDisplayed());
		
		WaitManager.get().waitShortTime();
	}
	
	public void clickOnAccessAndAuthorization() {
		Particle btn=(Particle) UiObjectRepo.get().get(BodySettingGeneral.ACCESSANDAUTHORIZATIONARROW);
		
		btn.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnManagePosteId() {
		Particle btn=(Particle) UiObjectRepo.get().get(BodySettingGeneral.MANAGEPOSTEIDARROW);
		
		btn.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnNotification() {
		Particle btn=(Particle) UiObjectRepo.get().get(BodySettingGeneral.NOTIFICATIONARROW);
		
		btn.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}
	
}

