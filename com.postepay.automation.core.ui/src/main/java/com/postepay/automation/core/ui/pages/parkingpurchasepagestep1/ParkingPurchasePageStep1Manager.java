package com.postepay.automation.core.ui.pages.parkingpurchasepagestep1;

import org.openqa.selenium.html5.Location;
import org.openqa.selenium.html5.LocationContext;

import com.postepay.automation.core.ui.molecules.BodyChooseParkingPurchase;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.SoftAssertion;

public class ParkingPurchasePageStep1Manager extends PageManager {

	public ParkingPurchasePageStep1Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	LayoutTools toolLayout = new LayoutTools();
	
	public void verifyHeaderpage(String titlePageInput)
	{
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}
	
	public void verifyLayout()
	{
		Particle zona=(Particle) UiObjectRepo.get().get(BodyChooseParkingPurchase.ZONEPARKINGPURCHASE);
		Particle time=(Particle) UiObjectRepo.get().get(BodyChooseParkingPurchase.TIMEPARKINGPURCHASE);
		Particle city=(Particle) UiObjectRepo.get().get(BodyChooseParkingPurchase.CITYPARKINGPURCHASE);
		
		SoftAssertion.get().getAssertions().assertThat(zona.getElement().isDisplayed()).withFailMessage("Zona non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(time.getElement().isDisplayed()).withFailMessage("Time non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(city.getElement().isDisplayed()).withFailMessage("City non visibile").isEqualTo(true);
	}
	
	public void clickOnParking() {
		Particle icon=(Particle) UiObjectRepo.get().get(BodyChooseParkingPurchase.ICONPARKINGPURCHASE);
		icon.getElement().click();
	}

}

