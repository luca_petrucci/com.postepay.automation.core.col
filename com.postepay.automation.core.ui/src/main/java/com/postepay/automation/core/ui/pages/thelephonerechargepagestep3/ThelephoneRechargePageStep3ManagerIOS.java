package com.postepay.automation.core.ui.pages.thelephonerechargepagestep3;

import static org.junit.Assert.assertTrue;

import ui.core.support.page.Page;

public class ThelephoneRechargePageStep3ManagerIOS extends ThelephoneRechargePageStep3Manager {

	public ThelephoneRechargePageStep3ManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyLayoutPage() {
		assertTrue(title.getElement().isDisplayed());
		assertTrue(pagaCon.getElement().isDisplayed());
		assertTrue(numeroTel.getElement().isDisplayed());
		assertTrue(operatoreTel.getElement().isDisplayed());
		assertTrue(amountRecharge.getElement().isDisplayed());
		assertTrue(commissioni.getElement().isDisplayed());
		assertTrue(btnPaga.getElement().isEnabled());
	}
}
