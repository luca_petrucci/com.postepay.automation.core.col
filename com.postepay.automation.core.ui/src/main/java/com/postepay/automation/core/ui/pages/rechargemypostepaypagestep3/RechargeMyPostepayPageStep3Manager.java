package com.postepay.automation.core.ui.pages.rechargemypostepaypagestep3;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.SelectACardToPayTheRechargeMyPostepay;
import com.postepay.automation.core.ui.molecules.SelectACardToRechargeMyPostepay;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.SoftAssertion;

public class RechargeMyPostepayPageStep3Manager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();
	
	Particle title=(Particle) UiObjectRepo.get().get(SelectACardToRechargeMyPostepay.TARGETTITLESENDERRECHARGE);
	Particle icon=(Particle) UiObjectRepo.get().get(SelectACardToRechargeMyPostepay.TARGETICONSENDERRECHARGE);
	Particle card_type=(Particle) UiObjectRepo.get().get(SelectACardToRechargeMyPostepay.TARGETTYPESENDERRECHARGE);
	Particle card_number=(Particle) UiObjectRepo.get().get(SelectACardToRechargeMyPostepay.TARGETNUMBERCARDSENDERRECHARGE);
	Particle avaibleLabel=(Particle) UiObjectRepo.get().get(SelectACardToRechargeMyPostepay.TARGETAVAIBLELABELSENDERRECHARGE);
	Particle card_amount=(Particle) UiObjectRepo.get().get(SelectACardToRechargeMyPostepay.TARGETAMOUNTSENDERRECHARGE);
	
	public RechargeMyPostepayPageStep3Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}
	
	public WebElement getElementTitle(WebDriver driver, String titleInput) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, title, titleInput);
		assertTrue(elm.isDisplayed());
		return elm;
	}
	
	public WebElement getElementType(WebDriver driver, String typeInput) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, card_type, typeInput);
		assertTrue(elm.isDisplayed());
		return elm;
	}
	
	public WebElement getElementCardNumber(WebDriver driver, String numberInput) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, card_number, numberInput);
		assertTrue(elm.isDisplayed());
		return elm;
	}
		
	public void verifyImageCard(WebDriver driver, String imageName, double discrepanza) {
		String nameText = "ricaricaPostepay/" + imageName;
		LayoutImage.get().verifyImage(driver, nameText, discrepanza);
	}

	public void verifyLayoutPage(WebDriver driver, String titleInput, String typeInput, String numberInput, double discrepanza) {
		getElementTitle(driver, titleInput);
//		getElementType(driver, typeInput);
		getElementCardNumber(driver, numberInput);
		System.out.println(avaibleLabel.getElement().getText());
		SoftAssertion.get().getAssertions().assertThat(avaibleLabel.getElement().getText().equals("SALDO")).withFailMessage("label SALDO non presente").isEqualTo(true);
//		assertTrue(avaibleLabel.getElement() != null);
		
		WebElement elCardAmount = toolLayout.replaceGenericPathOfElement(driver, card_amount, numberInput);
		System.out.println(elCardAmount.getText());
		SoftAssertion.get().getAssertions().assertThat(elCardAmount != null).withFailMessage("Importo non presente").isEqualTo(true);
		//assertTrue(card_amount.getElement().isDisplayed());
//		verifyImageCard(driver, "icona_nera", discrepanza);
//		System.out.println("\nCardBiancoNera è stata verificata");		
//		verifyImageCard(driver, "icona_grigia_chiaro", discrepanza);
//		System.out.println("\nCardBiancoGrigia è stata verificata");
//		verifyImageCard(driver, "icona_blu", discrepanza);
//		System.out.println("\nCardBiancoBlu è stata verificata");
	}
	
	public void selectCardToRecharge(WebDriver driver, String numberInput) {
		
		getElementCardNumber(driver, numberInput).click();
		WaitManager.get().waitLongTime();
	}
	
	public void clickOnBancoPostaButton() {
		Particle bancoPosta=(Particle) UiObjectRepo.get().get(SelectACardToPayTheRechargeMyPostepay.LINKAPPBANCOPOSTASELECTACARDTOPAY);
		
		bancoPosta.getElement().click();
		
		WaitManager.get().waitLongTime();
	}
	
	public void clickOnOtherCards() {
		Particle otherCard=(Particle) UiObjectRepo.get().get(SelectACardToPayTheRechargeMyPostepay.OTHERCARDFORRECHARGEBUTTON);
		
		otherCard.getElement().click();
		
		WaitManager.get().waitLongTime();
	}
	
	public void verifyLayoutScontiPoste(WebDriver driver, String titleInput, String typeInput, String numberInput) {
		getElementTitle(driver, titleInput);
		getElementType(driver, typeInput);
		getElementCardNumber(driver, numberInput);
	}
	
	public void clickOnCardToSetAsPreferred(WebDriver driver, String numberInput) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, card_number, numberInput);
		elm.click();
		WaitManager.get().waitMediumTime();
	}
	
	public String whoIsPreferredCard() {
		Particle rows = (Particle) UiObjectRepo.get().get(SelectACardToPayTheRechargeMyPostepay.ROWCONTAINERM062);
		Particle cardNumber = (Particle) UiObjectRepo.get().get(SelectACardToPayTheRechargeMyPostepay.CARDNUMBERGENERICM062);
		Particle preferred = (Particle) UiObjectRepo.get().get(SelectACardToPayTheRechargeMyPostepay.PREFERREDM062);
		System.out.println("whoIsPreferredCard | Particelle dichiarate");
		
		List<WebElement> elms = page.getDriver().findElements(rows.getXPath()); 
		String prefCard = "";
		
		System.out.println("whoIsPreferredCard | Inizio ciclo for");
		for (int i = 0; i < elms.size(); i++) {
			WebElement elm = elms.get(i);
			
			System.out.println("whoIsPreferredCard | isPreferred?");
			WebElement r = null;
			try {
				r = elm.findElement(preferred.getXPath());
			} catch (Exception e) {
				// TODO: handle exception
			}
			System.out.println("r: "+r);
			if (r != null) {
				System.out.println("preferred == true");
				prefCard = elm.findElement(cardNumber.getXPath()).getText();
				System.out.println("prefCard: " + prefCard);				
			}
		}
		System.out.println("prefCard: " + prefCard);
		return prefCard;
	}
	
	public String whoIsNotPreferredCard() {
		Particle rows = (Particle) UiObjectRepo.get().get(SelectACardToPayTheRechargeMyPostepay.ROWCONTAINERM062);
		Particle cardNumber = (Particle) UiObjectRepo.get().get(SelectACardToPayTheRechargeMyPostepay.CARDNUMBERGENERICM062);
		Particle preferred = (Particle) UiObjectRepo.get().get(SelectACardToPayTheRechargeMyPostepay.PREFERREDM062);
		
		List<WebElement> elms = rows.getListOfElements(); 
		String notPrefCard = "";
		
		for (int i = 0; i < elms.size(); i++) {
			WebElement elm = elms.get(i);
//			System.out.println("i: "+i);
//			System.out.println("elm: "+elm);
//			try {
//				if (elm.findElement(preferred.getXPath()).isDisplayed() == false) {
//					System.out.println("preferred == false");
//					notPrefCard = elm.findElement(cardNumber.getXPath()).getText();
//				}
//			} catch (Exception e) {
//				// TODO: handle exception
//			}		
			
			System.out.println("whoIsNotPreferredCard | isNotPreferred?");
			WebElement r = null;
			try {
				r = elm.findElement(preferred.getXPath());
			} catch (Exception e) {
				// TODO: handle exception
			}
			System.out.println("r: "+r);
			if (r == null) {
				System.out.println("preferred == true");
				notPrefCard = elm.findElement(cardNumber.getXPath()).getText();
				System.out.println("notPrefCard: " + notPrefCard);				
			}
		}
		System.out.println("notPrefCard: " + notPrefCard);
		return notPrefCard;
	}
	
	public void selectAsPreferredOtherCard() {
//		Particle rows = (Particle) UiObjectRepo.get().get(SelectACardToPayTheRechargeMyPostepay.ROWCONTAINERM062);
//		Particle cardNumber = (Particle) UiObjectRepo.get().get(SelectACardToPayTheRechargeMyPostepay.CARDNUMBERGENERICM062);
//		Particle preferred = (Particle) UiObjectRepo.get().get(SelectACardToPayTheRechargeMyPostepay.PREFERREDM062);
//		
//		List<WebElement> elms = rows.getListOfElements();
		String cardNumber = whoIsNotPreferredCard();
		System.out.println("cardNumber: " + cardNumber);
				
		WebElement notPrefCardEl = getElementCardNumber((WebDriver) page.getDriver(), cardNumber);
		notPrefCardEl.click();
		System.out.println("notPrefCardEl click");
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnBackArrow() {
		Particle backArrow = (Particle) UiObjectRepo.get().get(HeaderGenericAllPage.LEFTBUTTONGENERIC);
		backArrow.getElement().click();
		WaitManager.get().waitMediumTime();
	}
}

