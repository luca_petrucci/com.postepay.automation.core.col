package com.postepay.automation.core.ui.pages.sendg2gpagestep5;

import static org.junit.Assert.assertTrue;

import ui.core.support.page.Page;

public class SendG2GPageStep5ManagerIOS extends SendG2GPageStep5Manager {

	public SendG2GPageStep5ManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	public void verifyLayoutPage() {
		assertTrue(iconG2g.getElement()!=null);
		assertTrue(iconSender.getElement()!=null);
		assertTrue(iconReceiver.getElement()!=null);
		assertTrue(g2gAmount.getElement()!=null);
		assertTrue(g2gValue.getElement()!=null);
		assertTrue(text.getElement()!=null);
		assertTrue(description.getElement().getText().contains("I Giga trasferiti verranno aggiornati a breve sul tuo profilo. Avrai a disposizione ancora"));
		assertTrue(closeBtn.getElement()!=null);
	}
}
