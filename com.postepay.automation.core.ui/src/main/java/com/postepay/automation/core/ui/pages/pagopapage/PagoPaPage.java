package com.postepay.automation.core.ui.pages.pagopapage;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.PagoPaMolecola;


public class PagoPaPage extends Page {
	public static final String NAME="T151";
	

	public PagoPaPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(PagoPaMolecola.NAME, UiObjectRepo.get().get(PagoPaMolecola.NAME), true);
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new PagoPaPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new PagoPaPageManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void clickOnManuale() {
		((PagoPaPageManager)this.manager).clickOnManuale();
	}
	
	public void clickOnTabPostale() {
		((PagoPaPageManager)this.manager).clickOnTabPostale();
	}
	
	public void clickOnTabBanche() {
		((PagoPaPageManager)this.manager).clickOnTabBanche();
	}
	
	public void verifyLayoutBanche() {
		((PagoPaPageManager)this.manager).verifyLayoutBanche();
	}
	public void click() {
		((PagoPaPageManager)this.manager).click();
	}
}

