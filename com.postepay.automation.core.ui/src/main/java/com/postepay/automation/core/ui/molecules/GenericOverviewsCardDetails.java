package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class GenericOverviewsCardDetails extends Molecule {
	public static final String NAME="M047";
	
	public static final String FRAMELAYOUTOFCARD="frameLayoutOfCard";
public static final String PREFEEREDCARDDETAILS="prefeeredCardDetails";
public static final String NUMBERCARDDETAILS="numberCardDetails";
public static final String AMOUNTOFCARDCASH="amountOfCardCash";
public static final String SIMICONCARDDETAILS="simIconCardDetails";


	public GenericOverviewsCardDetails(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

