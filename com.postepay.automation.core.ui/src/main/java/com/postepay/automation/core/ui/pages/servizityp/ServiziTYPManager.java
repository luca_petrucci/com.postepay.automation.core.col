package com.postepay.automation.core.ui.pages.servizityp;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.BodyInfoG2gSend;
import com.postepay.automation.core.ui.molecules.ServiziTYPMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.CoreUtility;

public class ServiziTYPManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();

	Particle image = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZITYPIMAGE);
	Particle titolo = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZITYPTITOLO);
	Particle descrizione = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZITYPDESCRIZIONE);
	Particle btnRicevuta = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZITYPVAIALLARICEVUTADIPAGAMENTO);
	Particle btnBiglietto = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZITYPILTUOBIGLIETTO);
	Particle titoloTalloncino = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZITITOLOTALLONCINO);
	Particle imageTalloncino = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZIIMAGETALLONCINO);
	Particle titleTalloncino = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZITITLETALLONCINO);
	Particle descriptionTalloncino = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZIDESCRIPTIONTALLONCINO);
	Particle messaggioTalloncino = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZIMESSAGGIOTALLONCINO);
	Particle stampaButtonTalloncino = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZISTAMPABUTTONTALLONCINO);
	
	public ServiziTYPManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub.
	}

	public void verifyDescription(WebDriver driver, String serviceType) {

		String text;
		
		switch (serviceType) {
		case "extraurbano":
			text = "Puoi trovare il tuo biglietto nella sezione Biglietti attivi e nella mail indicata al momento dell'acquisto.";
			break;
			
		case "urbano":
			text = "Puoi trovare il tuo biglietto nella sezione Biglietti da attivare.";
			break;
			
		case "parcheggio":
		//	precedente: Puoi trovare il tuo biglietto nella sezione Biglietti da attivare.
			text = "Sarà attivo fino alle 22:00 in 7061 - Zona 61 - Parcheggio Interscambio. Riceverai una notifica quando starà per scadere.";
			break;
		default:
			text = " ";
			break;
		}
		
		WebElement descriptionEl = toolLayout.replaceGenericPathOfElement(driver, descrizione, text);
		System.out.println(descriptionEl);
		assertTrue(descriptionEl.isDisplayed());
	}

	public void verifyImageCard(WebDriver driver, double discrepanza) {

		LayoutImage.get().verifyImage(driver, "biglietto/typ", discrepanza);
	}
	
	public void verifyLayoutPageGeneric(WebDriver driver, String serviceType, double discrepanza) {
		CoreUtility.visibilityOfElement(page.getDriver(), image, 30);
		assertTrue(image.getElement().isDisplayed());
		verifyImageCard(driver, discrepanza);
		assertTrue(titolo.getElement().isDisplayed());
		verifyDescription(driver, serviceType);
		assertTrue(btnRicevuta.getElement().isDisplayed());
		
		if (serviceType.equals("extraurbano")) {
			assertTrue(btnBiglietto.getElement().isDisplayed());
		}
		
		if(serviceType.equals("parcheggio"))
		{
			assertTrue(btnBiglietto.getElement().isDisplayed());
		}
		WaitManager.get().waitMediumTime();
	}

	public void clickOnIlTuoBiglietto() {
		btnBiglietto.getElement().click();
		WaitManager.get().waitLongTime();
	}

	public void clickOnVaiAllaRicevuta() {
		btnRicevuta.getElement().click();
		WaitManager.get().waitLongTime();
	}

	
	//Controlla che siano presenti tutti gli elementi del talloncino e lo chiude
	public void verificaTalloncino() {
		Particle titoloTalloncino = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZITITOLOTALLONCINO);
		Particle imageTalloncino = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZIIMAGETALLONCINO);
		Particle titleTalloncino = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZITITLETALLONCINO);
		Particle descriptionTalloncino = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZIDESCRIPTIONTALLONCINO);
		Particle messaggioTalloncino = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZIMESSAGGIOTALLONCINO);
		Particle stampaButtonTalloncino = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZISTAMPABUTTONTALLONCINO);
		Particle serviziCloseButtonTalloncino = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZICLOSEBUTTONTALLONCINO);
		
		assertTrue(titoloTalloncino.getElement().isDisplayed());
		assertTrue(imageTalloncino.getElement().isDisplayed());
		assertTrue(titleTalloncino.getElement().isDisplayed());
		assertTrue(descriptionTalloncino.getElement().isDisplayed());
		assertTrue(messaggioTalloncino.getElement().isDisplayed());
		assertTrue(stampaButtonTalloncino.getElement().isDisplayed());
		assertTrue(serviziCloseButtonTalloncino.getElement().isDisplayed());
		
		//Clicco sulla "x" in alto a sinistra per chiudere il talloncino
		serviziCloseButtonTalloncino.getElement().click();
		
		
	}
	
	public void interrompiTalloncino() {
		Particle serviziInterrompiButtonBiglietto = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZIINTERROMPIBUTTONBIGLIETTO);
		assertTrue(serviziInterrompiButtonBiglietto.getElement().isDisplayed());
		serviziInterrompiButtonBiglietto.getElement().click();
		Particle serviziVaiAiMieiAcquistiButton = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZIVAIMIEIACQUISTIBUTTON);
		assertTrue(serviziVaiAiMieiAcquistiButton.getElement().isDisplayed());
		serviziVaiAiMieiAcquistiButton.getElement().click();
		Particle serviziBackArrowIMieiAcquisti = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZIBACKARROWIMIEIACQUISTI);
		assertTrue(serviziBackArrowIMieiAcquisti.getElement().isDisplayed());
		serviziBackArrowIMieiAcquisti.getElement().click();
		Particle serviziCloseButtonRicevuta = (Particle) UiObjectRepo.get().get(ServiziTYPMolecola.SERVIZICLOSEBUTTONRICEVUTA);
		assertTrue(serviziCloseButtonRicevuta.getElement().isDisplayed());
		serviziCloseButtonRicevuta.getElement().click();
	
	}
	
}

