package com.postepay.automation.core.ui.pages.salvadanaiopage;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.SalvadanaioPageMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutImage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class SalvadanaioPageManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();

	Particle imageView = (Particle) UiObjectRepo.get().get(SalvadanaioPageMolecola.SALVADANAIOIMAGEVIEW);
	Particle titleView = (Particle) UiObjectRepo.get().get(SalvadanaioPageMolecola.SALVADANAIOTITLEVIEW);
	Particle textView = (Particle) UiObjectRepo.get().get(SalvadanaioPageMolecola.SALVADANAIOTEXTVIEW);
	Particle accantonamentoName = (Particle) UiObjectRepo.get().get(SalvadanaioPageMolecola.SALVADANAIOACCANTONAMENTONAME);
	Particle accantonamentoIcon = (Particle) UiObjectRepo.get().get(SalvadanaioPageMolecola.SALVADANAIOACCANTONAMENTOICON);
	Particle accantonamentoActualAmount = (Particle) UiObjectRepo.get().get(SalvadanaioPageMolecola.SALVADANAIOACCANTONAMENTOACTUALAMOUNT);
	Particle accantonamentoTotalAmount = (Particle) UiObjectRepo.get().get(SalvadanaioPageMolecola.SALVADANAIOACCANTONAMENTOTOTALAMOUNT);
	Particle accantonamentoScadenza = (Particle) UiObjectRepo.get().get(SalvadanaioPageMolecola.SALVADANAIOACCANTONAMENTOSCADENZA);
	Particle accantonamentoDettagli = (Particle) UiObjectRepo.get().get(SalvadanaioPageMolecola.SALVADANAIOACCANTONAMENTODETTAGLI);
	Particle accantonamentoYellowIconDettagli = (Particle) UiObjectRepo.get().get(SalvadanaioPageMolecola.SALVADANAIOACCANTONAMENTOYELLOWICONDETTAGLI);
	Particle accantonamentoFooterText = (Particle) UiObjectRepo.get().get(SalvadanaioPageMolecola.SALVADANAIOACCANTONAMENTOFOOTERTEXT);
	Particle accantonamentoCreaNewObiettivo = (Particle) UiObjectRepo.get().get(SalvadanaioPageMolecola.SALVADANAIOACCANTONAMENTOCREANEWOBIETTIVO);
	Particle accantonamentoStatusBar = (Particle) UiObjectRepo.get().get(SalvadanaioPageMolecola.SALVADANAIOACCANTONAMENTOSTATUSBAR);
	Particle accantonamentoIconaOrologioRicorrenza = (Particle) UiObjectRepo.get().get(SalvadanaioPageMolecola.SALVADANAIOACCANTONAMENTOICONAOROLOGIORICORRENZA);

	public SalvadanaioPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}

	public WebElement getElementName(WebDriver driver, String nameInput) {
		WebElement elm = toolLayout.replaceGenericPathOfElement(driver, accantonamentoName, nameInput);
		assertTrue(elm.isDisplayed());
		return elm;
	}

	public WebElement firstRootCard(WebDriver driver) {
		List<WebElement> rootCard = (List<WebElement>) driver.findElements(By.xpath("//*[@resource-id='posteitaliane.posteapp.apppostepay:id/text_card_salvadanaio_nome_salvadanaio']"));
		int first = 0;
		WebElement firstRoot = rootCard.get(first);
		return firstRoot;
	}

	public WebElement lastRootCard(WebDriver driver) {
		List<WebElement> rootCard = (List<WebElement>) driver.findElements(By.xpath("//*[@resource-id='posteitaliane.posteapp.apppostepay:id/text_card_salvadanaio_nome_salvadanaio']"));
		int last = rootCard.size() - 1;
		WebElement lastRoot = rootCard.get(last);
		return lastRoot;
	}
	
	public void controlloLista(WebDriver driver) {
		List<WebElement> elmL = (List<WebElement>) driver.findElements(By.xpath("//*[@resource-id='posteitaliane.posteapp.apppostepay:id/recycler_card_home_salvadanaio']/android.widget.FrameLayout"));
		System.out.println(elmL.size());
		
		System.out.println("\nStart ciclo ID::\n\n");
		
		for (int i = 0; i < elmL.size(); i++) {
			MobileElement m= (MobileElement) elmL.get(i);
			System.out.println(m.getId());	
		}
		
		scrollToElementDOWN(driver);
		
		elmL = (List<WebElement>) driver.findElements(By.xpath("//*[@resource-id='posteitaliane.posteapp.apppostepay:id/recycler_card_home_salvadanaio']/android.widget.FrameLayout"));
		System.out.println(elmL.size());
		
		System.out.println("\nStart ciclo ID::\n\n");
		
		for (int i = 0; i < elmL.size(); i++) {
			MobileElement m= (MobileElement) elmL.get(i);
			System.out.println(m.getId());	
		}
	}

	public void scrollToElementUP(WebDriver driver) {

		WebElement firstRoot = firstRootCard(driver);
		toolLayout.scrollWithCoordinate(driver,
				firstRoot.getLocation().getX()+200, firstRoot.getLocation().getX()+200,
				firstRoot.getLocation().getY(), firstRoot.getLocation().getY()+1500);
		WaitManager.get().waitHighTime();
	}

	public void scrollToElementDOWN(WebDriver driver) {

		WebElement lastRoot = lastRootCard(driver);
		toolLayout.scrollWithCoordinate(driver,
				lastRoot.getLocation().getX()+200, lastRoot.getLocation().getX()+200,
				lastRoot.getLocation().getY(), lastRoot.getLocation().getY()-1500);
		WaitManager.get().waitHighTime();

	}

	public void verifyImage(WebDriver driver, String imageNameInput, double discrepanza) {
//		String imageName = "salvadanaio/" + imageNameInput;
//		LayoutImage.get().verifyImage(driver, imageName, discrepanza);
	}

	public void verifyLayoutPage(WebDriver driver, String nameInput, double discrepanza) {

		assertTrue(imageView.getElement().isDisplayed());
		assertTrue(titleView.getElement().isDisplayed());
		assertTrue(textView.getElement().isDisplayed());
		getElementName(driver, nameInput);
		assertTrue(accantonamentoIcon.getElement().isDisplayed());
		assertTrue(accantonamentoStatusBar.getElement().isDisplayed());
		assertTrue(accantonamentoActualAmount.getElement().isDisplayed());
		assertTrue(accantonamentoTotalAmount.getElement().isDisplayed());
		assertTrue(accantonamentoScadenza.getElement().isDisplayed());
		assertTrue(accantonamentoDettagli.getElement().isDisplayed());
		assertTrue(accantonamentoYellowIconDettagli.getElement().isDisplayed());
		System.out.println("Verifica l'immagine blu del Salvadanaio");
		verifyImage(driver, "salvadanaio_image", discrepanza);
		
		try {
			scrollToElementDOWN(driver);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		//		Parte del footer che non si vede se ho più elementi
		assertTrue(accantonamentoFooterText.getElement().getText().contains("Puoi creare fino a un massimo di 5 obiettivi, per un importo totale di €5000,00."));
		assertTrue(accantonamentoFooterText.getElement().getText().contains("Vai su App BancoPosta e crea il tuo obiettivo."));
		assertTrue(accantonamentoCreaNewObiettivo.getElement().isDisplayed());
		try {
			scrollToElementUP(driver);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		//		Icona orologio su automatismo
//		assertTrue(accantonamentoIconaOrologioRicorrenza.getElement().isDisplayed());
	}
	
	public void clickOnCard(WebDriver driver, String nameInput) {
		getElementName(driver, nameInput).click();
		WaitManager.get().waitHighTime();
	}

	public void verifyAutomatismoIcon(WebDriver driver, double discrepanza) {
		verifyImage(driver, "salvadanaio_automatismo", discrepanza);
		WaitManager.get().waitShortTime();
	}
}

