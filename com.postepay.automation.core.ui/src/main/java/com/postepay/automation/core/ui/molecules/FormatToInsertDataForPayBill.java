package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class FormatToInsertDataForPayBill extends Molecule {
	public static final String NAME="M124";
	
	public static final String CAPPAYBILLSTEP4="capPayBillStep4";
public static final String COUNTFIELDPAYBILLSTEP4="countFieldPayBillStep4";
public static final String SAVECONTACTOFPAYER="savecontactOfPayer";
public static final String NAMEPAYBILLSTEP4="namePayBillStep4";
public static final String HEADERLABELPAYBILLSTEP4="headerLabelPayBillStep4";
public static final String SAVEBENEFICIARYPAYBILLSTEP4="saveBeneficiaryPayBillStep4";
public static final String CAUSALFIELDPAYBILLSTEP4="causalFieldPayBillStep4";
public static final String PAYERDATAPAYBILLSTEP4="payerDataPayBillStep4";
public static final String CONFIRMBUTTONINSERTDATABILL="confirmButtonInsertDataBill";
public static final String BENEFICIARYDATAHEADERPAYBILLSTEP4="beneficiaryDataHeaderPayBillStep4";
public static final String ACCOUNTHOLDERPAYBILLSTEP4="accountHolderPayBillStep4";
public static final String PROVINCEPAYBILLSTEP4="provincePayBillStep4";
public static final String ACCOUNTFIELDPAYBILLSTEP4="accountFieldPayBillStep4";
public static final String INDIRIZZOPAYBILLSTEP4="indirizzoPayBillStep4";
public static final String COGNOMEPAYBILLSTEP4="cognomePayBillStep4";
public static final String CITYPAYBILLSTEP4="cityPayBillStep4";


	public FormatToInsertDataForPayBill(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

