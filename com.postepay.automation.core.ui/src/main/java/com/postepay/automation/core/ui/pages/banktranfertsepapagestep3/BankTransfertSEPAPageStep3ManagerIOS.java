package com.postepay.automation.core.ui.pages.banktranfertsepapagestep3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;

import com.postepay.automation.core.ui.molecules.WhomWontSendABonificoSepaSummary;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;

import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class BankTransfertSEPAPageStep3ManagerIOS extends BankTranfertSEPAPageStep3Manager{
	
	public BankTransfertSEPAPageStep3ManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifySummarySepa(String nameInput, String ibanInput, String amountInput, String reasonInput, String myIbanInput)
	{
		Particle intestatario=(Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepaSummary.SUMMARYHOLDERTORECHARGEPAYBANKTRANFERTSEPA);
		String locatorName= Utility.replacePlaceHolders(intestatario.getLocator(), new Entry("title", nameInput));
		assertTrue("Nome errato",page.getDriver().findElement(By.xpath(locatorName)).isDisplayed());
		
		Particle iban=(Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepaSummary.SUMMARYPAYTOIBANBANKTRANFERTSEPA);
		String locatorIban= Utility.replacePlaceHolders(iban.getLocator(), new Entry("title", ibanInput));
		assertTrue("Iban errato",page.getDriver().findElement(By.xpath(locatorIban)).isDisplayed());
		
		Particle amount=(Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepaSummary.SUMMARYAMOUNTTOPAYBANKTRANFERTSEPA);
		String euroPrefix="€";
		String locatorAmount= Utility.replacePlaceHolders(amount.getLocator(), new Entry("title", euroPrefix.concat(amountInput).replace(".", ",")));
		assertTrue("Importo errato",page.getDriver().findElement(By.xpath(locatorAmount)).isDisplayed());
	
		Particle country=(Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepaSummary.COUNTRYRESIDENCEHOLDERBANKTRANFERTSEPA);
		assertTrue("Paese errato", country.getElement().isDisplayed());
		
		Particle reason=(Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepaSummary.SUMMARYREASONOFBANKTRANFERTSEPASUMMARY);
		String locatorReason= Utility.replacePlaceHolders(reason.getLocator(), new Entry("title", reasonInput));
		assertTrue("Causale errato",page.getDriver().findElement(By.xpath(locatorReason)).isDisplayed());
		
		Particle commition=(Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepaSummary.SUMMARYAMOUTOFCOMMISSIONBANKTRANFERTSEPA);
		String locatorCommition= Utility.replacePlaceHolders(commition.getLocator(), new Entry("title", "€1,00"));
		assertTrue("Commissione errato",page.getDriver().findElement(By.xpath(locatorCommition)).isDisplayed());
		
		Particle myIban=(Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepaSummary.SUMMARYPAYWITHBANKTRANFERTSEPA);
		String locatorMyIban= Utility.replacePlaceHolders(myIban.getLocator(), new Entry("title", myIbanInput));
		assertTrue("Myban errato",page.getDriver().findElement(By.xpath(locatorMyIban)).isDisplayed());
		
		Particle sum=(Particle) UiObjectRepo.get().get(WhomWontSendABonificoSepaSummary.CONFIRMBANKTRANFERTSEPATOSEND);
		String importoAR = sum.getElement().getText();
		StringAndNumberOperationTools operationTool=new StringAndNumberOperationTools();
		double newAmountButtonInteger=operationTool.convertStringToDouble(amountInput.replace(".", ","));
		double sommaTotale= newAmountButtonInteger+1;
		String controlloSomma= sommaTotale+"";
		if(controlloSomma.length()<=3)
			controlloSomma+="0";
		String euroPagaPrefix="PAGA €";
		String importoER = euroPagaPrefix.concat(controlloSomma.replace(".", ","));
		assertTrue("Pulsante Paga AR: "+importoAR + " ER: "+ importoER ,importoAR.contains(importoER));
	}
}
