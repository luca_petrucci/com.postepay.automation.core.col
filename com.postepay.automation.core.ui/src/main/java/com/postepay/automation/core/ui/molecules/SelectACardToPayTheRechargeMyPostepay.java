package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class SelectACardToPayTheRechargeMyPostepay extends Molecule {
	public static final String NAME="M062";

	public static final String TARGETNUMBERCARDSENDERRECHARGETOPAY="targetNumberCardSenderRechargeToPay";
	public static final String SUBTITLEHOWTOPAY="subTitleHowToPay";
	public static final String OTHERCARDFORRECHARGEBUTTON="otherCardForRechargeButton";
	public static final String LINKAPPBANCOPOSTASELECTACARDTOPAY="linkAppBancoPostaSelectACardToPay";
	public static final String ROWCONTAINERM062 = "rowContainerM062";
	public static final String CARDNUMBERGENERICM062 = "cardNumberGenericM062";
	public static final String PREFERREDM062 = "preferredM062";

	public SelectACardToPayTheRechargeMyPostepay(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {

	}

	@Override
	protected void onInit() {

	}

}

