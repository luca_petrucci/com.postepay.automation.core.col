package com.postepay.automation.core.ui.pages.mapspaywithqrcode;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.QrPayMolecola;
import com.postepay.automation.core.ui.molecules.QrPaySummaryMolecola;

import io.appium.java_client.AppiumDriver;

import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;


public class MapsPayWithQRCode extends Page {
	public static final String NAME="T132";
	

	public MapsPayWithQRCode(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(QrPayMolecola.NAME, UiObjectRepo.get().get(QrPayMolecola.NAME), false);
		this.addToTemplate(QrPaySummaryMolecola.NAME, UiObjectRepo.get().get(QrPaySummaryMolecola.NAME), false);
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), false);
	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new MapsPayWithQRCodeManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyLayout() {
		((MapsPayWithQRCodeManager)this.manager).verifyLayout();
	}

	public void sendAmount(String amount) {
		((MapsPayWithQRCodeManager)this.manager).sendAmount(amount);
	}

	public void clickOnContinua() {
		((MapsPayWithQRCodeManager)this.manager).clickOnContinua();
	}
	
	public void clickOnX() {
		((MapsPayWithQRCodeManager)this.manager).clickOnX();
	}
	
	public void verifyLayoutSummary() {
		((MapsPayWithQRCodeManager)this.manager).verifyLayoutSummary();
	}
	
	public void verifySummaryData(String usedCard, String merchantAddress, String amount) {
		((MapsPayWithQRCodeManager)this.manager).verifySummaryData(usedCard, merchantAddress, amount);
	}
	
	public void clickOnPaga() {
		((MapsPayWithQRCodeManager)this.manager).clickOnPaga();
	}

	public void clickOnBackArrow() {
		((MapsPayWithQRCodeManager)this.manager).clickOnBackArrow();
	}
	
	public void clickOnAnnulla() {
		((MapsPayWithQRCodeManager)this.manager).clickOnAnnulla();
	}
	
	public void verifyChangeCard(String userCardNumber) {
		((MapsPayWithQRCodeManager)this.manager).verifyChangeCard(userCardNumber);
	}
}

