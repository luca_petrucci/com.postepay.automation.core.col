package com.postepay.automation.core.ui.pages.pagabollettinobiancot135;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.DatiPaganteM257;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class PagaBollettinoBiancoT135ManagerIOS extends PagaBollettinoBiancoT135Manager {

	public PagaBollettinoBiancoT135ManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void clickOnContinua() {

		Particle btn = (Particle) UiObjectRepo.get().get(DatiPaganteM257.M257BTNCONFERMA);
		WebElement el=btn.getElement();
		Rectangle rect=el.getRect();
		Point p= el.getLocation();
		int x= (int) (p.getX() + rect.getWidth()*0.5);
		int y= (int) (p.getY() + rect.getHeight()*0.5);
		TouchAction act = new TouchAction((PerformsTouchActions) page.getDriver());
		act.tap(PointOption.point(x, y)).perform();
		WaitManager.get().waitMediumTime();
	}
}
