package com.postepay.automation.core.ui.pages.productsconnectcarddetailspage;

import static org.junit.Assert.assertTrue;

import ui.core.support.page.Page;
import utility.ObjectFinderLight;

public class ProductsConnectCardDetailsPageManagerIOS extends ProductsConnectCardDetailsPageManager {

	public ProductsConnectCardDetailsPageManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyLayoutPage() {
//		assertTrue(card.getElement()!=null);
//		assertTrue(amountContabile.getElement()!=null);
//		assertTrue(amountDisponibile.getElement()!=null);
//		assertTrue(recurrentRechege.getElement()!=null);
		String pageSource= page.getDriver().getPageSource();
		assertTrue(ObjectFinderLight.elementExists(pageSource,card.getLocator()));
		assertTrue(ObjectFinderLight.elementExists(pageSource,amountContabile.getLocator()));
		assertTrue(ObjectFinderLight.elementExists(pageSource,amountDisponibile.getLocator()));
		assertTrue(ObjectFinderLight.elementExists(pageSource,recurrentRechege.getLocator()));
	}

}
