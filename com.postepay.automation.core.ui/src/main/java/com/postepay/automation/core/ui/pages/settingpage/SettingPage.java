package com.postepay.automation.core.ui.pages.settingpage;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;

import io.appium.java_client.android.AndroidDriver;

import com.postepay.automation.core.ui.molecules.BodySettingGeneral;


public class SettingPage extends Page {
	public static final String NAME="T065";
	

	public SettingPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(BodySettingGeneral.NAME, UiObjectRepo.get().get(BodySettingGeneral.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SettingPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}

	public void verifyHeaderPage(WebDriver driver, String inputTitle) {
		((SettingPageManager)this.manager).verifyHeaderPage(driver, inputTitle);
	}
	
	public void clickOnAccessAndAuthorization() {
		((SettingPageManager)this.manager).clickOnAccessAndAuthorization();
	}
	
	public void clickOnManagePosteId() {
		((SettingPageManager)this.manager).clickOnManagePosteId();
	}
	
	public void clickOnNotification() {
		((SettingPageManager)this.manager).clickOnNotification();
	}
}

