package com.postepay.automation.core.ui.pages.selezionafiltribachecapage;

import static org.junit.Assert.assertTrue;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.AbilitaInAppMolecola;
import com.postepay.automation.core.ui.molecules.BachecaPageMolecola;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.SelezionaFiltriMolecola;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.SoftAssertion;

public class SelezionaFiltriBachecaPageManager extends PageManager {

	LayoutTools toolLayout = new LayoutTools();
	String fromDataCorrente;
	String toDataCorrente;
	
	public SelezionaFiltriBachecaPageManager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}
	
	public void verifyLayout() {
		Particle btnSalva=(Particle) UiObjectRepo.get().get(HeaderGenericAllPage.SALVABUTTONGENERICICON);
		Particle btnAnnulla=(Particle) UiObjectRepo.get().get(HeaderGenericAllPage.ANNULLABUTTONGENERICICON);
		Particle titlePeriodo=(Particle) UiObjectRepo.get().get(SelezionaFiltriMolecola.TITLEPERIODO);
		Particle labelFrom=(Particle) UiObjectRepo.get().get(SelezionaFiltriMolecola.LABELFROMDATE);
		Particle labelTo=(Particle) UiObjectRepo.get().get(SelezionaFiltriMolecola.LABELTODATE);
		Particle titleService=(Particle) UiObjectRepo.get().get(SelezionaFiltriMolecola.TITLESERVICE);
		Particle titleCategoria=(Particle) UiObjectRepo.get().get(SelezionaFiltriMolecola.TITLECATEGORY);
		Particle labelCateg=(Particle) UiObjectRepo.get().get(SelezionaFiltriMolecola.LABELCATEGORIA);
		
		assertTrue(btnSalva.getElement().isDisplayed());
		assertTrue(btnAnnulla.getElement().isDisplayed());
		SoftAssertion.get().getAssertions().assertThat(titlePeriodo.getElement().isDisplayed()).withFailMessage("Title Periodo non visibile").isEqualTo(true);
		assertTrue(labelFrom.getElement().isDisplayed());
		assertTrue(labelTo.getElement().isDisplayed());
		SoftAssertion.get().getAssertions().assertThat(titleService.getElement().isDisplayed()).withFailMessage("Title Service non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(titleCategoria.getElement().isDisplayed()).withFailMessage("Title Categoria non visibile").isEqualTo(true);
		SoftAssertion.get().getAssertions().assertThat(labelCateg.getElement().isDisplayed()).withFailMessage("Label Categoria non visibile").isEqualTo(true);
	}
	
	public void inserisciData()
	{
		Particle labelFrom=(Particle) UiObjectRepo.get().get(SelezionaFiltriMolecola.LABELFROMDATE);
		labelFrom.getElement().click();
		WaitManager.get().waitShortTime();
		Particle giornoCorrente=(Particle) UiObjectRepo.get().get(SelezionaFiltriMolecola.GIORNOCORRENTE);
		SimpleDateFormat f = new SimpleDateFormat("dd MMMM yyyy", Locale.ITALIAN);
		fromDataCorrente=giornoCorrente.getElement().getAttribute("content-desc");
		try {
			Date date=f.parse(fromDataCorrente);
			 f = new SimpleDateFormat("dd MMM yyyy", Locale.ITALIAN);
			 fromDataCorrente=f.format(date);
			System.out.println(fromDataCorrente); 
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Particle btnOk=(Particle) UiObjectRepo.get().get(SelezionaFiltriMolecola.OKCALENDARFILTRO);
		btnOk.getElement().click();
		WaitManager.get().waitShortTime();
		
		Particle labelTo=(Particle) UiObjectRepo.get().get(SelezionaFiltriMolecola.LABELTODATE);
		labelTo.getElement().click();
		WaitManager.get().waitShortTime();
		toDataCorrente=giornoCorrente.getElement().getAttribute("content-desc");
		 f = new SimpleDateFormat("dd MMMM yyyy", Locale.ITALIAN);
		try {
			Date date=f.parse(toDataCorrente);
			 f = new SimpleDateFormat("dd MMM yyyy", Locale.ITALIAN);
			 toDataCorrente=f.format(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		btnOk.getElement().click();
		WaitManager.get().waitShortTime();
	}
	
	public void controlloFiltro()
	{
		Particle labelFrom=(Particle) UiObjectRepo.get().get(SelezionaFiltriMolecola.LABELFROMDATE);
		String ARFrom=labelFrom.getElement().getText();
		
		WaitManager.get().waitShortTime();
		
		Particle labelTo=(Particle) UiObjectRepo.get().get(SelezionaFiltriMolecola.LABELTODATE);
		String ARTo=labelTo.getElement().getText();
		System.out.println(fromDataCorrente);
		assertTrue("Controllo fromDate "+ARFrom , ARFrom.toLowerCase().equals(fromDataCorrente));
		assertTrue("Controllo toDate "+ARTo, ARTo.toLowerCase().equals(toDataCorrente));
	}
	
	public void clickOnSalva()
	{
		Particle btnSalva=(Particle) UiObjectRepo.get().get(HeaderGenericAllPage.SALVABUTTONGENERICICON);
		btnSalva.getElement().click();
	}
	
	public String getDataSelezionata()
	{
		return fromDataCorrente;
	}
	
	public void resettaFiltro()
	{
		Particle btnResetta=(Particle) UiObjectRepo.get().get(SelezionaFiltriMolecola.BTNRESETTA);
		btnResetta.getElement().click();
	}
	
	public void inserisciDateDifferenti()
	{
		Particle giornoCorrente=(Particle) UiObjectRepo.get().get(SelezionaFiltriMolecola.GIORNOCORRENTE);
		Particle labelFrom=(Particle) UiObjectRepo.get().get(SelezionaFiltriMolecola.LABELFROMDATE);
		labelFrom.getElement().click();
		WaitManager.get().waitShortTime();
		toDataCorrente=giornoCorrente.getElement().getAttribute("content-desc");
		SimpleDateFormat f = new SimpleDateFormat("dd MMMM yyyy");
		try {
			Date date=f.parse(toDataCorrente);
			 f = new SimpleDateFormat("dd MMM yyyy");
			 toDataCorrente=f.format(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WaitManager.get().waitShortTime();
		Particle btnOk=(Particle) UiObjectRepo.get().get(SelezionaFiltriMolecola.OKCALENDARFILTRO);
		btnOk.getElement().click();
		WaitManager.get().waitShortTime();
		
		Particle labelTo=(Particle) UiObjectRepo.get().get(SelezionaFiltriMolecola.LABELTODATE);
		labelTo.getElement().click();
		WaitManager.get().waitShortTime();
		if(!giornoCorrente.getElement().getText().equals("1"))
		{
			int giorno=Integer.parseInt(giornoCorrente.getElement().getText());
			Particle giornoPrecedente=(Particle) UiObjectRepo.get().get(SelezionaFiltriMolecola.GIORNOPRECEDENTE);
			String locatorPrecedente=Utility.replacePlaceHolders(giornoPrecedente.getLocator(), new Entry("title", ""+(giorno-1)));
			WebElement elm = page.getDriver().findElement(By.xpath(locatorPrecedente));
			fromDataCorrente=elm.getAttribute("content-desc");
			 f = new SimpleDateFormat("dd MMMM yyyy");
				try {
					Date date=f.parse(toDataCorrente);
					 f = new SimpleDateFormat("dd MMM yyyy");
					 toDataCorrente=f.format(date);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			elm.click();
			WaitManager.get().waitShortTime();
		}
		
		btnOk.getElement().click();
		WaitManager.get().waitShortTime();
		
		
	}
	
}