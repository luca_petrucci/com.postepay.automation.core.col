package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class BodyServicePaymentSection extends Molecule {
	public static final String NAME="M045";
	
	public static final String PURCHASEURBANTICKET="purchaseUrbanTicket";
public static final String PURCHASEEXTRAURBANTICKET="purchaseExtraUrbanTicket";
public static final String PURCHASEFUEL="purchaseFuel";
public static final String PURCHASEPARKING="purchaseParking";


	public BodyServicePaymentSection(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

