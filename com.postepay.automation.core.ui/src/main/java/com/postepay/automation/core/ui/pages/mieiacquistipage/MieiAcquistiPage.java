package com.postepay.automation.core.ui.pages.mieiacquistipage;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.MieiAcquistiPageMolecole;

import io.appium.java_client.android.AndroidDriver;


public class MieiAcquistiPage extends Page {
	public static final String NAME="T109";
	

	public MieiAcquistiPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(MieiAcquistiPageMolecole.NAME, UiObjectRepo.get().get(MieiAcquistiPageMolecole.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new MieiAcquistiPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeaderPage(String inputTitle) {
		((MieiAcquistiPageManager)this.manager).verifyHeaderPage(inputTitle);
	}

	public void verifyLayoutPage(WebDriver driver) {
		((MieiAcquistiPageManager)this.manager).verifyLayoutPage(driver);
	}
	
	public void clickToElementInToolbar(WebDriver driver, String toElement) {
		((MieiAcquistiPageManager)this.manager).clickToElementInToolbar(driver, toElement);
	}

	public void clickOnSelectedToolbar(WebDriver driver, String toElement) {
		((MieiAcquistiPageManager)this.manager).clickOnSelectedToolbar(driver, toElement);
	}
	
	public void clickOnCardSpecific(WebDriver driver, String titleImput, String dataImput, String importoImput) {
		((MieiAcquistiPageManager)this.manager).clickOnCardSpecific(driver, titleImput, dataImput, importoImput);
	}
}

