package com.postepay.automation.core.ui.pages.communitycardonepage;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import ui.core.support.page.Page;
import ui.core.support.waitutil.WaitManager;

public class CommunityCardOnePageManagerIOS extends CommunityCardOnePageManager {

	public CommunityCardOnePageManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	public WebElement lastRootTab(WebDriver driver) {
		List<WebElement> rootTab = (List<WebElement>) driver.findElements(By.xpath("//*[@*='XCUIElementTypeCollectionView' and ./*[./*[./*[@*='XCUIElementTypeOther']]]]"));
		int last = rootTab.size() - 1;
		WebElement lastRoot = rootTab.get(last);
		return lastRoot;
	}

	public void swipOnG2g(WebDriver driver) {
		WebElement lastRoot = lastRootTab(driver);
		toolLayout.scrollWithCoordinate(driver,
				lastRoot.getLocation().getX(), lastRoot.getLocation().getX()-500,
				lastRoot.getLocation().getY(), lastRoot.getLocation().getY());
		WaitManager.get().waitHighTime();
	}
	
}
