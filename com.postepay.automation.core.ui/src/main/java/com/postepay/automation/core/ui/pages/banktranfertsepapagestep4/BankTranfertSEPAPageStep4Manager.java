package com.postepay.automation.core.ui.pages.banktranfertsepapagestep4;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.BodyOfNewPosteID;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.NotificheAutorizzativeM265;
import com.postepay.automation.core.ui.verifytools.LayoutTools;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.HidesKeyboard;
import ui.core.support.Entry;
import ui.core.support.Utility;
import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class BankTranfertSEPAPageStep4Manager extends PageManager {

	public BankTranfertSEPAPageStep4Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	LayoutTools toolLayout = new LayoutTools();
	public void verifyHeaderpage(String titlePageInput) {
		toolLayout.verifyPresenceHeaderPage(titlePageInput);
	}

	public void insertPosteId(String posteID) {

//		((AppiumDriver<?>)page.getDriver()).hideKeyboard();

		WaitManager.get().waitShortTime();

		Particle posteIdEditText=(Particle) UiObjectRepo.get().get(BodyOfNewPosteID.ENTERPOSTID);

		posteIdEditText.getElement().sendKeys(posteID);

		WaitManager.get().waitShortTime();

//		((AppiumDriver<?>)page.getDriver()).hideKeyboard();
		

		WaitManager.get().waitShortTime();
	}

	public void clickOnConfirmButton() {

		try {
			((HidesKeyboard) page.getDriver()).hideKeyboard();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		Particle btn=(Particle) UiObjectRepo.get().get(BodyOfNewPosteID.BUTTONCONFIRM);

		btn.getElement().click();

		WaitManager.get().waitShortTime();
	}

	public void verifyHeaderAutorizzaWEB(String inputTitle) {
		System.out.println(page.getDriver().getClass());
		Particle title = (Particle) UiObjectRepo.get().get(HeaderGenericAllPage.TITLEGENERICWITHTITLE);
		title.setDriver(page.getDriver());
		
		System.out.println(title);
		String t = title.getLocator();
		String titleS = Utility.replacePlaceHolders(title.getLocator(), new Entry("title", inputTitle));
		System.out.println(titleS);
		WebElement titleEl = page.getDriver().findElement(By.xpath(titleS));
		System.out.println(titleEl);
		assertTrue("Error titolo non presente. ",titleEl.isDisplayed());
	}

	public void insertPosteIdAutorizzaWEB(String posteID) {

//		((AppiumDriver<?>)page.getDriver()).hideKeyboard();
		WaitManager.get().waitShortTime();

		Particle posteIdEditText=(Particle) UiObjectRepo.get().get(BodyOfNewPosteID.ENTERPOSTID);
		posteIdEditText.setDriver(page.getDriver());
		posteIdEditText.getElement().sendKeys(posteID);

		WaitManager.get().waitShortTime();
//		((AppiumDriver<?>)page.getDriver()).hideKeyboard();
		WaitManager.get().waitShortTime();
	}

	public void clickOnConfirmButtonAutorizzaWEB() {
		Particle btn=(Particle) UiObjectRepo.get().get(BodyOfNewPosteID.BTNAUTORIZZAM034);
		btn.setDriver(page.getDriver());
		btn.getElement().click();
		WaitManager.get().waitShortTime();
	}

}

