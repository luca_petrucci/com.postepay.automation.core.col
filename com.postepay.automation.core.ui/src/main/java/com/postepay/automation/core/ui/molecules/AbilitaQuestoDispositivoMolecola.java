package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class AbilitaQuestoDispositivoMolecola extends Molecule {
	public static final String NAME="M241";
	
	public static final String COPYABILITADISPOSITIVO="copyAbilitaDispositivo";
public static final String TITLEABILITADISPOSITIVO="titleAbilitaDispositivo";
public static final String SCOPRIABILITADISPOSITIVO="scopriAbilitaDispositivo";
public static final String INIZIAABILITADISPOSITIVO="iniziaAbilitaDispositivo";
public static final String IMAGEABILITADISPOSITIVO="imageAbilitaDispositivo";


	public AbilitaQuestoDispositivoMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

