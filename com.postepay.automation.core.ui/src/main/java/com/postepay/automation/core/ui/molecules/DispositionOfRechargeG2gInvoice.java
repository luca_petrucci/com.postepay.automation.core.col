package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class DispositionOfRechargeG2gInvoice extends Molecule {
	public static final String NAME="M017";
	
	public static final String RECHARGEPOSTEPAYICONG2GINVOICE="rechargePostePayIconG2gInvoice";
public static final String GTOGICON="gToGIcon";
public static final String PAYMENTBULLETINTEXTG2G="paymentBulletinTextG2g";
public static final String RECHARGEPOSTEPAYTEXTG2G="rechargePostePayTextG2g";
public static final String PTOPICONG2G="pToPIconG2g";
public static final String PAYMENTBULLETINICONG2G="paymentBulletinIconG2g";


	public DispositionOfRechargeG2gInvoice(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

