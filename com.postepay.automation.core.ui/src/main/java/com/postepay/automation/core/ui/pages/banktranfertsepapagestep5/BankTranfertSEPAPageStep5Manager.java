package com.postepay.automation.core.ui.pages.banktranfertsepapagestep5;

import static org.junit.Assert.assertTrue;

import org.jcodec.codecs.mjpeg.tools.AssertionException;

import com.postepay.automation.core.ui.molecules.ThankYouPageForTelephoneRecharge;
import com.postepay.automation.core.ui.molecules.ThankYouPageGeneric;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class BankTranfertSEPAPageStep5Manager extends PageManager {

	public BankTranfertSEPAPageStep5Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void clickOnCloseButton() {
		
		Particle btn=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.THANKYOUCLOSEBUTTONBANKTRANFERTSEPA);
		
		btn.getElement().click();
		
		WaitManager.get().waitMediumTime();
	}

	public void verifyLayout() {
		Particle img=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.THANKYOUIMAGEBANKTRANFERTSEPA);
		Particle title=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.THANKYOUTITLEBANKTRANFERTSEPA);
		Particle btn=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.THANKYOUCLOSEBUTTONBANKTRANFERTSEPA);
		Particle link=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.THANKYOULINKBANKTRANFERTSEPA);

		// Imagine
		assertTrue(img.getElement().isDisplayed());
		// Title
		assertTrue(title.getElement().isDisplayed());
		// Link
		try {
			assertTrue(link.getElement().isDisplayed());
			
		} catch (AssertionException e) {
			// TODO: handle exception
		}
	
		// Bottone
		assertTrue(btn.getElement().isDisplayed());
		
		WaitManager.get().waitShortTime();
	}
	
	public void verifyLayoutDinamic(String title, String description, String buttonTxt) {
		Particle eImg=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.TYPIMAGEM054);
		Particle eTitle=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.TYPTITLEM054);
		Particle eDescription=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.TYPDESCRIPTIONM054);
		Particle eBtnClose=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.TYPCLOSEBTNM054);

		// Imagine
		assertTrue(eImg.getElement().isDisplayed());
		// Title
		assertTrue(eTitle.getElement().isDisplayed());
		assertTrue(eTitle.getElement().getText().equals(title));
		// Descrizione
		assertTrue(eDescription.getElement().isDisplayed());
		assertTrue(eDescription.getElement().getText().equals(description));
		// Bottone
		assertTrue(eBtnClose.getElement().isDisplayed());
		assertTrue(eBtnClose.getElement().getText().equals(buttonTxt));
	}
	
	public void verifyLinkPresenza(String text) {
		Particle eLink=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.THANKYOULINKBANKTRANFERTSEPA);
		assertTrue(eLink.getElement().getText().toUpperCase().trim().equals(text.toUpperCase().trim()));
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnClose() {
		Particle eBtnClose=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.TYPCLOSEBTNM054);
		eBtnClose.getElement().click();
		WaitManager.get().waitMediumTime();
	}

	public void typPraticaMIT() {
		Particle img=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.TYPMITIMAGEM054);
		Particle title=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.TYPMITTITOLOM054);
		Particle desc=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.TYPMITDESCRIPTIONM054);
		Particle btnClose=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.TYPMITCLOSEM054);
		Particle btnRicevuta=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.TYPMITRICEVUTAM054);
		Particle link=(Particle) UiObjectRepo.get().get(ThankYouPageGeneric.TYPMITLINKM054);

		// Imagine
		assertTrue(img.getElement().isDisplayed());
		// Title
		assertTrue(title.getElement().isDisplayed());
		// Desc
		assertTrue(desc.getElement().isDisplayed());		
		// Link
		assertTrue(link.getElement().isDisplayed());
		// Bottone
		assertTrue(btnClose.getElement().isDisplayed());
		assertTrue(btnRicevuta.getElement().isDisplayed());
		
		WaitManager.get().waitShortTime();
		
	}
}

