package com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep2;

import static org.junit.Assert.assertTrue;

import ui.core.support.page.Page;

public class ExtraurbanPurchasePageStep2ManagerIOS extends ExtraurbanPurchasePageStep2Manager {

	public ExtraurbanPurchasePageStep2ManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	public void verifyLayoutPage() {
		assertTrue(startDate.getElement().isDisplayed());
		assertTrue(destination.getElement().isDisplayed());
		assertTrue(modifyDestination.getElement().isDisplayed());
		assertTrue(allFilter.getElement().isEnabled());
		assertTrue(principalFilter.getElement().isEnabled());
		assertTrue(trainFilter.getElement().isEnabled());
		assertTrue(busFilter.getElement().isEnabled());
//		assertTrue(trenitaliIcon.getElement().isEnabled());
//		assertTrue(travelFrom.getElement().isEnabled());
//		assertTrue(travelTo.getElement().isEnabled());
//		assertTrue(travelAmount.getElement().isEnabled());
	}
}
