package com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep3;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.molecules.BodyDetailsExtraurbanPurchase;

import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

public class ExtraurbanPurchasePageStep3ManagerIOS extends ExtraurbanPurchasePageStep3Manager {

	public ExtraurbanPurchasePageStep3ManagerIOS(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}
	
	public void clickOnOrdinaryTicket() {
		
		scrollToOrdinario();
		Particle biglietto = (Particle) UiObjectRepo.get().get(BodyDetailsExtraurbanPurchase.CHOOSERADIOBUTTONBASE);
		biglietto.getElement().click();
	}
}
