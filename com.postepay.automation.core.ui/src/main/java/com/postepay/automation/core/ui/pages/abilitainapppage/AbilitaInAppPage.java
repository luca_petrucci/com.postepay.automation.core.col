package com.postepay.automation.core.ui.pages.abilitainapppage;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;

import com.postepay.automation.core.ui.molecules.AbilitaInAppMolecola;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;


public class AbilitaInAppPage extends Page {
	public static final String NAME="T123";
	

	public AbilitaInAppPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(AbilitaInAppMolecola.NAME, UiObjectRepo.get().get(AbilitaInAppMolecola.NAME), false);
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(AbilitaInAppMolecola.NAME), false);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new AbilitaInAppPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return new AbilitaInAppPageManagerIOS(this);
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		((AbilitaInAppPageManager)this.manager).verifyHeaderpage(titlePageInput);
	}

	public void verifyLayout() {
		((AbilitaInAppPageManager)this.manager).verifyLayout(); 
	}
	
	public void clickOnAutorizzaConPreferito() {
		((AbilitaInAppPageManager)this.manager).clickOnAutorizzaConPreferito();
	}

	public void clickOnNonHoaccessoAlPreferito() {
		((AbilitaInAppPageManager)this.manager).clickOnNonHoaccessoAlPreferito();
	}

	public void verifyImage() {
		((AbilitaInAppPageManager)this.manager).verifyImage();
	}

	public void clickOnDeleteDevice() {
		((AbilitaInAppPageManager)this.manager).clickOnDeleteDevice();
		
	}
	
	public void clickOnOnbDevice(String cognome) {
		((AbilitaInAppPageManager)this.manager).clickOnOnbDevice(cognome);
	}
	
	public void inserisciMeseanno(String daticarta) {
		((AbilitaInAppPageManager)this.manager).inserisciMeseanno(daticarta);
	}
	
	public void inserisciCvv(String daticarta) {
		((AbilitaInAppPageManager)this.manager).inserisciCvv(daticarta);
	}
	
	public void createPosteID(String posteId) {
		((AbilitaInAppPageManager)this.manager).createPosteID(posteId);
	}
	
	public void clickOnOnbDeviceDurinOperations(String list) {
		((AbilitaInAppPageManager)this.manager).clickOnOnbDeviceDurinOperations(list);
	}

	public boolean isOnb() {
		return ((AbilitaInAppPageManager)this.manager).isOnb();
	}

	public boolean isCreaPosteIdVisible() {
		return ((AbilitaInAppPageManager)this.manager).isCreaPosteIdVisible();
	}

	public void clickOnProseguiButton() {
		((AbilitaInAppPageManager)this.manager).clickOnProseguiButton();
	}
	
}

