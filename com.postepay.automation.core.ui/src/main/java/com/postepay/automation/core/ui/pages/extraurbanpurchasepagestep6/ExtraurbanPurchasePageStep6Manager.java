package com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep6;

import static org.junit.Assert.assertTrue;

import com.postepay.automation.core.ui.molecules.ExtraurbaPurchaseTicketMolecola;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;
import ui.core.support.waitutil.WaitManager;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class ExtraurbanPurchasePageStep6Manager extends PageManager {

	Particle amount = (Particle) UiObjectRepo.get().get(ExtraurbaPurchaseTicketMolecola.AMOUNTSUMMARYEXTRAURBAN);
	Particle date = (Particle) UiObjectRepo.get().get(ExtraurbaPurchaseTicketMolecola.DATEOFPURCHASEEXTRAURBAN);
	Particle payWithCard = (Particle) UiObjectRepo.get().get(ExtraurbaPurchaseTicketMolecola.PAYWITHTHECARD);
	Particle btnAnnulla = (Particle) UiObjectRepo.get().get(ExtraurbaPurchaseTicketMolecola.ANNULLABTNEXTRAURBAN);
	Particle btnPaga = (Particle) UiObjectRepo.get().get(ExtraurbaPurchaseTicketMolecola.PAYBTNEXTRAURBAN);
	
	public ExtraurbanPurchasePageStep6Manager(Page page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public void verifyHeaderpage(String inputTitle) {
		LayoutTools tool = new LayoutTools();
		tool.verifyPresenceHeaderPage(inputTitle);
	}
	
	public void verifyLayoutPage() {
		Particle title = (Particle) UiObjectRepo.get().get(ExtraurbaPurchaseTicketMolecola.TITLETEXTSUMMARYEXTRAURBAN);
		Particle layoutPayWith = (Particle) UiObjectRepo.get().get(ExtraurbaPurchaseTicketMolecola.PAYWITHHEADEREXTRAURBAN);
		Particle cicero = (Particle) UiObjectRepo.get().get(ExtraurbaPurchaseTicketMolecola.PROVIDEROFSERVICEEXTRAURBAN);

		assertTrue(title.getElement().isDisplayed());
		assertTrue(date.getElement().isDisplayed());
		assertTrue(payWithCard.getElement().isDisplayed());
		assertTrue(btnAnnulla.getElement().isDisplayed());
		assertTrue(btnPaga.getElement().isDisplayed());
		assertTrue(layoutPayWith.getElement().isDisplayed());
		assertTrue(cicero.getElement().isDisplayed());
		assertTrue(amount.getElement().isDisplayed());
	}
	
	public void verifyDateOf() {
		StringAndNumberOperationTools tool = new StringAndNumberOperationTools();
		String toDay = tool.getDateExtraurban();
		
		assertTrue(date.getElement().getText().equals(toDay));
	}

	public void verifyPayWithCard(String cardInput) {
		String cardIn = ("Evolution  " + cardInput).trim();
		System.out.println(cardIn);
		System.out.println(payWithCard.getElement().getText());
		assertTrue(payWithCard.getElement().getText().trim().equals(cardIn));
	}
	
	public void clickOnPay() {
		btnPaga.getElement().click();
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnAnnulla() {
		Particle btn = (Particle) UiObjectRepo.get().get(HeaderGenericAllPage.RIGHTBUTTONGENERICICON);
		WaitManager.get().waitMediumTime();
		btn.getElement().click();
	}
}

