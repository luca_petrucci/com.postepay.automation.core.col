package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class ThankYouPageGeneric extends Molecule {
	public static final String NAME="M054";
	
	public static final String THANKYOUTITLEBANKTRANFERTSEPA="thankYouTitleBankTranfertSEPA";
	public static final String THANKYOUCLOSEBUTTONBANKTRANFERTSEPA="thankYouCloseButtonBankTranfertSEPA";
	public static final String THANKYOUIMAGEBANKTRANFERTSEPA="thankYouImageBankTranfertSEPA";
	public static final String THANKYOULINKBANKTRANFERTSEPA="thankYouLinkBankTranfertSEPA";
	public static final String TYPIMAGEM054="typImageM054";
	public static final String TYPTITLEM054="typTitleM054";
	public static final String TYPDESCRIPTIONM054="typDescriptionM054";
	public static final String TYPCLOSEBTNM054="typCloseBtnM054";
	public static final String NOG2GIMG = "typImageNoG2gM054";
	public static final String NOG2GTITLE = "typTitleNoG2gM054";
	public static final String NOG2GSEND = "typInvitaAmiciNoG2gM054";
	public static final String TYPMITIMAGEM054="typMITimageM054";
	public static final String TYPMITTITOLOM054="typMITtitoloM054";
	public static final String TYPMITDESCRIPTIONM054="typMITdescriptionM054";
	public static final String TYPMITLINKM054="typMITlinkM054";
	public static final String TYPMITCLOSEM054="typMITcloseM054";
	public static final String TYPMITRICEVUTAM054="typMITricevutaM054";

	public ThankYouPageGeneric(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

