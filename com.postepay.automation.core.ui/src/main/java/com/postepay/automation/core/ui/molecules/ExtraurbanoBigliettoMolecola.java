package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class ExtraurbanoBigliettoMolecola extends Molecule {
	public static final String NAME="M226";
	
	public static final String EXTRAURBANOBIGLIETTOTITOLO="extraurbanoBigliettoTitolo";
public static final String EXTRAURBANOBIGLIETTOSEARCHICON="extraurbanoBigliettoSearchIcon";
public static final String EXTRAURBANOBIGLIETTOBUTTONX="extraurbanoBigliettoButtonX";
public static final String EXTRAURBANOBIGLIETTOQRCODE="extraurbanoBigliettoQrCode";
public static final String EXTRAURBANOBIGLIETTOARRIVO="extraurbanoBigliettoArrivo";
public static final String EXTRAURBANOBIGLIETTOPNR="extraurbanoBigliettoPNR";
public static final String EXTRAURBANOBIGLIETTOREGIONE="extraurbanoBigliettoRegione";
public static final String EXTRAURBANOBIGLIETTOICONAPASSEGGERO="extraurbanoBigliettoIconaPasseggero";
public static final String EXTRAURBANOBIGLIETTOVALIDOFINOALABEL="extraurbanoBigliettoValidoFinoALabel";
public static final String EXTRAURBANOBIGLIETTOPASSEGGERI="extraurbanoBigliettoPasseggeri";
public static final String EXTRAURBANOBIGLIETTOARRIVOORA="extraurbanoBigliettoArrivoOra";
public static final String EXTRAURBANOBIGLIETTOPARTENZOORA="extraurbanoBigliettoPartenzoOra";
public static final String EXTRAURBANOBIGLIETTOVALIDOFINOA="extraurbanoBigliettoValidoFinoA";
public static final String EXTRAURBANOBIGLIETTOHAIBISOGNODIAIUTO="extraurbanoBigliettoHaiBisognoDiAiuto";
public static final String EXTRAURBANOBIGLIETTOUNODIUNO="extraurbanoBigliettoUnoDiUno";
public static final String EXTRAURBANOBIGLIETTOPARTENZA="extraurbanoBigliettoPartenza";
public static final String EXTRAURBANOBIGLIETTOREGIONALENUMERO="extraurbanoBigliettoRegionaleNumero";
public static final String EXTRAURBANOBIGLIETTOPNRLABEL="extraurbanoBigliettoPNRLabel";


	public ExtraurbanoBigliettoMolecola(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

