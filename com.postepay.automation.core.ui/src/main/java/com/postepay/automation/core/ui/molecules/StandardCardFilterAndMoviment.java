package com.postepay.automation.core.ui.molecules;

import java.util.Properties;
import java.util.function.BiFunction;

import org.openqa.selenium.WebDriver;

import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.UiObjectRepo;
public class StandardCardFilterAndMoviment extends Molecule {
	public static final String NAME="M048";
	
	public static final String DISPOSITIONOPERATIONCONTAINERSTANDARD="dispositionOperationContainerStandard";
	public static final String SPENDINGOUTRECORDSSTANDARD="spendingOutRecordsStandard";
	public static final String SPENDINGSERCHFIELDSTANDARD="spendingSerchFieldStandard";
	public static final String MANAGEG2G="manageG2G";
	public static final String OPERATIONTITLE="operationTitle";
	public static final String OPERATIONDESCRIPTION="operationDescription";
	public static final String OPERATIONDATE="operationDate";
	public static final String OPERATIONAMOUNT="operationAmount";



	public StandardCardFilterAndMoviment(WebDriver driver, Properties language) {
		super(driver, language);
		UiObjectRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	public void checkStyle(String styleRule, String styleValue, BiFunction<String, String, Boolean> check)
			throws Error {
		
	}

	@Override
	protected void onInit() {
		
	}

}

