package com.postepay.automation.core.ui.pages.servicepaymentpage;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.HeaderGenericAllPage;
import com.postepay.automation.core.ui.molecules.TabsPaymentSection;

import io.appium.java_client.android.AndroidDriver;

import com.postepay.automation.core.ui.molecules.FooterHomePage;
import com.postepay.automation.core.ui.molecules.BodyServicePaymentSection;


public class ServicePaymentPage extends Page {
	public static final String NAME="T028";


	public ServicePaymentPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {

	}

	@Override
	protected void onInit() {
		this.addToTemplate(HeaderGenericAllPage.NAME, UiObjectRepo.get().get(HeaderGenericAllPage.NAME), true);
		this.addToTemplate(TabsPaymentSection.NAME, UiObjectRepo.get().get(TabsPaymentSection.NAME), true);
		this.addToTemplate(FooterHomePage.NAME, UiObjectRepo.get().get(FooterHomePage.NAME), true);
		this.addToTemplate(BodyServicePaymentSection.NAME, UiObjectRepo.get().get(BodyServicePaymentSection.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {

		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {

		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {

		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new ServicePaymentPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {

		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {

		return null;
	}
	
	public void verifyHeaderpage() {
		((ServicePaymentPageManager)this.manager).verifyHeaderpage();
	}
	
	public void clickOnTabRecharge() {
		((ServicePaymentPageManager)this.manager).clickOnTabRecharge();
	}

	public void clickOnTabPayment() {
		((ServicePaymentPageManager)this.manager).clickOnTabPayment();
	}

	public void clickOnTabService() {
		((ServicePaymentPageManager)this.manager).clickOnTabService();
	}

	public void clickOnCardCarburante(WebDriver driver) {
		((ServicePaymentPageManager)this.manager).clickOnCardCarburante(driver);
	}

	public void clickOnCardParcheggio() {
		((ServicePaymentPageManager)this.manager).clickOnCardParcheggio();
	}

	public void clickOnCardExtraurbano(WebDriver driver) {
		((ServicePaymentPageManager)this.manager).clickOnCardExtraurbano(driver);
	}

	public void clickOnCardUrbano() {
		((ServicePaymentPageManager)this.manager).clickOnCardUrbano();
	}
}

