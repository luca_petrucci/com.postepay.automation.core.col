package com.postepay.automation.core.ui.pages.salvadanaioselezionatipologiapage;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import ui.core.support.page.Page;
import ui.core.support.page.PageManager;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import com.postepay.automation.core.ui.molecules.SalvadanaioSelezionaTipologiaMolecola;

import io.appium.java_client.android.AndroidDriver;


public class SalvadanaioSelezionaTipologiaPage extends Page {
	public static final String NAME="T144";
	

	public SalvadanaioSelezionaTipologiaPage(WebDriver driver, Properties language) {
		super(driver, language);
		PageRepo.get().set(NAME, this);
	}

	@Override
	public void checkLayout() throws Error {
		
	}

	@Override
	protected void onInit() {
		this.addToTemplate(SalvadanaioSelezionaTipologiaMolecola.NAME, UiObjectRepo.get().get(SalvadanaioSelezionaTipologiaMolecola.NAME), true);

	}

	@Override
	protected PageManager loadSafariPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadIExplorerPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadFirefoxPageManager() {
		
		return null;
	}

	@Override
	protected void loadDefaultPageManager() {
		this.manager = new SalvadanaioSelezionaTipologiaPageManager(this);
	}

	@Override
	protected PageManager loadIOSPageManager() {
		
		return null;
	}

	@Override
	protected PageManager loadAndroidPageManager() {
		
		return null;
	}
	
	public void verifyHeaderpage(String titlePageInput) {
		((SalvadanaioSelezionaTipologiaPageManager)this.manager).verifyHeaderpage(titlePageInput);
	}
	
	public void verifyLayoutPage() {
		((SalvadanaioSelezionaTipologiaPageManager)this.manager).verifyLayoutPage();
	}
	public void clickOnRicorrente() {
		((SalvadanaioSelezionaTipologiaPageManager)this.manager).clickOnRicorrente();
	}
}

