package utility;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ui.core.support.uiobject.Particle;

public class CoreUtility {

	public static WebElement visibilityOfElement(WebDriver driver,Particle p, int seconds)
	{
		WebDriverWait w = new WebDriverWait(driver,seconds);
		return w.until(ExpectedConditions.presenceOfElementLocated(p.getXPath()));
	}

	public static WebElement visibilityOfElement(WebDriver driver, By xpath, int seconds) 
	{
		WebDriverWait w = new WebDriverWait(driver,seconds);
		return w.until(ExpectedConditions.presenceOfElementLocated(xpath));
	}
}
